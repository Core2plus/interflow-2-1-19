<header id="topnav">
            <div class="topbar-main">
                <div class="container-fluid">

                    <!-- Logo container-->
                    <div class="logo">
                        <!-- Text Logo -->
                        <!--<a href="index.html" class="logo">-->
                        <!--Upcube-->
                        <!--</a>-->
                        <!-- Image Logo -->
                        <a href="<?php echo base_url(); ?>" class="logo">
                            <img src="<?php echo base_url(); ?>assets/images/logo-sm.png" alt="" height="22" class="logo-small">
                            <img src="<?php echo base_url(); ?>assets/images/ARTT.png" alt="" height="50" class="logo-large">
                            <small>
                            <?php
                            echo $this->session->userdata('signup_email');
                            ?>
                        </small>
                        </a>

                    </div>
                    <!-- End Logo container-->


                    <div class="menu-extras topbar-custom">

                        <!-- Search input -->
                        

                        <ul class="list-inline float-right mb-0">
                            <!-- Search -->
                            
                            <!-- Messages-->
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <i class="mdi mdi-email-outline noti-icon"></i>
                                    <span class="badge badge-danger noti-icon-badge">3</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h5><span class="badge badge-danger float-right">745</span>Messages</h5>
                                    </div>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon"><img src="<?php echo base_url(); ?>assets/images/users/avatar-2.jpg" alt="user-img" class="img-fluid rounded-circle" /> </div>
                                        <p class="notify-details"><b>Charles M. Jones</b><small class="text-muted">Dummy text of the printing and typesetting industry.</small></p>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon"><img src="<?php echo base_url(); ?>assets/images/users/avatar-3.jpg" alt="user-img" class="img-fluid rounded-circle" /> </div>
                                        <p class="notify-details"><b>Thomas J. Mimms</b><small class="text-muted">You have 87 unread messages</small></p>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon"><img src="<?php echo base_url(); ?>assets/images/users/avatar-4.jpg" alt="user-img" class="img-fluid rounded-circle" /> </div>
                                        <p class="notify-details"><b>Luis M. Konrad</b><small class="text-muted">It is a long established fact that a reader will</small></p>
                                    </a>

                                    <!-- All-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        View All
                                    </a>

                                </div>
                            </li>
                            <!-- notification-->
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <i class="mdi mdi-bell-outline noti-icon"></i>
                                    <span class="badge badge-danger noti-icon-badge">3</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h5>Notification (3)</h5>
                                    </div>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                        <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                                        <p class="notify-details"><b>Your order is placed</b><small class="text-muted">Dummy text of the printing and typesetting industry.</small></p>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-warning"><i class="mdi mdi-message"></i></div>
                                        <p class="notify-details"><b>New Message received</b><small class="text-muted">You have 87 unread messages</small></p>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-info"><i class="mdi mdi-martini"></i></div>
                                        <p class="notify-details"><b>Your item is shipped</b><small class="text-muted">It is a long established fact that a reader will</small></p>
                                    </a>

                                    <!-- All-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        View All
                                    </a>

                                </div>
                            </li>
                            <!-- User-->
                           
                            <li class="menu-item list-inline-item">
                                <!-- Mobile menu toggle-->
                                <a class="navbar-toggle nav-link">
                                    <div class="lines">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </a>
                                <!-- End mobile menu toggle-->
                            </li>
                        </ul>
                    </div>
                    <!-- end menu-extras -->

                    <div class="clearfix"></div>

                </div> <!-- end container -->
            </div>
            <!-- end topbar-main -->

            <!-- MENU Start -->
            <div class="navbar-custom">
                <div class="container-fluid">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">
                            <li class="has-submenu">
                                <a href="<?php echo base_url(); ?>"><i class="ti-home"></i>Dashboard</a>
                            </li>

                               <li class="has-submenu">
                                <a href="#"><i class="ti-crown"></i>Main</a>
                                <ul class="submenu">
                                    <li><a href="<?php echo base_url('Main/modules'); ?>">Modules</a></li>
                                    <li><a href="<?php echo base_url('Main/courses'); ?>">Courses</a></li>
                                    <li><a href="<?php echo base_url('Main/batchs'); ?>">Batchs</a></li>
                                    
                                </ul>
                            </li>

                            <li class="has-submenu">
                                <a href="#"><i class="ti-light-bulb"></i>Student </a>
                                <ul class="submenu megamenu">
                                    <li>
                                        <ul>
                                            <li><a href="<?php echo base_url('Main/show_stds');?>">Students</a></li>
                                            <li><a href="<?php echo base_url('Main/show_all_enrolled_students');?>">Enroll Student</a></li>
                                            
                                            <li><a href="<?php echo base_url('Main/discount_std');?>">Discount Student</a></li>
                                            <li><a href="ui-tabs-accordions.html">Report</a></li>
                                        </ul>
                                    </li>
                                   
 
                                </ul>
                            </li>

                            <li class="has-submenu">
                                <a href="#"><i class="ti-bookmark-alt"></i>Teacher</a>
                                <ul class="submenu">
                                     <li>
                                        <ul>
                                            <li><a href="<?php echo base_url('Main/show_teachers');?>">Teachers</a></li>
                                            <li><a href="<?php echo base_url('Main/enroll_teacher');?>">Enroll Teacher</a></li>
                                            
                                            <li><a href="ui-navs.html">Reports</a></li>
                                        </ul>
                                    </li>
                                    
                                
                                </ul>
                            </li>
                             <li class="has-submenu">
                                <a href="#"><i class="ti-layout-media-overlay"></i>Reports</a>
                                <ul class="submenu">
                                    <li><a href="<?php echo base_url('Main/show_paid_reports_bko'); ?>">Paid</a></li>
                                    <li><a href="<?php echo base_url('Main/show_unpaid_reports_bko'); ?>">UnPaid</a></li>
                                    <li><a href="<?php echo base_url('Main/show_freeze_reports_bko'); ?>">Freeze</a></li>
                                     <li><a href="<?php echo base_url('Main/show_discount_reports_bko'); ?>">Discount</a></li>
                                     <li><a href="<?php echo base_url('Main/show_expense_reports_bko'); ?>">Expence</a></li>
                                       <li><a href="<?php echo base_url('Main/show_voucher_reports_bko'); ?>">Voucher</a></li>
                                     
                                    
                                </ul>

                                </li>

                                                        <li class="has-submenu">
                                <a href="<?php echo base_url('Main/all_expense'); ?>"><i class="ti-bookmark-alt"></i>Expence</a>
                                
                            </li>



                                

                            <li class="has-submenu last-elements">
                                <a href="<?php echo base_url('Main/logout'); ?>"><i class="ti-power-off"></i>Logout</a>
                            </li>

                        </ul>
                        <!-- End navigation menu -->
                    </div> <!-- end #navigation -->
                </div> <!-- end container -->
            </div> <!-- end navbar-custom -->
        </header>