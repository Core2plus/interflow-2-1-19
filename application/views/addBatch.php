<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Add New Batch
        <small>Add / Edit User</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Batch Data Insertion</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body table-responsive">
                    <?php $this->load->helper("form"); ?>
                                   
                                <form action="<?php echo base_url('insertbatch'); ?>" method="post">
                                   
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Program<span class="required">*</span></label>
                                        <div>
                                            <select class="form-control" id="program_id" onchange="get_modules(this.value)" name="program_id">
                                                <option>Select</option>
                                            <?php
                                            foreach($all_pro as $pro):
                                            ?>
                    
                                          <option class="form-control" value="<?php echo $pro->program_id; ?>"><?php echo $pro->program_name ?></option>
                                                
                                                <?php
                                                    endforeach;
                                                ?>

                                          </select>
                                        </div>
                                    </div>  
                                    </div>

                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Module<span class="required">*</span></label>
                                        <div>
                                            <select class="form-control" id="module_id" name="module_id">
                                                
                                          </select>
                                        </div>
                                    </div>  
                                    </div>

                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Batch Name</label>
                                        <input type="text" name="batch_name" class="form-control" required placeholder="Type something"/>
                                    </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Start Date</label>
                                        <div>
                                            <input type="date" class="form-control" required
                                                   parsley-type="email" name="doj"/>
                                        </div>
                                    </div>
                                    </div>


                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Close Date</label>
                                        <div>
                                            <input type="date" class="form-control" name="doe"/>
                                        </div>
                                    </div>
                                    </div>


                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Select Status<span class="required">*</span></label>
                                        <div>
                                            <select class="form-control" id="sel1" name="sel1">
                                                <option>Select</option>
                                          <option class="form-control" value="1">Enable</option>
                                          <option class="form-control" value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                    </div>



                                    <div class="box-footer col-md-12">
                                    <div class="form-group">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light" style="width: 10%;">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                    


                                </form>
                                </div>
                </div></div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
<script>
    function get_modules(p)
    {
      param="name="+p;
      alert(param);
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      alert(this.responseText);
      document.getElementById("module_id").innerHTML=this.responseText;
         

         }
  };
  xhttp.open("POST", "<?php echo base_url().'user/get_module';?>", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(param);
    }


    </script>