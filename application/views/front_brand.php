
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
body {font-family: Arial;}

.navbar {
  margin-bottom: 0;
  border-radius: 0;
}


footer {
  background-color: #f2f2f2;
  padding: 25px;
}
/* Style the tab */
.tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 15px 32px;
    transition: 0.3s;
    font-size: 16px;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 15px 32px;
    -webkit-animation: fadeEffect 1s;
    animation: fadeEffect 1s;
}

/* Fade in tabs */
@-webkit-keyframes fadeEffect {
    from {opacity: 0;}
    to {opacity: 1;}
}

@keyframes fadeEffect {
    from {opacity: 0;}
    to {opacity: 1;}
}
a.btn.btn-primary {
  background-color: #4CAF50;
border: none;
color: white;
padding: 10px 28px;
text-align: center;
text-decoration: none;
font-size: 16px;

}
.pull-right{
  margin-left: 62%;
}
a.btn.btn-primary:hover {
    box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
}
.login-logo{
  margin-top:  4px;
}
.jumbotron{
  background: url('./assets/images/ads/light.jpg')
  center center !important;
 background-size: cover !important;
}
h1{
  color: #ffffff !important;
}
</style>
</head>
<body>




<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="login-logo">
          <img height="40" width="40" src="<?php base_url(); ?>./assets/images/InterFlowCom.png" >
      </div>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar" style="margin-left: 5%;">
      <ul class="nav navbar-nav">
        <li class="active"><a href="<?php base_url() ?>login/index2">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="<?php base_url() ?>Fronts/das_brand">Brands</a></li>
        <li><a href="#">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo base_url() ?>login/main"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="jumbotron">
  <div class="container text-center">
    <h1>𝓘𝓷𝓽𝓮𝓻𝓯𝓵𝓸𝔀 𝓒𝓸𝓶𝓶𝓾𝓷𝓲𝓪𝓽𝓲𝓸𝓷</h1>
    <p>This creative agency in Germany uses a unique design...</p>
  </div>
</div>

               <?php
                  //$no = 1;
                   foreach ($category as $v) {

                   ?>



<div class="card" id="cat" >

               <a href="<?php echo base_url().'Adv/das_video/'.$v->BrandName; ?>">
               <img src="<?php echo '../images/brands/' .$v->image; ?>" height="130px;" width="100%;"/>
               <hr>
               <h4><b><?php echo $v->BrandName; ?></b></h4></a>

</div>




<?php } ?>

<footer class="container-fluid text-center" style="background-color:#222d32;">
    <div class="pull-right hidden-xs">
      <b style="color:white;" >Interflow Communications</b> <span style="color:white;">Designed By: Core2Plus</span>
    </div>
    <strong style="color:white;">Copyright &copy; <a href="<?php echo base_url(); ?>">Core2Plus</a>.</strong>

</footer>




<script>
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>

</body>
</html>
