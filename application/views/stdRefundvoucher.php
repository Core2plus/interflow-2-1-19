<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Refund Voucher
            <small> Credit Voucher </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <div class="col-md-7">
                    <h3 class="box-title pull-left"> <b> Voucher </b></h3>
                </div>
                <div class="col-md-5">
                    <h3 class="box-title pull-left"> <b> Payment Details </b></h3>
                </div>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!-- Page-Title -->
                <div class="row">
                    <div class="offset-sm-1 col-sm-10">
                        <div class="page-title-box">


                        </div>
                    </div>

                </div>
                <!-- end page title end breadcrumb -->
                <?php  $t=1; ?>

                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="card">

                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6">


                                        <?php
                                        // print_r($show_discount_details_in_voucher->result_array());
                                        // print_r($show_fee_details_in_student_profile);


                                        //  $s =  0 ;
                                        //      foreach($show_discount_details_in_voucher->result() as $show2 ) {
                                        //   $s =  $show2->discount_amount ;

                                        // }



                                        //   foreach($show_fee_details_in_student_profile as $show ) {
                                        //   $t =  $show->discount_per ;

                                        // }
                                        //        if($t == "" ){

                                        ?>


                                        <?php

                                        foreach($show_student_details->result() as
                                        $show_student_profile1) { ?>

                                        <form action="<?php echo base_url('user/credit_voucher_insert/')?><?php echo $show_student_profile1->studentid; ?>" method="post">


                                            <div class="row">


                                                <label class="col-md-3">Student Name: </label>
                                                <p><?php echo $show_student_profile1->fname; ?></p>

                                            </div>
                                            <div class="row">


                                                <label class="col-md-3">Phone Number: </label>

                                                <p><?php echo $show_student_profile1->phone; ?>

                                                    <input type="hidden" name="studentid" value="<?php echo $show_student_profile1->studentid; ?>">
                                                    <input type="hidden" name="batch_name" value="<?php echo $show_student_profile1->batch_name; ?>">
                                                    <input type="hidden" name="batch_id" value="<?php echo $show_student_profile1->batch_id; ?>">

                                                </p>
                                            </div>
                                            <div class="row">


                                                <label class="col-md-3">ARTT ID: </label>
                                                <p><?php echo $show_student_profile1->artt_id; ?></p>
                                            </div>

                                            <div class="row">
                                                <label class="col-md-3">Batch Name: </label>
                                                <p><?php echo $show_student_profile1->batch_name; ?></p>
                                            </div>

                                            <?php  }

                                            ?>

                                    </div>


                                    <div class="col-md-6">


                                        <div class="row" >
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <label>Pay With</label>
                                                </div>

                                                <div class="col-md-8">
                    <span>Cash &nbsp;
                      <input type="radio" name="pay_with" required value="Cash" onclick="myFunction2()" id="payment_with"></span>
                                                    <span >Cheque &nbsp;
                      <input type="radio" name="pay_with" required value="Cheque" onclick="myFunction3()"   id="payment_with"></span>

                                                </div>

                                                <div class="col-md-12">

                                                    <div class="row" id="cheque_number" style="display:none;">


                                                        <div class="col-md-4">

                                                            <label>Cheque Number &nbsp;</label>

                                                        </div>
                                                        <div class="col-md-8">
                                                            <input  name="cheque_number"  type="text" id="cheque_number">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>



                                        <div class="row" >
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <label> Date:</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <input disabled style="background-color: white; border: none;" value="<?php echo date("d-m-y");?>" class="form-control" id="current_date" >
                                                    <input type="hidden" name="current_date" value="<?php echo date("d-m-y");?>" class="form-control" >
                                                </div>
                                            </div>
                                        </div>


<!--                                        <div class="row" >-->
<!--                                            <div class="col-md-12">-->
<!--                                                <div class="col-md-4">-->
<!--                                                    <label>Due date:</label>-->
<!--                                                </div>-->
<!--                                                <div class="col-md-6">-->
                                                    <input name="due_date" class="form-control" type="hidden" value="<?php date('Y-m-d'); ?>">
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!---->
<!---->
                                        <br>
<!--                                        <div class="row" >-->
<!--                                            <div class="col-md-12">-->
<!--                                                <div class="col-md-4">-->
<!--                                                    <label>Payment amount:</label>-->
<!--                                                </div>-->
<!--                                                <div class="col-md-6">-->
                                                    <input  id="paying_amount"  name="pay_amount" class="form-control" type="hidden" value="<?php echo $show_refund_details['refund_amount'] * (-1); ?>">
<!--                                                    <p id="demo">-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!---->
<!--                                        <div class="row" >-->
<!--                                            <div class="col-md-12">-->
<!--                                                <div class="col-md-4">-->
<!--                                                    <label>Remain Amount:</label>-->
<!--                                                </div>-->
<!---->
<!--                                                <div class="col-md-6">-->
<!---->
<!--        <span>Rs.<input disabled style="background-color: white; border: none;" id="remain1" type="text">-->
<!---->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->


                                        <div class="form-group row col-sm-10 col-sm-10 col-xs-12">
                                            <div  class="col-md-8">

                                                <button id="button" style="margin-left:140%;" type="submit" class="btn btn-primary waves-effect waves-light">
                                                    Voucher
                                                </button>
                                            </div>



                                        </div>
                                    </div>
                                </div>
                                <h1 style="font-size: 24px;">Courses Details</h1>
                                <hr>

                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-11">
                                            <table class="table table-hover">

                                                <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>Course Name</th>
                                                    <th>Course Code</th>
                                                    <th>Start Date</th>
                                                    <th>Fee</th>
                                                    <th>Refund%</th>
                                                    <th>Reason</th>
                                                    <th>Received Amt</th>
                                                    <th>Refund Left Amt</th>

                                                    <th>Voucher Amt</th>

                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php
                                                $no=1;
                                                $a=0;
                                                $b=0;
                                                $c= 0 ;
                                                $tr=0;
                                                $i=0;
                                                $d=0;
                                                $ac=0;

                                                foreach($show_fee_details_in_student_profile as $show) {
                                                $i++;
                                                ?>

                                                <tr>
                                                    <td><?php echo $no++; ?></td>
                                                    <td> <?php echo $show->coursename;   ?> </td>
                                                    <td><?php echo $show->coursecode;   ?></td>
                                                    <td><?php echo $show->startdate;   ?></td>
                                                    <td><?php echo $show->coursefee;   $a+= $show->coursefee; ?>

                                                    <td><?php echo $show_refund_details['per'];?>%</td>


                                                    <td><?php echo $show_refund_details['msg'];?>


                                                        <input type="hidden" value="<?php echo $show_refund_details['msg'];?>" name="msg">
                                                        <input type="hidden" value="<?php echo $show_refund_details['per'];?>" name="per">
                                                        <input type="hidden" value="<?php echo $show_refund_details['refund_amount'] * (-1) ;?>" name="refund_amount">
                                                        <input type="hidden" value="<?php echo $show_refund_details['course_id'];?>" name="course_id_refund">

                                                    </td>

<!--                                                    <td>--><?php // if($show->discount_per!=''){ $p= $show->coursefee*($show->discount_per/100);
//                                                        echo $ac=$show->coursefee-$p;$d+=$ac;}?>
<!--                                                        <input type="hidden" value="--><?php //echo $show->coursefee;    ?><!--" id="coursefee_amt">-->
<!--                                                        <input type="hidden" value="--><?php //echo $show->discount_amount;    ?><!--" name="discount_amount[]">-->
<!---->
<!---->
<!--                                                    </td>-->



                                                    <td>
                                                        <input type="hidden" value="<?php echo $show->course_id;    ?>" name="course_id[]">
                                                        <input type="hidden" value="<?php echo $show->student_id;    ?>" name="student_id[]">
                                                        <input type="hidden" value="<?php echo $show->received_amount;    ?>" id="received_amt" name="received_amount[]">

                                                        <?php echo $show->received_amount;  $tr+=$show->received_amount;    ?></td>

                                                    <td style="font-weight: bold">  <?php $total = $show->received_amount - $show_refund_details['refund_amount'];?>  <?php echo $total; ?></td>

                                                    <td><input type="number" id="vocher_ind<?php echo $i;?>" onchange="clicking(<?php echo $i;?>)" value="<?php echo $show_refund_details['refund_amount'] * (-1); ?>" class="element form-control" name="voucher_amount[]" disabled>
                                                        <input type="hidden" name="voucher_amount[]" value="<?php echo $show_refund_details['refund_amount'] * (-1); ?>">
                                                    </td>

                                                    <?php } ?>
                                                </tr>
                                                <input type="hidden"  id="totalrows" value="<?php echo $i;?>"/>


                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>

                                                    <td>

<!--                                                        Rs.-->

                                                        <input type="hidden"  name="total_fees" value="<?php echo   $a; ?>">

<!--                                                        --><?php //echo   $a; ?>

                                                    </td>
                                                    <td></td>
                                                    <td>

<!--                                                        --><?php //echo   $d; ?>

                                                    </td>
                                                    <td>

<!--                                                        Rs.--><?php //echo   $tr; ?>

                                                    </td>
                                                    <td></td>
                                                    <td>
<!--                                                        <input  disabled id="paying_amount1" required name="pay_amount1" class="form-control" type="text">-->
                                                    </td>


                                                </tr>


                                                </tbody>



                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="float:left;">

                                    <div class="row" >
                                        <div class="col-md-6">
                                            <div class="col-md-4">

                                            </div>
                                            <div class="col-md-8">
        <span>
    <input type="hidden" style="background-color: white; border: none;" value="<?php echo $a-$tr; ?>" name="remain_fees" id="remain"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>



                    <!-- MainIF -->        <?php //} ?>


                </div>
                <!-- /.box-body -->
                <div class="box-footer">

                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- end wrapper -->
<script>



    function myFunction2() {



        document.getElementById('cheque_number').style.display ='none';

    }
    function myFunction3() {
        document.getElementById('cheque_number').style.display ='block';

    }

</script>


<!---->
<!--<script>-->
<!--    -->
<!--    function clicking(index)-->
<!--    {-->
<!--        document.getElementById("button").disabled = true;-->
<!---->
<!--        paying=document.getElementById('paying_amount').value;-->
<!--        if(paying!='' && paying!=null)-->
<!--        {-->
<!--            totalpaid=0;-->
<!--            i=1;-->
<!--            total=document.getElementById("totalrows").value;-->
<!--            alert(total);-->
<!--            tok=0;-->
<!--            for(i=1;i<=total;i++)-->
<!--            {-->
<!---->
<!--                v=document.getElementById("vocher_ind"+i).value;-->
<!--                if(v!='' && v!=null)-->
<!--                {-->
<!--                    totalpaid+=parseInt(v);-->
<!---->
<!---->
<!---->
<!--                }-->
<!---->
<!---->
<!--            }-->
<!--            document.getElementById('paying_amount1').value=totalpaid;-->
<!--            paying=document.getElementById('paying_amount').value;-->
<!--            paying=parseInt(paying);-->
<!--            alert(paying);-->
<!--            if(paying==totalpaid)-->
<!--            {-->
<!---->
<!--                alert('Ok');-->
<!--                document.getElementById("button").disabled = false;-->
<!--                for(i=1;i<=total;i++)-->
<!--                {-->
<!--                    f=document.getElementById("vocher_ind"+i).value;-->
<!--                    if(f=='' || f==0)-->
<!--                    {-->
<!--                        document.getElementById("vocher_ind"+i).value=0;-->
<!--                        document.getElementById("vocher_ind"+i).readOnly=true;-->
<!--                    }-->
<!---->
<!--                }-->
<!---->
<!--            }-->
<!--        }-->
<!--        else{-->
<!--            alert("Please Enter Paying Amount");-->
<!---->
<!--        }-->
<!---->
<!--    }-->
<!--</script>-->
<!--<script>-->
<!--    remain1=document.getElementById("remain").value;-->
<!--    document.getElementById("remain1").value=remain1;-->
<!---->
<!---->
<!--</script>-->