<style>

#preloader {
  position: fixed;
  left: 0;
  top: 0;
  z-index: 99;
  width: 100%;
  height: 100%;
  overflow: visible;
  background: #ffffff
      url('https://loading.io/spinners/typing/lg.-text-entering-comment-loader.gif')
      no-repeat center center;
}
.content-wrapper{

       background: url('../assets/images/ads/img333.jpg') 
       center center !important;
      background-size: cover !important;

}


</style>
<script>
  $(document).ready( function() {
  $('#preloader').delay(5).fadeOut();
});
</script>
  <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>





<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 style="color:white;">
        <i class="fa fa-users"></i> Edit Advertisement
        <small style="color:white;">Edit Advertisement</small>
      </h1>
    </section>

    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->



                <div class="box" style="opacity:1; background-color: #e8e7e7; margin-top: 0%;">
                <div id="preloader"></div>
                    <div class="box-header">

                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body table-responsive">
                         <form action="<?php echo base_url('adv/update_adv'); ?>" method="post">


      <?php
                                foreach($cat as $k=>$v){
                                ?>

                                     <input type="hidden" name="cat_id" value="<?php echo $v['AdsID'] ?>">

                                     <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control" value="<?php echo $v['AdsName'];?>"  />
                                    </div>
                                    </div>

                                    <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Category</label>
                                        <select name="category" class="form-control">
                                            <?php if(@$category)
                                            {
                                                foreach($category as $k=>$c)
                                                {
                                                ?>
                                                <option value="<?php echo $c['CatID'];?>" <?php if($v['CatID']==$c['CatID']){echo "selected";}?>><?php echo $c['CatType'];?></option>

                                                <?php
                                            }


                                            }
                                            ?>

                                        </select>
                                    </div>
                                    </div>

                                    <div class="col-md-6">
                                     <div class="form-group">
                                        <label>AD. Duaration</label>
                                        <input type="text" name="dur" class="form-control" value="<?php echo $v['AdDuration'];?>"/>
                                    </div>
                                    </div>
                                     <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Ad. Slot</label>
                                        <input type="text" name="slot" class="form-control" value="<?php echo $v['AdSlot'];?>"/>
                                    </div>
                                    </div>
                                      <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Product</label>
                                        <select name="product" class="form-control">
                                             <?php if(@$product)
                                            {
                                                foreach($product as $k=>$p)
                                                {
                                                ?>
                                                <option value="<?php echo $p['ProductID'];?>" <?php if($v['ProductID']==$p['ProductID']){echo "selected";}?>><?php echo $p['ProductName'];?></option>

                                                <?php
                                            }


                                            }
                                            ?>


                                        </select>
                                    </div>
                                    </div>
                                      <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Agency</label>
                                        <select name="agency" class="form-control">
                                             <?php if(@$agency)
                                            {
                                                foreach($agency as $k=>$a)
                                                {
                                                ?>
                                                <option value="<?php echo $a['AgencyID'];?>" <?php if($v['AgencyID']==$a['AgencyID']){echo "selected";}?>><?php echo $a['AgencyName'];?></option>

                                                <?php
                                            }


                                            }
                                            ?>


                                        </select>
                                    </div>
                                    </div>
                                        <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Artist</label>
                                        <select name="artist" class="form-control">
                                             <?php if(@$artist)
                                            {
                                                foreach($artist as $k=>$aa)
                                                {
                                                ?>
                                                <option value="<?php echo $aa['ArtistID'];?>" <?php if($v['ArtistID']==$aa['ArtistID']){echo "selected";}?> ><?php echo $aa['Name'];?></option>

                                                <?php
                                            }


                                            }
                                            ?>


                                        </select>
                                    </div>
                                    </div>

                                    <div class="col-md-6">
                                     <div class="form-group">
                                        <label>date</label>
                                        <input type="date" name="date" class="form-control" value="<?php echo $v['AdBookingDate'];?>"/>
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Order No.</label>
                                        <input type="text" name="order" class="form-control" value="<?php echo $v['OrderNo'];?>"/>



                                </div>

                                    </div>
                                    <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Rating</label>
                                        <select name="artist" class="form-control">
                                             <option value="1" <?php if($v['AdRating']=="1"){echo "selected";}?>>1</option>
                                             <option value="2" <?php if($v['AdRating']=="2"){echo "selected";}?>>2</option>
                                             <option value="3" <?php if($v['AdRating']=="3"){echo "selected";}?>>3</option>
                                             <option value="4" <?php if($v['AdRating']=="4"){echo "selected";}?>>4</option>
                                             <option value="5" <?php if($v['AdRating']=="5"){echo "selected";}?>>5</option>


                                        </select>



                                </div>

                                    </div>



                                    <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Cost</label>
                                        <input type="text" class="form-control" name="cost" value="<?php echo $v['Cost'];?>" />
                                    </div>
                                    </div>










                                    <!--
                                    <div class="form-group">
                                        <label>Number</label>
                                        <div>
                                            <input data-parsley-type="number" type="text"
                                                   class="form-control" required
                                                   placeholder="Enter only numbers"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Alphanumeric</label>
                                        <div>
                                            <input data-parsley-type="alphanum" type="text"
                                                   class="form-control" required
                                                   placeholder="Enter alphanumeric value"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Textarea</label>
                                        <div>
                                            <textarea required class="form-control" rows="5"></textarea>
                                        </div>
                                    </div>-->

                                    <div class="form-group">
                                        <div class=" col-md-12">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light" style="width: 10%;">
                                                Update
                                            </button>
                                            <!-- <button type="reset" class="btn btn-secondary waves-effect m-l-5" style="width: 10%;">
                                                Cancel
                                            </button> -->
                                        </div>
                                    </div>

                                </form>
                                </div>
<?php } ?>
</div>
                </div>
            </div>
            <div class="col-md-4">
                <!-- <?php
                   // $this->load->helper('form');
                    //$error = //$this->session->flashdata('error');
                    //if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php //echo $this->session->flashdata('error'); ?>
                </div>
                <?php } ?>
                <?php
                    //$success = //$this->session->flashdata('success');
                    //if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php //echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                 -->
                <div class="row">
                    <div class="col-md-12">
                        <?php // echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
