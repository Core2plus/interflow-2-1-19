  -- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 30, 2018 at 02:52 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `artt`
--

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `artt_id` varchar(200) NOT NULL,
  `cr` text NOT NULL,
  `program_id` int(20) DEFAULT NULL,
  `batch_id` int(255) NOT NULL,
  `studentid` int(11) NOT NULL,
  `fname` varchar(100) DEFAULT NULL,
  `mname` varchar(100) DEFAULT NULL,
  `lname` varchar(100) DEFAULT NULL,
  `fathername` varchar(100) DEFAULT NULL,
  `father_email` varchar(150) NOT NULL,
  `father_cnic` varchar(150) NOT NULL,
  `father_phone` varchar(150) NOT NULL,
  `father_profession` varchar(150) NOT NULL,
  `CNIC` int(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` int(15) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `image` blob,
  `DOJ` date DEFAULT NULL,
  `permanent_address` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`artt_id`, `cr`, `program_id`, `batch_id`, `studentid`, `fname`, `mname`, `lname`, `fathername`, `father_email`, `father_cnic`, `father_phone`, `father_profession`, `CNIC`, `email`, `phone`, `gender`, `image`, `DOJ`, `permanent_address`) VALUES
('LR-12', '', NULL, 0, 44, 'syed', 'Ali', 'Saad', 'jjj', '', '', '', '', 888, 'sanaullah@gmail.com', 0, 'Female', 0x31353334363830373839646f776e6c6f61642e6a7067, '2018-08-16', 'da'),
('lr-dasd12', '', NULL, 0, 48, 'Ali', 'saad', 'saad', 'saad', '', '', '', '', 321312313, 'hamza.saleem.3154@facebook.com', 2147483647, 'Male', 0x31353334363930363336332e6a7067, '2018-08-17', 'dasd'),
('2234234v', '', NULL, 21, 49, 'jkjsdasd', 'jsdkj', 'kjkj', 'kjkj', '', '', '', '', 99999, '99@asd.com', 34234, 'Female', 0x31353335343433303533303274682d6567672d706572736f6e202831292e6a7067, '0099-09-09', 'f'),
('AR-101', '', NULL, 21, 50, 'Hamza', 'a', 'a', 'Muhammad Saleem', 'saleem@gmail.com', '13242-2341451-1', '012422352', 'Manager At Steel Mills', 12341, 'hamza.saleem.3154@facebook.com', 2147483647, 'Male', 0x31353335363133343039303274682d6567672d706572736f6e202831292e6a7067, '2018-08-22', 'House A-121 , Block 8 Gulshan'),
('AR-101', '12345', NULL, 21, 51, 'Hamza', '', '', 'Muhammad Saleem', 'saleem@gmail.com', '42515-1245151-1', '02415151511', 'Manager At Steel Mills', 41251, 'hamza.saleem.3154@facebook.com', 2147483647, 'Male', 0x31353335363133363134303274682d6567672d706572736f6e202831292e6a7067, '2018-08-01', 'House A-121  , Block  8 \r\nGulshan,  Karachi'),
('8', '88', NULL, 48, 52, '88', '88', '8', '8', '888', '88', '88', '8', 88, '88@3.com', 88, 'Male', 0x31353335363134323739303274682d6567672d706572736f6e202831292e6a7067, '0088-08-02', '88'),
('9', '99', NULL, 44, 53, '9', '99', '99', '99', '99', '999', '999', '99', 999, '888@asd.com', 9999, 'Male', 0x31353335363231303432, '0088-08-08', '999'),
('3123123', '878', NULL, 44, 54, 'Ali Chacha', '87', '878', 'jhjh', 'asd@asd.com', '8888', '888', '8', 78, '878787@asd.com', 9, 'Female', 0x31353335363236393734, '0007-07-08', '99'),
('89', '9898', NULL, 47, 55, '98989', '898', '9898', 'kj', 'kjk@ad.com', '88', '888', '8', 9898, '898@as.com', 999, 'Male', 0x31353335363331393834, '0009-09-08', '999');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`studentid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `studentid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
