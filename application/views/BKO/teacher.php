
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="offset-sm-1 col-sm-10">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                   
                                    <li class="breadcrumb-item active">New Teacher</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Teacher</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Add Detail</h4><br>                
                                <form action="<?php echo base_url('Main/insert_teacher'); ?>" method="post" enctype="multipart/form-data">
                                   

                                    <div class="col-md-12 col-sm-12 row">
                                        <div class="form-group row col-md-10 col-sm-10 col-xs-12">
                                            <label class="col-md-2">Teacher Name</label>
                                            <div class=" col-md-4">
                                                <input type="text" name="first_name" class="form-control" required placeholder="First Name"/>
                                            </div>
                                            <div class=" col-md-3">
                                            <input type="text" name="middle_name" class="form-control" required placeholder="Middle Name"/>
                                            </div>
                                            <div class=" col-md-3">
                                                <input type="text" name="last_name" class="form-control" required placeholder="Lastname"/>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row col-md-10 col-sm-10 col-xs-12">
                                            <label class="col-md-2">Father Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="father_name" class="form-control" required placeholder="Father Name">
                                            </div>

                                            <label class="col-md-2">Surname</label>
                                            <div class="col-md-4">
                                                <input type="text" name="sur_name" class="form-control" required placeholder="Surname">
                                            </div>
                                        </div>
                                        <div class="form-group row col-md-10 col-sm-10 col-xs-12">
                                            <label class="col-md-2">Date of Birth</label>
                                            <div class="col-md-4">
                                                <input type="date" name="dob" class="form-control" required>
                                            </div>

                                            <label class="col-md-2">CNIC</label>
                                            <div class="col-md-4">
                                                <input type="text" name="cnic" class="form-control" required placeholder="Enter CNIC">
                                            </div>
                                        </div>


                                        <div class="form-group row col-md-10 col-sm-10 col-xs-12">
                                            <label class="col-md-2">Date of Joining</label>
                                            <div class="col-md-4">
                                                <input type="date" name="doj" class="form-control" required>
                                            </div>

                                            <label class="col-md-2">Email</label>
                                            <div class="col-md-4">
                                                <input type="email" name="email" class="form-control" required placeholder="Enter Email">
                                            </div>
                                        </div>
                                    
                                        <div class="form-group row col-md-10 col-sm-10 col-xs-12">
                                            <label class="col-md-2">Gender</label>
                                            <div class="col-md-4">
                                                <label class="radio-inline">&nbsp;
      <input type="radio" name="optradio" value="Male" checked>Male
    </label>&nbsp;&nbsp;
    <label class="radio-inline">&nbsp;
      <input type="radio" name="optradio" value="Female">Female
    </label>&nbsp;&nbsp;
    <label class="radio-inline">&nbsp;
      <input type="radio" name="optradio" value="Other">Other
    </label>
                                            </div>

                                            <label class="col-md-2">Number</label>
                                            <div class="col-md-4">
                                                <input type="text" name="number" class="form-control" required placeholder="Enter Number">
                                            </div>
                                        </div>

                                        <div class="form-group row col-md-10 col-sm-10 col-xs-12">
                                            <label class="col-md-2">Temporary Address</label>
                                            <div class="col-md-4">
                                                <textarea class="form-control" name = "temporary_address" rows="2" id="comment" placeholder="Enter Your Current Address"></textarea>
                                            </div>

                                            <label class="col-md-2">Permanent Address</label>
                                            <div class="col-md-4">
                                                <textarea class="form-control"   name = "permanent_address" rows="2" id="comment" placeholder="Enter Your Domicile Address"></textarea>
                                            </div>
                                        </div>
                                        
<div class="form-group row col-md-10 col-sm-10 col-xs-12">
                                            <label class="col-md-4">Profile Picture</label>
                                            <div class="col-md-6">
                                                <input type="file" name="image" class="form-control form-group">
                                            </div>

                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                    <!-- <div class="form-group">
                                        <label>Select Subject<span class="required">*</span></label>
                                        <div>
                                            
                                            <select class="form-control" id="sel1">
                                          <option class="form-control">Enable</option>
                                          <option class="form-control">Disable</option>
                                            </select>
                                        </div>
                                    </div> -->
                                    <div class="form-group">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button>
                                            <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>


                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->

                    
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
       
<?php $this->load->view('BKO/includes/footer'); ?>
     
