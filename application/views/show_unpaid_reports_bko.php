

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> UnPaid Reports
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Paid Reports</h3>
                   
                </div><!-- /.box-header -->
                <div class="box-body table-responsive ">


 <a href="<?php echo base_url('Main/show_unpaid_reports_bko_for_print'); ?>">
      <button type="button" class="btn btn-info">Print</i></button></a>
                                
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Student Name</th>
                                        <th>Course Name</th>
                                        <th>Course Code</th>

                                        <th>Course Fee</th>
                                        <th>Received Amount</th>
                                        <th>Discount Amount</th>
                                     
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $no = 1;
                                        foreach($show_unpaid_reports as $show) { 

                                            ?>
 <tr>
                                         <th><?php  echo $no++ ; ?></th>
                                        <th><?php  echo $show->fname;    ?></th>
                                        <th><?php  echo $show->coursename;    ?></th>
                                        <th><?php  echo $show->coursecode;    ?></th>
                                     
                                        <th><?php  echo $show->coursefee;    ?></th>
                                        <th><?php  echo $show->received_amount;    ?></th>
                                        <th><?php  echo $show->discount_amount;    ?></th>
                                     
                                    </tr>

                                    <?php
                                }

                                    ?>



                           
                                    </tbody>
                                </table> 
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>















