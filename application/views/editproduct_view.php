<style>
#preloader {
  position: fixed;
  left: 0;
  top: 0;
  z-index: 99;
  width: 100%;
  height: 100%;
  overflow: visible;
  background: #ffffff
      url('https://loading.io/spinners/typing/lg.-text-entering-comment-loader.gif')
      no-repeat center center;
}
.content-wrapper{

       background: url('../assets/images/ads/img333.jpg')
       center center !important;
      background-size: cover !important;

}



</style>
<script>
  $(document).ready( function() {
  $('#preloader').delay(5).fadeOut();
});
</script>
  <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>







<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 style="color:white;">
        <i class="fa fa-users"></i> Edit Product
        <small style="color:white;">Add / Edit User</small>
      </h1>
    </section>

    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->



                <div class="box" style="opacity:1; background-color: #e8e7e7; margin-top: 0%;">
                <div id="preloader"></div>
                    <div class="box-header">

                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body table-responsive">


      <?php
                                foreach($cat as $k=>$v):
                                ?>
                                <form action="<?php echo base_url('product/update_product'); ?>" method="post" enctype="multipart/form-data">
                                     <input type="hidden" name="cat_id" value="<?php echo $v['ProductID'] ?>">

                                     <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Product Name</label>
                                        <input type="text" name="name" class="form-control" value="<?php echo $v['ProductName'] ?>" />
                                    </div>
                                    </div>


                                    <div class="col-md-6">
                                   <div class="form-group">
                                       <label>Image</label>
                                       <input type="file" name="filename" class="form-control"  />
                                   </div>
                                   </div>
                                     <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Brand Name</label>
                                        <select name="brandname" class="form-control">
                                            <?php if(@$brand)
                                            {
                                                foreach($brand as $k=>$b)
                                                {
                                                    ?>
                                                    <option value="<?php echo $b['BrandID'];?>" <?php if($v['BrandID']==$b['BrandID']){ echo "selected";}?> ><?php echo $b['BrandName']; ?> </option>

                                                    <?php


                                                }


                                            }
                                            ?>





                                        </select>
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Product Details</label>
                                        <textarea class="form-control" name="des"><?php echo $v['ProductDetail'];?></textarea>
                                    </div>
                                    </div>









                                    <!--
                                    <div class="form-group">
                                        <label>Number</label>
                                        <div>
                                            <input data-parsley-type="number" type="text"
                                                   class="form-control" required
                                                   placeholder="Enter only numbers"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Alphanumeric</label>
                                        <div>
                                            <input data-parsley-type="alphanum" type="text"
                                                   class="form-control" required
                                                   placeholder="Enter alphanumeric value"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Textarea</label>
                                        <div>
                                            <textarea required class="form-control" rows="5"></textarea>
                                        </div>
                                    </div>-->

                                    <div class="form-group">
                                        <div class=" col-md-12">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light" style="width: 10%;">
                                                Update
                                            </button>
                                           <!--  <button type="reset" class="btn btn-secondary waves-effect m-l-5" style="width: 10%;">
                                                Cancel
                                            </button> -->
                                        </div>
                                    </div>

                                </form>


</div>
                </div>
            </div>
</div>
        </div>
    </section>

</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
<?php endforeach; ?>
