<script>
window.onload = function () {
 
var chart = new CanvasJS.Chart("chartContainer", {
    animationEnabled: true,
    exportEnabled: true,
    title:{
        text: "Numbers of Students"
    },
    subtitles: [{
        text: "Program Wise"
    }],
   
  
    data: [
    {
        type: "bar",
        showInLegend: "true",
        legendText: "{label}",
        name: "Number of Students",
    showInLegend: true,
        indexLabelFontSize: 16,
        yValueFormatString: "#,##0",
        indexLabelPlacement: "inside",  
         indexLabelOrientation: "horizontal",
        dataPoints: <?php echo $students;?>
    }

    ]
});
chart.render();

var chart1 = new CanvasJS.Chart("chartContainer1", {
  exportEnabled: true,
  animationEnabled: true,
  title:{
    text: "Number Of Student Status"
  },
   
  
  axisY: {
    title: "Number Of Students",
    titleFontColor: "#4F81BC",
    lineColor: "#4F81BC",
    labelFontColor: "#4F81BC",
    tickColor: "#4F81BC"
  },
  

  toolTip: {
    shared: true,
  },
  legend: {
    cursor: "pointer",
    itemclick: toggleDataSeries
  },
  data: [
  {
    type: "column",
    name: "Regular Student ",
    axisYType: "secondary",
    showInLegend: true,
    yValueFormatString: "#,##0.# Student",
    indexLabel: "{y}",
         indexLabelPlacement: "outside",  
         indexLabelOrientation: "horizontal",
    dataPoints:<?php echo @$rarray?> 
  },
  {
    type: "column",
    name: "Freeze Students",
    showInLegend: true,      
    yValueFormatString: "#,##0.# Student",
    indexLabel: "{y}",
         indexLabelPlacement: "outside",  
         indexLabelOrientation: "horizontal",
    dataPoints: <?php echo @$farray;?>
  },
  {
    type: "column",
    name: "UnFreeze Student",
    showInLegend: true,      
    yValueFormatString: "#,##0.# Student",
    indexLabel: "{y}",
         indexLabelPlacement: "outside",  
         indexLabelOrientation: "horizontal",
    dataPoints: <?php echo @$uarray;?>
  }

  ]
});
chart1.render();

function toggleDataSeries(e) {
  if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
    e.dataSeries.visible = false;
  } else {
    e.dataSeries.visible = true;
  }
  e.chart.render();
}
var chart2 = new CanvasJS.Chart("chartContainer2", {
  exportEnabled: true,
  animationEnabled: true,
  title:{
    text: "Amount of Status"
  },
   
  
  axisY: {
    title: "Amount",
    titleFontColor: "#4F81BC",
    lineColor: "#4F81BC",
    labelFontColor: "#4F81BC",
    tickColor: "#4F81BC"
  },
  

  toolTip: {
    shared: true,
  },
  legend: {
    cursor: "pointer",
    itemclick: toggleDataSeries1
  },
  data: [
  {
    type: "column",
    name: "Regular Amount ",
    axisYType: "secondary",
    showInLegend: true,
    yValueFormatString: "#,##0.# Amount",
    indexLabel: "{y}",
         indexLabelPlacement: "outside",  
         indexLabelOrientation: "horizontal",
    dataPoints:<?php echo @$ararray?> 
  },
  {
    type: "column",
    name: "Freeze Amount",
    showInLegend: true,      
    yValueFormatString: "#,##0.# Amount",
    indexLabel: "{y}",
         indexLabelPlacement: "outside",  
         indexLabelOrientation: "horizontal",
    dataPoints: <?php echo @$afarray;?>
  },
  {
    type: "column",
    name: "UnFreeze Amount",
    showInLegend: true,      
    yValueFormatString: "#,##0.# Amount",
    indexLabel: "{y}",
         indexLabelPlacement: "outside",  
         indexLabelOrientation: "horizontal",
    dataPoints: <?php echo @$uarray;?>
  }

  ]
});
chart2.render();

function toggleDataSeries1(e) {
  if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
    e.dataSeries.visible = false;
  } else {
    e.dataSeries.visible = true;
  }
  e.chart.render();
}


 
}
</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-tachometer" aria-hidden="true"></i> Student  Statistics
            <small>Students Reports</small>
           
        </h1>
    </section>

    <section class="content">

        <div class="row">
            <div id="chartContainer" style="height: 370px; width: 100%;"></div>
             <div id="chartContainer1" style="height: 370px; width: 100%;"></div>
                         <div id="chartContainer2" style="height: 370px; width: 100%;"></div>

<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>




           
        </div>
    </section>
</div>