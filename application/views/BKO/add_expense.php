
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="offset-sm-1 col-sm-10">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                   
                                    <li class="breadcrumb-item active">New Expense</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Expense</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Add Detail</h4><br>                
                                <form action="<?php echo base_url('Main/insert_expense'); ?>" method="post" enctype="multipart/form-data">
                                   

                                    
                                        
                                        <div class="form-group row col-md-10 col-sm-10 col-xs-12">
                                            <label class="col-md-2">Title</label>
                                            <div class="col-md-4">
                                                <input type="text" name="expense_title" class="form-control" required placeholder="Enter Title">
                                            </div>

                                            <label class="col-md-2">Sub Title</label>
                                            <div class="col-md-4">
                                                <input type="text" name="expense_sub_title" class="form-control" placeholder="Enter Sub Title" required placeholder="Surname">
                                            </div>
                                        </div>
                                         <div class="form-group row col-md-10 col-sm-10 col-xs-12">
                                             <label class="col-md-2">Current Date</label>
                                            <div class="col-md-4">
                                           <input type="date" name="today_date" style="border:none;" value="<?php echo date('Y-m-d'); ?>" />
                                            </div>


                                            <label class="col-md-2">Due Date</label>
                                            <div class="col-md-4">
                                                <input type="date" name="due_date" class="form-control" required >
                                            </div>
                                        </div>
                                        <div class="form-group row col-md-10 col-sm-10 col-xs-12">
                                            <label class="col-md-2">Amount</label>
                                            <div class="col-md-4">
                                                <input type="number" placeholder="Enter Amount" name="amount" class="form-control" required>
                                            </div>

                                        </div>


                                        <div class="form-group row col-md-10 col-sm-10 col-xs-12">
                                            <label class="col-md-2">Expense Details</label>
                                            <div class="col-md-7">
                                                <textarea class="form-control" name = "expense_details" rows="2" id="comment" placeholder="Enter Details"></textarea>
                                            </div>

                                           
                                           
                                        </div>
                                        

                                    </div>
                                    
                                    
                                    
                                    
                                    <!-- <div class="form-group">
                                        <label>Select Subject<span class="required">*</span></label>
                                        <div>
                                            
                                            <select class="form-control" id="sel1">
                                          <option class="form-control">Enable</option>
                                          <option class="form-control">Disable</option>
                                            </select>
                                        </div>
                                    </div> -->
                                    <div class="form-group">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button>
                                            <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>


                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->

                    
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
       
<?php $this->load->view('BKO/includes/footer'); ?>
     


