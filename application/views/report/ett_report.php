
<!DOCTYPE html>
<html>

<?php
if (isset($this->session->userdata['logged_in'])) {
?>

<head>
	<title> ETT REPORT </title>
</head>
<body>

<div class="col-md-12">

	<img src="<?php base_url(); ?>assets/assets/img/logo.jpg" alt="" style="margin-left: 15%;">

	<?php foreach ($headinfo as $u_key){ ?>
	<h2 style="color: green; padding-left: 30%;    margin-top: -3%;"> <?php echo $u_key->main_title; ?> </h2>

</div>

<hr style="border: 1px solid black">
<hr style="border: 3px solid black">

<p style="margin-left: 20%;font-size: 16px;    margin-bottom: -1%;"><?php echo $u_key->Hno; ?><?php echo $u_key->streetNo; ?><?php echo $u_key->sectorNo; ?><?php echo $u_key->city; ?><?php echo $u_key->telephone; ?><?php echo $u_key->fax; ?></p>
<p style="margin-left: 21%;"><?php echo $u_key->email; ?><?php echo $u_key->website; ?></p>

<?php } ?>

<table style="    border: 2px solid #000;width: 100%;">
	<tbody>
	<?php foreach ($master as $u_key){ ?>
		<tr>
			<td style="text-align: left">MR # :</td>
			<td style="text-align: left"><?= $u_key->mrno; ?></td>
			<td style="text-align: left">Lab. # :</td>
			<td style="text-align: left"><?= $u_key->labno; ?></td>
		</tr>
		<tr>
			<td style="text-align: left">Patient Name :</td>
			<td style="text-align: left"><?= $u_key->patient_name; ?></td>
			<td style="text-align: left">Date :</td>
			<td style="text-align: left"><?= $u_key->report_date; ?></td>
		</tr>
		<tr>
			<td style="text-align: left">Employer Name :</td>
			<td style="text-align: left"><?= $u_key->employer; ?></td>
			<td style="text-align: left">DOB :</td>
			<td style="text-align: left"><?= $u_key->dob; ?></td>
		</tr>
		<tr>
			<td style="text-align: left">Gender :</td>
			<td style="text-align: left"><?= $u_key->gender; ?></td>
			<td style="text-align: left">Age :</td>
			<td style="text-align: left"><?= $u_key->age; ?></td>
		</tr>

	<?php }?>

	</tbody>
</table>

<h2 style="color: blue; margin-left: 32%"><a href="#">EXERCISE STRESS TEST</a></h2>

<h3 style="color: black;"><a href="#">PROCEDURE:</a></h3>

	<p>The patient was exercised by continuous graded treadmill for 15 min 00 Sec of the Bruce
		protocol.</p>
<h3 style="color: black;"><a href="#">SYMPTOMS:</a></h3>
<p>Exercise stopped due to THR achieved. No chest pain. </p>
<h3 style="color: black;"><a href="#">HEART RATE/BLOOD PRESSURE: </a> </h3>
<p>	The heart rate rose from 85 beats/min at rest to a maximum of 196 beats/min at peak
	exercise, which is the predicted maximum heart rate for an individual this age. The blood
	pressure increased from 120/80 mmHg at rest to 170/80 mmHg peak exercise. </p>
<h3 style="color: black;"><a href="#">ELECTROCARDIOGRAM: </a></h3>
	 <p>Resting ECG showed sinus rhythm, there were no ST-T changes observed during exercise
		 and recovery period. </p>
<h3 style="color: black;"><a href="#">ARRHYTHMIAS: </a></h3>
<p> No arrhythmias noted during exercise. </p>

<h3 style="color: black;"> <a href="#">CONCLUSIONS: </a></h3>
<nbsp></nbsp>
<p> Exercise ECG is NEGATIVE for ischemia. </p>



</body>
</html>


<?php
} else {
	header(base_url()); }

?>
