
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                   
                                    <li class="breadcrumb-item active">Update Module</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Update</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            
                <div class="row">
                    <div class="col-lg-8">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Update Data</h4>
                                <?php 
                                foreach($all_module as $show):
                                ?>                
                                <form action="<?php echo base_url('Main/update_mod'); ?>" method="post">
                                     <input type="hidden" name="text_hid" value="<?php echo $show->module_id; ?>">

                                     <div class="form-group">
                                        <label>Select Program<span class="required">*</span></label>
                                        
                                        <div>
                                            
                                            <select class="form-control" id="sel1" name="program_id">
                                                <option>Select</option>
                                          <?php
                                    foreach($all_pro as $sel):
                                    ?>
                                   <option value="<?php echo $sel->program_id; ?>"><?php echo $sel->program_name; ?></option>
                                    <?php
                                        endforeach;
                                    ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Module Name</label>
                                        <input type="text" name="name" class="form-control" value="<?php echo $show->module_name; ?>" />
                                    </div>
                                    

                             

                                     <div class="form-group">
                                        <label>Select Status<span class="required">*</span></label>
                                        <div>
                                            
                                            <select class="form-control" id="sel1" name="sel1">
                                                <option>Select</option>
                                          <option class="form-control" value="1">Enable</option>
                                          <option class="form-control" value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                   
                                    <div class="form-group">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                Update
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <a href="<?php echo base_url('Main/modules'); ?>">
                                <button style="float: right; margin: -52px 0px;" type="reset" class="btn btn-secondary waves-effect m-l-5">
                                                Cancel
                                            </button></a>
<?php endforeach; ?>
                            </div>
                        </div>
                    </div> <!-- end col -->

                    
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
       
<?php $this->load->view('BKO/includes/footer'); ?>
     
