<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Adv_model extends CI_Model{
	public function get_adv()
	{
		$result=$this->db->query('SELECT *
FROM
    `mediacomm_db`.`adsmasterinfo`
    LEFT JOIN `mediacomm_db`.`adscategory`
        ON (`adsmasterinfo`.`CatID` = `adscategory`.`CatID`)
    LEFT JOIN `mediacomm_db`.`agency`
        ON (`adsmasterinfo`.`AgencyID` = `agency`.`AgencyID`)
    LEFT JOIN `mediacomm_db`.`product`
        ON (`adsmasterinfo`.`ProductID` = `product`.`ProductID`)
    LEFT JOIN `mediacomm_db`.`artistprofile`
        ON (`adsmasterinfo`.`ArtistID` = `artistprofile`.`ArtistID`)');

		return $result;

	}
	public function adv_get_paginated_data($start,$end)
	{
		$result=$this->db->query('SELECT *
FROM
    `mediacomm_db`.`adsmasterinfo`
    LEFT JOIN `mediacomm_db`.`adscategory`
        ON (`adsmasterinfo`.`CatID` = `adscategory`.`CatID`)
    LEFT JOIN `mediacomm_db`.`agency`
        ON (`adsmasterinfo`.`AgencyID` = `agency`.`AgencyID`)
    LEFT JOIN `mediacomm_db`.`product`
        ON (`adsmasterinfo`.`ProductID` = `product`.`ProductID`)
    LEFT JOIN `mediacomm_db`.`artistprofile`
        ON (`adsmasterinfo`.`ArtistID` = `artistprofile`.`ArtistID`) limit '.$start.' , '.$end);

		return $result->result_array();

	}

	public function get_adv_brand(){
			$this->db->select('*');
  		$this->db->from('brand');
  		$result=$this->db->get();
  		return $result->result_array();



	}
	public function get_adv_sector()
	{
			$this->db->select('*');
  		$this->db->from('sector');
  		$result=$this->db->get();
  		return $result->result_array();

	}
	public function get_adv_artist()
	{
			$this->db->select('*');
  		$this->db->from('artistprofile');
  		$result=$this->db->get();
  		return $result->result_array();
	}
		public function get_adv_category()
		{
			$this->db->select('*');
  		$this->db->from('adscategory');
  		$result=$this->db->get();
  		return $result->result_array();
	}
	public function get_adv_product(){
			$this->db->select('*');
  		$this->db->from('product');
  		$result=$this->db->get();
  		return $result->result_array();


	}
	public function get_adv_agency(){
			$this->db->select('*');
  		$this->db->from('agency');
  		$result=$this->db->get();
  		return $result->result_array();


	}
	public function get_adv_id($id)
 	{
 		$this->db->select('*');
 		$this->db->from('adsmasterinfo');
 		$this->db->where('AdsID',$id);
 		$result=$this->db->get();
 		return $result->result_array();




 	}
	public function update_adv($name,$dur,$slot,$category,$product,$agency,$date,$artist,$order,$cost,$id,$rating)
 	{
 		$data=array('AdsName'=>$name,'CatID'=>$category,'AdDuration'=>$dur,'AdSlot'=>$slot,'ProductID'=>$product,'AgencyID'=>$agency,'ArtistID'=>$artist,'AdBookingDate'=>$date,'OrderNo'=>$order,'Cost'=>$cost,'AdRating'=>$rating);
 		$this->db->where('AdsID',$id);
 		$result=$this->db->update('adsmasterinfo',$data);
 		return $result;



 	}
	public function insertadv($name,$dur,$brand,$slot,$category,$product,$agency,$date,$artist,$order,$cost,$rating,$file,$filename,$width,$height,$forcc,$playtime,$playstring,$rate,$countframe,$filepath,$format,$comment){
		$this->db->trans_start();
		$data=array('AdsName'=>$name,'CatID'=>$category,'AdDuration'=>$dur,'AdSlot'=>$slot,'brandID'=>$brand,'ProductID'=>$product,'AgencyID'=>$agency,'ArtistID'=>$artist,'AdBookingDate'=>$date,'OrderNo'=>$order,'Cost'=>$cost,'Adrating'=>$rating,'FileName'=>$file,'FilePath'=>$filepath);
		$this->db->insert('adsmasterinfo',$data);
		$insert_id = $this->db->insert_id();
		$data1=array('AdsID'=>$insert_id ,'FileName'=>$filename,'Vresolution_x'=>$width,'brandID'=>$brand,'Vresolution_y'=>$height,'Vduration'=>$dur,'fourcc_lookup'=>$forcc,'Vplaytime_seconds'=>$playtime,'Vplaytime_string'=>$playstring,'Vframe_rate'=>$rate,'Vframe_count'=>$countframe,'FilePath'=>$filepath,'VideoFormat'=>$format,'comments'=>$comment);
		$result=$this->db->insert('videoinfo',$data1);


		$this->db->trans_complete();


		return $result;
	}
	public function delete_adv($cat)
	{
		$this->db->trans_start();
		$this->db->where('AdsID', $cat);
$this->db->delete('adsmasterinfo');
$this->db->where('AdsID',$cat);
$result=$this->db->delete('videoinfo');
$this->db->trans_complete();
return $result ;


	}





		public function detail_adv($id)
	{
		$result=$this->db->query('SELECT *
FROM
    `mediacomm_db`.`adsmasterinfo`
    INNER JOIN `mediacomm_db`.`adscategory`
        ON (`adsmasterinfo`.`CatID` = `adscategory`.`CatID`)
    INNER JOIN `mediacomm_db`.`agency`
        ON (`adsmasterinfo`.`AgencyID` = `agency`.`AgencyID`)
    INNER JOIN `mediacomm_db`.`product`
        ON (`adsmasterinfo`.`ProductID` = `product`.`ProductID`)
    LEFT JOIN `mediacomm_db`.`artistprofile`
        ON (`adsmasterinfo`.`ArtistID` = `artistprofile`.`ArtistID`) where `adsmasterinfo`.`AdsID`='.$id);

		return $result->result_array();




	}






	
	public function insertvideoinfo($filename,$filepath,$fileformat,$resolution_x,$resolution_y,$duration,$playtime_seconds,$playtime_string,$fourcc_lookup,$frame_rate,$frame_count,$comments)
	{
		$this->db->trans_start();
		$data1=array('AdsName'=>$filename,'FileName'=>$filename,'CatID'=>1,'AdDuration'=>$duration,'AdSlot'=>1,'ProductID'=>1,'AgencyID'=>1,'ArtistID'=>1,'AdRating'=>'5','AdBookingDate'=>'2018-11-08','OrderNo'=>1,'Cost'=>'100','FilePath'=>$filepath);
		$this->db->insert('adsmasterinfo',$data1);
		$this->db->select_max('AdsID');
		$max = $this->db->get('adsmasterinfo');
		$id=$max->result_array();
	$id=$id[0]['AdsID'];



		$data=array('AdsID'=>$id,'FileName'=>$filename,'FilePath'=>$filepath,'VideoFormat'=>$fileformat,'Vresolution_x'=>$resolution_x,'Vresolution_y'=>$resolution_y,
	                'Vduration'=> $duration,'Vplaytime_seconds'=>$playtime_seconds,'Vplaytime_string'=>$playtime_string,'fourcc_lookup'=>$fourcc_lookup,'Vframe_rate'=> $frame_rate,'Vframe_count'=>$frame_count,'Comments'=>$comments);
		$this->db->insert('videoinfo',$data);
		$this->db->trans_complete();









	}
	public function get_videos(){
		$this->db->select('*');
		$this->db->from('videoinfo');
		$result=$this->db->get();
		return $result->result_array();


	}
	public function update_directory($dir)
	{
		$data=array('DriveSymbol'=>$dir);
		$result=$this->db->update('drive',$data);
		return $result;


	}
public function get_drive()
{
	$this->db->select('*');
	$this->db->from('drive');
	$result=$this->db->get();
	return $result->result_array();

}
public function get_searhed_Video($video)
{

		$result=$this->db->query('SELECT *
FROM
    `mediacomm_db`.`adsmasterinfo`
    LEFT JOIN `mediacomm_db`.`adscategory`
        ON (`adsmasterinfo`.`CatID` = `adscategory`.`CatID`)
    LEFT JOIN `mediacomm_db`.`agency`
        ON (`adsmasterinfo`.`AgencyID` = `agency`.`AgencyID`)
    LEFT JOIN `mediacomm_db`.`product`
        ON (`adsmasterinfo`.`ProductID` = `product`.`ProductID`)
    LEFT JOIN `mediacomm_db`.`artistprofile`
        ON (`adsmasterinfo`.`ArtistID` = `artistprofile`.`ArtistID`) where FilePath LIKE "%'.$video.'%" OR FileName LIKE "%'.$video.'%"');

		return $result->result_array();
}
public function get_videos_brand($brand)
{
	// $result=$this->db->query('select * from adsmasterinfo where FilePath LIKE "%'.$brand.'%" OR FileName LIKE "%'.$brand.'%"');
	$this->db->select('*');
	$this->db->from('adsmasterinfo');
	$this->db->like('FilePath',$brand,'both');
	$this->db->like('FileName',$brand,'both');
	$result=$this->db->get();
	return $result->result_array();





}
public function get_brand_by_id($brand)
{
	$result=$this->db->select('*')
	->where('brandID',$brand)
	->from('brand')
	->get();
	return $result->result_array();

}

}
?>
