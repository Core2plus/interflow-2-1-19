<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Add New Module
        <small>Add / Edit User</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter User Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body table-responsive ">
                    <?php $this->load->helper("form"); ?>
                    <form role="form" id="addUser" action="<?php echo base_url() ?>insertModel" method="post" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email">Module Name</label>
                                        <input type="text" class="form-control " onchange="validate_module(this.value)" id="email" name="moduleName" maxlength="128">
                                        <p id="uniq_error" class="text-danger" style="display: none;">Please Enter Unique Module Name</p>
                                    </div>
                                      <?php echo form_error('moduleName','<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                                </div>


                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="role">Select Programs*</label>
                                        <select class="form-control" id="sel1" name="program_name">
                                                <option value="">Select</option>
                                          <?php
                                    foreach($all_pro as $sel):
                                    ?>
                                   <option value="<?php echo $sel->program_id; ?>"><?php echo $sel->program_name; ?></option>
                                    <?php
                                        endforeach;
                                    ?>
                                            </select>
                                    </div>
                                    <?php echo form_error('program_name','<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                                </div>    

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="role">Select Status*</label>
                                        <select class="form-control" id="sel1" name="modstatus">
                                                <option value="">Select</option>
                                          <option class="form-control" value="1">Enable</option>
                                          <option class="form-control" value="0">Disable</option>
                                            </select>
                                    </div>
                                    <?php echo form_error('modstatus','<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                                </div>    
                            </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer col-md-12">
                            <div>
                            <input type="submit" class="btn btn-primary" value="Submit" style="width: 10%;"/>
                            <input type="reset" class="btn btn-default" value="Reset" style="width: 10%;" />
                        </div>
                        </div>
                    </form>
                </div>
            </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                      
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
<script >
    function validate_module(v){
        param="module="+v;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     token=this.responseText;
     if(token=='true')
     {
        document.getElementById('uniq_error').style.display="none";






     }
     else if(token=='false')
     {
         document.getElementById('uniq_error').style.display="block";
        



     }
    }
  };
  xhttp.open("POST", "<?php echo base_url().'Validation_controller/validate_module';?>", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(param);


    }
    

</script>