

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Discount Reports
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title"></h3>
                   
                </div><!-- /.box-header -->
                <div class="box-body table-responsive ">


<a href="<?php echo base_url('show_paid_reports_bko_for_print'); ?>">
      <button type="button" class="btn btn-info">Print</i></button></a>
                               
                    <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                         <th style="width: 8%">ID</th>
                                        <th style="width: 8%">ARTT ID</th>
                                        <th style="width: 15%">Student Name</th>
                                   
                                        <th style="width: 8%">Dicount Per</th>
                                       
                                         <th style="width: 18%">Action</th>
                                     
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $no = 1;
                                        foreach($show_discount_reports as $show) { 

                                            ?>
 <tr>                                   <td><?php  echo $no++ ; ?></td>
                                        <td><?php  echo $show->artt_id;    ?></td>
                                        <td><?php  echo $show->fname;    ?></td>
                                        
                                         <td><?php  echo $show->percentage;    ?></td>
                                       
                                        <td>
                                         <a href="<?php echo base_url('view_note')?>?id=<?php echo $show->discount_id; ?>">
                <button class="btn btn-primary" >View</button></a>
                        <?php if($show->approve==0){ ?>
        
                    <button type="button" style="width:25%;" class="btn btn-defaut ">Pending..</i></button></a>

                        <?php } ?>

                <?php if($show->approve==1) { ?>
      
      <button type="button" style="width:25%;" class="btn btn-success ">Approved</i></button></a>
      
      <?php } ?> 

      <?php if($show->approve==2) { ?>
    
      
      <button type="button" style="width:23%;" class="btn btn-danger    ">Denied</i></button></a>
      
          <?php  if($show->admin_note != "" ) { ?>
            <a href="<?php echo base_url('Main/show_deny_message_discount/').$show->discount_id; ?>">
      <button   type="button" class="btn btn-primary "><i class="ti-eye"></i></button></a>
      
      <?php } } ?> 
                                        </td>
                                     
                                    </tr>

                                    <?php
                                }

                                    ?>



                           
                                    </tbody>
                                </table>

                
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>















