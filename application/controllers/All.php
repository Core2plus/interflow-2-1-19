<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

class All extends BaseController {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
         $this->load->model('all_model');
         $this->load->model('main_model');
        $this->isLoggedIn();
    }

	public function index()
	{
		$flag=$this->session->flashdata('flag');
		if($flag=='ok')
		{
			$data['flag']="SuccessFully Updated !";


		}
		elseif($flag=='fail')
		{
			$data['flag']="Something Went Wrong !";


		}
		elseif($flag=='inserted')
		{
			$data['flag']="Successfully Inserted";


		}
		elseif($flag=='failinserted')
		{
			$data['flag']="Insertion Fail";


		}
		elseif($flag=='deleted')
		{
			$data['flag']="Deleted Successfully !";


		}
		elseif($flag=='faildeleted')
		{
			$data['flag']="Deletion Failed";


		}
		$result=$this->all_model->get_artist();

		if(!empty($result))
		{

			$data['category']=$result;
		$this->global['pageTitle'] = 'Media : Communiation';
        $this->loadViews("artist_view", $this->global,$data, NULL);
    }else{
$data['nodata']="ok";

    	$this->global['pageTitle'] = 'Media : Communiation';
        $this->loadViews("artist_view", $this->global, $data, NULL);

    }
}
  public function load_edit_artist()
     {
     	$this->load->library('form_validation');
     	$this->form_validation->set_rules('id','','required');
     	if($this->form_validation->run())
     	{
     		$id=$this->input->post('id');
     		$result=$this->artist_model->get_artist_id($id);
     		if(!empty($result))
     		{
     		$data['cat']=$result;
     		$this->global['pageTitle'] = 'Media : Communiation';
         $this->loadViews("editartist_view", $this->global, $data, NULL);
 }
 else{
 	    $this->global['pageTitle'] = 'Media : Communiation';
         $this->loadViews("editartist_view", $this->global, NULL, NULL);

      }

     	}

     	 else{
     	 	redirect(base_url().'artist');



     	 }

}


    public function update_artist()
     {
     	$this->load->library('form_validation');
     	$this->form_validation->set_rules('cat_id','','required');
     	$this->form_validation->set_rules('name','Name','required');
        $this->form_validation->set_rules('lastname','Last Name','required');
        $this->form_validation->set_rules('gender','Gender','required');
        $this->form_validation->set_rules('age','Age','required');
        $this->form_validation->set_rules('rate','Rate','required');
        $this->form_validation->set_rules('role','Role','required');
        $this->form_validation->set_rules('desc','Description','required');
        if($this->form_validation->run())
        {
            $id=$this->input->post('cat_id');
            $name=$this->input->post('name');
            $lastname=$this->input->post('lastname');
            $gender=$this->input->post('gender');
            $age=$this->input->post('age');
            $rate=$this->input->post('rate');
            $role=$this->input->post('role');
            $desc=$this->input->post('desc');
     		$result=$this->artist_model->update_artist($id,$name,$lastname,$gender,$age,$rate,$role,$desc);
     		if($result)
     		{
     			$this->session->set_flashdata('flag','ok');
     			redirect(base_url().'artist/');

     		}
     		else{

     			$this->session->set_flashdata('flag','fail');
     			redirect(base_url().'artist/');

     		 }


     	}
     	else{
            $this->session->set_flashdata('flag','fail');
     		redirect(base_url().'artist/');

     	   }

     }

     public function addartist()
     {
     	$this->global['pageTitle'] = 'Media : Communiation';
         $this->loadViews("add_artist_view", $this->global, NULL, NULL);


    }
   public function add_artist()
   {
     	$this->form_validation->set_rules('name','Name','required');
     	$this->form_validation->set_rules('lastname','Last Name','required');
        $this->form_validation->set_rules('gender','Gender','required');
        $this->form_validation->set_rules('age','Age','required');
        $this->form_validation->set_rules('rate','Rate','required');
        $this->form_validation->set_rules('role','Role','required');
        $this->form_validation->set_rules('desc','Description','required');
     	if($this->form_validation->run())
     	{
     		$name=$this->input->post('name');
     		$lastname=$this->input->post('lastname');
            $gender=$this->input->post('gender');
            $age=$this->input->post('age');
            $rate=$this->input->post('rate');
            $role=$this->input->post('role');
            $desc=$this->input->post('desc');
    		$result=$this->artist_model->insertartist($name,$lastname,$gender,$age,$rate,$role,$desc);
    		if($result)
    		{
    			$this->session->set_flashdata('flag','inserted');
    			//echo $result;
    			redirect(base_url().'artist/');


    		}
    		else{
    			$this->session->set_flashdata('flag','failinserted');
    			//echo $result;
    			redirect(base_url().'artist/');


    		}



    	}
        else{
            $data['validation']=validation_errors();
            $this->global['pageTitle'] = 'Media : Communiation';
         $this->loadViews("add_artist_view", $this->global, $data, NULL);


        }

    }
    public function delete_artist(){
    	$this->form_validation->set_rules('cat_id','','required');
    	if($this->form_validation->run())
    	{
    		$cat=$this->input->post('cat_id');
    		$result=$this->artist_model->delete_artist($cat);
    		if($result)
    		{
    			$this->session->set_flashdata('flag','deleted');
    			redirect(base_url().'artist/');


    		}
    		else{
    				$this->session->set_flashdata('flag','faildeleted');
    			redirect(base_url().'artist/');



    		}


    	}


    }



	}
