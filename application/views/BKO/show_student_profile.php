
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>


     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="offset-sm-1 col-sm-10">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                   
                                    <li class="breadcrumb-item active">Student Profile</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Student Profile</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            
                <div class="row">
                  <div class="offset-lg-1 col-lg-10">
                      <div class="card">
                          <div class="card-body row">
                              <div class="col-md-3">
                                 <?php  foreach($show_student_profile->result() as 
                                    $show_student_profile) { ?>
                                <img src="<?php echo base_url(); ?>assets/images/users/<?php echo $show_student_profile->image; ?>"   class="img-circle" width="150px" height="150px">
                              </div>
                              <div class="col-md-9">
                               
                                  <div class="row">

                                   
                                    <label class="col-md-3">Student Name: </label>
                                    <p><?php echo $show_student_profile->fname; ?></p> &nbsp;&nbsp;
                                    <p><?php echo $show_student_profile->mname; ?></p>&nbsp;&nbsp;
                                    <p><?php echo $show_student_profile->lname; ?></p>
                                  </div>

                                  <div class="row">
                                    <label class="col-md-3">Father Name: </label>
                                    <p><?php echo $show_student_profile->fathername; ?></p>
                                  </div>

                                  <div class="row">
                                    <label class="col-md-3">Phone Number: </label>
                                    <p><?php echo $show_student_profile->phone; ?></p>
                                  </div>

                                  <div class="row">
                                    <label class="col-md-3">Address: </label>
                                    <p><?php echo $show_student_profile->permanent_address; ?></p>
                                  </div>

                                  <div class="row">
                                    <label class="col-md-3">Program Name: </label>
                                    <p>Program Name</p>
                                  </div>

                                  <div class="row">
                                      <label class="col-md-3">CNIC: </label>
                                      <p><?php echo $show_student_profile->CNIC; ?></p>
                                  </div>

                                  <div class="row">
                                    <label class="col-md-3">Gender: </label>
                                    <p><?php echo $show_student_profile->gender; ?></p>
                                  </div>

                                  <div class="row">
                                    <label class="col-md-3">Email: </label>
                                    <p><?php echo $show_student_profile->email; ?></p>
                                  </div>

                                  <div class="row">
                                    <label class="col-md-3">Date of Joining: </label>
                                    <p><?php echo $show_student_profile->DOJ; ?></p>
                                  </div>

                                 
                                  <?php } ?>
                                  
                            
                              </div>

                              <h3>Fees Details</h3>
                      <table class="table table-hover">
    <thead>
      <tr>
        <th>NO</th>
        <th>Course Name</th>
        <th>Course Code</th>
        <th>Fee</th>
        <th>Discount%</th>
        <th>Discount amount</th>
        <th>Received amount</th>
        <th>Payment</th>
        <th>Freez</th>
      </tr>
    </thead>
    <tbody>
     
       <?php 
       $no=1;
       $a=0;
       $b=0;
       $c=0;
         $f=0;

        foreach($show_fee_details_in_student_profile2 as $show) {
       ?>
      
        <tr>
        <td><?php echo $no++;   ?></td>
        <td> <?php echo $show->coursename;   ?> </td>
        <td><?php echo $show->coursecode;   ?></td>
        <td><?php echo $show->coursefee; $f=$f+$show->coursefee;    ?></td>
        <td><?php echo $show->discount_per;   ?></td>
        <td><?php if($show->discount_per!=null){$c=($show->discount_per)/100;}  echo $d=$show->coursefee*$c; $b=$b+$d;   ?></td>
        <td><?php $e=$show->received_amount+$d; echo $show->received_amount;  $a=$a+$show->received_amount; ?></td>
     
        <td >
        <?php if(($show->Paid == "1") || ($e>=$show->coursefee)) {?> 

          <span>Paid</span>
          <?php }else{ $a=$a+$show->coursefee; ?>
            <span>Unpaid</span>
            <?php }?>


        </td>
        <td>
         <?php if($show->Paid == "1" OR $show->Freez == "1") {
          $b=$b+$show->coursefee;?>
          <a href="<?php echo base_url('Main/show_freeze_from_student_profile?id='.$show->enrollment_id.'&id2='.$show->student_id.'&id3='.$show->module_id.''); ?>">          
          <button class="btn btn-primany">Unfreez</button></td>
        </a>
          <?php } else if(($show->Paid == "1") AND ($show->Freez == "0") OR ($e>=$show->coursefee)){?>

            <a href="<?php echo base_url('Main/insert_freeze_from_student_profile?id='.$show->enrollment_id.'&id2='.$show->student_id.''); ?>">
         <button class="btn btn-primany">Freez</button></a></td>
        

        <?php } else{?>
          <button class="btn btn-primany">Not Allow</button></td>
          <?php 


           }    ?>
      </tr>

      <?php

        }
        
      ?>
            <tr>
        <td></td>
        <td> </td>
        <td></td>
        <td><?php echo $f;    ?></td>
        <td></td>
        <td><?php echo  $b;   ?></td>
        <td><?php echo   $a; $g=$b+$a;?></td>
     
        <td >
    


        </td>
        <td>
      
         </td>
      </tr>

      </tbody>
  </table>
     


    
                          </div>
                      </div>
                  </div> <!-- end col -->
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

