<?php
 
$totalVisitors = 883000;
 if(@$program)
 {
    $i=1;
    foreach($program as $k=>$v)
    {

        $newVsReturningVisitorsDataPoints[]=array("y"=>$v['program_id'],"name"=>$v['program_name'],"id"=>$v['program_id']);


        $i++;


    }


 }


 

 
?>

<script>
window.onload = function () {
 
var totalVisitors = <?php echo $totalVisitors ?>;
var visitorsData = {
    "New vs Returning Visitors": [{
        click: visitorsChartDrilldownHandler,
        cursor: "pointer",
        legendText: "{name}",
        explodeOnClick: false,
        innerRadius: "75%",
        legendMarkerType: "square",
        indexLabel: "{name}",
        name: "New vs Returning Visitors",
        radius: "100%",
         toolTip:{
        enabled: false,       //disable here
        animationEnabled: true //disable here
      },
  
        showInLegend: true,
        startAngle: 90,
        type: "doughnut",
        dataPoints: <?php echo json_encode($newVsReturningVisitorsDataPoints, JSON_NUMERIC_CHECK); ?>
    }


    ]
   

   
};
 
var newVSReturningVisitorsOptions = {
    animationEnabled: true,
     toolTip:{
        enabled: false,       //disable here
        animationEnabled: true //disable here
      },
    theme: "light2",
    title: {
        text: "Programs"
    },
    subtitles: [{
        text: "Click on Any Segment to Drilldown",
        backgroundColor: "#2eacd1",
        fontSize: 16,
        fontColor: "white",
        padding: 5
    }],
    legend: {
        fontFamily: "calibri",
        fontSize: 14,
        itemTextFormatter: function (e) {
            return e.dataPoint.name + ": " + Math.round(e.dataPoint.y / totalVisitors * 100) + "%";  
        }
    },
    data: []
};
 
var visitorsDrilldownedChartOptions = {
    animationEnabled: true,
    theme: "light2",
    axisX: {
        labelFontColor: "#717171",
        lineColor: "#a2a2a2",
        tickColor: "#a2a2a2"
    },
    subtitles: [{
        text: "Click on Any Segment to Drilldown",
    }],
    axisY: {
        gridThickness: 0,
        includeZero: false,
        labelFontColor: "#717171",
        lineColor: "#a2a2a2",
        tickColor: "#a2a2a2",
        lineThickness: 1
    },
    data: []
};
 
var chart = new CanvasJS.Chart("chartContainer", newVSReturningVisitorsOptions);
chart.options.data = visitorsData["New vs Returning Visitors"];
chart.render();
 
function visitorsChartDrilldownHandler(e) {

    chart = new CanvasJS.Chart("chartContainer", visitorsDrilldownedChartOptions);
    var param="id="+e.dataPoint.y;
    var module;
    
         /*   var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var chart = new CanvasJS.Chart("chartContainer", {
    theme: "light2",
    animationEnabled: true,
    title: {
        text: "Modules"
    },
    
    data: [{
        type: "doughnut",
        indexLabel: "{name}",
        showInLegend: true,
        legendText: "{name}",
        click:gettabledata,
        dataPoints: JSON.parse(this.responseText)
    }]
});
        alert(this.responseText);
chart.render();
 

    $("#backButton").toggleClass("invisible");
      
    }
  };

  xhttp.open("POST", "<?php //echo base_url().'Stat/load_data_by_program';?>");
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(param);*/
$.ajax({
        type:"POST",
        cache:false,
        url:"<?php echo base_url().'Stat/load_data_by_program_batch';?>",
        data:param,    // multiple data sent using ajax
        success: function (html) {
            var chart = new CanvasJS.Chart("chartContainer", {
    theme: "light2",
    animationEnabled: true,
    title: {
        text: "Batch Info."
    },
    exportEnabled: true,
    
    data: [{
        type: "pie",
        indexLabel: "{name} {y}",
        showInLegend: true,
        legendText: "{name} {y}",      
        dataPoints: JSON.parse(html),
        click:get_data_course
    }]
});
        
chart.render();
infodiv= document.getElementById("infodiv");
$(infodiv).toggleClass('invisible');
parent=document.getElementById('program');

while (parent.firstChild) {
    parent.removeChild(parent.firstChild);
}  
$(program).attr('class', 'visible');
div=document.createElement("div");
h2=document.createElement("h2");
text=document.createTextNode("Batch Info.");
h2.appendChild(text);
div.appendChild(h2);
    data=JSON.parse(html);
    myarr=[];
    for(d in data)
    {
        if(typeof data[d].namef!='undefined')
        {
        div=document.createElement('div');
        div.setAttribute('class','lefted');
        h2=document.createElement('h3');
        text=document.createTextNode(" "+data[d].namef + " Total Amount : "+ data[d].y +" Paid Amount :"+data[d].rec);
        h2.appendChild(text);  
        div.appendChild(h2);
        parent.appendChild(div);
}


    }
    

  

parent.appendChild(div);



    $("#backButton").toggleClass("invisible");

            

        }
      });

 
}
$("#backButton").click(function() {

    var myNode = document.getElementById("table");
while (myNode.firstChild) {
    myNode.removeChild(myNode.firstChild);
}  
    var infodiv = document.getElementById("infodiv");
    $(infodiv).toggleClass("invisible");
var program = document.getElementById("program");
    $(program).attr('class', 'invisible');


    $(this).toggleClass("invisible");
    chart = new CanvasJS.Chart("chartContainer", newVSReturningVisitorsOptions);
    chart.options.data = visitorsData["New vs Returning Visitors"];
    chart.render();
});
 
}
function get_data_course(e){
    param="id="+e.dataPoint.batchid;
   
     var myNode = document.getElementById("table");
while (myNode.firstChild) {
    myNode.removeChild(myNode.firstChild);
} 
$.ajax({
        type:"POST",
        cache:false,
        url:"<?php echo base_url().'Stat/load_data_courses';?>",
        data:param,    // multiple data sent using ajax
        success: function (html) {
            
            myNode.innerHTML=html;

            
 

}

});
}

</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Programs Info. 
        <small>Chart and data</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">

              <!-- general form elements -->
              <div id="chartContainer" style="height: 370px; width: 100%;"></div>
<button class="btn invisible" id="backButton">&lt; Back</button>

                <div class="box-body table-responsive" style="max-height:500px;">
                    <div id="parent">
                        <div id="program"></div>

                    <?php if(@$program)
                    {
                        ?>
                        <div id="infodiv">
                        

                        <h2 style="text-align:center">Programs</h2>
                        <?php
                        $i=1;
                        foreach($program as $k=>$v)
                        {
                            ?>
     <div id="infodiv" style="float:left">

        <h3 style="padding-left:20px;padding-right:20px;"><?php echo $v['program_name'];?></h3>

    </div>
                        

                            <?php 



                        }


                    }

                    ?>
                </div>
            </div>


        <table class="table table-striped table-bordered" id="example1">
              

                <thead id="table"></thead>

               </table>
               </div> 
  
 

            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>

<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
