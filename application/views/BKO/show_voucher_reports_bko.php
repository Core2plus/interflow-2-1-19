
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                    <li class="breadcrumb-item active">Voucher Reports</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Voucher Reports</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
    <div class="row" style="margin: 0px;">
        <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div>

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">
 <a href="<?php echo base_url('Main/show_voucher_reports_bko_for_print'); ?>">
      <button type="button" class="btn btn-info">Print</i></button></a>
                          

                               
                                <table id="datatable" class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>ARTT ID</th>
                                        <th>Student Name</th>
                                        <th>Total Fee</th>
                                        <th>Remaining Fee</th>
                                        <th>Paying Amount</th>
                                        <th>Voucher Date</th>

                                        <th>Due Date</th>
                                        <th>Paid With</th>
                                        <th>Cheque Number</th>


                                     
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $no = 1;
                                        foreach($show_voucher_reports_bko as $show) { 

                                            ?>
 <tr> <th><?php  echo $no++;?></th>
                                         <th><?php  echo $show->artt_id; ?></th>
                                        <th><?php  echo $show->fname;    ?></th>
                                        <th><?php  echo $show->total_fees;    ?></th>
                                        <th><?php  echo $show->total_fees-$show->pay_amount;    ?></th>
                                        <th><?php  echo $show->pay_amount;    ?></th>
                                        <th><?php  echo $show->current_date;    ?></th>
                                        <th><?php  echo $show->due_date;    ?></th>
                                        <th><?php  echo $show->pay_with;    ?></th>
                                        <th><?php  echo $show->cheque_number;    ?></th>
                                       
                                     
                                    </tr>

                                    <?php
                                }

                                    ?>



                           
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

               

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <?php $this->load->view('BKO/includes/footer'); ?>