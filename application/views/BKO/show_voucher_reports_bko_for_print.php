


<head>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<style type="text/css" media="print">
.dontprint
{ display: none; }
</style>
    <!-- <div class="row" style="width:90% ;">
        <div class="col-sm-12 col-md-6" >
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div> -->


<div style="width :80%; margin: auto">
                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">





                                <div class="dontprint" class=" col col-md-3" style="margin-left:-1.5%; padding-top:1%;" >
       <select  id="datee"   onchange="title_change()" class="form-control form-control-sm font-weight-normal">
          
          <option value="101010">Select Current Date</option>
          <?php
        $no=1;
        foreach ($show_voucher_reports_current_date as $key2 ) {
        
        
      ?>
          <option value="<?php echo $key2->c_date; ?>"><?php echo $key2->c_date; ?></option>
          <?php }?>

        </select>
      </div>

                                <div class="dontprint" class=" col col-md-3" style="margin-left:-1.5%; padding-top:1%;" >
       <select  id="due_datee"   onchange="title_change2()" class="form-control form-control-sm font-weight-normal">
          
          <option value="101010">Select Due Date</option>
          <?php
        $no=1;
        foreach ($show_voucher_reports_due_date as $key3 ) {
        
        
      ?>
          <option value="<?php echo $key3->due_date; ?>"><?php echo $key3->due_date; ?></option>
          <?php }?>

        </select>
      </div>

  
<div style="padding-top: 5%;">
                               <h3>Voucher Details</h3>
                                <table   id="datatable" class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>ARTT ID</th>
                                        <th>Student Name</th>
                                        <th>Total Fee</th>
                                        <th>Remaining Fee</th>
                                        <th>Voucher Date</th>
                                        <th>Due Date</th>
                                        <th>Paid With</th>
                                        <th>Cheque Number</th>


                                     
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $no = 1;
                                        foreach($show_voucher_reports_bko as $show) { 

                                            ?>
 <tr> <th><?php  echo $no++;?></th>
                                         <th><?php  echo $show->artt_id; ?></th>
                                        <th><?php  echo $show->fname;    ?></th>
                                        <th><?php  echo $show->total_fees;    ?></th>
                                        <th><?php  echo $show->remain_fees;    ?></th>
                                        <th><?php  echo $show->pay_amount;    ?></th>
                                        <th><?php  echo $show->current_date;    ?></th>
                                        <th><?php  echo $show->due_date;    ?></th>
                                        <th><?php  echo $show->pay_with;    ?></th>
                                        <th><?php  echo $show->cheque_number;    ?></th>
                                       
                                     
                                    </tr>

                                    <?php
                                }

                                    ?>



                           
                                    </tbody>
                                </table>
                                </div>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

               


</div>

<script>
    function title_change()
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('GET','show_voucher_reports_print_ajax?datee='+document.getElementById('datee').value ,false);
        xmlhttp.send(null);
        
        document.getElementById('datatable').innerHTML=xmlhttp.responseText;
    }
</script>


<script>
    function title_change2()
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('GET','show_voucher_reports_print_due_datee_ajax?due_datee='+document.getElementById('due_datee').value ,false);
        xmlhttp.send(null);
        
        document.getElementById('datatable').innerHTML=xmlhttp.responseText;
    }
</script>


          <!--  </div> end container         </div> -->


         <!-- end wrapper -->


        <!-- Footer -->
        