
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                    <li class="breadcrumb-item active">All Students</li>
                                </ol>
                            </div>
                            <h4 class="page-title">STUDENTS</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
    <div class="row" style="margin: 0px;">
        <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div>

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">

                              <a href="<?php echo base_url('Main/student'); ?>">
      <button type="button" class="btn btn-success"><i class="ti-plus"></i></button></a>

                                <h4 class="mt-0 header-title">Students</h4>
                                
                                <table id="datatable" class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>ARTT ID</th>
                                        <th>CR#</th>
                                        <th>Name</th>
                                        <th  >Email</th>
                                        <th style = "width : 4%;" >Status</th>
                                        <th  >Batch Name</th>
                                        
                                        <th>Phone</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>


                                    <tbody>
                                       
                                   
                                   <?php
                                   
                                   $no=1;
     //                   print_r($students);
       //                 die();


        foreach($students->result() as $student) {
         
        
      ?>
                           <tr>               <td><?php echo $no++; ?></td>
                            <td><?php echo $student->artt_id;   ?> </td>
                            <td><?php echo $student->cr;   ?> </td>
                                        <td>    <textarea style = "border:none;  " bold rows="2" cols="10" name="usrtxt" wrap="hard">
 <?php echo $student->fname;  ?>
</textarea></td>
                                        <td>
                                        <textarea style = "border:none;  " bold rows="2" cols="10" name="usrtxt" wrap="hard">
 <?php echo $student->email;  ?>
</textarea>
<td style = "width : 20px" > <?php
if($student->fstatus=="UnFreeze")
{

  echo "Regular";
}
if($student->fstatus=="Freeze")
{

  echo "Freezed";
}

  ?>
<td > 
<span style = "font-size : 14px; ">


<a href="<?php echo base_url('Main/show_enrolled_student/').$student->studentid; ?>"><?php echo $student->batch_name;  ?>
</span>
</td>
</td>
                                       </td>
                                       
                                     <td>   <textarea style = "border:none;  " bold rows="2" cols="10" name="usrtxt" wrap="hard">
 <?php echo $student->phone;  ?>
</textarea></td>
                                     <td>   
    

      <a href="<?php echo base_url('Main/show_enrolled_student/').$student->studentid; ?>">
      <button type="button" class="btn btn-primary "><i class="ti-archive"></i></button></a>

        <a href="<?php echo base_url('Main/show_student_profile/').$student->studentid; ?>">
      <button type="button" class="btn btn-success "><i class="ti-user"></i></button></a>

         <a href="<?php echo base_url('Main/show_update_student/').$student->studentid; ?>">
      <button type="button" class="btn btn-warning "><i class="ti-pencil-alt"></i></button></a>
      
       <a href="<?php echo base_url('Main/delete_student/').$student->studentid; ?>">
      <button type="button" class="btn btn-danger"><i class="ti-cut"></i></button></a>
            <a href="<?php echo base_url('Main/insert_voucher/').$student->studentid; ?>">
      <button type="button" class="btn btn-info">v</button></a>


      <a href="<?php echo base_url('Main/discount_insert/').$student->studentid; ?>">
      <button type="button" class="btn btn-info">D</button></a>



                                        </td>

                                   </tr>
                                   <?php   } ?>

                              
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

               

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <?php $this->load->view('BKO/includes/footer'); ?>
