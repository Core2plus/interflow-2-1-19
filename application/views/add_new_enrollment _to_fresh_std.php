
      
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Add Course
       
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i></a></li>
        <li><a href="#"></a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        
    <div class="box-body">
        <div class="card">
             <div class="card-body">
                <form action="<?php echo base_url('insert_student_courses_new'); ?>" method="post" enctype="multipart/form-data">  

                    <div class="form-group row col-sm-10 col-sm-10 col-xs-12">
                        <label class="col-sm-3">Select Batch</label>
                       
                            <?php foreach($show_student as $key){ ?>
                                <input type="text" disabled value="<?php echo $key['fname'] ?>">
                              <input id = "student_id"  type="hidden" value="<?php echo $key['studentid']; ?>">
     
                            <?php } ?>
      

                     <div class=" col-md-4">
                                              
                        <select class="form-control" id="batch_id" name="batch_id" onChange="course_change()" required>
                            <option selected disabled value="">Select Batch</option>
                                <?php
                                    foreach($show_batch_for_new_student as $pro):
                                ?>
                                           
                          <option  value="<?php echo $pro->batch_id; ?>"><?php echo $pro->batch_name; ?></option>
                             <?php
                                endforeach;
                            ?>
                    </select>
                    </div>                      
                 </div>
                                         

                                        

     <div class="form-group row col-sm-10 col-sm-10 col-xs-12">
      <div id="course" class="form-group"  style="box-shadow: 3px 4px 7px -3px #bfbfbf; border-bottom: 1px dotted #4d54674a; padding: 10px 0px;">
                    <label class="col-sm-3" >Course Name</label><br>
                             
            </div>
     </div>
    
    <div class="form-group row col-sm-10 col-sm-10 col-xs-12">
        <div  class="col-sm-6">
            <button type="submit" class="btn btn-primary waves-effect waves-light">
            </a>  Submit  </button>
     </div>
    </div>                                      
                                          
  </form>

  </div>
   </div>
                              
</div>
 </div>
</div> <!-- end col -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script>
    function course_change()
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('GET','user/ajax?batch_id='+document.getElementById('batch_id').value + '&student_id='+document.getElementById('student_id').value,false);
        xmlhttp.send(null);
        
        document.getElementById('course').innerHTML=xmlhttp.responseText;
    }
</script>