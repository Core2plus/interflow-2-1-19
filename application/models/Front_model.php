<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Front_model extends CI_Model{
	public function get_brand()
	{
		$this->db->select('*');
		$this->db->from('brand');
		$result=$this->db->get();
		return $result;
	}
public function get_brand_videos($brand){
  $result=$this->db->select('*')
  ->from('videoinfo')
  ->where('brandID',$brand)
  ->get();
  return $result;
}



}
