
<!DOCTYPE html>
          <html>
          <head>
              <meta charset="utf-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <title>AdminLTE 2 | Data Tables</title>
              <!-- Tell the browser to be responsive to screen width -->
              <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
              <!-- Bootstrap 3.3.7 -->
              <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
              <!-- Font Awesome -->
              <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
              <!-- Ionicons -->
              <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
              <!-- DataTables -->
              <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
              <!-- Theme style -->
              <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
              <!-- AdminLTE Skins. Choose a skin from the css/skins
                   folder instead of downloading all of them to reduce the load. -->
              <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

              <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
              <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
              <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
              <![endif]-->

              <!-- Google Font -->
              <link rel="stylesheet"
                    href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
          </head>
          <body class="hold-transition skin-blue sidebar-mini">

          <!-- Content Wrapper. Contains page content -->
          <div class="content-wrapper" >

              <!-- Content Header (Page header) -->
              <!--        <section class="content-header">-->
              <!--            <h1>-->
              <!--                Data Tables-->
              <!--                <small>advanced tables</small>-->
              <!--            </h1>-->
              <!--            <ol class="breadcrumb">-->
              <!--                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>-->
              <!--                <li><a href="#">Tables</a></li>-->
              <!--                <li class="active">Data tables</li>-->
              <!--            </ol>-->
              <!--        </section>-->
              <section class="content-header">
                  <h1>
                      <i class="fa fa-users"></i> <?= $Head; ?>
                      <small>Add, Edit, Delete</small>
                  </h1>
              </section>
              <section class="content">

<div class="col-md-9" style="margin-left: 12%;">
                  <section class="content-header">

                      <h3 style="font-size: 28px;font-weight: 300; text-align: left"><b>COMPANY PROFILE</b></h3>

                      <?php

                      if($this->session->flashdata('error') !== null)
                          echo "<p class=\"text-center bg-success text-success\" style=\"padding: 5px;font-size:14px; font-weight: 600;\" >" . $this->session->flashdata('error') . "</p>";
                      ?>

                      <div class="box-body" style="background-color: white;">
                          <?php $this->load->helper("form"); ?>
                          <form id="setting" action="<?php echo base_url('user/') ?>setting_update" method="post" enctype="multipart/form-data">

                                  <div class="row">
                                      <div class="col-md-4">
                                          <div class="form-group">
                                              <label for="fname">Company Name</label>
                                              <input type="text" class="form-control required" value="" id="cname" name="cname" maxlength="128" required>
                                          </div>

                                      </div>
                                      
                                      <div class="col-md-4">
                                          <div class="form-group">
                                              <label for="info">Company Launch Date</label>
                                              <input type="date" class="form-control required" id="info" name="cdate" required>
                                          </div>
                                      </div>
                                      <div class="col-md-4">
                                          <div class="form-group">
                                              <label for="website">Company Website</label>
                                              <input type="url" class="form-control required" id="info" value="" name="cwebsite" maxlength="128" required>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-4">
                                          <div class="form-group">
                                              <label for="email">Company Email</label>
                                              <input type="email" class="form-control required email" id="cmail" value="" name="cemail" maxlength="128" required>
                                          </div>
                                      </div>
                                      

                                      <div class="col-md-4">
                                          <div class="form-group">
                                              <label for="email">Company Contact</label>
                                              <input type="text" class="form-control required" id="cellno" value="" name="ccell" maxlength="128" required>
                                          </div>
                                      </div>

                                      <div class="col-md-4">
                                      <div class="form-group">
                                          <label for="file">Company Logo</label>
                                          <input type="file" name="clogo" class="form-control-file form-group" required style="width: 99%;">
                                      </div>
                                      </div>
                                      <div class="col-md-12">
                                          <div class="form-group">
                                              <label for="email">Company INFO</label>
                                              <input type="text" class="form-control required" id="info" value="" name="cinfo" maxlength="128" required>
                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                          <div class="form-group">
                                              <label for="email">Company Address</label>
                                              <input type="text" class="form-control required" id="cadd" value="" name="caddress" maxlength="128" required>
                                          </div>
                                      </div>
                                  </div>


                                      <input type="submit"  value="Submit Changes" class="btn btn-primary">
                          </form>

                      </div></div>

                  </section>


                  <!-- Main content -->
                  <section class="content">
                      <div class="row">
                          <div class="col-xs-12">

<!--                              <div class="box">-->
<!--                                  <div class="box-header">-->
<!--                                      <h3 class="box-title">View Changes</h3>-->
<!--                                  </div>-->

                             <?php if(!empty($cdata)):


                               foreach ($cdata as $u_key){ ?>
                                      <a href="#" style="color: Black;">
                                          <h3 style="color: Black; margin-left: 38%">COMPANY PROFILE</h3>
                                      </a>

                                      <p style="    font-size: 18px;    font-family: sans-serif;"><b>Company Name </b>: <?= $u_key->companyName; ?></p>

                                      <p style="    font-size: 18px;    font-family: sans-serif;"><b>Company About </b>: <?= $u_key->aboutCompany ; ?></p>

                                      <p style="    font-size: 18px;    font-family: sans-serif;"><b>Launch Date </b>: <?= $u_key->foundingDate; ?></p>

                                      <p style="    font-size: 18px;    font-family: sans-serif;"><b>Website </b>: <?= $u_key->website; ?></p>

                                      <p style="    font-size: 18px;    font-family: sans-serif;"><b>Address</b>: <?= $u_key->address; ?></p>

                                      <p style="    font-size: 18px;    font-family: sans-serif;"><b>Email</b>: <?= $u_key->email; ?></p>

                                      <br>

                                      <p style="    font-size: 18px;    font-family: sans-serif;    margin-bottom: 26%;"><b>Contact No: </b>: <?= $u_key->contactNo; ?></p>

                                      <a href=" <?= $u_key->logo; ?>" style="    font-size: 18px;    font-family: sans-serif;    margin-bottom: 26%;">View Image</a>

                                  <?php } endif; ?>
<!--                                  <!-- /.box-header -->
<!--                                  <div class="box-body">-->
<!--                                      <table id="example1" class="table table-bordered table-striped">-->
<!--                                          <thead>-->
<!--                                          <tr>-->
<!--                                              <th style="text-align: center">Name</th>-->
<!--                                              <th style="text-align: center">Email</th>-->
<!--                                              <th style="text-align: center">Mobile</th>-->
<!--                                              <th style="text-align: center">Role</th>-->
<!--                                              <th style="text-align: center">Created On</th>-->
<!--                                              <th style="text-align: center">Actions</th>-->
<!--                                              <th style="text-align: center">Role</th>-->
<!--                                              <th style="text-align: center">Created On</th>-->
<!--                                              <th style="text-align: center">Actions</th>-->
<!--                                          </tr>-->
<!--                                          </thead>-->
<!--                                          <tbody>-->
<!--                                          --><?php
//                                          if(!empty($cdata))
//                                          {
//                                              foreach($cdata as $record)
//                                              {
//                                                  ?>
<!--                                                  <tr>-->
<!--                                                      <td style="text-align: center">--><?php //echo $record->companyName ?><!--</td>-->
<!--                                                      <td style="text-align: center">--><?php //echo $record->aboutCompany ?><!--</td>-->
<!--                                                      <td style="text-align: center">--><?php //echo $record->foundingDate ?><!--</td>-->
<!--                                                      <td style="text-align: center">--><?php //echo $record->website ?><!--</td>-->
<!--                                                      <td style="text-align: center">--><?php //echo $record->address ?><!--</td>-->
<!--                                                      <td style="text-align: center">--><?php //echo $record->email ?><!--</td>-->
<!--                                                      <td style="text-align: center">--><?php //echo $record->contactNo ?><!--</td>-->
<!--                                                      <td style="text-align: center">--><?php //echo $record->logo ?><!--</td>-->
<!---->
<!---->
<!--                                                      <td style="text-align: center" class="text-center">-->
<!--                                                          <a class="btn btn-sm btn-danger deleteUser" href="#" data-userid="--><?php //echo $record->userId; ?><!--" title="Delete"><i class="fa fa-trash"></i></a>-->
<!--                                                      </td>-->
<!--                                                  </tr>-->
<!--                                                  --><?php
//                                              }
//                                          }
//                                          ?>
<!--                                          </tbody>-->
<!--                                          <tfoot>-->
<!--                                          <tr>-->
<!--                                              <th style="text-align: center">Name</th>-->
<!--                                              <th style="text-align: center">Email</th>-->
<!--                                              <th style="text-align: center">Mobile</th>-->
<!--                                              <th style="text-align: center">Role</th>-->
<!--                                              <th style="text-align: center">Created On</th>-->
<!--                                              <th style="text-align: center">Actions</th>-->
<!---->
<!--                                          </tr>-->
<!--                                          </tfoot>-->
<!--                                      </table>-->
<!--                                  </div>-->
<!--                                  <!-- /.box-body -->
<!--                              </div>-->
<!--                              <!-- /.box -->
<!--                          </div>-->
<!--                          <!-- /.col -->
<!--                      </div>-->
<!--                      <!-- /.row -->
                  </section>
<!--                  <!-- /.content -->
          </div>
<!--          <!-- /.content-wrapper -->

          <!-- Control Sidebar -->

          <!-- ./wrapper -->

          <!-- jQuery 3 -->
          <script src="../../bower_components/jquery/dist/jquery.min.js"></script>
          <!-- Bootstrap 3.3.7 -->
          <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
          <!-- DataTables -->
          <script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
          <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
          <!-- SlimScroll -->
          <script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
          <!-- FastClick -->
          <script src="../../bower_components/fastclick/lib/fastclick.js"></script>
          <!-- AdminLTE App -->
          <script src="../../dist/js/adminlte.min.js"></script>
          <!-- AdminLTE for demo purposes -->
          <script src="../../dist/js/demo.js"></script>
          <!-- page script -->
          <script>
              $(function () {
                  $('#example1').DataTable()
                  $('#example2').DataTable({
                      'paging'      : true,
                      'lengthChange': false,
                      'searching'   : false,
                      'ordering'    : true,
                      'info'        : true,
                      'autoWidth'   : false
                  })
              })
          </script>
          </body>
          </html>

