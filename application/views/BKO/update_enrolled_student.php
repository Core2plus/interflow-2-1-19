
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="offset-sm-1 col-sm-10">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                   
                                    <li class="breadcrumb-item active">Enroll student</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Enrollment</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Student</h4>                
                                <form action="<?php echo base_url('Main/update_enroll_student'); ?>" method="post">
                                   

                                    <div class="form-group">
                                        <label style = "margin-left: 160px; font-size : 20px;"><span style = " font-size : 20px;">Student Name</label> :<span style = " font-size : 20px;"> Hamza </span>
  
                                        <label style = "margin-left: 20px;  font-size : 20px;"><span style = " font-size : 20px;">Program Name</label> : <span style = " font-size : 20px;">CA  </span> 

                                        <label style = "margin-left: 20px;  font-size : 20px;"><span style = " font-size : 20px;">Batch : </label> : <span style = " font-size : 20px;">AFC MARCH 19 Batch-2</span> 
                                          
                                    </div>
                                    
                                    
                                     
                                        
                                          
                                   
                                    <div class="form-group">
                                        <label>Course Name</label>
                                        
                                            <select class="form-control" id="sel1" name="course_id">
                                                <option>Select</option>
                                          <?php
                                    foreach($show_courses as $sel):
                                    ?>
                                   <option value="<?php echo $sel->course_id; ?>"><?php echo $sel->coursename; ?></option>
                                    <?php
                                        endforeach;
                                    ?>
                                            </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Select Status<span class="required">*</span></label>
                                        <div>
                                            
                                            <select class="form-control" id="sel1" name="status">
                                                <option>Select</option>
                                          <option class="form-control" value="1">Enable</option>
                                          <option class="form-control" value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>




                                     <div class="form-group">
                                        <label>Select Paid Status<span class="required">*</span></label>
                                        <div>
                                            
                                            <select class="form-control" id="sel1" name="Paid">
                                                <option>Select</option>
                                          <option class="form-control" value="1">Paid</option>
                                          <option class="form-control" value="0">UnPaid</option>
                                            </select>
                                        </div>
                                    </div>



                                   
                                    <div class="form-group">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button>
                                        </div>
                                    </div>


                                </form>
<a href="<?php echo base_url('Main/show_stds'); ?>">
                                            <button style="float: right; margin: -52px 0px;" type="reset" class="btn btn-secondary waves-effect m-l-5">
                                                Cancel
                                            </button></a>
                            </div>
                        </div>
                    </div> <!-- end col -->

                    
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
       
<?php $this->load->view('BKO/includes/footer'); ?>
     
