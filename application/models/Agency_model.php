<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Agency_model extends CI_Model{
	public function get_category()
	{
		$this->db->select('*');
		$this->db->from('agency');
		$result=$this->db->get();
		return $result->result_array();
	}
	public function get_agency_img($img)
{
	$this->db->select('*');
	$this->db->from('agency');
	$this->db->where('agency_image',$img);
	$result=$this->db->get();
	return $result->result_array();
}
	public function get_agency_id($id)
 	{
 		$this->db->select('*');
 		$this->db->from('agency');
 		$this->db->where('AgencyID',$id);
 		$result=$this->db->get();
 		return $result->result_array();




 	}
	public function update_agency($id,$name,$des,$img)
 	{
 		$data=array('AgencyName'=>$name,'AgencyDetail'=>$des,'agency_image'=>$img);
 		$this->db->where('AgencyID',$id);
 		$result=$this->db->update('agency',$data);
 		return $result;



 	}
	public function insertagency($name,$des,$img){
		$data=array('AgencyName'=>$name,'AgencyDetail'=>$des,'agency_image'=>$img);
		$result=$this->db->insert('agency',$data);
		return $result;


	}
	public function delete_agency($cat)
	{
		$this->db->where('AgencyID', $cat);
$this->db->delete('agency');


	}
	public function get_agency_data($id)
	{
		$result=$this->db->select('*')->from('agency')->where('AgencyID',$id)->get();
		return $result->result_array();
	}


}
?>
