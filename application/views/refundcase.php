<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><i class="fa fa-users"></i>
            Refund Case
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">


        <div class="row"  >
            <div class="col-sm-offset-1 col-sm-10" style=" background:white" >
                <h3>Refund Details</h3>

                <table class="table table-hover" style=" background: white">
                    <thead>
                    <tr>
                        <th>NO</th>
                        <th>Course Name</th>
                        <th>Course Fee</th>
                        <th>Received amount</th>
                        <th>Refund%</th>
                        <th>Refund Amount</th>
                        <th> Remarks/Reasons  </th>
                        <th>Approval</th>
                        <th>Payment</th>

                    </tr>
                    </thead>


                    <tbody>

                    <?php
                    $no=1;
                    $a=0;
                    $b=0;
                    $c=0;
                    $f=0;
                    $d=0;
                    $total = 0;
                    $i=1;

                    foreach($show_fee_details_in_student_profile2 as $show) {
                        ?>
                        <tr>

                            <form action="<?= base_url('user/std_refund_voucher/');?><?= $show->studentid ?>" method="post">

                            <td><?php echo $no++; ?></td>


                        <td> <?php echo $show->coursename; ?> </td>


                        <td style="text-align: center"><?php echo $show->coursefee; $f = $f + $show->coursefee; ?></td>


                        <td style="text-align: center">

                            <input type="text" id="input1" value="<?php $e = $show->received_amount + $d;
                            echo $show->received_amount;
                            $a = $a + $show->received_amount; ?>" disabled>

                        </td>


                        <td>

                            <select name="per" class="btn btn-default btn-sm" id="select_per<?php echo $i; ?>"
                                    onchange="myFunction(this.value,<?php echo $i; ?>)" required>
                                <option value=""> Select %
                                <option value="10">10
                                <option value="15">15
                                <option value="20">20
                                <option value="25">25
                                <option value="30">30
                            </select>


                        </td>


                        <td>

                            <p class="hidden" id="percentage<?php echo $i; ?>"></p>
                            <input type="text" id="template<?php echo $i; ?>" value="" disabled>
                            <input type="hidden" name="refund_amount" id="template1<?php echo $i; ?>" value="">

                        </td>

                        <td><textarea rows="2" cols="15" name="msg" id="reason" maxlength="50" required> </textarea></td>

                        <td>

                            <?php if ($show->Paid == "2"): ?>

                               <button type="button" class="btn btn-primary btn-sm" disabled>Refund Voucher</button>

                            <?php endif; ?>

                            <?php if (($show->Paid == "1") || ($show->Paid == "0")):?>

                                    <input type="hidden" name="row_id" value="<?= $no++; ?>">

                                    <input type="hidden" name="course_id" value="<?= $show->course_id; ?>">

                                    <input type="submit" onclick="formVal()" value="Refund voucher" class="btn btn-primary btn-sm">

                            <?php endif; ?>

                        </td>


                        <td>

                        <?php if($show->Paid == "2"){ ?>

                            <span class="label label-success large">REFUNDED</span>


                        <?php } else{ ?>

                            <?php if (($show->Paid == "1") || ($e >= $show->coursefee)) { ?>

                                <span class="label label-success large">Paid</span>
                            <?php } else {
                                $a = $a + $show->coursefee; ?>
                                <span class="label label-danger large">Unpaid</span>
                            <?php } ?>


                        <?php } ?>

                        </td>
                    </form>

                        </tr>

                            <?php


                            $i++;

                        }

                    ?>
                    <tr>
                        <td></td>
                        <td> </td>
                        <td></td>
                        <td><b>

<!--                                --><?php //echo $f;    ?>

                            </b></td>
                        <td><b>

<!--                                --><?php //echo $a; $g=$b+$a;?>

                            </b></td>

                        <td></td>
<!--                        <td><b>--><?php //echo $b;   ?><!--</b></td>-->
                        <td></td>
                        <td><b>


<!--                                --><?//= $total ?>

                            </b></td>

                        <!--                        <td><b>--><?php //echo $a; $g=$b+$a;?><!--</b></td>-->

                        <td >



                        </td>
                        <td>

                        </td>
                    </tr>

                    </tbody>


                </table>


            </div>
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>



<script>

    function myFunction(v,i){

        var y = document.getElementById("input1").value;
        document.getElementById("percentage"+i).innerHTML = y - ((y * v) /100);
        var a = document.getElementById("percentage"+i).innerHTML = y - ((y * v) /100);
        document.getElementById('template'+i).value = a;
        document.getElementById('template1'+i).value = a;
    }
</script>





<script>

function formVal() {

    alert('Are you sure you want to refund it?');
}


</script>







<!---->
<!--<script>-->
<!---->
<!--    function formVal(){-->
<!---->
<!--        var x = document.getElementById("reason").value;-->
<!--        document.getElementById("msg").innerHTML = x;-->
<!---->
<!--        var a = document.getElementsById("").value;-->
<!--        document.getElementById('template').innerHTML = a;-->
<!---->
<!--        var b = document.getElementsById("per").value;-->
<!--        document.getElementById('percent').innerHTML = b;-->
<!---->
<!--    }-->
<!--</script>-->
