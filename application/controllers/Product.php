<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

class Product extends BaseController {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
         $this->load->model('main_model');
          $this->load->model('product_model');
				$this->load->model('brand_model');
        $this->isLoggedIn();
    }

	public function index()
	{
		$flag=$this->session->flashdata('flag');
		if($flag=='ok')
		{
			$data['flag']="SuccessFully Updated !";


		}
		elseif($flag=='fail')
		{
			$data['flag']="Something Went Wrong !";


		}
		elseif($flag=='inserted')
		{
			$data['flag']="Successfully Inserted";


		}
		elseif($flag=='failinserted')
		{
			$data['flag']="Insertion Fail";


		}
		elseif($flag=='deleted')
		{
			$data['flag']="Deleted Successfully !";


		}
		elseif($flag=='faildeleted')
		{
			$data['flag']="Deletion Failed";


		}
		$result=$this->product_model->get_product();

		if(!empty($result))
		{


			$data['category']=$result;
		$this->global['pageTitle'] = 'Media : Communiation';
        $this->loadViews("product_view", $this->global,$data, NULL);
    }

    else{
$data['nodata']="ok";

    	$this->global['pageTitle'] = 'Media : Communiation';
        $this->loadViews("product_view", $this->global, $data, NULL);

    }
}
  public function load_edit_product()
     {
     	$this->load->library('form_validation');
     	$this->form_validation->set_rules('id','','required');
     	if($this->form_validation->run())
     	{
     		$id=$this->input->post('id');
     		$result=$this->product_model->get_product_id($id);
            $brand=$this->product_model->get_product_brand();
            $sector=$this->product_model->get_product_sector();

            if(!empty($brand))
            {
                $data['brand']=$brand;


            }

            if(!empty($sector))
            {
                $data['sector']=$sector;

            }

     		if(!empty($result))
     		{
     		$data['cat']=$result;

     		$this->global['pageTitle'] = 'Media : Communiation';
         $this->loadViews("editproduct_view", $this->global, $data, NULL);
 }
 else
 {
 	    $this->global['pageTitle'] = 'Media : Communiation';
         $this->loadViews("editproduct_view", $this->global, NULL, NULL);

      }
     	}

     	 else
         {
     	 	redirect(base_url().'product');
     	 }
}

     //}
    public function update_product()
    {
			$file=@$_FILES['filename'];
      	$this->load->library('form_validation');
      	$this->form_validation->set_rules('cat_id','','required');
     	$this->form_validation->set_rules('name','','required');
     	$this->form_validation->set_rules('des','','required');
        //$this->form_validation->set_rules('sectorname','','required');
        $this->form_validation->set_rules('brandname','','required');

     	if($this->form_validation->run())
     	{
				$img='';
     		$id=$this->input->post('cat_id');
     		$name=$this->input->post('name');
     		$des=$this->input->post('des');
           // $sector=$this->input->post('sectorname');
            $brand=$this->input->post('brandname');
						$prev=$this->product_model->get_product_data($id);
						if(!empty($_FILES['filename']['name']))
            {
              $config['upload_path']          = './images/products/';
                $config['allowed_types']        ='png|jpeg|jpg|gif';
                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('filename'))
                {
                  $error = array('error' => $this->upload->display_errors());
                  $this->session->set_flashdata('flag','fail');
                  redirect(base_url().'product');
                }
                else
                {
                   $data = array('upload' => $this->upload->data());
                   print_r($data);
                   $img=$data['upload']['file_name'];
     		$result=$this->product_model->update_product($id,$name,$des,$brand,$img);
     		if($result)
     		{
     			$this->session->set_flashdata('flag','ok');
     			redirect(base_url().'product');
     		}
     		else{
     			$this->session->set_flashdata('flag','fail');
     			redirect(base_url().'product');
     		}
     	}
}

     			else{
						              $img=$prev[0]['pimage'];
						               $result=$this->product_model->update_product($id,$name,$des,$brand,$img);
						        if($result)
						        {
						         $this->session->set_flashdata('flag','ok');

						         redirect(base_url().'product');

						        }
						        else{

						         $this->session->set_flashdata('flag','fail');
						         redirect(base_url().'product');

						        }

						   }
     }else{
          $this->session->set_flashdata('flag','fail');
     		redirect(base_url().'product');



     	}
     }


     public function add_product()
     {
         $brand=$this->product_model->get_product_brand();
            $data['brand']=$brand;
     	$this->global['pageTitle'] = 'Media : Communiation';
         $this->loadViews("add_product_view", $this->global, $data, NULL);

    }
   public function addproduct(){
		 $file=@$_FILES['filename'];
     	$this->form_validation->set_rules('name','Name','required');
     	$this->form_validation->set_rules('des','Description','required');
        $this->form_validation->set_rules('brand','Brand','required');

     	if($this->form_validation->run())
     	{
     		$name=$this->input->post('name');
     		$des=$this->input->post('des');
            $brand=$this->input->post('brand');
						if(!empty($_FILES['filename']['name']))
					             {
					                $config['upload_path']          = 'images/products/';
					                 $config['allowed_types']        ='png|jpeg|jpg|gif';
					                 $this->load->library('upload', $config);
					                  if ( ! $this->upload->do_upload('filename'))
					                 {
					                  $error = array('error' => $this->upload->display_errors());
					                        $this->session->set_flashdata('flag','fail');
					                  redirect(base_url().'product');
					                 }
					                 else{
					                   $data = array('upload' => $this->upload->data());
					                   $upload=$data['upload']['file_name'];

    		$result=$this->product_model->insertproduct($name,$des,$brand,$upload);
    		if($result)
    		{
    			$this->session->set_flashdata('flag','inserted');
    			echo $result;
    			redirect(base_url().'product');

    		}
    		else{
    			$this->session->set_flashdata('flag','failinserted');
    			echo $result;
    			redirect(base_url().'product');

    		}
    	}
		}
		else
		{
   $this->session->set_flashdata('flag','failinserted');
          echo $result;
          redirect(base_url().'product');
    }
  }
        else{
            $data['validation']=validation_errors();
             $brand=$this->product_model->get_product_brand();
            $data['brand']=$brand;
        $this->global['pageTitle'] = 'Media : Communiation';
         $this->loadViews("add_product_view", $this->global, $data, NULL);
        }

     }

    public function delete_product(){
    	$this->form_validation->set_rules('cat_id','','required');
    	if($this->form_validation->run())
    	{
    		$cat=$this->input->post('cat_id');
    		$result=$this->product_model->delete_product($cat);
    		if($result)
    		{
    			$this->session->set_flashdata('flag','deleted');
    			redirect(base_url().'product/');


    		}
    		else{
    				$this->session->set_flashdata('flag','faildeleted');
    			redirect(base_url().'product/');


    		}


    	}


    }

}
