<style>

#preloader {
  position: fixed;
  left: 0;
  top: 0;
  z-index: 99;
  width: 100%;
  height: 100%;
  overflow: visible;
  background: #ffffff
      url('https://loading.io/spinners/typing/lg.-text-entering-comment-loader.gif')
      no-repeat center center;
}
.content-wrapper{

       background: url('./assets/images/ads/img333.jpg')
       center center !important;
      background-size: cover !important;

}


</style>
<script>
  $(document).ready( function() {
  $('#preloader').delay(5).fadeOut();
});
</script>
  <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>

<div class="content-wrapper">

    <section class="content-header">
      <h1 style="color: #ffffff">
        <i class="fa fa-users"></i> All Artists.
      <small style="color:white;">Add, Edit, Delete</small>
      </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url() ?>artist/add_artist"><i class="fa fa-plus"></i> Add New</a>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box" style="opacity:1; background-color: #e8e7e7; margin-top: 0%;">
                <div id="preloader"></div>

                <div class="box-header">
                    <h3 class="box-title">Artist List</h3>
                </div>

                <div class="box-body table-responsive">
                     <table id="example1" class="table table-hover">
                <thead>
                    <tr>

                                        <th>Artist Id</th>
                                        <th>Name</th>
                                        <th>Last Name</th>
                                        <th>Gender</th>
                                        <th>Age</th>
                                        <th>Rate</th>
                                        <th>Role</th>
                                        <th>Description</th>
                                        <th>Image</th>
                                        <th>Actions</th>




                    </tr>
                </thead>
                <tbody>
                 <?php
                 if(@$category)
                 {
                  $no = 1;
                   foreach ($category as $k=>$v) {

                   ?>

                <tr>

                                        <td><?php echo $v['ArtistID'];?></td>
                                        <td><?php echo $v['Name']; ?></td>
                                         <td><?php echo $v['LastName']; ?></td>
                                         <td><?php echo $v['Gender']; ?></td>
                                         <td><?php echo $v['Age']; ?></td>
                                         <td><?php echo $v['Rate']; ?></td>
                                         <td><?php echo $v['Role']; ?></td>
                                         <td><?php echo $v['Description']; ?></td>
                                  <td><img src="<?php echo base_url('/images/artists/').$v['artist_image']; ?>" width="100px" height="50px"></td>


                        <td class="text-center" style="padding:4px;">
                            <div class="col-xs-2">
                             <form action="<?php echo base_url().'artist/load_edit_artist';?>" method="POST">
                                <input type="hidden" value="<?php echo $v['ArtistID'];?>" name="id"/>
                                <button class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></button>
                            </form>
                        </div>
                        <div class="col-xs-2" >
                             <form action="<?php echo base_url().'artist/delete_artist';?>" method="POST">
                                <input type="hidden" value="<?php echo $v['ArtistID'];?>" name="cat_id"/>
                                <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                            </form>
                        </div>

                        </td>

                </tr>


               <?php }} ?>
                </tbody>

              </table>



                </div>
              </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();
            var link = jQuery(this).get(0).href;
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>
