
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Expence
        
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url('show_insert_expense'); ?>"><i class="fa fa-plus"></i> Add New Expence</a>
                      
                </div>


            
                  
            
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    
                   
                </div><!-- /.box-header -->
                <div class="box-body table-responsive ">



 <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Sub Title</th>
                                     
                                        <th>Amount</th>
                                        <th>Expense Date</th>
                                        <th>Due Date</th>
                                           <th style="width:0.4%;">Details</th>
                                        <th style="width:16%;">Approved/Denied</th>
                                        <th  style="width:3%;">Pay Status</th>
                                    </tr>
                                    </thead>


                                    <tbody>
                                       
                                   
                                   <?php
                                   
                                   $no=1;
     //                   print_r($students);
       //                 die();


        foreach($all_expense->result() as $key) {
         
        
      ?>
                           <tr>               <td><?php echo $no++; ?></td>
                            <td><?php echo $key->expense_title;   ?> </td>
                            <td><?php echo $key->expense_sub_title;   ?> </td>
                                    
                                        <td><?php echo $key->amount;  ?></td>
                                        <td><?php echo $key->today_date;  ?></td>
                                        <td><?php echo $key->due_date;  ?></td>
                                            <td>
      <a href="<?php echo base_url('user/show_expense_details/').$key->expense_id; ?>">
      <button  style="margin-left:10px;" type="button" class="btn btn-primary"> View </button> </a>
    </td>
                                        
                                     <td>   
    

      
<?php if($key->approve_status==0){ ?>
        
      <button type="button" style="width:50%;" class="btn btn-defaut ">Pending..</i></button></a>
<?php } ?>

<?php if($key->approve_status==1) { ?>
      
      <button type="button" style="width:50%;" class="btn btn-success ">Approved</i></button></a>
      
      <?php } ?> 

      <?php if($key->approve_status==2) { ?>
    
      
      <button type="button" style="width:50%;" class="btn btn-danger ">Denied</i></button></a>
      <a href="<?php echo base_url('user/show_deny_message/').$key->expense_id ?> ">
          <?php  if($key->deny_message != "" ) { ?>
      <button   type="button" class="btn btn-primary ">Deny</button></a>
      
      <?php } } ?> 
        

                                        </td>

                                        <td>
                    <?php  if($key->approve_status==1){ 
                           if($key->pay_status==0){

                        ?>

        <a href="<?php echo base_url('Main/pay_expense/').$key->expense_id; ?>">
      <button type="button"   class="btn btn-info">Pay</i></button></a>
<?php   } } ?>
  <?php  if($key->pay_status==1){  ?>


      <button type="button" disabled  class="btn btn-success">Paid</i></button></a>

<?php   }  ?>

       

                                                            </td>

                                   </tr>
                                   <?php   } ?>

                              
                                    </tbody>
                                </table>










                  
                    
                
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>















