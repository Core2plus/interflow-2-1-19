
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                    <li class="breadcrumb-item active">Expence</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Expence</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
    <div class="row" style="margin: 0px;">
        <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div>

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">

                              <a href="<?php echo base_url('Main/show_insert_expense'); ?>">
      <button type="button" class="btn btn-success"><i class="ti-plus"></i></button></a>

                                <h4 class="mt-0 header-title">Expence</h4>
                                
                                <table id="datatable" class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Sub Title</th>
                                     
                                        <th>Amount</th>
                                        <th>Expense Date</th>
                                        <th>Due Date</th>
                                           <th style="width:0.4%;">Details</th>
                                        <th style="width:16%;">Approved/Denied</th>
                                        <th  style="width:3%;">Pay Status</th>
                                    </tr>
                                    </thead>


                                    <tbody>
                                       
                                   
                                   <?php
                                   
                                   $no=1;
     //                   print_r($students);
       //                 die();


        foreach($all_expense->result() as $key) {
         
        
      ?>
                           <tr>               <td><?php echo $no++; ?></td>
                            <td><?php echo $key->expense_title;   ?> </td>
                            <td><?php echo $key->expense_sub_title;   ?> </td>
                                    
                                        <td><?php echo $key->amount;  ?></td>
                                        <td><?php echo $key->today_date;  ?></td>
                                        <td><?php echo $key->due_date;  ?></td>
                                            <td><a href="<?php echo base_url('Main/show_expense_details/').$key->expense_id; ?>">
      <button  style="margin-left:10px;" type="button" class="btn btn-primary "><i class="ti-eye"></i></button></a></td>
                                        
                                     <td>   
    

      
<?php if($key->approve_status==0){ ?>
        
      <button type="button" style="width:50%;" class="btn btn-defaut ">Pending..</i></button></a>
<?php } ?>

<?php if($key->approve_status==1) { ?>
      
      <button type="button" style="width:50%;" class="btn btn-success ">Approved</i></button></a>
      
      <?php } ?> 

      <?php if($key->approve_status==2) { ?>
    
      
      <button type="button" style="width:50%;" class="btn btn-danger    ">Denied</i></button></a>
      <a href="<?php echo base_url('Main/show_deny_message/').$key->expense_id; ?>">
          <?php  if($key->deny_message != "" ) { ?>
      <button   type="button" class="btn btn-primary "><i class="ti-eye"></i></button></a>
      
      <?php } } ?> 
        

                                        </td>

                                        <td>
                    <?php  if($key->approve_status==1){ 
                           if($key->pay_status==0){

                        ?>

        <a href="<?php echo base_url('Main/pay_expense/').$key->expense_id; ?>">
      <button type="button"   class="btn btn-info">Pay</i></button></a>
<?php   } } ?>
  <?php  if($key->pay_status==1){  ?>


      <button type="button" disabled  class="btn btn-success">Paid</i></button></a>

<?php   }  ?>

       

                                                            </td>

                                   </tr>
                                   <?php   } ?>

                              
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

               

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <?php $this->load->view('BKO/includes/footer'); ?>
