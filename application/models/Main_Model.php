<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_select()
	{
		$this->load->database();
		$query = $this->db->query("select * from roll");
		return $query->result();
	}

		public function insert_bkofficer($data)
		{

			$data = array(

				'signup_name' => $data['name'],
				'signup_email'  => $data['email'],
				'signup_password' => $data['password'],
				'position' => $data['position'],
				'roll_id' => 3,
				'status'  => 1
			);
			$query = $this->db->insert('signup',$data);
			return $query;
		}

		public function show_bko()
		{
			$query = $this->db->get('signup');
			return $query->result();
		}

		public function delete_bko($id)
		{
			$this->db->where('signup_id',$id);
			$query = $this->db->delete('signup');
			return $query;
		}

		public function update_bkofficer($data)
		{
			$id = $this->input->post('text_hid');

			$datas = array(

				'signup_name' => $data['name'],
				'signup_email' => $data['email'],
				'signup_password' => $data['password'],
				'position' => $data['position']

				);

			$this->db->where("signup_id",$id);
			$this->db->update('signup',$datas);
			//return $query->result();

			if($this->db->affected_rows()> 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		public function upd_bko($id)
		{
			$this->db->where('signup_id',$id);
			$query = $this->db->get('signup');
			return $query->result();

			if($query->num_rows() > 0)
			{
				return $query->row_array();
			}
			else
			{
				return false;
			}
		}

		public function show_stds()
		{
			$query = $this->db->query('select * from student inner join batch on student.batch_id = batch.batch_id');
			return $query->result();
		}

		public function show_teachers()
		{
			$query = $this->db->get('teacher');
			return $query->result();
		}

		public function show_pro()
		{
			$query = $this->db->get('program');
			return $query->result();
		}

		public function show_bat()
		{
			$query = $this->db->get('batch');
			return $query->result();
		}


		public function show_batch()
		{
			$query = $this->db->query("SELECT  batch.*  , module.* , program.* FROM batch
                                       LEFT JOIN module ON batch.module_id = module.module_id
                                       LEFT JOIN program ON batch.program_id = program.program_id");
			return $query->result();
		}
			public function show_batch_for_new_batch_transfer($id3){

	$query = $this->db->query('SELECT * FROM batch Where module_id="'.$id3.'"');
			return $query->result() ;

		}




		//OSMAN

		public function print_voucher_refund($id,$course_id)
    {

        $query = $this->db->query("SELECT enroll_student.* , student.* , courses.*  FROM enroll_student
						left JOIN student ON enroll_student.`student_id` = student.`studentid`
						left JOIN courses ON enroll_student.`course_id` = courses.`course_id`
						WHERE student.`studentid` = '" . $id . "' and courses.`course_id` = '" . $course_id . "' ");

        return $query;
    }

    public function print_voucher1_refund($id, $pay_amount, $remain_fees)
    {

        $query = $this->db->query("SELECT *  FROM master_vocher
						WHERE student_id = '" . $id . "' AND  pay_amount='" . $pay_amount . "' AND  remain_fees='" . $remain_fees . "'");


        return $query;
    }



    public function show_fee_details_in_student_profile1($id,$cid)
    {

        $query = $this->db->query("SELECT student.* , courses.* , enroll_student.* FROM enroll_student
                        INNER JOIN student ON enroll_student.student_id = student.studentid
                        INNER JOIN courses ON enroll_student.course_id = courses.course_id
                        WHERE enroll_student.student_id = '" . $id . "'
                        AND enroll_student.course_id = '" . $cid . "'

                        ");

        return $query->result();
    }



    public function update_refund_status($data,$id)

{
    $refundtbl = array(


        'student_id' => $id,
        'batch_id' => $data['batch_id'],
        'remarks' => $data['msg'],
        'percentage' => $data['per'],
        'refund_amount' => $data['refund_amount']

    );
    $query = $this->db->insert('refund', $refundtbl);


    $datas = array(

        'Paid' => 2,
        'refund_amount' => $data['refund_amount'],
        'refund_per' => $data['per']

    );


    $this->db->where('student_id',$id);
    $this->db->where('course_id',$data['courseid']);
    $this->db->update('enroll_student',$datas);

}


		//OSMAN






















		public function show_batch_for_new_batch(){

	$query = $this->db->query("SELECT * FROM batch ");
			return $query->result() ;

		}

		public function show_batch_for_update_student($id){

	$query = $this->db->query("SELECT student.* , batch.* FROM student
                              INNER JOIN batch on student.batch_id = batch.batch_id
	                             WHERE studentid = '".$id."' ");
			return $query->result() ;

		}
		public function show_batch_for_new_student(){

	$query = $this->db->query('SELECT * FROM batch ORDER BY batch_id desc');
			return $query->result() ;

		}

		public function show_program_for_update_batch($id){

$query = $this->db->query("SELECT program.* , batch.* FROM program
                              INNER JOIN batch on program.program_id = batch.program_id
	                             WHERE batch_id = '".$id."' ");
			return $query->result();


		}
		public function show_mod_for_update_batch($id){

$query = $this->db->query("SELECT module.* , batch.* FROM module
                              INNER JOIN batch on module.module_id = batch.module_id
	                             WHERE batch_id = '".$id."' ");
			return $query->result() ;


		}


	public function insert_program($data)
	{
			$data = array(
				'program_name' => $data['name'],
				'startdate'  => $data['doj'],
				'enddate' => $data['doe'],
				'status'  => 1
			);
			$query = $this->db->insert('program',$data);
			return $query;
	}

		public function upd_pro($id)
		{
			$this->db->where('program_id',$id);
			$query = $this->db->get('program');
			return $query->result();

			if($query->num_rows() > 0)
			{
				return $query->row_array();
			}
			else
			{
				return false;
			}
		}

		public function update_program($data)
		{
			$id = $this->input->post('text_hid');

			$datas = array(

				'program_name' => $data['name'],
				'startdate'  => $data['doj'],
				'enddate' => $data['doe'],
				'status'  => 1

				);

			$this->db->where("program_id",$id);
			$this->db->update('program',$datas);
			//return $query->result();

			if($this->db->affected_rows()> 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function delete_pro($id)
		{
			$this->db->where('program_id',$id);
			$query = $this->db->delete('program');
			return $query;
		}

		public function insert_batch($data)
		{

			$data = array(
				'program_id' => $data['program_id'],
				'module_id' => $data['module_id'],
				'batch_name' => $data['batch_name'],
				'startdate'  => $data['doj'],
				'enddate' => $data['doe'],
				'status'  => $data['sel1']
			);
			$query = $this->db->insert('batch',$data);
			return $query;
		}

		public function insert_batch1($data)
		{

			$data = array(

				'batch_name' => $data['batch_name'],
				'startdate'  => $data['doj'],
				'enddate' => $data['doe']
				);
			$query = $this->db->insert('batch',$data);
			return $query;
		}

		public function delete_batch($id)
		{
			$this->db->where('batch_id',$id);
			$query = $this->db->delete('batch');
			return $query;
		}

		public function update_batch($id)
		{
			$this->db->where('batch_id',$id);
			$query = $this->db->get('batch');
			return $query->result();

			if($query->num_rows() > 0)
			{
				return $query->row_array();
			}
			else
			{
				return false;
			}
		}

		public function update_bat($data,$id)
		{
			$id = $this->input->post('text_hid');

			$datas = array(

				'program_id' => $data['program_id'],
				'module_id' => $data['module_id'],
				'batch_name' => $data['name'],
				'startdate'  => $data['doj'],
				'enddate' => $data['doe'],
				'status'  => $data['sel1']

				);

			$this->db->where("batch_id",$id);
			$this->db->update('batch',$datas);
			//return $query->result();

			if($this->db->affected_rows()> 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function show_co()
		{
			$query = $this->db->get('courses');
			return $query->result();
		}

		public function show_mod()
		{
			$query = $this->db->get('module');
			return $query->result();
		}

		public function show_modules()
		{

        $query = $this->db->query('SELECT module.* , program.* FROM module INNER JOIN program on module.program_id = program.program_id');
			return $query->result();
		}

		public function insert_module($data)
		{
			$data = array(


				'program_id' => $data['program_name'],

				'module_name' => $data['moduleName'],

				'mod_status' => $data['modstatus']

				);

			$query = $this->db->insert('module',$data);
			return $query;
		}


		public function update_module($id)
		{
			$this->db->where('module_id',$id);
			$query = $this->db->get('module');
			return $query->result();

			if($query->num_rows() > 0)
			{
				return $query->row_array();
			}
			else
			{
				return false;
			}
		}

		public function update_mod($data)
		{
			$id = $this->input->post('text_hid');

			$datas = array(

				'program_id' => $data['program_id'],
				'module_name' => $data['name'],

				'mod_status'  => $data['sel1']

				);

			$this->db->where("module_id",$id);
			$this->db->update('module',$datas);
			//return $query->result();

			if($this->db->affected_rows()> 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		public function delete_module($id)
		{
			$this->db->where('module_id',$id);
			$query = $this->db->delete('module');
			return $query;
		}

		public function login_process($email,$pass)
		{
			$this->db->where('signup_email',$email);
			$this->db->where('signup_password',$pass);

			$query = $this->db->get('signup');

			if($query->num_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function show_courses()
		{
			$query = $this->db->query('select courses.* , program.* , module.*	 from courses LEFT join program on courses.program_id = program.program_id LEFT join module on courses.module_id = module.module_id');
			return $query->result();
		}

		public function insert_course($data)
		{

			$data = array(

				'program_id' => $data['program_id'],
				'module_id' => $data['module_id'],
				'coursename' => $data['name'],
				'coursecode' => $data['code'],
				'coursefee' => $data['fees'],
				'co_duration' => $data['duration'],
				'co_status' => $data['sel1']

				);

			$query = $this->db->insert('courses',$data);
			return $query;
		}


		public function update_course($id)
		{
			$this->db->where('course_id',$id);
			$query = $this->db->get('courses');
			return $query->result();

			if($query->num_rows() > 0)
			{
				return $query->row_array();
			}
			else
			{
				return false;
			}
		}

		public function update_cor($data)
		{
			$id = $this->input->post('text_hid');

			$datas = array(

				'program_id' => $data['program_id'],
				'module_id' => $data['module_id'],
				'coursename' => $data['name'],
				'coursecode' => $data['code'],
				'coursefee' => $data['fees'],

				'co_duration' => $data['duration'],
				'co_status' => $data['sel1']

				);

			$this->db->where("course_id",$id);
			$this->db->update('courses',$datas);
			//return $query->result();

			if($this->db->affected_rows()> 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		public function delete_course($id)
		{
			$this->db->where('course_id',$id);
			$query = $this->db->delete('courses');
			return $query;
		}

		public function retrieve_student_data_for_show_stds($id){

			$query = $this->db->query('SELECT * FROM student WHERE studentid = "'.$id.'" ');
	return $query;
		}


             public function insert_student($data)
		{
			$target_dir = "./assets/images";
        $target_file = $target_dir.time().basename($_FILES["image_student"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $imgName = time().basename($_FILES["image_student"]["name"]);
            move_uploaded_file($_FILES["image_student"]["tmp_name"], $target_file);

			$data = array(
                'artt_id' => $data['artt_id'],
                'cr' => $data['cr'],
				'fname' => $data['fname'],
				'fathername' => $data['father_name'],
				'cnic' => $data['cnic'],
				'fstatus' =>  $data['fstatus'],
				'freeze_note' => $data['freeze_note'],
				'DOJ'  => $data['DOJ'],
				'email' => $data['email'],
				'gender' => $data['optradio'],
				'phone' => $data['number'],
				'permanent_address' => $data['permanent_address'],
					'father_cnic' => $data['father_cnic'],
						'father_email' => $data['father_email'],
							'father_phone' => $data['father_phone'],
								'father_profession' => $data['father_profession'],
				'image' => $imgName
			);



			$query = $this->db->insert('student',$data);







			$this->db->query('UPDATE student SET artt_id = (
SELECT CONCAT("ARTT-",studentid) AS student_id
  ) WHERE studentid = (SELECT MAX(studentid) );
				              ');
			return $query;





		}



public function retrieve_student_id_to_assign_courses(){

	$query = $this->db->query('SELECT MAX(studentid) AS student_id FROM student');

	return $query;
}


public function edit_student($data,$id)
		{
        // $target_dir = "assets/images/users/";
        // $target_file = $target_dir.time().basename($_FILES["image_student"]["name"]);
        // $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // $imgName = time().basename($_FILES["image_student"]["name"]);
        //     move_uploaded_file($_FILES["image_student"]["tmp_name"], $target_file);


          $image_exact_path = $data['image'];
          $img = explode('student/', $image_exact_path);
          $newpath = $img[1];



			$datas = array(
'artt_id' => $data['artt_id'],
                   'cr' => $data['cr'],

				'fname' => $data['fname'],

				'fathername' => $data['father_name'],
				'cnic' => $data['cnic'],
				'DOJ'  => $data['DOJ'],
				'email' => $data['email'],
				'gender' => $data['optradio'],
				'phone' => $data['number'],

				'permanent_address' => $data['permanent_address'],
					'father_cnic' => $data['father_cnic'],
						'father_email' => $data['father_email'],
							'father_phone' => $data['father_phone'],
								'father_profession' => $data['father_profession'],


				'image' => $newpath
			);
             $this->db->where('studentid',$id);
			 $query = $this->db->update('student',$datas);

		}


public function edit_student1($data,$id)
		{
        // $target_dir = "assets/images/users/";
        // $target_file = $target_dir.time().basename($_FILES["image_student"]["name"]);
        // $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // $imgName = time().basename($_FILES["image_student"]["name"]);
        //     move_uploaded_file($_FILES["image_student"]["tmp_name"], $target_file);






			$datas = array(
						'artt_id' => $data['artt_id'],
                   		'cr' => $data['cr'],
						'fname' => $data['fname'],
						'fathername' => $data['father_name'],
						'cnic' => $data['cnic'],
						'DOJ'  => $data['DOJ'],
						'email' => $data['email'],
						'gender' => $data['optradio'],
						'phone' => $data['number'],
						'permanent_address' => $data['permanent_address'],
						'father_cnic' => $data['father_cnic'],
						'father_email' => $data['father_email'],
						'father_phone' => $data['father_phone'],
						'father_profession' => $data['father_profession'],
						'fstatus' => $data['fstatus']

			);
             $this->db->where('studentid',$id);
			 $query = $this->db->update('student',$datas);
			 return $query;

		}


         public function update_student($id){

			$query= $this->db->query("select * from student where studentid='".$id."'");
			return $query;
		}



		  public function retrieve_student_data(){

		  $query= $this->db->query("SELECT
    `student`.`artt_id`
    , `student`.`studentid`
    , `student`.`fname`
    , `student`.`cr`
    , `student`.`email`
    , `student`.`fstatus`
    , `student`.`phone`
    , `batch`.`batch_name`
    , `student`.`batch_id`
FROM
    `artt_db`.`student`
    INNER JOIN `artt_db`.`batch`
        ON (`student`.`batch_id` = `batch`.`batch_id`)
        WHERE `student`.`studentid` IN (SELECT `student_id` FROM enroll_student)" );
			return $query;
		}



			public function insert_teacher($data)
		{
			$target_dir = "assets/images/users/";
        $target_file = $target_dir.time().basename($_FILES["image"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $imgName = time().basename($_FILES["image"]["name"]);
            move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);

			$data = array(

				'first_name' => $data['first_name'],

				'father_name' => $data['father_name'],

				'dob'  =>  $data['dob'],
				'cnic' => $data['cnic'],
				'doj'  => $data['doj'],
				'email' => $data['email'],
				'gender' => $data['optradio'],
				'number' => $data['number'],
				'main_courses' => $data['main_courses'],
				'permanent_address' => $data['permanent_address'],
				'image' => $imgName
			);
			$query = $this->db->insert('teacher',$data);
			return $query;
		}

	     public function retrieve_teacher_data(){

		$query= $this->db->query("select * from teacher");
			return $query;



		}


		public function update_teacher($id)
	{
		$query= $this->db->query("select * from teacher where teacher_id='".$id."'");
			return $query;

	}


         public function edit_teacher($data,$id)
		{
        $target_dir = "assets/images/users/";
        $target_file = $target_dir.time().basename($_FILES["image"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $imgName = time().basename($_FILES["image"]["name"]);
            move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);

			$datas = array(

				'first_name' => $data['first_name'],

				'father_name' => $data['father_name'],

				'dob'  =>  $data['dob'],
				'cnic' => $data['cnic'],
				'doj'  => $data['doj'],
				'email' => $data['email'],
				'gender' => $data['optradio'],
				'number' => $data['number'],
				'main_courses' => $data['main_courses'],
				'permanent_address' => $data['permanent_address'],
				'image' => $imgName
			);
            $this->db->where('teacher_id',$id);
			$query = $this->db->update('teacher',$datas);

		}

		public function show_stds1($id,$pid,$bid,$mid)
		{
			$query = $this->db->query('SELECT
    `course_id`
    , `coursecode`
    , `coursename`,MODULE_ID
FROM
    `artt_db`.`courses`
    WHERE PROGRAM_ID="'.$pid.'"
    AND MODULE_ID="'.$mid.'"');
			return $query->result();
		}




		public function show_all_enrolled_students()
		{

			$query = $this->db->query('SELECT s.studentid ,es.batch_id,es.student_id,es.program_id, s.`fname`,b.`batch_name`,p.`program_name`, m.module_name,es.module_id,es.enrollment_id
					FROM enroll_student es,student s,batch b, program p, module m
					WHERE es.student_id=s.studentid
					AND es.batch_id=b.`batch_id`
					AND es.program_id=p.program_id
					AND es.module_id=m.module_id
					GROUP BY es.batch_id,es.student_id,es.program_id');

			return $query;
				/*$query = $this->db->query('SELECT es.batch_id,es.student_id,es.program_id, s.`fname`,s.fstatus,b.`batch_name`,p.`program_name`, m.module_name, es.enrollment_id
					FROM enroll_student es,student s,batch b, program p, module m
					WHERE es.student_id=s.studentid
					AND es.batch_id=b.`batch_id`
					AND es.program_id=p.program_id
					AND es.module_id=m.module_id
					GROUP BY es.batch_id,es.student_id,es.program_id');*/


        	/*$query = $this->db->query('SELECT student.*, program.*, courses.*, module.*,batch.*, enroll_student.* FROM enroll_student
                	INNER JOIN student ON enroll_student.student_id = student.studentid
                    INNER JOIN program ON enroll_student.program_id = program.program_id
                    inner join module on enroll_student.module_id = module.module_id
                    INNER JOIN courses ON enroll_student.course_id =courses.course_id
                    INNER JOIN batch ON enroll_student.batch_id =batch.batch_id

                    ');
        	/*
                     $query = $this->db->query("
						select student_id,program_id,batch_id from enroll_student group by student_id, program_id, batch_id

						");
        	*/
        		return $query;
		}

		public function show_enrolled_teachers()
		{

        	return $query;

		}

//insert student
    	public function insert_enroll_student($data)
    	{

			$query = $this->db->insert('enroll_student',$data);

			if ($this->db->affected_rows() == '1')
    		{
        		return TRUE;
    		}
    			return FALSE;
    	}

        public function total_fees($data)
        {

    		$data = array(
    			'artt_id'=> $data['artt_id']
				);
			$query = $this->db->insert('total_fees',$data);

    	}
    //end insert student


		public function program_from_module()
		{

 			$query = $this->db->query("SELECT program.*
 	                        FROM program
 	        	INNER JOIN module ON program.program_id = module.program_id");
 				return $query->result();
		}





	    public function insert_deny_message($id,$data)
{
	$datas = array(

		'deny_message' => $data['deny_message']


		);


	$this->db->where('expense_id',$id);
	$this->db->update('expense',$datas);


	}
public function undo_approve($id){

$datas2 = array(
	'pay_status' => 0


		);

	$this->db->where('expense_id',$id);
	$this->db->update('expense',$datas2);
$datas = array(

		'approve_status' => 0


		);

	$this->db->where('expense_id',$id);
	$this->db->update('expense',$datas);

}
public function undo_deny($id){
$datas3 = array(
	'deny_message' => ""


		);


	$this->db->where('expense_id',$id);
	$this->db->update('expense',$datas3);


	$datas2 = array(
	'pay_status' => 0


		);

	$this->db->where('expense_id',$id);
	$this->db->update('expense',$datas2);



$datas = array(

		'approve_status' => 0


		);

	$this->db->where('expense_id',$id);
	$this->db->update('expense',$datas);


}







public function get_category()
	{
		$this->db->select('*');
		$this->db->from('adscategory');
		$result=$this->db->get();
		return $result->result_array();




	}
	public function get_category_id($id)
	{
		$this->db->select('*');
		$this->db->from('adscategory');
		$this->db->where('CatID',$id);
		$result=$this->db->get();
		return $result->result_array();




	}
	public function update_cateogy($id,$name,$des,$img)
	{
		$data=array('CatType'=>$name,'Description'=>$des,'cat_image'=>$img);
		$this->db->where('CatID',$id);
		$result=$this->db->update('adscategory',$data);
		return $result;



	}
	public function insertcategory($name,$des,$img){
		$data=array('CatType'=>$name,'Description'=>$des,'cat_image'=>$img);
		$result=$this->db->insert('adscategory',$data);
		return $result;



	}
	public function delete_cat($cat)
	{
		$this->db->where('CatID', $cat);
$this->db->delete('adscategory');


	}
	public function get_category_data($id)
	{
		$result=$this->db->select('*')->from('adscategory')->where('CatID',$id)->get();
		return $result->result_array();
	}














}
