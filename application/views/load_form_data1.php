<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<div class="content-wrapper bgimage" >
<section class="content-header">
      <h1 style="color:#fff; color: black"><i class="fa fa-users"></i> Select Program And Batch </h1>
    </section>
    <!-- Main content -->
    <section class="content" >

      <div class="row">
       
        <!-- /.col -->
        <div class="col-md-8" style="margin-left: 15%; ">
               <form action="<?php echo base_url().'stat/get_courses_data1'; ?>" method="post" enctype="multipart/form-data" class="form-horizontal"  >
                <div class="form-group">
                  <div class="row">
                    <label for="inputName" class="col-md-2 control-label">Program</label>
                    <div class="col-md-10">
                      <select name="program" class="form-control" onchange="myfunct(this.value)">
                        <option selected disabled value="">Select Program</option>

                        <?php foreach($program as $k=>$v)
                        {
                          
                          ?>
                          <option value="<?php echo $v['program_id'];?>"><?php echo $v['program_name'];?></option>

                          <?php

                        }
                        ?>
                        
                      </select>
                    </div>
                  </div>
                  <br/>
                  <div class="form-group">
                  
                    <label for="inputEmail" class="col-md-2 control-label">Batch</label>
                    <div class="col-sm-10">
              <select id="sel1" class="form-control" name="batch">

              </select>
                    </div>
                  </div>

                  <br>

                  <input type="submit" class="col-md-10 btn btn-danger col-md-offset-2" value="search"/>


                  
                 
                </form>
              </div>
              </div>
              <!-- /.tab-pane -->

    
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      
      <!-- /.row -->

    </section>

  </div>
  <script>
function myfunct(id)
{
    
    param="id="+id;
  
    $.ajax({
        type:"POST",
        cache:false,
        url:"<?php echo base_url().'stat/get_batches';?>",
        data:param,    // multiple data sent using ajax
        success: function (html) {
         
            document.getElementById('sel1').innerHTML=html;

        }
    });
   /* alert(param);

$.ajax({
        type:"POST",
        cache:false,
        url:"<?php //echo base_url().'User/get_batches';?>",
        data:param,    // multiple data sent using ajax
        success: function (html) {
            document.getElementById('sel1').innerHTML=html;

        }
        );*/
 }

</script>

  