
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="offset-sm-1 col-sm-10">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                   
                                    <li class="breadcrumb-item active">Student Registration</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Student Registration</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Course Data Insertion</h4>                
                                <form action="<?php echo base_url('Main/insert_program'); ?>" method="post">
                                   
                                    
                                    <div class="form-group row col-md-12 col-sm-12 col-xs-12">
                                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"> module Name <span class="required" >*</span>
                                        </label>
                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                          <select class="form-control" name="module_id[]"  onchange="course_change()" id="sub_title" >
                                              <option value="">Select an Option</option>
                                          </select>
                                        </div>
                                    </div>
                                    <div id="addmore_entry">
                                      <div id="addagain">
               <!--  <input type="hidden" name="text_hid" value="<?php// echo $module->module_id; ?>
        -->
                                         
                                         <div class="form-group row col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"> Course Name <span class="required" >*</span>
                                            </label>
                                            <div class="col-md-10 col-sm-10 col-xs-12">
                                             <select class="form-control" name="course_id[]" id="course">
                                              <option value="">Select an Option</option>
                                             </select>
                                            </div>
                                         </div>
                                      </div>
                                    </div>
                                    
                                    <div class="form-group row col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group pull-left col-md-6">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button>
                                            <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                                Cancel
                                            </button>
                                        </div>

                                        <div class="col-md-6 pull-right">
                                          <span class="btn-sm btn-danger pull-right" id="addMoreremove" style="margin-left: 5px;">-</span>
                                          <span class="btn-sm btn-primary pull-right"  id="addMore">+</span>
                                        </div>
                                    </div>

                                    

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->

                    
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("#addMore").click(function(){
        $("#addagain").clone().appendTo("#addmore_entry");
        return false;   
    }); 
});

/*
$(document).ready(function(){
    $("#addMoreremove").click(function(){
       
      
        $("#addagain").remove();

        return false;   
      
        
    }); 

   
});
</script>
<script type="text/javascript">
  function title_change()
  {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('GET',"ajax?title="+document.getElementById('title_tb').value,false);
    xmlHttp.send(null);
  document.getElementById('sub_title').innerHTML=xmlHttp.responseText;
  //alert(xmlHttp.responseText);
}
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">
  function course_change()
  {
    alert(document.getElementById('sub_title').value);
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('GET',"ajax1?module="+document.getElementById('sub_title').value,false);
    xmlHttp.send(null);
  document.getElementById('course').innerHTML=xmlHttp.responseText;
  //alert(xmlHttp.responseText);
}
</script>
        <!-- Footer -->
       
<?php $this->load->view('BKO/includes/footer'); ?>
     
