<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product_model extends CI_Model{
	public function get_product()
	{
		$result=$this->db->query('SELECT *
FROM
    `mediacomm_db`.`product`
    INNER JOIN `mediacomm_db`.`brand`
        ON (`product`.`BrandID` = `brand`.`BrandID`)');

		return $result->result_array();

	}
	public function get_product_brand(){
			$this->db->select('*');
  		$this->db->from('brand');
  		$result=$this->db->get();
  		return $result->result_array();

	}
	public function get_product_sector(){
			$this->db->select('*');
  		$this->db->from('sector');
  		$result=$this->db->get();
  		return $result->result_array();
	}

	public function get_product_id($id)
 	{
 		$this->db->select('*');
 		$this->db->from('product');
 		$this->db->where('ProductID',$id);
 		$result=$this->db->get();
 		return $result->result_array();
 	}
	public function get_product_img($img)
 	{
 		$this->db->select('*');
 		$this->db->from('product');
 		$this->db->where('pimage',$img);
 		$result=$this->db->get();
 		return $result->result_array();
 	}

	public function dashboardproduct($name,$img,$des){
	$data=array('ProductName'=>$name,'ProductDetail'=>$des,'pimage'=>$img);
	$result=$this->db->update('product',$data);
	return $result;
}

public function publicdata(){
	$query =  $this->db->get('product');
return $query->result();
}

	public function update_product($id,$name,$des,$brand,$img)
 	{
 		$data=array('ProductName'=>$name,'ProductDetail'=>$des,'BrandID'=>$brand,'pimage'=>$img);
 		$this->db->where('ProductID',$id);
 		$result=$this->db->update('product',$data);
 		return $result;
 	}
	public function insertproduct($name,$des,$brand,$img){
		$data=array('ProductName'=>$name,'ProductDetail'=>$des,'BrandID'=>$brand,'pimage'=>$img);
		$result=$this->db->insert('product',$data);
		return $result;
	}

	public function delete_product($cat)
	{
		$this->db->where('ProductID', $cat);
		$this->db->delete('product');
	}

	public function get_product_data($id)
{
	$result=$this->db->select('*')->from('product')->where('ProductID',$id)->get();
	return $result->result_array();
}



}
?>
