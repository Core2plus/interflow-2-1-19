


<head>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<style type="text/css" media="print">
.dontprint
{ display: none; }
</style>
    <!-- <div class="row" style="width:90% ;">
        <div class="col-sm-12 col-md-6" >
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div> -->


<div style="width :80%; margin: auto">
                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">





                                <div class="dontprint" class=" col col-md-3" style="margin-left:-1.5%; padding-top:1%;" >
       <select  id="datee"   onchange="title_change()" class="form-control form-control-sm font-weight-normal">
          
          <option value="101010">Select Expense Date</option>
          <?php
        $no=1;
        foreach ($all_expense->result()  as $key2 ) {
        
        
      ?>
          <option value="<?php echo $key2->today_date; ?>"><?php echo $key2->today_date; ?></option>
          <?php }?>

        </select>
      </div>

                                <div class="dontprint" class=" col col-md-3" style="margin-left:-1.5%; padding-top:1%;" >
       <select  id="due_datee"   onchange="title_change2()" class="form-control form-control-sm font-weight-normal">
          
          <option value="101010">Select Due Date</option>
          <?php
        $no=1;
        foreach ($all_expense->result() as $key3 ) {
        
        
      ?>
          <option value="<?php echo $key3->due_date; ?>"><?php echo $key3->due_date; ?></option>
          <?php }?>

        </select>
      </div>

  
<div style="padding-top: 5%;">
                               <h3>Expense Details</h3>
                                <table id="datatable" class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Sub Title</th>
                                     
                                        <th>Amount</th>
                                        <th>Expense Date</th>
                                        <th>Due Date</th>
                                           
                                    </tr>
                                    </thead>


                                    <tbody>
                                       
                                   
                                   <?php
                                   
                                   $no=1;
     //                   print_r($students);
       //                 die();


        foreach($all_expense->result() as $key) {
         
        
      ?>
                           <tr>               <td><?php echo $no++; ?></td>
                            <td><?php echo $key->expense_title;   ?> </td>
                            <td><?php echo $key->expense_sub_title;   ?> </td>
                                    
                                        <td><?php echo $key->amount;  ?></td>
                                        <td><?php echo $key->today_date;  ?></td>
                                        <td><?php echo $key->due_date;  ?></td>
                                         

                                   </tr>
                                   <?php   } ?>

                              
                                    </tbody>
                                </table>
                                </div>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

               


</div>

<script>
    function title_change()
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('GET','show_expense_reports_print_ajax?datee='+document.getElementById('datee').value ,false);
        xmlhttp.send(null);
        
        document.getElementById('datatable').innerHTML=xmlhttp.responseText;
    }
</script>


<script>
    function title_change2()
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('GET','show_expense_reports_print_due_date_ajax?due_datee='+document.getElementById('due_datee').value ,false);
        xmlhttp.send(null);
        
        document.getElementById('datatable').innerHTML=xmlhttp.responseText;
    }
</script>


          <!--  </div> end container         </div> -->


         <!-- end wrapper -->


        <!-- Footer -->
        