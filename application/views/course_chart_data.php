<script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
  exportEnabled: true,
  animationEnabled: true,
  title:{
    text: "Courses Fee Data"
  },
   
  axisX: {
    title: "Courses"
  },
  axisY: {
    title: "Courses Actual Amount",
    titleFontColor: "#4F81BC",
    lineColor: "#4F81BC",
    labelFontColor: "#4F81BC",
    tickColor: "#4F81BC"
  },
  axisY2: {
    title: "Received Amount",
    titleFontColor: "#C0504E",
    lineColor: "#C0504E",
    labelFontColor: "#C0504E",
    tickColor: "#C0504E"
  },

  toolTip: {
    shared: true,
  },
  legend: {
    cursor: "pointer",
    itemclick: toggleDataSeries
  },
  data: [{
    type: "column",
    name: "Actual Fee",
    showInLegend: true,      
    yValueFormatString: "#,##0.# Fee",
    indexLabel: "{y}",
         indexLabelPlacement: "outside",  
         indexLabelOrientation: "horizontal",
    dataPoints: <?php echo @$actual;?>
  },
  {
    type: "column",
    name: "Receive Fee",
    axisYType: "secondary",
    showInLegend: true,
    yValueFormatString: "#,##0.# Fee",
    indexLabel: "{y}",
         indexLabelPlacement: "outside",  
         indexLabelOrientation: "horizontal",
    dataPoints:<?php echo @$received;?> 
  },
  {
    type: "column",
    name: "Student",
    showInLegend: true,      
    yValueFormatString: "#,##0.# Student",
    indexLabel: "{y}",
         indexLabelPlacement: "outside",  
         indexLabelOrientation: "horizontal",
    dataPoints: <?php echo @$student;?>
  }]
});
chart.render();

function toggleDataSeries(e) {
  if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
    e.dataSeries.visible = false;
  } else {
    e.dataSeries.visible = true;
  }
  e.chart.render();
}

}
</script>

<div class="content-wrapper bgimage" >
<section class="content-header">
      <h1 style="color:#fff;"><i class="fa fa-users"></i> Add New Student</h1>
    </section>
    <!-- Main content -->
    <section class="content" >

      <div class="row">
       
        <!-- /.col -->
        <div class="col-md-12" >
          <div id="chartContainer" style="height: 300px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
              <!-- /.nav-tabs-custom -->
        </div>
      </div>
        <!-- /.col -->
      
      <!-- /.row -->

    </section>

  </div>

  