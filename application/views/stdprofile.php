  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Student Profile
      </h1>
      
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-sm-8" style="margin-left: 15%;">
           

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile table-responsive">

              <?php  foreach($show_student_profile->result_array() as 
                                    $show_student_profile) { 


                                      ?>


              <ul class="list-group list-group-unbordered col-sm-12" style="float: left;">
                <img class=" img-responsive img-square" style="margin-left: 1%; float: left; height: 150px; width: 150px; " src="<?php echo base_url('/assets/artt_images/student/'.$show_student_profile['image']); ?>" alt="User profile picture">
                     


              <h2 class="profile-username text-center"><?php echo $show_student_profile['fname']; ?> &nbsp;&nbsp;/&nbsp;
                <?php echo $show_student_profile['fathername']; ?>&nbsp;&nbsp;/&nbsp;(<?php echo $show_student_profile['program_name']; ?>)</h2>
                

                <!-- <li class="list-group-item" style="margin-left: 25%;">
                  <b>Father Name:</b> <a class="pull-right"><?php echo $show_student_profile->fathername; ?></a>
                </li> -->

                <li class="list-group-item" style="margin-left: 25%;">
                  <b>Phone Number:</b> <a class="pull-right"><?php echo $show_student_profile['phone']; ?></a>
                </li>

                 <li class="list-group-item" style="margin-left: 25%;">
                  <b>CNIC:</b> <a class="pull-right"><?php echo $show_student_profile['CNIC']; ?></a>
                </li>

                 <li class="list-group-item" style="margin-left: 25%;">
                  <b>Gender:</b> <a class="pull-right"><?php echo $show_student_profile['gender']; ?></a>
                </li>

                 <li class="list-group-item" style="margin-left: 25%;">
                  <b>Email:</b> <a class="pull-right"><?php echo $show_student_profile['email']; ?></a>
                </li>

                 <li class="list-group-item" style="margin-left: 25%;">
                  <b>Date of Joining:</b> <a class="pull-right"> <?php echo $show_student_profile['DOJ']; ?></a>
                </li>
                <li class="list-group-item" style="margin-left: 25%;">
                  <b>Address:</b> <a class="pull-right"><?php echo $show_student_profile['permanent_address']; ?></a>
                </li>

              </ul>

           
 <?php } ?>




            </div>
            <!-- /.box-body -->
          </div>
         
         </div>

        <!-- /.col -->
      </div>

      <div class="row"  >
        <div class="col-sm-12"style=" background:white" >
 <h3>Fees Details</h3>
                      <table class="table table-hover" style=" background: white">
    <thead>
      <tr>
        <th>NO</th>
        <th>Course Name</th>
        <th>Course Code</th>
        <th>Fee</th>
        <th>Discount%</th>
        <th>Discount amount</th>
        <th>Received amount</th>
        <th>Status</th>
        <th>Payment</th>
        <th>Freez</th>
      </tr>
    </thead>
    <tbody>
     
       <?php 
       $no=1;
       $a=0;
       $b=0;
       $c=0;
         $f=0;

        foreach($show_fee_details_in_student_profile2 as $show) {
       ?>
      
        <tr>

        <td><?php echo $no++;   ?></td>
        <td> <?php echo $show->coursename;   ?> </td>
        <td><?php echo $show->coursecode;   ?></td>
        <td><?php echo $show->coursefee; $f=$f+$show->coursefee;    ?></td>
        <td><?php echo $show->discount_per;   ?></td>
        <td><?php if($show->discount_per!=null){$c=($show->discount_per)/100;}  echo $d=$show->coursefee*$c; $b=$b+$d;   ?></td>
        <td><?php $e=$show->received_amount+$d; echo $show->received_amount;  $a=$a+$show->received_amount; ?></td>

        <td><?php echo $show->Freez;   ?></td>
     
        <td>

        <?php 

        if(($show->Paid == "0") || ($e>=$show->coursefee)) {?> 

          <span>Paid</span>
          <?php }else{ $a=$a+$show->coursefee; ?>
            <span>Unpaid</span>
            <?php }

        ?>


        </td>


        <td>
         <?php

          if($show->Paid == 0 AND $show->Freez == "Freez"){
          $b=$b+$show->coursefee;?>
          <a href="<?php echo base_url('show_freeze_from_student_profile') ?>?enrollment_id=<?php echo $show->enrollment_id ?>&student_id=<?php echo $show->student_id ?>&module_id=<?php echo $show->module_id ?>&?course_id=<?php echo $show->course_id ?>&batch_id= <?php echo $show->batch_id?>">          
          <button class="btn btn-primany">Unfreez</button></td>
        </a>
          <?php } else if(($show->Freez == "UnFreez")){ ?>

            <a href="<?php 
            echo base_url('user/insert_freeze_from_student_profile') ?>?id=<?php echo $show->enrollment_id ?>&id2=<?php echo $show->student_id ?> ">


         <button class="btn btn-primany">Freez</button></a></td>
        

        <?php } else{?>
          <button class="btn btn-primany">Not Allow</button></td>
          <?php 


           }    ?>
      </tr>

      <?php

        }
        
      ?>
            <tr>
        <td></td>
        <td> </td>
        <td></td>
        <td><?php echo $f;    ?></td>
        <td></td>
        <td><?php echo $b;   ?></td>
        <td><?php echo $a; $g=$b+$a;?></td>
     
        <td >
    


        </td>
        <td>
      
         </td>
      </tr>

      </tbody>
  </table>

</div>
</div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <?php if(@$error || $error=='1')
  {
    echo '<script>alert("You have Not Selected New Course Try Again!")</script>';

  }
  ?>
