<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

class Category extends BaseController {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
         $this->load->model('Main_Model');

        $this->isLoggedIn();
    }

	public function index()
	{
		$flag=$this->session->flashdata('flag');
		if($flag=='ok')
		{
			$data['flag']="SuccessFully Updated !";


		}
		elseif($flag=='fail')
		{
			$data['flag']="Something Went Wrong !";


		}
		elseif($flag=='inserted')
		{
			$data['flag']="Successfully Inserted";


		}
		elseif($flag=='failinserted')
		{
			$data['flag']="Insertion Fail";


		}
		elseif($flag=='deleted')
		{
			$data['flag']="Deleted Successfully !";


		}
		elseif($flag=='faildeleted')
		{
			$data['flag']="Deletion Failed";


		}
		$result=$this->Main_Model->get_category();

		if(!empty($result))
		{

			$data['category']=$result;
		$this->global['pageTitle'] = 'Media : Communiation';
        $this->loadViews("category_view", $this->global,$data, NULL);
    }else{

$data['nodata']="ok";
    	$this->global['pageTitle'] = 'Media : Communiation';
        $this->loadViews("category_view", $this->global, $data, NULL);

    }
}
    public function load_edit_category()
    {
    	$this->load->library('form_validation');
    	$this->form_validation->set_rules('id','','required');
    	if($this->form_validation->run())
    	{
    		$id=$this->input->post('id');
    		$result=$this->Main_Model->get_category_id($id);
    		if(!empty($result))
    		{
    		$data['cat']=$result;
    		$this->global['pageTitle'] = 'Media : Communiation';
        $this->loadViews("editcategory_view", $this->global, $data, NULL);
}
else{
	    $this->global['pageTitle'] = 'Media : Communiation';
        $this->loadViews("editcategory_view", $this->global, NULL, NULL);

     }

    	}

    	 else{
    	 	redirect(base_url().'category');



    	 }



    }
      public function update_category()
    {
			$file=@$_FILES['filename'];
    	$this->load->library('form_validation');
    	$this->form_validation->set_rules('cat_id','','required');
    	$this->form_validation->set_rules('name','','required');
    	$this->form_validation->set_rules('des','','required');
    	if($this->form_validation->run())
    	{
				$img='';
    		$id=$this->input->post('cat_id');
    		$name=$this->input->post('name');
    		$des=$this->input->post('des');
				$prev=$this->Main_Model->get_category_data($id);
					if(!empty($_FILES['filename']['name']))
						 {
							 $config['upload_path']          = './images/category/';
								 $config['allowed_types']        ='png|jpeg|jpg|PNG';
								 $this->load->library('upload', $config);

								 if ( ! $this->upload->do_upload('filename'))
								 {
									 $error = array('error' => $this->upload->display_errors());
									 //print_r($error);
									 //echo "Uplaod Error";
									 //$this->session->flashdata('f');
									 $this->session->set_flashdata('flag','fail');
									 redirect(base_url().'category');
								 }
								 else
								 {
										$data = array('upload' => $this->upload->data());
										print_r($data);
										$img=$data['upload']['file_name'];
    		$result=$this->Main_Model->update_cateogy($id,$name,$des,$img);
    		if($result)
    		{
    			$this->session->set_flashdata('flag','ok');
    			redirect(base_url().'category');


    		}
    		else{
    			$this->session->set_flashdata('flag','fail');
    			redirect(base_url().'category');


    		}
			}
    	}
			else{
							$img=$prev[0]['cat_image'];
					$result=$this->Main_Model->update_cateogy($id,$name,$des,$img);
			 if($result)
			 {
				$this->session->set_flashdata('flag','ok');

				redirect(base_url().'category');

			 }
    	else{
            $this->session->set_flashdata('flag','fail');
    		redirect(base_url().'category');


}
    	}

    }
	}

    public function addcategory()
    {
    	$this->global['pageTitle'] = 'Media : Communiation';
        $this->loadViews("add_category_view", $this->global, NULL, NULL);
    }

    public function add_category(){
			 $file=@$_FILES['filename'];
    	$this->form_validation->set_rules('name','Name','required');
    	$this->form_validation->set_rules('des','Name','required');
    	if($this->form_validation->run())
    	{
    		$name=$this->input->post('name');
    		$des=$this->input->post('des');
				if(!empty($_FILES['filename']['name']))
					 {
							$config['upload_path']          = './images/category/';
							 $config['allowed_types']        ='png|jpeg|jpg';
							 $this->load->library('upload', $config);
								if ( ! $this->upload->do_upload('filename'))
							 {
								$error = array('error' => $this->upload->display_errors());
											$this->session->set_flashdata('flag','fail');
								redirect(base_url().'category');

							 }

							 else{
								 $data = array('upload' => $this->upload->data());
								 $upload=$data['upload']['file_name'];

    		$result=$this->Main_Model->insertcategory($name,$des,$upload);
    		if($result)
    		{
    			$this->session->set_flashdata('flag','inserted');
    			//echo $result;
    			redirect(base_url().'category');


    		}
    		else{
    			$this->session->set_flashdata('flag','failinserted');
    			//echo $result;
    			redirect(base_url().'category');


    		}


}
    	}
        else{
            $v=validation_errors('<div class="text-danger">','</div>');
            $data['validation']=$v;
                $this->global['pageTitle'] = 'Media : Communiation';
        $this->loadViews("add_category_view", $this->global, $data, NULL);

}
        }

    }
    public function delete_category(){
    	$this->form_validation->set_rules('cat_id','','required');
    	if($this->form_validation->run())
    	{
    		$cat=$this->input->post('cat_id');
    		$result=$this->Main_Model->delete_cat($cat);
    		if($result)
    		{
    			$this->session->set_flashdata('flag','deleted');
    			redirect(base_url().'category');


    		}
    		else{
    				$this->session->set_flashdata('flag','faildeleted');
    			redirect(base_url().'category');



    		}


    	}


    }



	}
