<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';
require APPPATH . '/libraries/pdf.php';

class Reports extends BaseController
{
    function __construct()
    {
        parent::__construct();

//        $this->load->helper('url');
        $this->load->model('reports_model');
        $this->isLoggedIn();
    }


    public function index()
    {

        $this->global['pageTitle'] = 'ARTT : Reports';

        $this->loadViews("reports_view", $this->global, NULL , NULL);

    }


    public function singlestd(){

$this->global['pageTitle'] = 'ARTT : Reports';

          $data['batch'] = $this->reports_model->getbatch();

        $data['student'] = $this->reports_model->getstudentlist();


        $this->loadViews("reportsTab/singlestdPage", $this->global, $data , NULL);


    }



    public function students(){

        $this->global['pageTitle'] = 'ARTT : Reports';

          $data['batch'] = $this->reports_model->getbatch();

        $this->loadViews("reportsTab/studentPage", $this->global, $data , NULL);


//        $data['show_paid_reports'] = $this->reports_model->show_paid_reports();
//        $this->load->view('reportsTab/studentPage');
    }

//     public function singlestd_report(){
//        // var_dump($this->input->post()); 
//        //  die ();

//         $batchname = $this->input->post('batchname');
//         $artt_id = $this->input->post('artt_id');
//         $artt_id_new = explode('-', $artt_id);
//         $id = $artt_id_new[1];



// if ($batchname == 'all') {

//              $data['cinfo'] = $this->reports_model->get_cinfo();
//              $data['result'] = $this->reports_model->std_record_by_id($id);
//              // $data['result'] = $this->reports_model->show_singlestd_record($batchname);
//              $this->load->library('pdf');
//              $data['title'] = 'Single Student Report';
//              $this->pdf->load_view('reports_area/studentfee_rp', $data);

    
// }else{
//       $data['cinfo'] = $this->reports_model->get_cinfo();
//             $data['result'] = $this->reports_model->std_record($artt_id,$batchname);
//              // $data['result'] = $this->reports_model->show_singlestd_record($batchname);
//              $this->load->library('pdf');
//              $data['title'] = 'Single Student Report';
//              $this->pdf->load_view('reports_area/studentfee_rp', $data);
// }

           


//     }
    public function singlestd_report()
    {
        // var_dump($this->input->post());
        //  die ();

        $batchname = $this->input->post('batchname');
        $id_from = $this->input->post('artt_id_from');
        $id_to = $this->input->post('artt_id_to');


        if ($id_from > $id_to) {

            redirect(base_url() . 'reports/singlestd');
        }

//        $artt_id = $this->input->post('artt_id');
//        $artt_id_new = explode('-', $artt_id);
//        $id = $artt_id_new[1];


        elseif ($batchname == 'all') {

            $data['cinfo'] = $this->reports_model->get_cinfo();
            $data['result'] = $this->reports_model->std_record_by_id1($id_from,$id_to);
            // $data['result'] = $this->reports_model->show_singlestd_record($batchname);
            $this->load->library('pdf');
            $data['title'] = 'Single Student Report (All Batch)';
            $this->pdf->load_view('reports_area/studentfee_rp', $data);


        }


//        else {
//            $data['cinfo'] = $this->reports_model->get_cinfo();
//            $data['result'] = $this->reports_model->std_record($id, $batchname);
//            // $data['result'] = $this->reports_model->show_singlestd_record($batchname);
//            $this->load->library('pdf');
//            $data['title'] = 'Single Student Report';
//            $this->pdf->load_view('reports_area/studentfee_rp', $data);
//        }


    }



    public function student_report()
    {

//        $this->global['pageTitle'] = 'ARTT : Reports';

//        $data['batch'] = $this->reports_model->getbatch();

//        $this->loadViews("reportsTab/studentPage", $this->global, $data , NULL);





        $batchname = $this->input->post('batchname');
        $report_type = $this->input->post('report_type');
//        $std_rollno = $this->input->post('roll_no');


            if ($batchname == 'all' and $report_type == 'p') {
             $data['cinfo'] = $this->reports_model->get_cinfo();
             $data['result'] = $this->reports_model->show_all_reports_paid();
             $this->load->library('pdf');
             $data['title'] = 'All Students Paid Report';
             $this->pdf->load_view('reports_area/student_rp', $data);    
                       
            }elseif ($batchname == 'all' and $report_type == 'u' ){
                $data['cinfo'] = $this->reports_model->get_cinfo();
                $data['result'] = $this->reports_model->show_all_reports_unpaid();
                $this->load->library('pdf');
                $data['title'] = 'All Students UnPaid Report';
                $this->pdf->load_view('reports_area/student_rp', $data);
            }elseif ($batchname == 'all' and $report_type == 'f' ){
                $data['cinfo'] = $this->reports_model->get_cinfo();
                $data['result'] = $this->reports_model->show_all_reports_freeze();
                $this->load->library('pdf');
                $data['title'] = 'All Students Freeze Report';
                $this->pdf->load_view('reports_area/student_rp', $data);
            }elseif ($batchname == 'all' and $report_type == 'uf' ){
                $data['cinfo'] = $this->reports_model->get_cinfo();
                $data['result'] = $this->reports_model->show_all_reports_unfreeze();
                $this->load->library('pdf');
                $data['title'] = 'All Students UnFreeze Report';
                $this->pdf->load_view('reports_area/student_rp', $data);
            }elseif ($report_type == 'p' ){
                $data['cinfo'] = $this->reports_model->get_cinfo();
                $data['result'] = $this->reports_model->show_paid_reports($batchname);
                $this->load->library('pdf');
                $data['title'] = 'Paid Students Report';
                $this->pdf->load_view('reports_area/student_rp', $data);
            }elseif ($report_type == 'u' ){
                $data['cinfo'] = $this->reports_model->get_cinfo();
                $data['result'] = $this->reports_model->show_unpaid_reports($batchname);
                $this->load->library('pdf');
                $data['title'] = 'UnPaid Students Report';
                $this->pdf->load_view('reports_area/student_rp', $data);
            }elseif ($report_type == 'f' ){
                $data['cinfo'] = $this->reports_model->get_cinfo();
                $data['result'] = $this->reports_model->show_freeze_reports($batchname);
                $this->load->library('pdf');
                $data['title'] = 'Freeze Students Report';
                $this->pdf->load_view('reports_area/student_rp', $data);
            }elseif ($report_type == 'uf' ){
                $data['cinfo'] = $this->reports_model->get_cinfo();
                $data['result'] = $this->reports_model->show_unfreeze_reports($batchname);
                $this->load->library('pdf');
                $data['title'] = 'UnFreeze Students Report';
                $this->pdf->load_view('reports_area/student_rp', $data);
            }


             


        // if ($report_type == 'p' && $batchname!='all') 
        // {
        //     $data['cinfo'] = $this->reports_model->get_cinfo();
        //     $data['result'] = $this->reports_model->show_paid_reports($batchname);
        //     //print_r($data['result']);
        //     $this->load->library('pdf');
        //     $data['title'] = 'Paid Report';
        //     $this->pdf->load_view('reports_area/student_rp', $data);
        // } elseif ($report_type == 'u' && $batchname!='all') {
        //     $data['cinfo'] = $this->reports_model->get_cinfo();
        //     $data['result'] = $this->reports_model->show_unpaid_reports($batchname);
        //     $this->load->library('pdf');
        //     $data['title'] = 'Unpaid Report';
        //     $this->pdf->load_view('reports_area/student_rp', $data);
        // } elseif ($report_type == 'f' && $batchname!='all') {

        //     $data['cinfo'] = $this->reports_model->get_cinfo();
        //     $data['result'] = $this->reports_model->show_freeze_reports($batchname);
        //     $this->load->library('pdf');
        //     $data['title'] = 'Freeze Report';
        //     $this->pdf->load_view('reports_area/student_rp', $data);

//        $data['show_paid_reports'] = $this->reports_model->show_paid_reports();
//        $this->load->view('reportsTab/studentPage');
        // }elseif ($batchname == 'all' && $report_type =! 'p') {
        //     $data['cinfo'] = $this->reports_model->get_cinfo();
        //     $data['result'] = $this->reports_model->show_all_reports();
        //      $this->load->library('pdf');
        //      $data['title'] = 'Single Student Report';
        //      $this->pdf->load_view('reports_area/student_rp', $data);
        // }
        // elseif ($batchname == 'all' && $report_type =! 'u') {
        //     $data['cinfo'] = $this->reports_model->get_cinfo();
        //     $data['result'] = $this->reports_model->show_unpaid_reports();
        //      $this->load->library('pdf');
        //      $data['title'] = 'Single Student Report';
        //      $this->pdf->load_view('reports_area/student_rp', $data);
        // }
        // elseif ($batchname == 'all' && $report_type =! 'f') {
        //     $data['cinfo'] = $this->reports_model->get_cinfo();
        //     $data['result'] = $this->reports_model->show_freeze_reports();
        //      $this->load->library('pdf');
        //      $data['title'] = 'Single Student Report';
        //      $this->pdf->load_view('reports_area/student_rp', $data);
        // }



    }

    public function select()
    {
        $report_type = $this->input->post('report_type');
        $report_date = $this->input->post('date_value');

        if ($report_type == 'students') {

            $data['cinfo'] = $this->Reports_model->get_cinfo();
            $data['master'] = $this->Reports_model->get_report_master_data($report_date);
            $this->load->library('pdf');
            $this->pdf->load_view('reports_area/student_rp', $data);

        } elseif ($report_type == 'programs') {


            $data['cinfo'] = $this->Reports_model->get_cinfo();
            $data['master'] = $this->Reports_model->get_report_master_data($report_date);
            $this->load->library('pdf');
            $this->pdf->load_view('reports_area/program_rp', $data);

        } elseif ($report_type == 'modules') {
            $data['cinfo'] = $this->reports_model->get_cinfo();
            $data['master'] = $this->reports_model->get_report_master_data();
            $this->load->library('pdf');
            $this->pdf->load_view('reports_area/module_rp', $data);
        }

        elseif($report_type == 'courses'){

            $data['cinfo'] = $this->Reports_model->get_cinfo();
            $data['master'] = $this->Reports_model->get_report_master_data($report_date);
            $this->load->library('pdf');
            $this->pdf->load_view('reports_area/course_rp', $data);

        }    
    }


public function crs_std()
    {
        $this->global['pageTitle'] = 'ARTT : Reports';

        $data['batch'] = $this->reports_model->fetch_batch();

        $this->loadViews("reportsTab/crs_std", $this->global, $data, NULL);

    }



public function allReports()
    {
        $this->global['pageTitle'] = 'ARTT : Reports';

        $data['batch'] = $this->reports_model->fetch_batch();

        $data['batch1'] = $this->reports_model->getbatch();


        $this->loadViews("reportsTab/allReports", $this->global, $data, NULL);

    }


function fetch_course()
    {
//        echo $this->input->post('batch_id');
//        if($this->input->post('batch'))
//        {
        $o=$this->reports_model->fetch_courses($this->input->post('batch_id'));
        $output='<option value="">Select Course</option>';
        $output='<option value="all">All Courses</option>';


        foreach($o->result() as $row)
        {
            $output .= '<option value="'.$row->course_id.'">'.$row->coursename.'</option>';
        }
        print_r($output);
//    }
    }


public function course_based_report()
    {
        $batch = $this->input->post('batch');
        $course = $this->input->post('course');


        if($course == 'all')
        {
            $data['cinfo'] = $this->reports_model->get_cinfo();
            $data['result'] = $this->reports_model->show_course_based_reports_all_courses($batch);
            $this->load->library('pdf');
            $data['title'] = 'Course Based Report';
            $this->pdf->load_view('reports_area/course_rp', $data);

        }

        else
            {
                $data['cinfo'] = $this->reports_model->get_cinfo();
                $data['result'] = $this->reports_model->show_course_based_reports($batch,$course);
                $this->load->library('pdf');
                $data['title'] = 'Course Based Report';
                $this->pdf->load_view('reports_area/course_rp', $data);


            }
    }









}

