<style>
.disabledTab{
  display: none;
}
</style>
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="offset-sm-1 col-sm-10">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                   
                                    <li class="breadcrumb-item active">Assignn Courses</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Assign Courses</h4>
                        </div>
               
                
                <!-- end page title end breadcrumb -->

            
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="card">
                            <div class="card-body">
    <form action="<?php echo base_url('Main/insert_student_courses'); ?>" method="post" enctype="multipart/form-data">  

     

  

<!-- <?php foreach($retrieve_student_id->result() as $key){ ?>
     
   
       <input type="text" value="<?php echo $key->student_id; ?>">

     <?php } ?> -->
                   

<br>
    <div class="form-group row col-sm-10 col-sm-10 col-xs-12">
                                       
                       <label class="col-sm-3">Select Batch</label>

                       <?php foreach($retrieve_student_id->result() as $key){ ?>
     
   
       <input id = "student_id" type="hidden" value="<?php echo $key->student_id; ?>">

     <?php } ?>
                                            <div class=" col-md-4">
                                              
                                                   <select class="form-control" id="batch_id" name="batch_id" onChange="course_change()">
                                                   <option>Select Batch</option>
                                               <?php
                                            foreach($show_batch_for_new_student as $pro):
                                            ?>
                                               
                                           
                    
                                          <option  value="<?php echo $pro->batch_id; ?>"><?php echo $pro->batch_name; ?></option>
                                                
                                                <?php
                                                    endforeach;
                                                ?>

                                          </select>
                                            </div>

                                        </div>

                                         <div class="form-group row col-sm-10 col-sm-10 col-xs-12">
                                            <div id = "course" class="form-group"  style="box-shadow: 3px 4px 7px -3px #bfbfbf;
    border-bottom: 1px dotted #4d54674a;
    padding: 10px 0px;">
                                        <label>Course Name</label><br>
                              <!--      <center>
                                    <?php
                                        foreach($show_courses as $sel):
                                    ?>
                                        <label><input class="form-control" name="course_id" type="checkbox" value="<?php echo $sel->course_id; ?>">&nbsp;<?php echo $sel->coursename; ?></label>&nbsp;&nbsp;&nbsp;
                                    <?php
                                        endforeach;
                                    ?>
                                </center> -->
                                        
                             
                                    </div>
                                        </div>
    <div class="form-group row col-sm-10 col-sm-10 col-xs-12">
                                              <div  class="col-sm-6">
                  
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            </a>
                                                Submit
                                            </button>
                                          </div>
                                        </div>
  </form>

  </div>



                                </div>
                              

                            </div>
                        </div>
                    </div> <!-- end col -->

                    
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
       
<?php $this->load->view('BKO/includes/footer'); ?>
<script>
    function course_change()
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('GET','ajax_student_courses?batch_id='+document.getElementById('batch_id').value + '&student_id='+document.getElementById('student_id').value,false);
        xmlhttp.send(null);
        
        document.getElementById('course').innerHTML=xmlhttp.responseText;
    }
</script>
     
