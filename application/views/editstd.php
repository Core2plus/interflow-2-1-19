<div class="content-wrapper">
<section class="content-header">
      <h1><i class="fa fa-users"></i> Edit Student Detail</h1>
    </section>
    <!-- Main content -->
    <section class="content">

      <div class="row">
       
        <!-- /.col -->
        <div class="col-md-8" style="margin-left: 15%; margin-top: 6%;">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#activity" data-toggle="tab"> <b> Student Details </b> </a></li>
              <li><a href="#timeline" data-toggle="tab"> <b> Guardian </b> </a></li>
              
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
               
              <div class="tab-pane" id="settings">
                 <?php foreach($std_data->result_array() as $key) {?>

                <form action="<?php echo base_url('updatestd')?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                  

                  <div class="row">
                    <label for="inputName" class="col-sm-2 control-label"  >ARTT ID</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control" name="artt_id" id="inputName" value="<?php echo $key['artt_id']; ?>"placeholder="Name">
                      <input type="hidden" name="id" value="<?php echo $key['studentid']; ?>">
                    </div>
                  

                    <label for="inputEmail" class="col-sm-2 control-label">CR#</label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control" name="cr" id="inputEmail" value="<?php echo $key['cr']; ?>" placeholder="Email">
                    </div>
                  </div><br>

                  <div class="row">
                    <label for="inputName" class="col-sm-2 control-label">Student Name</label>

                    <div class="col-sm-4">
                      <input type="text" name="fname" value="<?php echo $key['fname']; ?>" class="form-control" id="inputName" placeholder="Name">
                    </div>
                  

                  
                    <label for="inputExperience" class="col-sm-2 control-label">CNIC</label>

                    <div class="col-sm-4">
                      <input  class="form-control" name="cnic" value="<?php echo $key['CNIC']; ?>" id="inputExperience" placeholder="Experience">
                    </div>
                  </div><br>

                  <div class="row">
                    <label for="inputSkills" class="col-sm-2 control-label">Date of Joining</label>

                    <div class="col-sm-4">
                      <input type="date" name="DOJ"
value="<?php echo $key['DOJ']; ?>" class="form-control" id="inputSkills" placeholder="Skills">
                    </div>
                  

                    <label for="inputSkills" class="col-sm-2 control-label"> Email </label>
                    <div class="col-sm-4">
                      <input type="email" name="email"
value="<?php echo $key['email']; ?>" class="form-control" id="inputSkills" placeholder="Skills">
                    </div>
                  </div><br>

                   <div class="row">
                    <label for="inputSkills" class="col-sm-2 control-label">Gender</label>

                   <div class="col-sm-4">
                                                <label class="radio-inline">&nbsp;
                                                      <input type="radio" name="optradio" value="Male" <?php  if($key['gender']=="Male"){?> checked <?php } ?> >Male
                                                    </label>&nbsp;&nbsp;
                                                    <label class="radio-inline">&nbsp;
                                                      <input type="radio" name="optradio" value="Female" <?php  if($key['gender']=="Female"){?> checked <?php } ?>>Female
                                                    </label>&nbsp;&nbsp;
    
                                            </div>
                                            <label for="inputSkills" class="col-sm-2 control-label"> Number</label>
                    <div class="col-sm-4">
                      <input type="text" name="number" value="<?php echo $key['phone']; ?>" class="form-control" class="form-control" id="inputSkills" placeholder="Skills">
                    </div>
                  </div><br>





<div class="row">
                                            <label for="inputSkills" class="col-sm-2 control-label">Profile Picture</label>
                    <div class="col-sm-4">
                      <input type="file" name="image_student" class="form-control-file form-group" style="width: 100%; margin-left: 1%; margin-top: 2%;">
                    </div>
                    

                    </div>
                  <br>

                   <div class="row">
                    <label class="col-sm-2 control-label"><span style = " font-size: 14.8px;">Student Status</span></label>

                    <div class="col-sm-4">
                         <span>UnFreeze &nbsp;

  <input type="radio" <?php if($key['fstatus']=="UnFreeze"){ echo "checked";}?> name="fstatus"  required value = "UnFreeze" onclick="myFunction2()" id="payment_with">
                    
                    </span>
                     <span >Freeze &nbsp;

  <input type="radio" <?php if($key['fstatus']=="Freeze"){ echo "checked";}?> name="fstatus" required value = "Freeze" onclick="myFunction3()"   id="payment_with">

                    </span>
                    </div>
                  


                      <span style=" display: none;" id="cheque_number">
                                         
                                         <label class="col-sm-2">Student Note </label>
                                         <div class="col-sm-4">
                                            <input type="text"  class="form-control" name="freeze_note"   >
                                         </div></span>
                    </div><br>
                  

                  <div class="row">
                    <label for="inputSkills" class="col-sm-2 control-label"> Permanant Address   </label>
                    <div class="col-sm-10">
                      <input type="text" name= "permanent_address" value=" <?php echo $key['permanent_address']; ?>" class="form-control" class="form-control" id="inputSkills" placeholder="Skills">
                    </div>
                  </div>

                 
                
                
              </div>
              </div>
              
              <!-- /.tab-pane -->
              


              <div class="tab-pane" id="timeline">
              
 <div class="active tab-pane" id="activity">

                <div class="row">
                    <label for="inputName" class="col-sm-2 control-label">Father Name</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control" name="father_name" value="<?php echo $key['fathername']; ?>" id="inputName" placeholder="Name">
                    </div>
                  

                   
                    <label for="inputName" class="col-sm-2 control-label">Father Email</label>

                    <div class="col-sm-4">
                      <input type="email" name="father_email" value="<?php echo $key['father_email']; ?>" class="form-control" id="inputName" placeholder="Name">
                    </div>
                  </div><br>

                   <div class="row">
                    <label for="inputName" class="col-sm-2 control-label">Father CNIC</label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control" name="father_cnic" value="<?php echo $key['father_cnic']; ?>" id="inputName" placeholder="Name">
                  </div>


                    <label for="inputName" class="col-sm-2 control-label">Fathers Phone</label>
                    <div class="col-sm-4">
                      <input type="text" name="father_phone" value="<?php echo $key['father_phone']; ?>" placeholder="Phone" class="form-control"  >
                    </div>
                  </div><br>

                  <div class="row">
                    <label for="inputEmail" class="col-sm-2 control-label">Father Profession</label>
                    <div class="col-sm-4">
                       <input type="text" name="father_profession" value="<?php echo $key['father_profession']; ?>" class="form-control"   placeholder="Father's Profession">
                    </div>
                  </div>


                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-12">
                      <button type="submit" class="btn btn-primary" style="width: 15%;">Submit</button>
                    </div>
                  </div><br>



              </div>
             

             </form>
<?php } ?> 

    
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>

  </div>
  <script>
    function course_change()
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('GET','ajax_student_courses?batch_id='+document.getElementById('batch_id').value,false);
        xmlhttp.send(null);
        
        document.getElementById('course').innerHTML=xmlhttp.responseText;
    }
</script>
             <script>



function myFunction2() {

 
   
  document.getElementById('cheque_number').style.display ='none';

    }
 function myFunction3() {
       document.getElementById('cheque_number').style.display ='block';

    }
    


</script>