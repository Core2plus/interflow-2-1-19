<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Edit Course
        <small>Add / Edit User</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body table-responsive">
                    <?php $this->load->helper("form"); ?>
                                            
      <?php 
                                foreach($all_course as $show):
                                ?>                
                                <form action="<?php echo base_url('update_cor'); ?>" method="post">
                                     <input type="hidden" name="text_hid" value="<?php echo $show->course_id; ?>">

                                     <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control" value="<?php echo $show->coursename; ?>" />
                                    </div>
                                    </div>

                                    <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Course Code</label>
                                        <input type="text" name="code" class="form-control" value="<?php echo $show->coursecode; ?>" required placeholder="####"/>
                                    </div>
                                    </div>

                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Course Fees</label>
                                        <input type="text" value="<?php echo $show->coursefee; ?>" name="fees" class="form-control" required placeholder="12345"/>
                                    </div>
                                    </div>
                                                                 
                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Course Duration</label>
                                        <input type="text" name="duration" value="<?php echo $show->co_duration; ?>" class="form-control" required placeholder="**-months"/>
                                    </div>
                                    </div>

                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Select Program<span class="required">*</span></label>
                                        <div>
                                            
                                            <select class="form-control" id="sel1" name="program_id">
                                                <option>Select</option>
                                          <?php
                                    foreach($all_pro as $sel):
                                    ?>
                                   <option value="<?php echo $sel->program_id; ?>"><?php echo $sel->program_name; ?></option>
                                    <?php
                                        endforeach;
                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Select Module<span class="required">*</span></label>
                                        <div>
                                            
                                            <select class="form-control" id="sel1" name="module_id">
                                                <option>Select</option>
                                          <?php
                                    foreach($all_mod as $sel):
                                    ?>
                                   <option value="<?php echo $sel->module_id; ?>"><?php echo $sel->module_name; ?></option>
                                    <?php
                                        endforeach;
                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Select Status<span class="required">*</span></label>
                                        <div>  
                                            <select class="form-control" id="sel1" name="sel1">
                                                <option>Select</option>
                                          <option class="form-control" value="1">Enable</option>
                                          <option class="form-control" value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                    </div>
                                    
                                    <!--
                                    <div class="form-group">
                                        <label>Number</label>
                                        <div>
                                            <input data-parsley-type="number" type="text"
                                                   class="form-control" required
                                                   placeholder="Enter only numbers"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Alphanumeric</label>
                                        <div>
                                            <input data-parsley-type="alphanum" type="text"
                                                   class="form-control" required
                                                   placeholder="Enter alphanumeric value"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Textarea</label>
                                        <div>
                                            <textarea required class="form-control" rows="5"></textarea>
                                        </div>
                                    </div>-->
                                    
                                    <div class="form-group">
                                        <div class="box-footer col-md-12">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light" style="width: 10%;">
                                                Update
                                            </button>
                                            <button type="reset" class="btn btn-secondary waves-effect m-l-5" style="width: 10%;">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                
                                </form>
                                </div>
<?php endforeach; ?>
</div>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>