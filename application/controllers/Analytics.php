<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';
include APPPATH . '/third_party/getid3/getid3.php';


class Analytics extends BaseController {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
         $this->load->model('adv_model');
         $this->load->model('analytics_model');
         $this->load->model('main_model');
        $this->isLoggedIn();   
    }

	public function index()
    {
        
           
            $data=$this->analytics_model->get_rating_count();
            if(!empty($data))
            {

                 $data['rating']=$data;
                 //print_r($data);
                 $this->global['pageTitle'] = 'Media : Communiation';
                 $this->loadViews("load_rating_view", $this->global, $data, NULL);

            }
           


         
        
        else
        {
        $this->global['pageTitle'] = 'Media : Communiation';

        $this->loadViews("load_rating_view", $this->global, NULL, NULL);
    }

    }
    public function agency_cost()
    {
        
           
            $data=$this->analytics_model->get_agency_cost();
            if(!empty($data))
            {

                 $data['rating']=$data;
                 //print_r($data);
                 $this->global['pageTitle'] = 'Media : Communiation';
                 $this->loadViews("load_agency_view", $this->global, $data, NULL);
            }
           


         
        
        else
        {
        $this->global['pageTitle'] = 'Media : Communiation';

        $this->loadViews("load_agency_view", $this->global, NULL, NULL);
    }

    }
    public function agency_adv()
    {
        
           
            $data=$this->analytics_model->get_agency_adv();
            if(!empty($data))
            {

                 $data['rating']=$data;
                // print_r($data);
                 $this->global['pageTitle'] = 'Media : Communiation';
                 $this->loadViews("load_agencyad_view", $this->global, $data, NULL);
            }
           


         
        
        else
        {
        $this->global['pageTitle'] = 'Media : Communiation';

        $this->loadViews("load_agencyad_view", $this->global, NULL, NULL);
    }

    }

}