<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Add New Program
        <small>Add / Edit User</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter User Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body table-responsive ">
                    <?php $this->load->helper("form"); ?>               
                                <form action="<?php echo base_url('insertprogram'); ?>" method="post">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Program Name</label>
                                        <input type="text" name="name" class="form-control" placeholder="Type something" onchange="validate_program(this.value)"/>
                                    </div>
                                    <p id="uniq_error" class="text-danger" style="display: none;">Please Enter Unique Program Name</p>
                                    <?php echo form_error('name','<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                                    </div>
                                    <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Start Date</label>
                                        <div>
                                            <input type="date" class="form-control" 
                                                   parsley-type="email" name="doj"/>
                                        </div>
                                    </div>
                                    <?php echo form_error('doj','<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                                    </div>
                                    <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Close Date</label>
                                        <div>
                                            <input type="Date" class="form-control" name="doe"/>
                                        </div>
                                    </div>
                                    <?php echo form_error('doe','<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                                    </div>
<!--
                                    <div class="form-group">
                                        <label>Digits</label>
                                        <div>
                                            <input data-parsley-type="digits" type="text"
                                                   class="form-control" required
                                                   placeholder="Enter only digits"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Number</label>
                                        <div>
                                            <input data-parsley-type="number" type="text"
                                                   class="form-control" required
                                                   placeholder="Enter only numbers"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Alphanumeric</label>
                                        <div>
                                            <input data-parsley-type="alphanum" type="text"
                                                   class="form-control" required
                                                   placeholder="Enter alphanumeric value"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Textarea</label>
                                        <div>
                                            <textarea required class="form-control" rows="5"></textarea>
                                        </div>
                                    </div>-->
                                    <div class="box-footer col-md-12">
                                    <div class="form-group">
                                            <button type="submit" id="button" class="btn btn-primary waves-effect waves-light" style="width: 10%;">
                                                Submit
                                            </button>
                                     </div>

                                </form>
                                </div>
                                </div>
                               
                
            
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        
                    </div>
                </div>
            </div>
        
    </section>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
<script >
    function validate_program(v){
        param="program="+v;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     token=this.responseText;
     if(token=='true')
     {
        document.getElementById('uniq_error').style.display="none";






     }
     else if(token=='false')
     {
         document.getElementById('uniq_error').style.display="block";
        



     }
    }
  };
  xhttp.open("POST", "<?php echo base_url().'Validation_controller/validate_program';?>", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(param);


    }
    

</script>