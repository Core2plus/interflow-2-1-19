
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="offset-sm-1 col-sm-10">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                   
                                    <li class="breadcrumb-item active">New Course</li>
                                </ol>
                            </div>
                            <h4 class="page-title">New Course</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Course Data Insertion</h4>                
                                <form action="<?php echo base_url('Main/insert_course'); ?>" method="post">

                                    <div class="form-group">
                                        <label>Select Program<span class="required">*</span></label>
                                        
                                        <div>
                                            
                                            <select class="form-control" id="sel1" name="program_id">
                                                <option>Select</option>
                                          <?php
                                    foreach($all_pro as $sel):
                                    ?>
                                   <option value="<?php echo $sel->program_id; ?>"><?php echo $sel->program_name; ?></option>
                                    <?php
                                        endforeach;
                                    ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Select Module<span class="required">*</span></label>
                                        
                                        <div>
                                            
                                            <select class="form-control" id="sel1" name="module_id">
                                                <option>Select</option>
                                          <?php

                                    foreach($all_mod as $sel):
                                    ?>
                                   <option value="<?php echo $sel->module_id; ?>"><?php echo $sel->module_name; ?></option>
                                    <?php
                                        endforeach;
                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                   
                                    <div class="form-group">
                                        <label>Course Code</label>
                                        <input type="text" name="code" class="form-control" required placeholder="####"/>
                                    </div>

                                    <div class="form-group">
                                        <label>Course Name</label>
                                        <input type="text" name="name" class="form-control" required placeholder="Type something"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Course Fees</label>
                                        <input type="text" name="fees" class="form-control" required placeholder="12345"/>
                                    </div>
                                                                 
                                    

                               
                                    <div class="form-group">
                                        <label>Course Duration</label>
                                        <input type="text" name="duration" class="form-control" required placeholder="**-months"/>
                                    </div>

                                    <div class="form-group">
                                        <label>Select Status<span class="required">*</span></label>
                                        
                                        <div>
                                            
                                            <select class="form-control" id="sel1" name="sel1">
                                                <option>Select</option>
                                          <option class="form-control" value="1">Enable</option>
                                          <option class="form-control" value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </form>
<a href="<?php echo base_url('Main/modules'); ?>">
                                            <button style="float: right; margin: -52px 0px;" type="reset" class="btn btn-secondary waves-effect m-l-5">
                                                Cancel
                                            </button></a>
                            </div>
                        </div>
                    </div> <!-- end col -->

                    
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
       
<?php $this->load->view('BKO/includes/footer'); ?>
     
