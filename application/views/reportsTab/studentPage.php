

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" xmlns="http://www.w3.org/1999/html">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-tachometer" aria-hidden="true"></i> Reports Management
            <small>View Reports</small>
        </h1>
    </section>
    <section class="content-header">
        <img src="<?php echo base_url()."assets/images/"; ?>ARTT_BS2.png" class="img-responsive center-block" style="width: 45%; min-width: 300px;  height: 45%  alt="header-logo">
        <!--<h1>-->

        <br>
        <br>


        <div class="row">
            <div class="col-md-4">
                <div class="col-md-12">
                    <form method="post" action="<?php echo base_url()."reports/student_report"; ?>" target="_blank">
                       
                </div>
            </div>

                <div class="col-md-4">
                    <div class="col-md-12">

<label style="font-size: 16px;">Select Batch/Year</label>

                    <select name="batchname" id="" class="btn btn-block btn-default btn-md" style="width: 100%; margin-bottom: 4%;" required>
                        <option value="">Select</option>
                        <option value="all">All</option>

                        <?php if(@$batch)
                {

                    foreach($batch as $k=>$v)

                        {
                            ?>
                            <option value="<?= $v['batch_id'];?>"><?= $v['batch_name'];?></option>

                            <?php
                        }
                }
                ?>

            </select>
            <br>
            <label style="font-size: 16px;">Select Status</label>
             <select name="report_type" id="" class="btn btn-block btn-default btn-md" style="width: 100%; margin-bottom: 4%;" required>
                            <option value="">Select</option>
                            <option value="p">Paid</option>
                            <option value="u">Unpaid</option>
                            <option value="f">Freeze</option>
                            <option value="uf">UnFreeze</option>
                        </select>
  <br>  <br>
<input type="submit" value="Show Report" class="btn btn-block btn-primary btn-md" style="width: 100%">
            </div>
                </div>

<!--            <div class="col-md-2">-->
<!--                <div class="form-group has-feedback">-->
<!--                    <input type="text" name="batch" class="form-control" placeholder="Batch">-->
<!--                    <span class="glyphicon glyphicon-user form-control-feedback"></span>-->
<!--                </div>-->
<!--            </div>-->

<!--            <div class="col-md-2">-->
<!--                <div class="form-group">-->
<!---->
<!--                    <div class="input-group date">-->
<!--                        <div class="input-group-addon">-->
<!--                            <i class="fa fa-calendar"></i>-->
<!--                        </div>-->
<!--                        <input type="text" name="date_value" class="form-control pull-right" id="datepicker" required>-->
<!--                    </div>-->
<!--                    <!-- /.input group -->
<!--                </div>-->
<!--            </div>-->


            <div class="col-md-4">
                <div class="col-md-9">
                    
                </div>
            </div>
        </div>

                </form>


                <?php

        if($this->session->flashdata('message_name') !== null)
            echo "<p class=\"text-center bg-success text-success\" style=\"padding: 5px;font-size:14px; font-weight: 600;\" >" . $this->session->flashdata('message_name') . "</p>";
        ?>


        <?php
        if($this->session->flashdata('msg_to_user') !== null)
            echo "<p class=\"text-center bg-success text-success\" style=\"padding: 5px;font-size:14px; font-weight: 600;\" >" . $this->session->flashdata('msg_to_user') . "</p>";
        ?>



    </section>
    <br>
    <!---->
    <!--<script>-->
    <!--	$(document).ready(function() {-->
    <!--	$('#example2').DataTable( {-->
    <!--	"order": [[ 5, "desc" ]]-->
    <!--	} );-->
    <!--	} );-->
    <!--</script>-->

    <!-- Main content -->
    <!-- /.content -->

</div>

