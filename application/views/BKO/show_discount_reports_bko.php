
<?php $this->load->view('BKO/includes/header'); ?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
   <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/faviicon.png">

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/morris/morris.css">
        
         <!-- DataTables -->
        <link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url(); ?>assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- Responsive datatable examples -->
        <link href="<?php echo base_url(); ?>assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css" />
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                    <li class="breadcrumb-item active">Discount Reports</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Discount Reports</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
    <div class="row" style="margin: 0px;">
        <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div>

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">

                          

                                
                                
                                <table id="datatable" class="table table-bordered">
                                    <thead>
                                    <tr>
                                         <th style="width: 8%">ID</th>
                                        <th style="width: 8%">ARTT ID</th>
                                        <th style="width: 15%">Student Name</th>
                                   
                                        <th style="width: 8%">Dicount Per</th>
                                       
                                         <th style="width: 18%">Action</th>
                                     
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $no = 1;
                                        foreach($show_discount_reports as $show) { 

                                            ?>
 <tr>                                   <td><?php  echo $no++ ; ?></td>
                                        <td><?php  echo $show->artt_id;    ?></td>
                                        <td><?php  echo $show->fname;    ?></td>
                                        
                                         <td><?php  echo $show->percentage;    ?></td>
                                       
                                        <td>
                                         <a href="<?php echo base_url('Main/view_note/').$show->discount_id; ?>">
         <button class="btn btn-primary" >View</button></a>
<?php if($show->approve==0){ ?>
        
      <button type="button" style="width:25%;" class="btn btn-defaut ">Pending..</i></button></a>


                                           
<?php } ?>

<?php if($show->approve==1) { ?>
      
      <button type="button" style="width:25%;" class="btn btn-success ">Approved</i></button></a>
      
      <?php } ?> 

      <?php if($show->approve==2) { ?>
    
      
      <button type="button" style="width:23%;" class="btn btn-danger    ">Denied</i></button></a>
      
          <?php  if($show->admin_note != "" ) { ?>
            <a href="<?php echo base_url('Main/show_deny_message_discount/').$show->discount_id; ?>">
      <button   type="button" class="btn btn-primary "><i class="ti-eye"></i></button></a>
      
      <?php } } ?> 
                                        </td>
                                     
                                    </tr>

                                    <?php
                                }

                                    ?>



                           
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

               

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <?php $this->load->view('BKO/includes/footer'); ?>