<style>

#preloader {
  position: fixed;
  left: 0;
  top: 0;
  z-index: 99;
  width: 100%;
  height: 100%;
  overflow: visible;
  background: #ffffff
      url('https://loading.io/spinners/typing/lg.-text-entering-comment-loader.gif')
      no-repeat center center;
}
.content-wrapper{

       background: url('../assets/images/ads/img333.jpg')
       center center !important;
      background-size: cover !important;

}


</style>
<script>
  $(document).ready( function() {
  $('#preloader').delay(5).fadeOut();
});
</script>
  <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>



<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 style="color:white;">
        <i class="fa fa-users"></i> Add Advertisement
        <small style="color:white;">Add </small>
      </h1>
    </section>

    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->



                 <div class="box" style="opacity:1; background-color: #e8e7e7; margin-top: 0%;">
                <div id="preloader"></div>
                    <div class="box-header">

                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body table-responsive">



                                <form action="<?php echo base_url('adv/add_adv'); ?>" method="post" enctype="multipart/form-data">


                                     <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Adv. Name</label>

                                        <input type="text" name="name" class="form-control"  />

                                    </div>

                                    </div>
                                    <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Adv. Video</label>

                                        <input type="file" name="filename" class="form-control">


                                    </div>
                                    </div>



                                      <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Adv. Slot</label>

                                        <input type="text" name="slot" class="form-control">


                                    </div>

                                    </div>
                                     <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Rating</label>

                                        <select name="rating" class="form-control">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>




                                        </select>







                                    </div>

                                    </div>
                                      <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Category</label>

                                        <select name="category" class="form-control">
                                            <?php if(@$category)
                                            {
                                                foreach($category as $k=>$v)
                                                {
                                                    ?>
                                                    <option value="<?php echo $v['CatID'];?>"><?php echo $v['CatType'];?></option>

                                                    <?php


                                                }


                                            }
                                            ?>




                                        </select>







                                    </div>

                                    </div>
                                    <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Product</label>

                                        <select name="product" class="form-control">

                                                <?php if(@$product)
                                            {
                                                foreach($product as $k=>$v)
                                                {
                                                    ?>
                                                    <option value="<?php echo $v['ProductID'];?>"><?php echo $v['ProductName'];?></option>

                                                    <?php


                                                }


                                            }
                                            ?>





                                        </select>
                                    </div>

                                    </div>
                                    <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Brand</label>

                                        <select name="brand" class="form-control">

                                                <?php if(@$brand)
                                            {
                                                foreach($brand as $k=>$v)
                                                {
                                                    ?>
                                                    <option value="<?php echo $v['BrandID'];?>"><?php echo $v['BrandName'];?></option>

                                                    <?php


                                                }


                                            }
                                            ?>





                                        </select>
                                    </div>

                                    </div>
                                    <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Agency</label>

                                        <select name="agency" class="form-control">
                                                <?php if(@$agency)
                                            {
                                                foreach($agency as $k=>$v)
                                                {
                                                    ?>
                                                    <option value="<?php echo $v['AgencyID'];?>"><?php echo $v['AgencyName'];?></option>

                                                    <?php


                                                }


                                            }
                                            ?>





                                        </select>
                                    </div>

                                    </div>
                                      <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Artist</label>

                                        <select name="artist" class="form-control">

                                                <?php if(@$artist)
                                            {
                                                foreach($artist as $k=>$v)
                                                {
                                                    ?>
                                                    <option value="<?php echo $v['ArtistID'];?>"><?php echo $v['Name'];?></option>

                                                    <?php


                                                }


                                            }
                                            ?>





                                        </select>
                                    </div>

                                    </div>
                                      <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Booking Date</label>

                                        <input type="date" name="date" class="form-control">






                                    </div>

                                    </div>


                                    <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Order Number</label>

                                        <input type="text" name="order" class="form-control">






                                    </div>

                                    </div>

                                    <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Cost</label>

                                        <input type="text" name="cost" class="form-control">






                                    </div>

                                    </div>


                                        <div class="modal" id="myModal1">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Validation Error!</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      <?php echo @$validation;?>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>










                                    <!--
                                    <div class="form-group">
                                        <label>Number</label>
                                        <div>
                                            <input data-parsley-type="number" type="text"
                                                   class="form-control" required
                                                   placeholder="Enter only numbers"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Alphanumeric</label>
                                        <div>
                                            <input data-parsley-type="alphanum" type="text"
                                                   class="form-control" required
                                                   placeholder="Enter alphanumeric value"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Textarea</label>
                                        <div>
                                            <textarea required class="form-control" rows="5"></textarea>
                                        </div>
                                    </div>-->

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light " style="width: 10%;">
                                            Add
                                            </button>
                                          <!--   <button type="reset" class="btn btn-secondary waves-effect m-l-5" style="width: 10%;">
                                                Cancel
                                            </button> -->
                                        </div>
                                    </div>

                                </form>
                                </div>

</div>
                </div>
            </div>
            <div class="col-md-4">
                <!-- <?php
                   // $this->load->helper('form');
                    //$error = //$this->session->flashdata('error');
                    //if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php //echo $this->session->flashdata('error'); ?>
                </div>
                <?php } ?>
                <?php
                    //$success = //$this->session->flashdata('success');
                    //if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php //echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                 -->
                <div class="row">
                    <div class="col-md-12">
                        <?php // echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title" style="text-align: center;padding: 20px;background: #036;">Status</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      <?php echo $flag;?>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
<?php if(@$flag)
{
    echo "<script>$('myModal').modal('show');</script>";


}
?>
