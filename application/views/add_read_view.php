<style>
#preloader {
  position: fixed;
  left: 0;
  top: 0;
  z-index: 99;
  width: 100%;
  height: 100%;
  overflow: visible;
  background: #ffffff
      url('https://loading.io/spinners/typing/lg.-text-entering-comment-loader.gif')
      no-repeat center center;
}

.content-wrapper{

       background: url('./assets/images/ads/img333.jpg')
       center center !important;
      background-size: cover !important;
}
h2{
  margin-top: 20%;
  text-align: center;
}


</style>
<style>
  @base:#212121;
@color:silver;
@accent:#27ae60;
@borderRadius:4px;

.twoToneCenter {
    text-align:center;
    margin:1em 0;
}
.twoToneButton {
    display: inline-block;
    border:1px solid darken(@base,50%);
    outline:none;
    padding:10px 20px;
    line-height: 1.4;
    background: @base;
    background: linear-gradient(to bottom,  lighten(@base,20%) 0%,lighten(@base,15%) 50%,darken(@base,5%) 51%,darken(@base,2.5%) 100%);
    border-radius: @borderRadius;
    border: 1px solid darken(@base,30%);
    color:lighten(@color,10%);
    text-shadow: darken(@color,80%) -1px -1px 0px;

    position: relative;
    transition: padding-right .3s ease;
    font-weight:700;
    box-shadow:0 1px 0 lighten(@base,30%) inset, 0px 1px 0 lighten(@base,10%);

}

.twoToneButton:hover {
    box-shadow:0 0 10px darken(@base,10%) inset, 0px 1px 0 lighten(@base,10%);
    color:lighten(@color,20%);
}

.twoToneButton:active{
    box-shadow:0 0 10px darken(@base,10%) inset, 0px 1px 0 lighten(@base,10%);
    color:lighten(@color,30%);
    background: darken(@base,10%);
    background: linear-gradient(to bottom,  lighten(@base,10%) 0%,lighten(@base,5%) 50%,darken(@base,5%) 51%,darken(@base,10%) 100%);

}

.twoToneButton.spinning {
    background-color: @base;
    padding-right: 40px;
}
.twoToneButton.spinning:after {
    content: '';
    position: absolute;
    right: 6px;
    top: 50%;
    width: 0;
    height: 0;
    box-shadow: 0px 0px 0 1px darken(@base,10%);
    position: absolute;
    border-radius: 50%;
    animation: rotate360 .5s infinite linear, exist .1s forwards ease;
}

.twoToneButton.spinning:before {
    content: "";
    width: 0px;
    height: 0px;
    border-radius: 50%;
    right: 6px;
    top: 50%;
    position: absolute;
    border: 2px solid darken(@base,40%);
    border-right: 3px solid @accent;
    animation: rotate360 .5s infinite linear, exist .1s forwards ease ;

}



@keyframes rotate360 {
    100% {
        transform: rotate(360deg);
    }
}
@keyframes exist {
    100% {
        width: 15px;
        height: 15px;
        margin: -8px 5px 0 0;
    }
}

body {background:@base; }
</style>
<script>
  $(function(){

    var twoToneButton = document.querySelector('.twoToneButton');


    twoToneButton.addEventListener("click", function() {
        twoToneButton.innerHTML = "Loading Data.....";
        twoToneButton.classList.add('spinning');

      setTimeout(
            function  (){
                twoToneButton.classList.remove('spinning');
                twoToneButton.innerHTML = "Execute";

            }, 20000000);
    }, false);

});
</script>
<script>
  $(document).ready( function() {
  $('#preloader').delay(1000).fadeOut();
});
</script>
  <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>


<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 style="color: #ffffff;">
        <i class="fa fa-users"></i> Search Directory.
        <small style="color: #ffffff;">Directory Info.</small>
      </h1>
    </section>

    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-12" >
              <!-- general form elements -->



                <div class="box" style="opacity:1; background-color: #e8e7e7; margin-top: 0%;">
                  <div id="preloader"></div>
                   <!--  <div id="preloader"><h2>Loading.....</h2></div> -->

                    <!-- form start -->
                    <div class="box-body table-responsive">



                    <form action="<?php echo base_url('reading_dir/gotdir'); ?>" method="post" >



                                        <label>Directory</label>

                             <input type="text" name="dir" class="form-control"  /><br>
                            <div class="twoToneCenter">
                            <button type="submit" class="btn btn-primary pull-right twoToneButton" style="width: 15%; margin-bottom: 1%;">
                                            Execute
                                            </button></div>



                                </form>
                                <br>






                     <table id="example1" class="table table-hover">
                <thead>
                    <tr>

                                        <th>S.No</th>
                                        <th>File Name</th>
                                        <th>File Path</th>
                                        <th>Width</th>
                                        <th>Height</th>
                                         <th>Duration</th>
                                         <th>PlayString</th>








                    </tr>
                </thead>
                <tbody>
                 <?php
                 if(@$dir)
                 {
                  //print_r($dir);
                  $no = 1;
                   foreach ($dir as $k=>$v) {

                   ?>
                   <tr>
                    <td><?php echo $no;?></td>
                    <td><?php echo @$filename[$k];?></td>
                   <td><?php echo @$v['filepath'];?></td>
                   <td><?php echo @$width[$k];?></td>
                   <td><?php echo @$height[$k];?></td>
                   <td><?php echo @$duration[$k];?></td>
                   <td><?php echo @$playstring[$k];?></td>

                 </tr>




               <?php $no++;} } ?>
                </tbody>

              </table>





                  </div>

                </div>
            </div>
        </div>
    </section>
</div>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();
            var link = jQuery(this).get(0).href;
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>
<script>
  style=document.getElementById('tab').style.display;
  alert(style);

</script>
