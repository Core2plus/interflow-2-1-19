<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

class Sector extends BaseController {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
         $this->load->model('sector_model');
         $this->load->model('main_model');
        $this->isLoggedIn();
    }

	public function index()
	{
		$flag=$this->session->flashdata('flag');
		if($flag=='ok')
		{
			$data['flag']="SuccessFully Updated !";


		}
		elseif($flag=='fail')
		{
			$data['flag']="Something Went Wrong !";


		}
		elseif($flag=='inserted')
		{
			$data['flag']="Successfully Inserted";


		}
		elseif($flag=='failinserted')
		{
			$data['flag']="Insertion Fail";


		}
		elseif($flag=='deleted')
		{
			$data['flag']="Deleted Successfully !";


		}
		elseif($flag=='faildeleted')
		{
			$data['flag']="Deletion Failed";


		}
		$result=$this->sector_model->get_sector();

		if(!empty($result))
		{

			$data['category']=$result;
		$this->global['pageTitle'] = 'Media : Communiation';
        $this->loadViews("sector_view", $this->global,$data, NULL);
    }else{
$data['nodata']="ok";

    	$this->global['pageTitle'] = 'Media : Communiation';
        $this->loadViews("sector_view", $this->global, $data, NULL);

    }
}
  public function load_edit_sector()
     {
     	$this->load->library('form_validation');
     	$this->form_validation->set_rules('id','','required');
     	if($this->form_validation->run())
     	{
     		$id=$this->input->post('id');
     		$result=$this->sector_model->get_sector_id($id);
     		if(!empty($result))
     		{
     		$data['cat']=$result;
     		$this->global['pageTitle'] = 'Media : Communiation';
         $this->loadViews("editsector_view", $this->global, $data, NULL);
 }
 else{
 	    $this->global['pageTitle'] = 'Media : Communiation';
         $this->loadViews("editsector_view", $this->global, NULL, NULL);

      }

     	}

     	 else{
     	 	redirect(base_url().'sector');



     	 }

}

    public function update_sector()
     {
			 $file=@$_FILES['filename'];
     	$this->load->library('form_validation');
     	$this->form_validation->set_rules('cat_id','','required');
     	$this->form_validation->set_rules('name','','required');

     	if($this->form_validation->run())
     	{
					$img='';
     		$id=$this->input->post('cat_id');
     		$name=$this->input->post('name');
     		$des=$this->input->post('des');
				$prev=$this->sector_model->get_sector_data($id);
				if(!empty($_FILES['filename']['name']))
				{
					$config['upload_path']          = './images/sectors/';
						$config['allowed_types']        ='png|jpeg|jpg|gif';
						$this->load->library('upload', $config);

						if ( ! $this->upload->do_upload('filename'))
						{
							$error = array('error' => $this->upload->display_errors());
							$this->session->set_flashdata('flag','fail');
							redirect(base_url().'sector');
						}
						else
						{
							 $data = array('upload' => $this->upload->data());
							 print_r($data);
							 $img=$data['upload']['file_name'];
     		$result=$this->sector_model->update_sector($id,$name,$img);
     		if($result)
     		{
     			$this->session->set_flashdata('flag','ok');
     			redirect(base_url().'sector');
     		}
     		else{
     			$this->session->set_flashdata('flag','fail');
     			redirect(base_url().'sector');


     		}
     	}
		}
		else{
											$img=$prev[0]['sec_image'];
											 $result=$this->sector_model->update_sector($id,$name,$img);
								if($result)
								{
								 $this->session->set_flashdata('flag','ok');

								 redirect(base_url().'sector');

								}
     	else{
            $this->session->set_flashdata('flag','fail');
     		redirect(base_url().'sector');

     	}
     }
}else{
		 $this->session->set_flashdata('flag','fail');
	 redirect(base_url().'sector');



 }
}



     public function addsector()
     {
     	$this->global['pageTitle'] = 'Media : Communiation';
         $this->loadViews("add_sector_view", $this->global, NULL, NULL);
    }
   public function add_sector(){
     	$this->form_validation->set_rules('name','Name','required');

     	if($this->form_validation->run())
     	{
     		$name=$this->input->post('name');
     		//$des=$this->input->post('des');
    		$result=$this->sector_model->insertsector($name);
    		if($result)
    		{
    			$this->session->set_flashdata('flag','inserted');
    			//echo $result;
    			redirect(base_url().'sector/');


    		}
    		else{
    			$this->session->set_flashdata('flag','failinserted');
    			//echo $result;
    			redirect(base_url().'sector/');


    		}



    	}
        else{
            $data['validation']=validation_errors();
            $this->global['pageTitle'] = 'Media : Communiation';
         $this->loadViews("add_sector_view", $this->global, $data, NULL);


        }

    }
    public function delete_sector(){
    	$this->form_validation->set_rules('cat_id','','required');
    	if($this->form_validation->run())
    	{
    		$cat=$this->input->post('cat_id');
    		$result=$this->sector_model->delete_sector($cat);
    		if($result)
    		{
    			$this->session->set_flashdata('flag','deleted');
    			redirect(base_url().'sector/');


    		}
    		else{
    				$this->session->set_flashdata('flag','faildeleted');
    			redirect(base_url().'sector/');



    		}


    	}


    }



	}
