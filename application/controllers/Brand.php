<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

class Brand extends BaseController
{
	public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
         $this->load->model('brand_model');
          $this->load->model('main_model');
        $this->isLoggedIn();
    }

	public function index()
	{
		$flag=$this->session->flashdata('flag');
		if($flag=='ok')
		{
			$data['flag']="SuccessFully Updated !";


		}
		elseif($flag=='fail')
		{
			$data['flag']="Something Went Wrong !";


		}
		elseif($flag=='inserted')
		{
			$data['flag']="Successfully Inserted";


		}
		elseif($flag=='failinserted')
		{
			$data['flag']="Insertion Fail";


		}
		elseif($flag=='deleted')
		{
			$data['flag']="Deleted Successfully !";


		}
		elseif($flag=='faildeleted')
		{
			$data['flag']="Deletion Failed";


		}
		$result=$this->brand_model->get_brand();

		if(!empty($result))
		{

			$data['category']=$result;
		$this->global['pageTitle'] = 'Media : Communiation';
        $this->loadViews("brand_view", $this->global,$data, NULL);
    }else{
$data['nodata']="ok";

    	$this->global['pageTitle'] = 'Media : Communiation';
        $this->loadViews("brand_view", $this->global, $data, NULL);
    }
}

  public function load_edit_brand()
      {
      	$this->load->library('form_validation');
      	$this->form_validation->set_rules('id','','required');
      	if($this->form_validation->run())
      	{
      		$id=$this->input->post('id');
     		$result=$this->brand_model->get_brand_id($id);
      		if(!empty($result))
      		{
      		$data['cat']=$result;
      		$this->global['pageTitle'] = 'Media : Communiation';
          $this->loadViews("editbrand_view", $this->global, $data, NULL);
  }
  else{
  	    $this->global['pageTitle'] = 'Media : Communiation';
          $this->loadViews("editbrand_view", $this->global, NULL, NULL);
       }

      	}

      	 else{
      	 	redirect(base_url().'brand');



      	 }

 }


    public function update_brand()
     {
      $file=@$_FILES['filename'];

 // $config['upload_path']          = './upload/brand/';
 //                $config['allowed_types']        = 'gif|jpg|png|JPEG';
 //                $config['max_size']             = 1000;
 //                $config['max_width']            = 1024;
 //                $config['max_height']           = 768;
 //                $this->load->library('upload', $config);

     	$this->load->library('form_validation');
     	$this->form_validation->set_rules('cat_id','','required');
     	$this->form_validation->set_rules('name','','required');
     	$this->form_validation->set_rules('des','','required');
      //$this->form_validation->set_rules('img','image','required');
     	if($this->form_validation->run())
     	{
        $img='';

     	 $id=$this->input->post('cat_id');
     	 $name=$this->input->post('name');
     	 $des=$this->input->post('des');
       $prev=$this->brand_model->get_brand_data($id);
         if(!empty($_FILES['filename']['name']))
            {
              $config['upload_path']          = './images/brands/';
                $config['allowed_types']        ='png|jpeg|jpg|PNG';
                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('filename'))
                {
                  $error = array('error' => $this->upload->display_errors());
                  //print_r($error);
                  //echo "Uplaod Error";
                  //$this->session->flashdata('f');
                  $this->session->set_flashdata('flag','fail');
                  redirect(base_url().'brand');



                }
                else
                {
                   $data = array('upload' => $this->upload->data());
                   print_r($data);
                   $img=$data['upload']['file_name'];
                   $result=$this->brand_model->updatebrand($id,$name,$des,$img);
        if($result)
        {

         $this->session->set_flashdata('flag','ok');
      redirect(base_url().'brand');

        }
        else{
         $this->session->set_flashdata('flag','fail');

         redirect(base_url().'brand');

        }


                }

            }
            else{
              $img=$prev[0]['image'];
               $result=$this->brand_model->updatebrand($id,$name,$des,$img);
        if($result)
        {
         $this->session->set_flashdata('flag','ok');

         redirect(base_url().'brand');

        }
        else{

         $this->session->set_flashdata('flag','fail');
         redirect(base_url().'brand');

        }


            }


       //print_r($file);

     		// $result=$this->brand_model->updatebrand($id,$name,$des,$img);
     		// if($result)
     		// {
     		// 	$this->session->set_flashdata('flag','ok');
     		// 	redirect(base_url().'brand');


     		// }
     		// else{
     		// 	$this->session->set_flashdata('flag','fail');
     		// 	redirect(base_url().'brand');


     		// }



     	}
     	else{
          $this->session->set_flashdata('flag','fail');
     		redirect(base_url().'brand');



     	}
     }

      public function addbrand()
      {
      	$this->global['pageTitle'] = 'Media : Communiation';
          $this->loadViews("add_brand_view", $this->global, NULL, NULL);
     }

     public function das_brand()
      {
				$config['upload_path'] = './images/brands/';
											 $config['allowed_types']        = 'gif|jpg|png|JPEG|PNG|jpeg';
											 $config['max_size']             = 1000;
											 $config['max_width']            = 1024;
											 $config['max_height']           = 768;
											$this->load->helper(array('form', 'url'));
											 $this->load->library('upload', $config);

						 $this->load->library('form_validation');
						 $this->form_validation->set_rules('name','BrandName','required');
						 $this->form_validation->set_rules('img','image','required');

						 $this->load->model('Brand_model');
						$data['category']= $this->Brand_model->publicdata();
            $this->loadViews("dashboard_brand", $this->global,$data);
						 if($this->form_validation->run())
						 {
							 $name=$this->input->post('name');
							 $img=$this->input->post('img');
							 $result=$this->brand_model->publicdata($name,$img);
							 if($result)
							 {
								 $this->session->set_flashdata('flag','ok');

								 	redirect(base_url().'dashboard_brand');
							 }
							 else{
								 $this->session->set_flashdata('flag','fail');
							 }
						 }
     }

   public function add_brand(){
      $file=@$_FILES['filename'];
     	$this->form_validation->set_rules('name','Name','required');
     	$this->form_validation->set_rules('des','Name','required');
      //$this->form_validation->set_rules('img','image','required');
     	if($this->form_validation->run())
     	{
     		$name=$this->input->post('name');
     		$des=$this->input->post('des');
         if(!empty($_FILES['filename']['name']))
            {
               $config['upload_path']          = './images/brands/';
                $config['allowed_types']        ='png|jpeg|jpg';
                $this->load->library('upload', $config);
                 if ( ! $this->upload->do_upload('filename'))
                {
                 $error = array('error' => $this->upload->display_errors());
                       $this->session->set_flashdata('flag','fail');
                 redirect(base_url().'brand');

                }

                else{
                  $data = array('upload' => $this->upload->data());
                  $upload=$data['upload']['file_name'];

    		$result=$this->brand_model->insertbrand($name,$des,$upload);
    		if($result)
    		{
    			$this->session->set_flashdata('flag','inserted');
    			//echo $result;
    			redirect(base_url().'brand');
    		}
    		else
        {
    			$this->session->set_flashdata('flag','failinserted');
    			//echo $result;
    			redirect(base_url().'brand');

    		}
    	}
    }
    else{
   $this->session->set_flashdata('flag','failinserted');
          //echo $result;
          redirect(base_url().'brand');

    }
  }
      else{
        $data['validaion']=validation_errors();
        // print_r($data['validation']);
          $this->global['pageTitle'] = 'Media : Communiation';
          $this->loadViews("add_brand_view", $this->global, $data, NULL);

      }

    }



    public function delete_brand()
    {
    	$this->form_validation->set_rules('cat_id','','required');
    	if($this->form_validation->run())
    	{
    		$cat=$this->input->post('cat_id');
    		$result=$this->brand_model->delete_brand($cat);
    		if($result)
    		{
    			$this->session->set_flashdata('flag','deleted');
    			redirect(base_url().'brand/');


    		}
    		else{
    				$this->session->set_flashdata('flag','faildeleted');
    			redirect(base_url().'brand/');



    		}


    	}


    }



	}
