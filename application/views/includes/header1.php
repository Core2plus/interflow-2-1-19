

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Interflow</title>
    <link rel="shortcut icon" href="./assets/images/InterFlowCom.png">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- FontAwesome 4.3.0 -->
    <link href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
     <link href="<?php echo base_url(); ?>assets/loader/css/modal-loading.css" rel="stylesheet" type="text/css" />
     <link href="<?php echo base_url(); ?>assets/loader/css/modal-loading-animate.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
    	.error{
    		color:red;
    		font-weight: normal;
    	}
      .canvasjs-chart-container a
      {

       display: none;

      }


    </style>

    <style>
  #backButton {
    border-radius: 4px;
    padding: 8px;
    border: none;
    font-size: 16px;
    background-color: #2eacd1;
    color: white;
    position: absolute;
    top: 10px;
    left: 50px;
    cursor: pointer;

  }
  .invisible {
    display: none;
  }
  .lefted
  {
    float:left;
    margin-left:20px;

  }

</style>

    <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript">
        var baseURL = "<?php echo base_url(); ?>";
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  </head>
  <body class="hold-transition skin-blue sidebar-mini">

    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo base_url(); ?>" class="logo"style="background-color:#222d32;">
          <!-- mini logo for sidebar mini 50x50 pixels -->

          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"style="background-color:#222d32;"><b>Interflow Libaray</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation" style="background-color:#222d32;">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                  <i class="fa fa-history"></i>
                </a>
                <ul class="dropdown-menu">
                  <li class="header"> Last Login : <i class="fa fa-clock-o"></i> <?= empty($last_login) ? "First Time Login" : $last_login; ?></li>
                </ul>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo base_url(); ?>assets/dist/img/avatar.png" class="user-image" alt="User Image"/>
                  <span class="hidden-xs"><?php echo $name; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?php echo base_url(); ?>assets/dist/img/avatar.png" class="img-circle" alt="User Image" />
                    <p>
                      <?php echo $name; ?>
                      <small><?php echo $role_text; ?></small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo base_url(); ?>profile" class="btn btn-warning btn-flat"><i class="fa fa-user-circle"></i> Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo base_url(); ?>logout" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">

            <li class="treeview">
              <a href="<?php echo base_url(); ?>dashboard">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
              </a>
            </li>

            <?php
            if($role == ROLE_ADMIN || $role == ROLE_MANAGER)
            {
            ?>
             <li class="treeview">
              <a href="<?php echo base_url(); ?>adv">
                <i class="fa fa-fw fa-odnoklassniki"></i> <span>Advertisements</span></i>
              </a>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>adv/load_videos">
                <i class="fa fa-dashboard"></i> <span>Videos</span></i>
              </a>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>reading_dir/load_searhing">
                <i class="fa fa-plane"></i> <span>Search</span></i>
              </a>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>category">
                <i class="fa fa-thumb-tack"></i> <span>Category</span></i>
              </a>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>agency">
                <i class="fa fa-plane"></i> <span>Agency</span></i>
              </a>
            </li>

            <li class="treeview">
              <a href="<?php echo base_url(); ?>artist">
                <i class="fa fa-ticket"></i> <span>Artist</span></i>
              </a>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>brand">
                <i class="fa fa-book"></i> <span>Brand</span></i>
              </a>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>product">
                <i class="fa fa-thumb-tack"></i> <span>Product</span></i>
              </a>
            </li>
             <li class="treeview">
              <a href="<?php echo base_url(); ?>sector">
                <i class="fa fa-dashboard"></i> <span>Sector</span></i>
              </a>
            </li>

            <li class="treeview">
              <a href="<?php echo base_url(); ?>reading_dir">
                <i class="fa fa-dashboard"></i> <span>Read File</span></i>
              </a>
            </li>

           <!-- <ul class="sidebar-menu" data-widget="tree">



      <!--   <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span>Back Office</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>program"><i class="fa fa-circle-o"></i> Program </a></li>
            <li><a href="<?php echo base_url(); ?>module"><i class="fa fa-circle-o"></i> Modules </a></li>
            <li><a href="<?php echo base_url('course') ?>"><i class="fa fa-circle-o"></i> Courses </a></li>
            <li><a href="<?php echo base_url('batch') ?>"><i class="fa fa-circle-o"></i> Batches </a></li>
          </ul>
        </li> -->



         <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-odnoklassniki"></i> <span>Student</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>

          <ul class="treeview-menu">
            <li>
            <a href="<?php echo base_url('student') ?>">
              <i class="fa fa-circle-o"></i> Student
            </a>
            </li>

            <li><a href="<?php echo base_url('stddiscount') ?>"><i class="fa fa-circle-o"></i> Student Discount</a></li>
            <li><a href=""><i class="fa fa-circle-o"></i> Report </a></li>
          </ul>
        </li> -->



        <!--  <li class="treeview">
          <a href="#">
            <i class="fa fa-thumb-tack"></i> <span>Enrollement</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>

          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('show_all_enrolled_students') ?>"><i class="fa fa-circle-o"></i> Enrolle Student </a></li>
            <li><a href="<?php echo base_url('fresh_studentd'); ?>"><i class="fa fa-circle-o"></i> Fresh Student </a></li>

          </ul>
        </li>

</ul> -->
             <!-- <li class="treeview">
              <a href="<?php echo base_url('freezunfreez') ?>" >
                <i class="fa fa-plane"></i>
                <span>Freez And UnFreez</span>
              </a>
            </li>

              <li class="treeview">
                  <a href="<?php echo base_url('refund') ?>">
                      <i class="fa fa-ticket"></i>
                      <span> Refund Policy </span>
                  </a>
              </li>

                <li class="treeview">
              <a href="<?php echo base_url('all_expense') ?>" >
                <i class="fa fa-thumb-tack"></i>
                <span>Expence</span>
              </a>
            </li>






  <ul class="sidebar-menu" data-widget="tree">










        <li class="treeview">
         <a href="#">
            <i class="fa fa-book"></i> <span>Report</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="<?php echo base_url('show_paid_reports_bko') ?>">
                <i class="fa fa-circle-o"></i> Paid
              </a>
            </li>
            <li>
              <a href="<?php echo base_url('show_unpaid_reports_bko'); ?>">
                <i class="fa fa-circle-o"></i> UnPaid
              </a>
            </li>
            <li>
              <a href="<?php echo base_url('show_freeze_reports_bko') ?>">
                <i class="fa fa-circle-o"></i> Freeze
              </a>
            </li>
            <li>
              <a href="<?php echo base_url('show_discount_reports_bko') ?>">
                <i class="fa fa-circle-o"></i> Discount
              </a>
            </li>
            <li>
              <a href="<?php echo base_url('show_expense_reports_bko') ?>">
                <i class="fa fa-circle-o"></i> Expense
              </a>
            </li>
            <li>
              <a href="<?php echo base_url('show_voucher_reports_bko') ?>">
                <i class="fa fa-circle-o"></i> Voucher </a>
            </li>

          </ul>
        </li>


      </ul> -->



            <?php
            }
            if($role == ROLE_ADMIN)
            {
            ?>
           <!--  <li class="treeview">
              <a href="<?php echo base_url(); ?>userListing">
                <i class="fa fa-users"></i>
                <span>Users</span>
              </a>
            </li> -->






<!-- <ul class="sidebar-menu" data-widget="tree">

       <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span> Dynamic Reports </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>reports/allReports"><i class="fa fa-address-card-o"></i> Reporting Dashboard </a></li>
                        <li><a href="<?php echo base_url(); ?>reports/students"><i class="fa fa-book"></i>Student Reports </a></li>
                        <li><a href="<?php echo base_url(); ?>reports/singlestd"><i class="fa fa-book"></i> Individuals Reports </a></li>
                        <li><a href="<?php echo base_url(); ?>reports/crs_std"><i class="fa fa-book"></i> Course Based Reports </a></li>
          </ul>
        </li>
 -->

            <?php
            }
            ?>
         <!--  </ul> -->
          </ul>






        </section>
        <!-- /.sidebar -->
      </aside>
    
