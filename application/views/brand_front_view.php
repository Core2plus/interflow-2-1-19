<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>

  body {font-family: Arial;
  background-color:#222d32;}

  .navbar {
    margin-bottom: 0;
    border-radius: 0;
  }


 
  /* Style the tab */
  .tab {
      overflow: hidden;
      border: 1px solid #ccc;
      background-color: #f1f1f1;
  }

  /* Style the buttons inside the tab */
  .tab button {
      background-color: inherit;
      float: left;
      border: none;
      outline: none;
      cursor: pointer;
      padding: 15px 32px;
      transition: 0.3s;
      font-size: 16px;
  }

  /* Change background color of buttons on hover */
  .tab button:hover {
      background-color: #ddd;
  }

  /* Create an active/current tablink class */
  .tab button.active {
      background-color: #ccc;
  }

  /* Style the tab content */
  .tabcontent {
      display: none;
      padding: 15px 32px;
      -webkit-animation: fadeEffect 1s;
      animation: fadeEffect 1s;
  }

  /* Fade in tabs */
  @-webkit-keyframes fadeEffect {
      from {opacity: 0;}
      to {opacity: 1;}
  }

  @keyframes fadeEffect {
      from {opacity: 0;}
      to {opacity: 1;}
  }
  a.btn.btn-primary {
    background-color: #4CAF50;
  border: none;
  color: white;
  padding: 10px 28px;
  text-align: center;
  text-decoration: none;
  font-size: 16px;

  }
  .pull-right{
    margin-left: 62%;
  }
  a.btn.btn-primary:hover {
      box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
  }
  .login-logo{
    margin-top:  4px;
  }
  .jumbotron{
    background: url('./assets/images/ads/light.jpg')
    center center !important;
   background-size: cover !important;
  }
  h1{
    color: #ffffff !important;
  }

  .card {
      /* Add shadows to create the "card" effect */
      box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
      transition: 0.3s;
      padding-left: 0%;
      background-color: #ffffff;
      margin-left: 2%;
      margin-bottom: 2%;
      float: left;
      width: 234px;
      height: 238px;
  }

  /* On mouse-over, add a deeper shadow */
  .card:hover {
      box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
  }


  h4{
    text-align: center;
    font-family: cursive;
  }
  /* Add some padding inside the card container */

  img{
    margin-top: auto;
    object-fit: unset;
  }
  input[type=text] {
      width: 130px;
      -webkit-transition: width 0.4s ease-in-out;
      transition: width 0.4s ease-in-out;
  }

  /* When the input field gets focus, change its width to 100% */
  input[type=text]:focus {
      width: 100%;
  }

  input.btn.btn-danger {
      width: 234px;
  }
  </style>
</head>
<body>
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div class="login-logo">
            <img height="40" width="40" src="<?php base_url(); ?>../assets/images/InterFlowCom.png" >
        </div>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar" style="margin-left: 5%;">
        <ul class="nav navbar-nav">
          <li class="active"><a href="<?php echo base_url().'Login/index2'; ?>">Home</a></li>
          <li><a href="#">About</a></li>
          <li><a href="<?php echo base_url().'Fronts/das_brand'; ?>">Brands</a></li>
          <li><a href="#">Contact</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="<?php echo base_url() ?>login/main"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        </ul>
      </div>
    </div>
  </nav>


  <?php
  if(@$result)
  {
  foreach($result as $k=>$v)
  {
    //echo $v['image'];
    ?>
    <div class="card">

    <form action="<?php echo base_url().'fronts/video_brand';?>" method="POST">
      <input type="hidden" value="<?php echo $v['BrandID'];?>" name="brand"/>
    <img src="<?php echo base_url('images/brands/').$v['image'];?>"  style=" width: 234px; height: 150px;"/>
    <br>
    <input type="submit" value="View" class="btn btn-danger" />
    <h4><b><?php echo $v['BrandName']; ?></b></h4>
</form>
</div>
    <?php



  }
}


  ?>

  <footer class="container-fluid text-center" style="background-color:#222d32;  ">
      <div class="pull-right hidden-xs" style="background-color:#222d32; width: 100%; ">
        <p class="copyright" style="color:white; text-align: left; font-weight: bold; display: inline-block;">Copyright &copy; <a href="http://www.core2plus.com/">Core2Plus</a>.</p>
        <b style="color:white;" >Interflow Communications Designed By: Core2Plus </b>

      
</div>
  </footer>








</body>

</html>
