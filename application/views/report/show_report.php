<?php
if (isset($this->session->userdata['logged_in'])) {
	$mrno = ($this->session->userdata['logged_in']['mrno']);
	$email = ($this->session->userdata['logged_in']['email']);
	$username = ($this->session->userdata['logged_in']['username']);

} else {
header("location: http://localhost/Dah_official_project/");
}

?>


<html>

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>DAH APP</title>

</head>

<body>

<h2> Header Information </h2>

<table width="600" border="1" cellpadding="5">

	<tr>
		<th scope="col">Id</th>

		<th scope="col">Title</th>

		<th scope="col">Hno</th>

		<th scope="col">StreetNo</th>

		<th scope="col">Sector No</th>

		<th scope="col">City</th>

		<th scope="col">Telephone</th>

		<th scope="col">Fax</th>

		<th scope="col">Email</th>

		<th scope="col">Website</th>




	</tr>

	<?php foreach ($headinfo as $u_key){ ?>

		<tr>

			<td><?php echo $u_key->header_id; ?></td>

			<td><?php echo $u_key->main_title; ?></td>


			<td><?php echo $u_key->Hno; ?></td>

			<td><?php echo $u_key->streetNo; ?></td>

			<td><?php echo $u_key->sectorNo; ?></td>

			<td><?php echo $u_key->city; ?></td>

			<td><?php echo $u_key->telephone; ?>

			<td><?php echo $u_key->fax; ?></td>

			<td><?php echo $u_key->email; ?></td>

			<td><?php echo $u_key->website; ?></td>



		</tr>

	<?php }?>

</table>


<h2> MASTER-DETAIL </h2>

<table width="600" border="1" cellpadding="5">

	<tr>
		<th scope="col">Id</th>

		<th scope="col">CATEGORY</th>

		<th scope="col">Test name</th>

		<th scope="col">Result</th>

		<th scope="col">Unit Value</th>

		<th scope="col">Normal Range</th>


	</tr>

	<?php foreach ($user_list as $u_key){ ?>

		<tr>

			<td><?php echo $u_key->report_id; ?></td>

			<td><?php echo $u_key->category; ?></td>

			<td><?php echo $u_key->testname; ?></td>

			<td><?php echo $u_key->result; ?></td>

			<td><?php echo $u_key->unit; ?></td>

			<td><?php echo $u_key->normal; ?></td>


		</tr>

	<?php }?>

</table>



<h2> MASTER </h2>

<table width="600" border="1" cellpadding="5">

	<tr>
		<th scope="col">PID</th>
		<th scope="col">MR #</th>
		<th scope="col">PATIENT NAME</th>
		<th scope="col">EMLOYER</th>
		<th scope="col">GENDER </th>
		<th scope="col">LAB NO</th>
		<th scope="col">REPORT TYPE</th>
		<th scope="col">REPORT DATE</th>
		<th scope="col">REPORT TIME</th>
		<th scope="col">DOB</th>
		<th scope="col">AGE</th>
		<th scope="col">STATUS</th>
		<th scope="col">DELETED DATE</th>




	</tr>

	<?php foreach ($master as $u_key){ ?>

		<tr>

			<td><?php echo $u_key->Pid; ?></td>
			<td><?php echo $u_key->MRno; ?></td>
			<td><?php echo $u_key->Pname; ?></td>
			<td><?php echo $u_key->Employer; ?></td>
			<td><?php echo $u_key->Gender; ?></td>
			<td><?php echo $u_key->labno; ?></td>
			<td><?php echo $u_key->report_type; ?></td>
			<td><?php echo $u_key->report_date; ?></td>
			<td><?php echo $u_key->report_time; ?></td>
			<td><?php echo $u_key->DOB; ?></td>
			<td><?php echo $u_key->Age; ?></td>
			<td><?php echo $u_key->status; ?></td>
			<td><?php echo $u_key->deleted_date; ?></td>


		</tr>

	<?php } ?>

</table>

</body>

</html>
