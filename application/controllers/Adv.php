<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';
include APPPATH . '/third_party/getid3/getid3.php';


class Adv extends BaseController {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
         $this->load->model('adv_model');
         $this->load->model('product_model');
         $this->load->model('main_model');
        $this->isLoggedIn();
    }

	public function index()
    {
      //$this->load->helper('uri');
       if($this->uri->segment(3))
       {
        $segment=$this->uri->segment(3);



       }
       else{
        $segment=0;

       }
       //echo $segment;

        $flag=$this->session->flashdata('flag');

        if($flag=='ok')
        {
            $data['flag']="SuccessFully Updated !";


        }
        elseif($flag=='fail')
        {
            $data['flag']="Something Went Wrong !";


        }
        elseif($flag=='inserted')
        {
            $data['flag']="Successfully Inserted";


        }
        elseif($flag=='failinserted')
        {
            $data['flag']="Insertion Fail";


        }
        elseif($flag=='deleted')
        {
            $data['flag']="Deleted Successfully !";


        }
        elseif($flag=='faildeleted')
        {
            $data['flag']="Deletion Failed";


        }
        $this->load->library ( 'pagination' );




        $result=$this->adv_model->get_adv();


        if($result->num_rows()>0)
        {
           $config ['base_url'] = base_url () .'adv/index';
    $config ['total_rows'] = $result->num_rows();
    //$config ['uri_segment'] = $segment;
    $config ['per_page'] = 5;
    $config ['num_links'] = 3;
//     $config['full_tag_open'] = '<ul class="pagination">';
// $config['full_tag_close'] = '</ul>';
// $config['first_link'] = false;
// $config['last_link'] = false;
// $config['first_tag_open'] = '<li class="page-item">';
// $config['first_tag_close'] = '</li>';
// $config['prev_link'] = '&laquo';
// $config['prev_tag_open'] = '<li class="page-item prev">';
// $config['prev_tag_close'] = '</li>';
// $config['next_link'] = '&raquo';
// $config['next_tag_open'] = '<liclass="page-item">';
// $config['next_tag_close'] = '</li>';
// $config['last_tag_open'] = '<liclass="page-item">';
// $config['last_tag_close'] = '</li>';
// $config['cur_tag_open'] = '<li class="page-item active"><a href="#">';
// $config['cur_tag_close'] = '</a></li>';
// $config['num_tag_open'] = '<li class="page-item">';
// $config['num_tag_close'] = '</li>';
    $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';



    $this->pagination->initialize($config);
    $data['links']=$this->pagination->create_links();
    $category=$this->adv_model->adv_get_paginated_data($segment,5);
    $data['category']=$category;
    $this->global['pageTitle'] = 'Media : Communiation';
    $this->loadViews1("adv_view", $this->global,$data, NULL);
    }

    else
    {
        $data['nodata']="ok";
        $this->global['pageTitle'] = 'Media : Communiation';
        $this->loadViews("adv_view", $this->global,$data,NULL);

    }
  }
   public function addadv()
     {
         $brand=$this->adv_model->get_adv_brand();
            $sector=$this->adv_model->get_adv_sector();
            $artist=$this->adv_model->get_adv_artist();
            $category=$this->adv_model->get_adv_category();
            $product=$this->adv_model->get_adv_product();
            $agency=$this->adv_model->get_adv_agency();
            $data['brand']=$brand;
            $data['sector']=$sector;
            $data['artist']=$artist;
            $data['category']=$category;
            $data['product']=$product;
            $data['agency']=$agency;
            $this->global['pageTitle'] = 'Media : Communiation';
            $this->loadViews("add_adv_view", $this->global, $data, NULL);

    }

public function das_video()
{

   $brand=$this->uri->segment('3');
  $arr=array('brand'=>$brand);
   $this->form_validation->set_data($arr);
   $this->form_validation->set_rules('brand','','required|trim');
   if($this->form_validation->run())
   {
    $videos=$this->adv_model->get_videos_brand($brand);
    if(!empty($videos))
    {
     $this->global['pageTitle'] = 'Media : Communiation';
     $data['videos']=$videos;
      $this->loadViews("brand_video", $this->global, $data, NULL);
    }
    else

    {
      $this->global['pageTitle'] = 'Media : Communiation';
     $data['nodata']='no data';
      $this->loadViews("brand_video", $this->global, $data, NULL);
    }



   }
   else
   {
    // echo validation_errors();
    redirect(base_url().'Brand/das_brand');



   }

 }

  // $video=$this->adv_model->get_videos();
  //           $data['video']=$video;
  // $this->load->model('Adv_model');
  //           $this->global['pageTitle'] = 'Media : Communiation';
  //            $this->loadViews("brand_video", $this->global, $data);
  //


       // }











    public function add_adv()
    {
        $file=@$_FILES['filename'];
        //print_r($file);


        $this->form_validation->set_rules('name','Name','required');
        //$this->form_validation->set_rules('dur','Duration','required');
        //$this->form_validation->set_rules('slot','Slot','required');
        $this->form_validation->set_rules('category','Category','required');
         $this->form_validation->set_rules('product','Product','required');
         // $this->form_validation->set_rules('agency','Agency','required');
         //  $this->form_validation->set_rules('artist','Brand','required');
          //$this->form_validation->set_rules('date','Date','required');
          //$this->form_validation->set_rules('order','Order','required');
           //$this->form_validation->set_rules('cost','Cost','required');
           //$this->form_validation->set_rules('rating','Rating','required');


        if($this->form_validation->run())
        {
            if(!empty($_FILES['filename']['name']))
            {
							$brand=$this->input->post('brand');
							$b=$this->adv_model->get_brand_by_id($brand);
							//print_r($b);
								//$=$this->input->post('brand');


                $config['upload_path']          = './uploads/'.$b[0]['BrandName'].'/';
                $config['allowed_types']        ='mp3|mpeg4|mp4|mov|MOV';
                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('filename'))
                {
            $error = array('error' => $this->upload->display_errors());
            $brand=$this->adv_model->get_adv_brand();
            $sector=$this->adv_model->get_adv_sector();
            $artist=$this->adv_model->get_adv_artist();
            $category=$this->adv_model->get_adv_category();
            $product=$this->adv_model->get_adv_product();
            $agency=$this->adv_model->get_adv_agency();
            $data['brand']=$brand;
            $data['sector']=$sector;
            $data['artist']=$artist;
            $data['category']=$category;
            $data['product']=$product;
            $data['agency']=$agency;
            $this->global['pageTitle'] = 'Media : Communiation';
            //$v=validation_errors('<div class="text-danger">','</div>');
            $data['validation']=$error['error'];


//print_r($data['validation']);
            $this->loadViews("add_adv_view", $this->global, $data, NULL);

                         //print_r($error);
                        //$this->session->set_flashdata('flag','');
               // echo $result;
                //redirect(base_url().'adv/');

                       // $this->load->view('upload_form', $error);
                }
            else
            {

            $data = array('upload' => $this->upload->data());
            $upload=$data['upload']['file_name'];
						//print_r($data);


                 $getID3 = new getID3();
         $filename=$upload;
         $fileinfo = $getID3->analyze($filename);
         //echo '<pre>';print_r($fileinfo);echo '</pre>';

         $width=@$fileinfo['video']['resolution_x'];
         $height=@$fileinfo['video']['resolution_y'];
         $dur=@$fileinfo['quicktime']['moov']['subatoms'][0]['duration'];
         $filenameget=@$fileinfo['filename'];
         $forcc=@$fileinfo['video']['fourcc_lookup'];
         $playtime=@$fileinfo['playtime_seconds'];
         $playstring=@$fileinfo['playtime_string'];
         $rate=@$fileinfo['video']['frame_rate'];
         $comment=@$fileinfo['comments']['language'][0];
         $countframe=@$fileinfo['quicktime']['video']['frame_count'];
         if($countframe!='' || $countframe!=Null)
         {
          $countframe=@$fileinfo['quicktime']['video']['frame_count'];

         }
         else{
          $countframe='';


         }
         $filepath='./uploads/'.$b[0]['BrandName']."/".$upload;
         $format=@$fileinfo['fileformat'];





            $name=$this->input->post('name');
            //$dur=$this->input->post('dur');
            $slot=@$this->input->post('slot');
            $category=@$this->input->post('category');
            $product=@$this->input->post('product');
            $agency=@$this->input->post('agency');
            $date=@$this->input->post('date');
            $artist=@$this->input->post('artist');
            $order=@$this->input->post('category');
            $cost=@$this->input->post('cost');
            $rating=@$this->input->post('rating');



            $result=$this->adv_model->insertadv($name,$dur,$brand,$slot,$category,$product,$agency,$date,$artist,$order,$cost,$rating,$upload,$filename,$width,$height,$forcc,$playtime,$playstring,$rate,$countframe,$filepath,$format,$comment);
            if($result)
            {
               // echo $result;
                $this->session->set_flashdata('flag','inserted');

               redirect(base_url().'adv/');

            }
            else
						{
                $this->session->set_flashdata('flag','failinserted');
               // echo $result;
                redirect(base_url().'adv/');
            }





                        //$this->load->view('upload_success', $data);
                }
            }
            else{
                $brand=$this->adv_model->get_adv_brand();
            $sector=$this->adv_model->get_adv_sector();
            $artist=$this->adv_model->get_adv_artist();
            $category=$this->adv_model->get_adv_category();
            $product=$this->adv_model->get_adv_product();
            $agency=$this->adv_model->get_adv_agency();
            $data['brand']=$brand;
            $data['sector']=$sector;
            $data['artist']=$artist;
            $data['category']=$category;
            $data['product']=$product;
            $data['agency']=$agency;
            $this->global['pageTitle'] = 'Media : Communiation';
            //$v=validation_errors('<div class="text-danger">','</div>');
            $data['validation']='<p class="text-danger">File was Not Present</p>';


//print_r($data['validation']);
            $this->loadViews("add_adv_view", $this->global, $data, NULL);




            }



        }
        //
        else{
            $brand=$this->adv_model->get_adv_brand();
            $sector=$this->adv_model->get_adv_sector();
            $artist=$this->adv_model->get_adv_artist();
            $category=$this->adv_model->get_adv_category();
            $product=$this->adv_model->get_adv_product();
            $agency=$this->adv_model->get_adv_agency();
            $data['brand']=$brand;
            $data['sector']=$sector;
            $data['artist']=$artist;
            $data['category']=$category;
            $data['product']=$product;
            $data['agency']=$agency;
            $this->global['pageTitle'] = 'Media : Communiation';
            $v=validation_errors('<div class="text-danger">','</div>');
            $data['validation']=$v;


//print_r($data['validation']);
            $this->loadViews("add_adv_view", $this->global, $data, NULL);



        }



     }

       public function load_edit_adv()
     {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id','','required');
        if($this->form_validation->run())
        {
            $id=$this->input->post('id');
            $result=$this->adv_model->get_adv_id($id);
             $brand=$this->adv_model->get_adv_brand();
            $sector=$this->adv_model->get_adv_sector();
            $artist=$this->adv_model->get_adv_artist();
            $category=$this->adv_model->get_adv_category();
            $product=$this->adv_model->get_adv_product();
            $agency=$this->adv_model->get_adv_agency();
            $data['brand']=$brand;
            $data['sector']=$sector;
            $data['artist']=$artist;
            $data['category']=$category;
            $data['product']=$product;
            $data['agency']=$agency;


            if(!empty($result))
            {
                //print_r($result);
            $data['cat']=$result;

            $this->global['pageTitle'] = 'Media : Communiation';
         $this->loadViews("editadv_view", $this->global, $data, NULL);
 }
 else
 {
        $this->global['pageTitle'] = 'Media : Communiation';
         $this->loadViews("editadv_view", $this->global, NULL, NULL);

      }

        }

         else
         {
            $v=validation_errors('<div class="text-danger>','</div>');
            $this->session->flashdata('v',$v);
            redirect(base_url().'adv');
         }

}
public function update_adv()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('cat_id','','required');
         $this->form_validation->set_rules('name','Name','required');
        //$this->form_validation->set_rules('dur','Duration','required');
        //$this->form_validation->set_rules('slot','Slot','required');
        //$this->form_validation->set_rules('category','Category','required');
         $this->form_validation->set_rules('product','Product','required');
          //$this->form_validation->set_rules('rating','Rating','required');
          //$this->form_validation->set_rules('agency','Agency','required');
         //  $this->form_validation->set_rules('artist','Brand','required');
          //$this->form_validation->set_rules('date','Date','required');
          //$this->form_validation->set_rules('order','Order','required');
           //$this->form_validation->set_rules('cost','Cost','required');


        if($this->form_validation->run())
        {
            $id=$this->input->post('cat_id');
             $name=$this->input->post('name');
            $dur=$this->input->post('dur');
                $slot=$this->input->post('slot');
            $category=$this->input->post('category');
             $product=$this->input->post('product');
              $agency=$this->input->post('agency');
               $date=$this->input->post('date');
                $artist=@$this->input->post('artist');
                 $order=$this->input->post('category');
                 $rating=$this->input->post('rating');
                  $cost=$this->input->post('cost');

            $result=$this->adv_model->update_adv($name,$dur,$slot,$category,$product,$agency,$date,$artist,$order,$cost,$id,$rating);
            if($result)
            {
                $this->session->set_flashdata('flag','ok');
                redirect(base_url().'adv/');

            }
            else{
                $this->session->set_flashdata('flag','fail');
                redirect(base_url().'adv/');

            }



        }
        else{
            $this->session->set_flashdata('flag','fail');
                redirect(base_url().'adv/');




        }

     }
       public function delete_adv(){
        $this->form_validation->set_rules('cat_id','','required');
        if($this->form_validation->run())
        {
            $cat=$this->input->post('cat_id');
            $result=$this->adv_model->delete_adv($cat);
            if($result)
            {

                $this->session->set_flashdata('flag','deleted');
                redirect(base_url().'adv/');

            }
            else{
                    $this->session->set_flashdata('flag','faildeleted');
                redirect(base_url().'adv/');



            }


        }
        else{
                $this->session->set_flashdata('flag','fail');
                redirect(base_url().'adv/');
        }


    }
        public function detail_adv(){
        $this->form_validation->set_rules('cat_id','','required');
        if($this->form_validation->run())
        {
            $cat=$this->input->post('cat_id');
            $result=$this->adv_model->detail_adv($cat);
            if($result)
            {
                $data['adv']=$result;

                $this->global['pageTitle'] = 'Media : Communiation';
         $this->loadViews("adv_detail_view", $this->global, $data, NULL);

            }
            else{
                $data['nodata']='ok';
                $this->global['pageTitle'] = 'Media : Communiation';
         $this->loadViews("adv_detail_view", $this->global, $data, NULL);




            }


        }
        else
        {
                $this->session->set_flashdata('flag','fail');
                redirect(base_url().'adv/');

        }


    }





    public function load_videos(){
      $dir=$this->adv_model->get_videos();
      if(!empty($dir))
      {
        $data['dir']=$dir;
        $this->global['pageTitle'] = 'Media : Communiation';
         $this->loadViews("video_view", $this->global, $data, NULL);
      }
      else{
        $this->global['pageTitle'] = 'Media : Communiation';
         $this->loadViews("video_view", $this->global, NULL, NULL);


      }



    }







    
    public function load_videos_details(){
      $id=$this->input->post('id');

      $dir=$this->adv_model->get_videos($id);
      if(!empty($dir))
      {
        $data['dir']=$dir;
        $this->global['pageTitle'] = 'Media : Communiation';
        $this->loadViews("video_view", $this->global, $data, NULL);

      }
      else
      {
        $this->global['pageTitle'] = 'Media : Communiation';
         $this->loadViews("video_view", $this->global, NULL, NULL);


      }



    }



}
