
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                    <li class="breadcrumb-item active">All Batchs</li>
                                </ol>
                            </div>
                            <h4 class="page-title">BATCHS</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
    <div class="row" style="margin: 0px;">
        <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div>

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">

                              <a href="<?php echo base_url('Main/newbatch'); ?>">
      <button type="button" class="btn btn-success"><i class="ti-plus"></i></button></a>

                                <h4 class="mt-0 header-title">Batch</h4>
                                
                                <table id="datatable" class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Program</th>
                                        <th>Batch</th>
                                        <th>Module</th>
                                        <th>Start</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>


                                    <tbody>
                                         <?php 
                                        $no = 1;
                                        foreach($show_batch as $show):
                                        ?> 
                                    <tr>
                                         <td><?php echo $no++; ?></td>
                                        <td><?php echo $show->program_name; ?></td> 
                                        <td><?php echo $show->batch_name; ?></td>
                                         <td><?php echo $show->module_name; ?></td>
                                        <td><?php echo $show->startdate; ?></td>
                                        <td><?php if($show->status == 1){
                                            echo "Active";
                                        }
                                        else{
                                            echo "In-Active";
                                        } ?></td>
                                        <td>
    
         <a href="<?php echo base_url('Main/update_batch/'.$show->batch_id); ?>">
      <button type="button" class="btn btn-success "><i class="ti-pencil-alt"></i></button></a>
      
       <a href="<?php echo base_url('Main/delete_batch/'.$show->batch_id); ?>">
      <button type="button" class="btn btn-danger"><i class="ti-cut"></i></button></a>


                                        </td>
                                   </tr>
                               <?php endforeach; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

               

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <?php $this->load->view('BKO/includes/footer'); ?>
