<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Add New Course
        <small>Add / Edit User</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter User Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body table-responsive">
                    <?php $this->load->helper("form"); ?>
                      <form action="<?php echo base_url('insert_course'); ?>" method="post">
                        <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Select Program<span class="required">*</span></label>
                                        
                                        <div>
                                            
                                            <select onchange="get_modules(this.value)" class="form-control" id="sel1" name="program_id">
                                                <option value="">Select</option>
                                          <?php
                                    foreach($all_pro as $sel):
                                    ?>
                                   <option value="<?php echo $sel->program_id; ?>"><?php echo $sel->program_name; ?></option>
                                    <?php
                                        endforeach;
                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                      <?php echo form_error('program_id','<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Select Module<span class="required">*</span></label>
                                        
                                        <div>
                                            
                                            <select class="form-control" id="sel2" name="module_id">
                                                <option value="" >Select</option>
                                          
                                            </select>
                                        </div>
                                    </div>
                                     <?php echo form_error('module_id','<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                                </div>
                                   <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Course Code</label>
                                        <input type="text" name="code" onchange="validate_code(this.value)" class="form-control" placeholder="####"/>
                                         <p id="uniq_error" class="text-danger" style="display: none;">Please Enter Unique Code </p>
                                    </div>
                                     <?php echo form_error('code','<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Course Name</label>
                                        <input type="text" name="name" onchange="validate_name(this.value)" class="form-control" placeholder="Type something"/>
                                          <p id="uniq_error1" class="text-danger" style="display: none;">Please Enter Unique Name </p>
                                    </div>
                                     <?php echo form_error('name','<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Course Fees</label>
                                        <input type="text" name="fees" class="form-control" placeholder="12345"/>
                                    </div>
                                     <?php echo form_error('fees','<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                                </div>
                                                                 
                                    

                               <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Course Duration</label>
                                        <input type="text" name="duration" class="form-control"  placeholder="**-months"/>
                                    </div>
                                     <?php echo form_error('duration','<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Select Status<span class="required">*</span></label>
                                        
                                        <div>
                                            
                                            <select class="form-control" id="sel1" name="sel1">
                                                <option value="" >Select</option>
                                          <option class="form-control" value="1">Enable</option>
                                          <option class="form-control" value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                     <?php echo form_error('sel1','<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                                </div>

                                <div class="box-footer col-md-12">
                                    <div class="form-group">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light" style="width: 10%;">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                </div></div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                      
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
<script >
    function validate_code(v){
        param="code="+v;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     token=this.responseText;
     if(token=='true')
     {
        document.getElementById('uniq_error').style.display="none";






     }
     else if(token=='false')
     {
         document.getElementById('uniq_error').style.display="block";
        



     }
    }
  };
  xhttp.open("POST", "<?php echo base_url().'Validation_controller/validate_code';?>", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(param);


    }

     function validate_name(v){
               param="name="+v;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     token=this.responseText;
     if(token=='true')
     {
        document.getElementById('uniq_error1').style.display="none";






     }
     else if(token=='false')
     {
         document.getElementById('uniq_error1').style.display="block";
        



     }
    }
  };
  xhttp.open("POST", "<?php echo base_url().'Validation_controller/validate_name';?>", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(param);


    }
    function get_modules(p)
    {
      param="name="+p;
      alert(param);
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      alert(this.responseText);
      document.getElementById("sel2").innerHTML=this.responseText;
         

         }
  };
  xhttp.open("POST", "<?php echo base_url().'user/get_module';?>", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(param);
    }
  
    
    

</script>