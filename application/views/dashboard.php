<style>
#preloader {
  position: fixed;
  left: 0;
  top: 0;
  z-index: 99;
  width: 100%;
  height: 100%;
  overflow: visible;
  background: #ffffff
      url('https://loading.io/spinners/typing/lg.-text-entering-comment-loader.gif')
      no-repeat center center;
}
.content-wrapper{

       background: url('./assets/images/ads/img333.jpg')
       center center !important;
      background-size: cover !important;

}
.bg-green{
  border-style: solid;
  border-color: #5D6D7E;
  border-width: 7px;
   background-size: cover !important;
}
.bg-aqua{
  border-style: solid;
  border-color: #5D6D7E;
  border-width: 7px;
   background-size: cover !important;
}
.bg-yellow{
  border-style: solid;
  border-color: #5D6D7E;
  border-width: 7px;
   background-size: cover !important;
}
.bg-red{
  border-style: solid;
  border-color: #5D6D7E;
  border-width: 7px;
   background-size: cover !important;
}
h4{
  color:#000000;
  font-weight: bolder;
   background-color: #ffffff;
   width: auto !important;
    display: inline-block;
}
h3{
  color:#000000;
  font-weight: bolder;
  background-color: #ffffff;
  width: auto !important;
    display: table;
    margin: auto;

}
.small-box-footer{
  color:#000000 !important;
  font-weight: bolder !important;
  background-color: #ffffff !important;
  width: auto !important;
}
</style>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
  <script>
  $(document).ready( function() {
  $('#preloader').delay(5).fadeOut();
});
</script>
<script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
  animationEnabled: true,

  theme: "light1", // "light1", "light2", "dark1", "dark2"
  title:{
    text: "Number of Ads. a/c Rating"
  },
  toolTip:{
        enabled: false   //enable here
      },
  data: [{
    type: "column", //change type to bar, line, area, pie, etc
    //indexLabel: "{y}", //Shows y value on all Data Points
    indexLabelFontColor: "#5A5757",
    indexLabelPlacement: "outside",
    dataPoints: [
      { x: 10, y: 71 },
      { x: 20, y: 55 },
      { x: 30, y: 50 },
      { x: 40, y: 65 },
      { x: 50, y: 92},
      { x: 60, y: 68 },
      { x: 70, y: 38 },

    ]
  }]
});
chart.render();

var chart1 = new CanvasJS.Chart("chartContainer1", {
  animationEnabled: true,

  theme: "light1", // "light1", "light2", "dark1", "dark2"
  title:{
    text: "No. By Agency"
  },
  toolTip:{
        enabled: false   //enable here
      },
  data: [{
    type: "pie", //change type to bar, line, area, pie, etc
    //indexLabel: "{y}", //Shows y value on all Data Points
    indexLabelFontColor: "#5A5757",
    indexLabelPlacement: "outside",
    dataPoints: [
      { x: 10, y: 71 },
      { x: 20, y: 55 },
      { x: 30, y: 50 },
      { x: 40, y: 65 },
      { x: 50, y: 92},
      { x: 60, y: 68 },
      { x: 70, y: 38 },

    ]
  }]
});
chart1.render();

var chart2 = new CanvasJS.Chart("chartContainer2", {
  animationEnabled: true,

  theme: "light1", // "light1", "light2", "dark1", "dark2"
  title:{
    text: "Student Fee Statistics"
  },
  toolTip:{
        enabled: false   //enable here
      },
  data: [{
    type: "area", //change type to bar, line, area, pie, etc
    //indexLabel: "{y}", //Shows y value on all Data Points
    indexLabelFontColor: "#5A5757",
    indexLabelPlacement: "outside",
    dataPoints: [
      { x: 10, y: 71 },
      { x: 20, y: 55 },
      { x: 30, y: 50 },
      { x: 40, y: 65 },
      { x: 50, y: 92},
      { x: 60, y: 68 },
      { x: 70, y: 38 },

    ]
  }]
});
chart2.render();
var chart3 = new CanvasJS.Chart("chartContainer3", {
  animationEnabled: true,

  theme: "light1", // "light1", "light2", "dark1", "dark2"
  title:{
    text: "Agency Cost"
  },
  toolTip:{
        enabled: false   //enable here
      },
  data: [{
    type: "area", //change type to bar, line, area, pie, etc
    //indexLabel: "{y}", //Shows y value on all Data Points
    indexLabelFontColor: "#5A5757",
    indexLabelPlacement: "outside",
    dataPoints: [
      { x: 10, y: 71 },
      { x: 20, y: 55 },
      { x: 30, y: 50 },
      { x: 40, y: 65 },
      { x: 50, y: 92},
      { x: 60, y: 68 },
      { x: 70, y: 38 },

    ]
  }]
});
chart3.render();
}



</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 style="color:white;">
        <i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard
        <small style="color:white;">Control panel</small>
      </h1>
    </section>
    <div id="preloader"></div>
    <section class="content">
        <div class="row">

            <div class="col-lg-3 col-xs-6">
              <div class="small-box bg-aqua" style="opacity:0.9; height:150px;">
                <div class="inner">
                   <h4>#</h4>
                  <h3>220</h3>
                </div>
              </a>
                <a href="<?php echo base_url('all') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
              <div class="small-box bg-green" style="opacity:0.9; background-image: url(http://localhost/InterFlow2/assets/images/brand/brand.jpg); height:150px;">
                <div class="inner">
                <h4>Number of Brands</h4>
                <h3>220</h3>
            </div>
          <a href="<?php echo base_url('Brand/das_brand') ?>" class="small-box-footer">
   More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>

            <div class="col-lg-3 col-xs-6">
              <div class="small-box bg-yellow" style="opacity:0.9;height:150px;">
                <div class="inner">
                  <h4>#</h4>
                  <h3>220</h3>
                </div>
                <a href="<?php echo base_url().'batchstat';?>" class="small-box-footer"> CA Fee <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>

            <div class="col-lg-3 col-xs-6">
              <div class="small-box bg-red" style="opacity:0.9;height:150px;">
                <div class="inner">
                  <h4>#</h4>
                  <h3>220</h3>
                </div>
                <a href="<?php echo base_url().'Analytics/agency_cost';?>" class="small-box-footer" >More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>

            <div class="col-lg-3 col-xs-6">
              <div class="small-box bg-red" style="opacity:0.9;height:150px;">
                <div class="inner">
                  <h4>#</h4>
                  <h3>220</h3>
                </div>
                <a href="<?php echo base_url().'Analytics/';?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>

            <div class="col-lg-3 col-xs-6">
              <div class="small-box bg-green" style="opacity:0.9;height:150px;">
                <div class="inner">
                   <h4>#</h4>
                  <h3>220</h3>
                </div>
                <a href="<?php echo base_url().'Analytics/agency_adv'?>" class="small-box-footer" >More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>

            <div class="col-lg-3 col-xs-6">
              <div class="small-box bg-aqua" style="opacity:0.9;height:150px;">
                <div class="inner">
                   <h4>#</h4>
                  <h3>220</h3>
                </div>
                <a href="<?php echo base_url().'drilreport'?>" class="small-box-footer" >More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>


    </section>
</div>
