
<!DOCTYPE html>

<html>

<?php
if (isset($this->session->userdata['logged_in'])) {
?>

<head>
    <title>X-ray</title>
</head>
<style>
    p.c {
        word-spacing: 1cm;
    }
</style>
<body>
<?php foreach ($headinfo as $u_key){ ?>
<div class="col-md-12">
<img src="<?php base_url(); ?>assets/assets/img/logo.jpg" alt="" style="    margin-left: 15%;">
<h2 style="color: green; padding-left: 30%;    margin-top: -3%;">
<?php echo $u_key->main_title; ?>
</h2>
</div>

<hr style="border: 1px solid black">
<hr style="border: 3px solid black">

<p style="margin-left: 15%;font-size: 16px;    margin-bottom: -1%;    font-size: 15px;    margin-top: -5px;"><?php echo $u_key->Hno; ?><?php echo $u_key->streetNo; ?><?php echo $u_key->sectorNo; ?><?php echo $u_key->city; ?><?php echo $u_key->telephone; ?><?php echo $u_key->fax; ?></p></p>
<p style="margin-left: 16%;    font-size: 15px;"><?php echo $u_key->email; ?><?php echo $u_key->website; ?></p>

<?php } ?>
<!---->
<?php foreach ($master as $u_key){ ?>
<a href="#" style="color: Black;">
<h3 style="color: Black; margin-left: 38%">X-RAY REPORT</h3>
</a>

<p style="    font-size: 11px;    font-family: sans-serif;"><b>No </b>: <?= $u_key->cno; ?></p>

<p style="    font-size: 11px;    font-family: sans-serif;"><b>MR # : </b>: <?= $u_key->mrno; ?></p>

<p style="    font-size: 11px;    font-family: sans-serif;"><b>NAME : </b>: <?= $u_key->patient_name; ?></p>

<p style="    font-size: 11px;    font-family: sans-serif;"><b>EMPLOYER: </b>: <?= $u_key->employer; ?></p>

<p style="    font-size: 11px;    font-family: sans-serif;"><b>DATE : </b>: <?= $u_key->report_date; ?></p>

<p style="    font-size: 11px;    font-family: sans-serif;"><b>X-RAY : </b>: <?= $u_key->xray_type; ?></p>

<br>

<p style="    font-size: 11px;    font-family: sans-serif;    margin-bottom: 26%;"><b>REPORT : </b>: <?= $u_key->xray_report_remarks; ?></p>

<?php }?>

<hr style="border: 1px solid black">

   <p style="font-size: 10px; font-family: cursive;    margin-left: 10%;"><b>Dr. Agha Muhammad Sami Khan</b> <b style="   margin-left: 55%;">Dr. Syed Ahmed Abdullah</b></p>

   <p style="font-size: 10px; font-family: cursive;    margin-left: 15%;">M.B.B.S, MCPS, <j style="    margin-left: 63%;">MBBS, DDU (NUST), FRCRA (UK)</j> </p>
   <p style="font-size: 10px; font-family: cursive;    margin-left: 10%;    margin-top: -8px;"> (M.Phil - Diagnostic Radiology) <j style="    margin-left: 56%;">Consultant Radiologist & Sonologist</j> </p>
   <p style="font-size: 10px; font-family: cursive;    margin-left: 8%;    margin-top: -8px;"> Senior Fellowship in Diagnostic Radiology Post </p>
   <p style="font-size: 10px; font-family: cursive;    margin-left: 10%;    margin-top: -8px;"> Graduate Medical Institute, Singapore</p>
   <p style="font-size: 10px; font-family: cursive;    margin-left: 10.2%;    margin-top: -8px;"> Consultant Radiologist & Sonologist </p>

<div class="col-md-12">
<!--<img src="img1.png" alt="" style="margin-left: 3%;    height: 60px;-->
    <!--width: 60px;">-->
    <img src="<?php base_url(); ?>assets/assets/img/img1.jpg" alt="" style="padding-left: 85%;    height: 60px;
    width: 60px;">

</div>

<?php foreach ($footinfo as $row): ?>
	<p style="margin-top: -1%;%">This is a verified computer generated report. It does not need a stamp or a signature.</p>
	<p style="margin-top: -1%;%"><em><?= $row->disclaimer; ?></em></p>
	<p style="margin-left: 60%;"><em><?= $row->report_no; ?> <?= $row->registration_no; ?></em></p>

<?php endforeach; ?>


</body>
</html>

<?php
} else { header(base_url()); } ?>
