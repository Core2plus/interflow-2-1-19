<?php
class Artist_model extends CI_Model{
	public function get_artist()
	{
		$this->db->select('*');
		$this->db->from('artistprofile');
		$result=$this->db->get();
		return $result->result_array();


	}
	public function get_artist_img($img)
{
	$this->db->select('*');
	$this->db->from('artistprofile');
	$this->db->where('artist_image',$img);
	$result=$this->db->get();
	return $result->result_array();
}


	public function get_artist_id($id)
 	{
 		$this->db->select('*');
 		$this->db->from('artistprofile');
 		$this->db->where('ArtistID',$id);
 		$result=$this->db->get();
 		return $result->result_array();


 	}
	public function update_artist($id,$name,$lastname,$gender,$age,$rate,$role,$desc,$img)
 	{
 		$data=array('Name'=>$name,'LastName'=>$lastname,'Gender'=>$gender,'Age'=>$age,'Rate'=>$rate,'Role'=>$role,'Description'=>$desc,'artist_image'=>$img);
 		$this->db->where('ArtistID',$id);
 		$result=$this->db->update('artistprofile',$data);
 		return $result;



 	}
	public function insertartist($name,$lastname,$gender,$age,$rate,$role,$desc,$img){
		$data=array('Name'=>$name,'LastName'=>$lastname,'Gender'=>$gender,'Age'=>$age,'Rate'=>$rate,'Role'=>$role,'Description'=>$desc,'artist_image'=>$img);
		$result=$this->db->insert('artistprofile',$data);
		return $result;
	}
	public function delete_artist($cat)
	{
		$this->db->where('ArtistID', $cat);
$this->db->delete('artistprofile');

}
public function get_artist_data($id)
{
	$result=$this->db->select('*')->from('artistprofile')->where('ArtistID',$id)->get();
	return $result->result_array();
}



}
?>
