<!DOCTYPE html>
<html>

<head>
    <title> <?= $title ?> </title>
</head>
<body>

<div class="col-md-12">

    <?php foreach ($cinfo as $u_key){ ?>
    <img src="<?php base_url(); ?>assets/images/faviicon.png" style="height: 60px; width: 60px; margin-top: 1%;">
    <h1 style="color: green; text-align: center;    margin-top: -6%;"> <?php echo $u_key->companyName; ?>  </h1>

    <?php date_default_timezone_set("Asia/Karachi"); ?>
    <p style="text-align: right; margin-left: 8%; font-size: 12px;"><?= date("h:i:sa"); ?> <?= date('d-m-Y'); ?></p>


    <p style="text-align: center; margin-top: -120px; font-weight: bold;"><?= $title ?>    </p>
    <hr>

</div>

<section class="content">
<div class="row"  >
        <div class="col-sm-12" >
          <div class="box">
            <div class="box-body">
                    <div style="overflow-x:auto;">
                      <table id="example2" class="table table-bordered table-hover" style="width: 100%; ">
    <thead>
      <tr style="background-color: #e0e7f7">
        <th>NO</th>
          <th>Student ID
          <th>Student Name</th>
        <th>Course Name</th>
        <th>Course Code</th>
        <th>Fee</th>
        <th>Discount%</th>
        <th>Discount amount</th>
        <th>Received amount</th>
        <th>Payment</th>
      </tr>
    </thead>
    <tbody>
     
       <?php 
       $no=1;
       $a=0;
       $b=0;
       $c=0;
         $f=0;

        foreach($result as $show) {
       ?>
      
        <tr>
        <td><?php echo $no++;   ?></td>
            <td><?= $show->artt_id;  ?></td>
            <td><?= $show->fname;  ?></td>
            <td> <?php echo $show->coursename;   ?> </td>
        <td><?php echo $show->coursecode;   ?></td>
        <td><?php echo $show->coursefee; $f=$f+$show->coursefee;    ?></td>
        <td><?php echo $show->discount_per;   ?></td>
        <td><?php if($show->discount_per!=null){$c=($show->discount_per)/100;}  echo $d=$show->coursefee*$c; $b=$b+$d;   ?></td>
        <td><?php $e=$show->received_amount+$d; echo $show->received_amount;  $a=$a+$show->received_amount; ?></td>
     
        <td >
        <?php if(($show->Paid == "1") || ($e>=$show->coursefee)) {?> 

          <span>Paid</span>
          <?php }else{ $a=$a+$show->coursefee; ?>
            <span>Unpaid</span>
            <?php }?>


        </td>
      </tr>

          <?php }?>
      </tbody>
  </table><br>
</div></div></div></div>
    <!-- /.row -->

</section>

<!--<hr style="border: 3px solid black; margin-top: -4px;">-->

<div style="z-index: 100; position: fixed; bottom: 38px;">
    <hr>
    <p style="font-size: 13px;    margin-bottom: -1%; text-align: center"><?php echo $u_key->aboutCompany; ?></p>
    <p style="text-align: center; font-size: 13px; padding-top: -5px;"><?php echo $u_key->website;?>&nbsp;&nbsp;<?php echo $u_key->contactNo; ?>&nbsp;&nbsp;<?php echo $u_key->address; ?>&nbsp;&nbsp;<?php echo $u_key->email; ?></p>
</div>

<?php } ?>

</body>
</html>