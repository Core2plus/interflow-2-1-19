<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class All_model extends CI_Model{
	public function get_artist()
	{
		$this->db->select('*');
		$this->db->from('artistprofile');
		$result=$this->db->get();
		return $result->result_array();


	}

	public function get_artist_id($id)
 	{
 		$this->db->select('*');
 		$this->db->from('artistprofile');
 		$this->db->where('ArtistID',$id);
 		$result=$this->db->get();
 		return $result->result_array();


 	}
	public function update_artist($id,$name,$lastname,$gender,$age,$rate,$role,$desc)
 	{
 		$data=array('Name'=>$name,'LastName'=>$lastname,'Gender'=>$gender,'Age'=>$age,'Rate'=>$rate,'Role'=>$role,'Description'=>$desc);
 		$this->db->where('ArtistID',$id);
 		$result=$this->db->update('artistprofile',$data);
 		return $result;



 	}
	public function insertartist($name,$lastname,$gender,$age,$rate,$role,$desc){
		$data=array('Name'=>$name,'LastName'=>$lastname,'Gender'=>$gender,'Age'=>$age,'Rate'=>$rate,'Role'=>$role,'Description'=>$desc);
		$result=$this->db->insert('artistprofile',$data);
		return $result;



	}
	public function delete_artist($cat)
	{
		$this->db->where('ArtistID', $cat);
$this->db->delete('artistprofile');

}
public function get_videos(){
	$this->db->select('*');
	$this->db->from('videoinfo');
	$result=$this->db->get();
	return $result->result_array();
} 



}
?>
