

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> ALL Modules
        <small>Add, Edit, Delete</small>
      </h1>
    </section>

 <div class="row" style="margin: 0px;">
        <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div>

    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url() ?>moduleAdd"><i class="fa fa-plus"></i> Add New</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">ALL Modules</h3>
                  
                </div><!-- /.box-header -->
                <div class="box-body table-responsive ">

                     <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                       <th>ID</th>
                        <th>Program Name</th>
                        <th>Module Name</th>
                        <th>Status</th>
                        
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                 <?php
                  $no = 1;
                   foreach ($data as $row) {
                     
                   ?>
               
                <tr>
                  <td><?php echo $no++; ?></td>
                        <td><?php echo $row['program_name']; ?></td>
                        <td><?php echo $row['module_name']; ?></td>
                        <td><?php if($row['mod_status'] == 1){
                                            echo "Active";
                                        }
                                        else{
                                            echo "In-Active";
                                        } ?></td>
                       
                        <td class="text-center">
                                                                  
                            <a class="btn btn-sm btn-info" href="<?php echo base_url('editmodule'); ?>?id=<?php echo $row['module_id'] ?> " title="Edit"><i class="fa fa-pencil"></i></a>
                            <a class="btn btn-sm btn-danger deleteUser" href="<?php echo base_url('deleteModule') ?>?id=<?php echo $row['module_id'] ?>" data-userid="" title="Delete"><i class="fa fa-trash"></i></a>
                        </td>
                        <?php } ?>
                  
                </tr>
              
 
              
                </tbody>
                
              </table>
                  
               
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>















