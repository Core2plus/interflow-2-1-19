
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="offset-sm-1 col-sm-10">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                   
                                    <li class="breadcrumb-item active">Student Profile</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Student Profile</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            
                <div class="row">
                  <div class="offset-lg-1 col-lg-10">
                      <div class="card">
                          <div class="card-body row">
                              <div class="col-md-3">
                                 <?php  foreach($show_teacher_profile->result() as 
                                    $show_teacher_profile) { ?>
                                <img src="<?php echo base_url(); ?>assets/images/users/<?php echo $show_teacher_profile->image; ?>"  " class="img-circle" width="150px;" height="150px;">
                              </div>
                              <div class="col-md-9">
                                <form>
                                  <div class="row">

                                   
                                    <label class="col-md-3">Student Name: </label>
                                    <p><?php echo $show_teacher_profile->first_name; ?></p> &nbsp;&nbsp;
                                    <p><?php echo $show_teacher_profile->middle_name; ?></p>&nbsp;&nbsp;
                                    <p><?php echo $show_teacher_profile->last_name; ?></p>
                                  </div>

                                  <div class="row">
                                    <label class="col-md-3">Father Name: </label>
                                    <p><?php echo $show_teacher_profile->father_name; ?></p>
                                  </div>

                                  <div class="row">
                                    <label class="col-md-3">Phone Number: </label>
                                    <p><?php echo $show_teacher_profile->number; ?>r</p>
                                  </div>

                                  <div class="row">
                                    <label class="col-md-3">Address: </label>
                                    <p><?php echo $show_teacher_profile->permanent_address; ?></p>
                                  </div>

                                  <div class="row">
                                    <label class="col-md-3">Program Name: </label>
                                    <p>Program Name</p>
                                  </div>

                                  <div class="row">
                                      <label class="col-md-3">CNIC: </label>
                                      <p><?php echo $show_teacher_profile->cnic; ?></p>
                                  </div>

                                  <div class="row">
                                    <label class="col-md-3">Gender: </label>
                                    <p><?php echo $show_teacher_profile->gender; ?></p>
                                  </div>

                                  <div class="row">
                                    <label class="col-md-3">Email: </label>
                                    <p><?php echo $show_teacher_profile->email; ?></p>
                                  </div>

                                  <div class="row">
                                    <label class="col-md-3">Date of Joining: </label>
                                    <p><?php echo $show_teacher_profile->doj; ?></p>
                                  </div>

                                  <div class="row">
                                    <label class="col-md-3">Date of Birth: </label>
                                    <p><?php echo $show_teacher_profile->dob; ?></p>

                                  <?php } ?>
                                  </div>
                                </form>
                              </div>
                          </div>
                      </div>
                  </div> <!-- end col -->
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("#addMore").click(function(){
        $("#addagain").clone().appendTo("#addmore_entry");
        return false;   
    }); 
});

/*
$(document).ready(function(){
    $("#addMoreremove").click(function(){
       
      
        $("#addagain").remove();

        return false;   
      
        
    }); 

   
});
</script>
<script type="text/javascript">
  function title_change()
  {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('GET',"ajax?title="+document.getElementById('title_tb').value,false);
    xmlHttp.send(null);
  document.getElementById('sub_title').innerHTML=xmlHttp.responseText;
  //alert(xmlHttp.responseText);
}
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">
  function course_change()
  {
    alert(document.getElementById('sub_title').value);
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('GET',"ajax1?module="+document.getElementById('sub_title').value,false);
    xmlHttp.send(null);
  document.getElementById('course').innerHTML=xmlHttp.responseText;
  //alert(xmlHttp.responseText);
}
</script>
        <!-- Footer -->
       
<?php $this->load->view('BKO/includes/footer'); ?>
     