  
<div class="content-wrapper">
<section class="content-header">
      <h1><i class="fa fa-users"></i> Add New Student</h1>
    </section>
    <!-- Main content -->
    <section class="content">

      <div class="row">
       
        <!-- /.col -->
        <div class="col-md-8" style="margin-left: 15%; margin-top: 6%;">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#activity" data-toggle="tab"><b>Student Details</b></a></li>
              <li><a href="#timeline" data-toggle="tab"><b>Guardian</b></a></li>
              
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
               
              <div class="tab-pane" id="settings">
                <form action="<?php echo base_url('insertstudent') ?>" method="post" enctype="multipart/form-data" class="form-horizontal"  >
                  <div class="row">
                    <label for="inputName" class="col-sm-2 control-label">CR Number</label>
                    <div class="col-sm-4">
                      <input type="hidden" name="artt_id" class="form-control"  placeholder="Artt ID">
                      <input type="text" name="cr" class="form-control"  placeholder="CR#">
                    </div>
                  
                    <label for="inputEmail" class="col-sm-2 control-label">Student Name</label>
                    <div class="col-sm-4">
              <input type="text" name="fname" class="form-control"   placeholder="Enter Student Name"/>
                    </div>
                  </div><br>


                  <div class="row">
                    <label for="inputName" class="col-sm-2 control-label">CNIC</label>
                    <div class="col-sm-4">
                       <input type="text"  name="cnic" class="form-control"  placeholder="Student CNIC">
                    </div>
                  


                    <label for="inputSkills" class="col-sm-2 control-label">Date of Joining</label>

                    <div class="col-sm-4">
                      <input type="date" name="DOJ" class="form-control" >
                    </div>
                  </div><br>





                     <div class="row">
                    <label class="col-sm-2 control-label"><span style = " font-size: 14.8px;">Student Status</span></label>

                    <div class="col-sm-4">
                <span>UnFreeze &nbsp;

  <input type="radio" name="fstatus"   value = "UnFreeze" onclick="myFunction2()" id="payment_with">
                    
                    </span>
                     <span >Freeze &nbsp;

  <input type="radio" name="fstatus"  value = "Freeze" onclick="myFunction3()"   id="payment_with">

                    </span>
                    <br/>


      <span> Ragular &nbsp;
  
            <input type="radio" name="fstatus" checked value = "Ragular" id="payment_with">

        </span>
                    </div>
                  



                                          
                   

                    
                      <span style=" display: none;" id="cheque_number">
                                         
                                         <label class="col-sm-2">Student Note </label>
                                         <div class="col-sm-4">
                                            <input type="text"  class="form-control" name="freeze_note"   >
                                         </div></span>
                    </div><br>
                  

                                
                                


               
                  


                  <div class="row">
                    <label for="inputSkills" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-4">
                      <input type="email" name="email" class="form-control"  placeholder="Student Email">
                    </div>
                  


                  
                    <label for="inputSkills" class="col-sm-2 control-label">Gender</label>

                    <div class="col-sm-4">
                     <label class="radio-inline">&nbsp;
                                                      <input type="radio" name="optradio" value="Male" checked>Male
                                                    </label>&nbsp;&nbsp;
                                                    <label class="radio-inline">&nbsp;
                                                      <input type="radio" name="optradio" value="Female">Female
                                                    </label>&nbsp;&nbsp;
                    </div>
                  </div><br>



                  <div class="row">
                    <label for="inputSkills" class="col-sm-2 control-label">Number</label>

                    <div class="col-sm-4">
                      <input type="text" name="number" class="form-control"  placeholder="0000-0000000">
                    </div>

                    
                    <label for="inputSkills" class="col-sm-2 control-label">Profile Picture</label>
                    <div class="col-sm-4">
                      <input type="file" name="image_student" class="form-control-file form-group" style="width: 100%; margin-left: 1%; margin-top: 2%;">
                    </div>
                  </div>
                  

                  <!--  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Permanant Address</label>

                    <div class="col-sm-10">
                      <inpur type="text" class="form-control" name= "permanent_address"  id="comment" placeholder="Enter Your Domicile Address">
                    </div>
                  </div> -->

                  
                 
                
                
              </div>
              </div>
              
              <!-- /.tab-pane -->
              


              <div class="tab-pane" id="timeline" >
                <!-- The timeline -->
             <div class ="tab-content form-group">

              
                  <div class="row">
                    <label for="inputName" class="col-sm-2 control-label">Father Name</label>
                    <div class="col-sm-4">
                     <input type="text"  name="father_name" class="form-control"  placeholder="Father Name">
                    </div>
                  


                  
                    <label for="inputEmail" class="col-sm-2 control-label">Father Email</label>
                    <div class="col-sm-4">
                     <input type="text" name="father_email" class="form-control"  placeholder="Father Email">
                    </div>
                  </div><br>



                  <div class="row">
                    <label for="inputName" class="col-sm-2 control-label">Father CNIC</label>
                    <div class="col-sm-4">
                     <input type="text" name="father_cnic" class="form-control"  placeholder="Father CNIC">
                    </div>
                  


                  
                    <label for="inputName" class="col-sm-2 control-label">Fathers Phone</label>
                    <div class="col-sm-4">
                     <input type="text" name="father_phone" placeholder="Phone" class="form-control" >
                    </div>
                  </div><br>

                   <div class="row">
                    <label for="inputName" class="col-sm-2 control-label">Father Profession</label>
                    <div class="col-sm-4">
                      <input type="text" name="father_profession" class="form-control"  placeholder="Father's Profession">
                    </div>
                  </div>


                
                 
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-12">
                      <input type="submit" name="submit" value="Submit" class="btn btn-primary" style="width: 15%;">
                    </div>
                  </div><br>
                </form>
              </div>
              </div>
              <!-- /.tab-pane -->

    </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      
      <!-- /.row -->

    </section>

  </div>

  <script>
    function course_change()
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('GET','ajax_student_courses?batch_id='+document.getElementById('batch_id').value,false);
        xmlhttp.send(null);
        
        document.getElementById('course').innerHTML=xmlhttp.responseText;
    }
</script>
             <script>



function myFunction2() {

 
   
  document.getElementById('cheque_number').style.display ='none';

    }
 function myFunction3() {
       document.getElementById('cheque_number').style.display ='block';

    }
    


</script>
