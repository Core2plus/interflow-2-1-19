<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Edit Batch Detail
        <small>Add / Edit User</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Update User Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body table-responsive">
                    <?php $this->load->helper("form"); ?>
                                            
                                <?php 
                                foreach($all_batch as $show):
                                ?>                
                                <form action="<?php echo base_url('updatebatch') ?>" method="post">

                                     <input type="hidden" name="text_hid" value="<?php echo $show->batch_id; ?>">
                                     <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Program<span class="required">*</span></label>
                                        <div>
                                            <select class="form-control" id="program_id" name="program_id">
                                               
                                              <?php
                                            foreach($show_program_for_update_batch as $pro):
                                            ?>
                                                <option value="<?php echo $pro->batch_id; ?>"><?php echo $pro->program_name ?></option>


                                                   <?php
                                                    endforeach;
                                                ?>
                                            <?php
                                            foreach($all_pro as $pro):
                                            ?>
                    
                                          <option class="form-control" value="<?php echo $pro->program_id; ?>"><?php echo $pro->program_name ?></option>
                                                
                                                <?php
                                                    endforeach;
                                                ?>

                                          </select>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label>Module<span class="required">*</span></label>
                                        <div>
                                            <select class="form-control" id="module_id" name="module_id">
                                               
                                                

    
                                              <?php
                                            foreach($show_mod_for_update_batch as $pro):
                                            ?>
                                                <option value="<?php echo $pro->module_id; ?>"><?php echo $pro->module_name ?></option>


                                                   <?php
                                                    endforeach;
                                                ?>

                                            <?php
                                            foreach($show_mod as $pro):
                                            ?>
                    
                                          <option class="form-control" value="<?php echo $pro->module_id; ?>"><?php echo $pro->module_name ?></option>
                                                
                                                <?php
                                                    endforeach;
                                                ?>

                                          </select>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control" value="<?php echo $show->batch_name; ?>" />
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Start</label>
                                        <div>
                                            <input type="date" class="form-control" name="doj" 
                                                   value="<?php echo $show->startdate; ?>"/>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label>End</label>
                                        <div>
                                            <input type="date" class="form-control" name="doe" 
                                                   value="<?php echo $show->enddate; ?>"/>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                     <div class="form-group">
                                        <label>Select Status<span class="required">*</span></label>
                                        <div>
                                            <select class="form-control" id="sel1" name="sel1">
                                                <option>Select</option>
                                          <option class="form-control" value="1">Enable</option>
                                          <option class="form-control" value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                    </div>
                                    </div>
                                    <!--
                                    <div class="form-group">
                                        <label>Number</label>
                                        <div>
                                            <input data-parsley-type="number" type="text"
                                                   class="form-control" required
                                                   placeholder="Enter only numbers"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Alphanumeric</label>
                                        <div>
                                            <input data-parsley-type="alphanum" type="text"
                                                   class="form-control" required
                                                   placeholder="Enter alphanumeric value"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Textarea</label>
                                        <div>
                                            <textarea required class="form-control" rows="5"></textarea>
                                        </div>
                                    </div>-->
                                    <div class="box-footer col-md-12">
                                    <div class="form-group">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light" style="width: 10%;">
                                                Update
                                            </button>
                                           
                                        </div>
                                        </div>
                                    </div>
                                </form>
                               
<?php endforeach; ?>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>