<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = "login";
$route['404_override'] = 'error';


/*********** USER DEFINED ROUTES *******************/
$route['program'] = 'user/program';
$route['addprogram'] = 'user/addprogram';
$route['insertprogram'] = 'user/insertprogram';
$route['editprogram'] = 'user/editprogram';
$route['updateprogram'] = 'user/updateprogram';
$route['deleteprogram'] = 'user/deleteprogram';




//MODULE
$route['module'] = 'user/Module';
$route['moduleAdd'] = 'user/moduleAdd';
$route['insertModel'] = 'user/insertModel';
$route['editmodule'] = 'user/editmodule';
$route['updatemodule'] = 'user/updatemodule';
$route['deleteModule'] = 'user/deleteModule';

//COURSES
$route['course'] = 'user/course';
$route['editCourse'] = 'user/editCourse';
$route['addcourse'] = 'user/addcourse';
$route['update_cor'] = 'user/update_cor';
$route['insert_course'] = 'user/insert_course';
$route['deletecourse'] = 'user/delete_course';

//BATCHES
$route['batch'] = 'user/batch';
$route['editbatch'] = 'user/editbatch';
$route['updatebatch'] = 'user/updatebatch';
$route['addbatch'] = 'user/addbatch';
$route['insertbatch'] = 'user/insertbatch';
$route['deletebatch'] = 'user/deletebatch';

//STUDENT
$route['student'] = 'user/student';
$route['editstd'] = 'user/editstd';
$route['updatestd'] = 'user/updatestd';
$route['profilestd'] = 'user/profilestd';
$route['addstd'] = 'user/addstd';
$route['insertstudent'] = 'user/insertstudent';
$route['coursesregisterstudent'] = 'user/coursesregisterstudent';
$route['insert_student_courses'] = 'user/insert_student_courses';
$route['enrollestuden'] = 'user/enrollestuden';
$route['editEnrollstd'] = 'user/editEnrollstd';
$route['insert_new_enroll_student'] = 'user/insert_new_enroll_student';
$route['update_new_enroll_student'] = 'user/update_new_enroll_student';
$route['update_update_new_enroll_student'] = 'user/update_update_new_enroll_student';
$route['delete_enroll_student'] = 'user/delete_enroll_student';
$route['stdvoucher'] = 'user/stdvoucher';
$route['voucher_insert'] = 'user/voucher_insert';
$route['delete_student'] = 'user/delete_student';
$route['disscountInsert'] = 'user/disscountInsert';
$route['insert_discount'] = 'user/insert_discount';
$route['show_all_enrolled_students'] = 'user/show_all_enrolled_students';
$route['enroll_std'] = 'user/enroll_std';
$route['insert_enroll_student'] = 'user/insert_enroll_student';
$route['show_data_to_update_enroll_student'] = 'user/show_data_to_update_enroll_student';
$route['update_enroll_student'] = 'user/update_enroll_student';
$route['stddiscount'] = 'user/stddiscount';
$route['show_paid_reports_bko'] = 'user/show_paid_reports_bko';
$route['show_paid_reports_bko_for_print'] = 'user/show_paid_reports_bko_for_print';

$route['fresh_studentd'] = 'user/fresh_studentd';
$route['freezunfreez'] = 'user/freezunfreez';
$route['freezcourse'] = 'user/freezcourse';
$route['freez_unfreez_course'] = 'user/freez_unfreez_course';
$route['unfreez_course'] = 'user/unfreez_course';
$route['show_expense_reports_bko'] = "user/show_expense_reports_bko";
$route['show_voucher_reports_bko'] = "user/show_voucher_reports_bko";


$route['coursesregisterstudentnew'] = 'user/coursesregisterstudentnew';
$route['insert_student_courses_new'] = 'user/insert_student_courses_new';
$route['show_unpaid_reports_bko'] = 'user/show_unpaid_reports_bko';
$route['show_freeze_reports_bko'] = 'user/show_freeze_reports_bko';
$route['show_discount_reports_bko'] = 'user/show_discount_reports_bko';
$route['view_note'] = 'user/view_note';
$route['all_expense'] = 'user/all_expense';
$route['show_freeze_from_student_profile'] = "user/show_freeze_from_student_profile";

$route['show_insert_expense'] = 'user/show_insert_expense';
$route['insert_expense'] = 'user/insert_expense';

$route['show_expense_details'] = 'user/show_expense_details';







//osman

$route['mainReports'] = 'reports';
$route['studentReports'] = "reports/students";
$route['singleReport'] = "reports/singlestd";
$route['CourseBasedReports'] = 'reports/crs_std';
$route['All_Reports'] = 'reports/allReports';

$route['refund'] = "user/refund";
$route['refundCase'] = "user/refundcase";
$route['Approvals'] = "user/refund_applications";

$route['refundVoucher/(:num)'] = "user/std_refund_voucher/$1";
$route['creditVoucher/(:num)'] = "user/voucher_credit_insert/$1";











//ZulkarNainReport
$route['stat'] = 'stat';
$route['charts'] = 'stat/load_program_stat';
$route['batchstat']='stat/load_batch_stat';
$route['studentstat'] = "stat/student_stats";
//ZulkarNainend


$route['drilreport'] = "program_report";

$route['loginMe'] = 'login/loginMe';
$route['dashboard'] = 'user';
$route['logout'] = 'user/logout';
$route['userListing'] = 'user/userListing';
$route['userListing/(:num)'] = "user/userListing/$1";
$route['addNew'] = "user/addNew";
$route['addNewUser'] = "user/addNewUser";
$route['editOld'] = "user/editOld";
$route['editOld/(:num)'] = "user/editOld/$1";
$route['editUser'] = "user/editUser";
$route['deleteUser'] = "user/deleteUser";
$route['profile'] = "user/profile";
$route['profile/(:any)'] = "user/profile/$1";
$route['profileUpdate'] = "user/profileUpdate";
$route['profileUpdate/(:any)'] = "user/profileUpdate/$1";

$route['loadChangePass'] = "user/loadChangePass";
$route['changePassword'] = "user/changePassword";
$route['changePassword/(:any)'] = "user/changePassword/$1";
$route['pageNotFound'] = "user/pageNotFound";
$route['checkEmailExists'] = "user/checkEmailExists";
$route['login-history'] = "user/loginHistoy";
$route['login-history/(:num)'] = "user/loginHistoy/$1";
$route['login-history/(:num)/(:num)'] = "user/loginHistoy/$1/$2";

$route['forgotPassword'] = "login/forgotPassword";
$route['resetPasswordUser'] = "login/resetPasswordUser";
$route['resetPasswordConfirmUser'] = "login/resetPasswordConfirmUser";
$route['resetPasswordConfirmUser/(:any)'] = "login/resetPasswordConfirmUser/$1";
$route['resetPasswordConfirmUser/(:any)/(:any)'] = "login/resetPasswordConfirmUser/$1/$2";
$route['createPasswordUser'] = "login/createPasswordUser";

/* End of file routes.php */
/* Location: ./application/config/routes.php */
