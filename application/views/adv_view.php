<style>
#preloader {
  position: fixed;
  left: 0;
  top: 0;
  z-index: 99;
  width: 100%;
  height: 100%;
  overflow: visible;
  background: #ffffff
      url('https://loading.io/spinners/typing/lg.-text-entering-comment-loader.gif')
      no-repeat center center;
}
.content-wrapper{

       background: url('./assets/images/ads/img333.jpg')
       center center !important;
      background-size: cover !important;

}
td.tdc {

    text-align: center;
    vertical-align: middle !important;
    font-size: 18px;
    font-weight: bold;
}
.table-hover>tbody>tr:hover {
    background-color: #FBFCFC;
}
tr{
  font-size: 20px;

}
</style>
<script>
$(document).ready( function() {
$('#preloader').delay(5).fadeOut();
});
</script>
  <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>

<div class="content-wrapper">
    <section class="content-header">
      <h1 style="color:#ffffff">
        <i class="fa fa-users"></i> All Advertisement
        <small style="color:white;">Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">

        <div class="row">
            <div class="col-md-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url() ?>adv/addadv"><i class="fa fa-plus"></i> Add New</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
              <div class="box" style=" background-color: #e8e7e7;">
                <div id="preloader"></div>

                <div class="box-header">
                    <h3 class="box-title">Advertisement List</h3>
                </div>

                <div class="box-body table-responsive">
                     <table id="example1" class="table table-hover">
                <thead>
                    <tr>

                                        <th>Ad. Id</th>
                                        <th>Ad.Name</th>
                                        <th>Duration</th>
                                        <th>Slot</th>
                                        <th>Ad. Date</th>
                                        <th>Cost</th>
                                        <th>Video</th>
                                        <th>Actions</th>





                    </tr>
                </thead>
                <tbody>
                 <?php
                 if(@$category)
                 {
                  $no = 1;
                   foreach ($category as $k=>$v) {
                    $url =ltrim($v['FilePath'] , ".'\'");

                   ?>

                <tr class="bind">

                                        <td class="tdc"><?php echo $v['AdsID'];?></td>
                                        <td class="tdc"><?php echo $v['AdsName']; ?></td>
                                         <td class="tdc"><?php echo $v['AdDuration']; ?></td>
                                         <td class="tdc"><?php echo $v['AdSlot']; ?></td>
                                         <td class="tdc"><?php echo $v['AdBookingDate']; ?></td>
                                         <td class="tdc"><?php echo $v['Cost']; ?></td>
                                         <td>

                                        <video style=" width: 200px; height: 150px;" poster="http://video-js.zencoder.com/oceans-clip.png" controls>
  <source
    src="<?php echo base_url().$url;?>"
    type="video/mp4" preload="metadata">
  <source
    src="<?php echo $v['FilePath'];?>"
    type="video/mp4" preload="metadata">
  <source
    src=""
    type="video/ogg">
  <source
    src="<?php echo '../'.$v['FilePath'];?>"
    type="video/avi" preload="metadata">
  <source
    src="<?php echo $v['FilePath'];?>"
    type="video/avi" preload="metadata">
  <source
    src="<?php echo base_url().$url;?>"
    type="video/mov" preload="metadata">
  <source
    src="<?php echo base_url().$url;?>"
    type="video/mov" preload="metadata">
  Your browser doesn't support HTML5 video tag.
</video>

                                         </td>



                        <td class="tdc" >
                            <div class="col-xs-2">
                             <form action="<?php echo base_url().'adv/load_edit_adv';?>" method="POST">
                                <input type="hidden" value="<?php echo $v['AdsID'];?>" name="id"/>
                                <button class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></button>
                            </form>
                        </div>
                        <div class="col-xs-2" >
                    <form action="<?php echo base_url().'adv/delete_adv';?>" method="POST">
                     <input type="hidden" value="<?php echo $v['AdsID'];?>" name="cat_id"/>
                   <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                            </form>
                        </div>
                        <div class="col-xs-2" >
                             <form action="<?php echo base_url().'adv/detail_adv';?>" method="POST">
                                <input type="hidden" value="<?php echo $v['AdsID'];?>" name="cat_id"/>
                                <button class="btn btn-sm btn-danger"><i class="fa fa-user"></i></button>
                            </form>
                        </div>

                        </td>

                </tr>


               <?php }} ?>
                </tbody>

              </table>
              <div>
              <?php echo @$links;?>


              </div>




                </div>
              </div>
            </div>
        </div>
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
