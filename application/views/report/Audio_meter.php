<!-- <!DOCTYPE html>
<html>
<head>
    <title>Audiometry</title>
</head>
<style>
    p.c {
        word-spacing: 1cm;
    }
    table, th, td {
        border: 1px solid black;
    }
</style>
<body>

<div class="col-md-12">
<img src="C:\xampp\htdocs\dah_portal\assets\img\logo.jpg" alt="" style="    margin-left: 15%;">
<h2 style="color: green; padding-left: 30%;    margin-top: -3%;">
Dr. Arshad Health Associates
</h2>
</div>

<hr style="border: 1px solid black">
<hr style="border: 3px solid black">

<p style="margin-left: 15%;font-size: 16px;    margin-bottom: -1%;    font-size: 15px;    margin-top: -5px;">No. 20, Street 1, F-6/3, Islamabad. Tel: +92 51 2829666, Fax: +92 51 2270066</p>
<p style="margin-left: 16%;    font-size: 15px;">E-Mail: hlthline@comsats.net.pk Web:http://drarshadhealthassociates.com/</p>

 -->
<?php
if (isset($this->session->userdata['logged_in'])) {
	$email = ($this->session->userdata['logged_in']['email']);
	$username = ($this->session->userdata['logged_in']['username']);

} else {
	header("location: http://localhost/Dah_official_project/");

}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Audiometry</title>
</head>
<style>
    p.c {
        word-spacing: 1cm;
    }

   .chart-container
        {
        width: 640px;
        height: auto;
        }

</style>
<body>




<?php foreach ($headinfo as $u_key){ ?>
<div class="col-md-12">
<img src="<?php base_url(); ?>assets/assets/img/logo.jpg" alt="" style="    margin-left: 15%;">
<h2 style="color: green; padding-left: 30%;    margin-top: -3%;">
<?php echo $u_key->main_title; ?>
</h2>
</div>

<hr style="border: 1px solid black">
<hr style="border: 3px solid black">

<p style="margin-left: 15%;font-size: 16px;    margin-bottom: -1%;    font-size: 15px;    margin-top: -5px;"><?php echo $u_key->Hno; ?><?php echo $u_key->streetNo; ?><?php echo $u_key->sectorNo; ?><?php echo $u_key->city; ?><?php echo $u_key->telephone; ?><?php echo $u_key->fax; ?></p></p>
<p style="margin-left: 16%;    font-size: 15px;"><?php echo $u_key->email; ?><?php echo $u_key->website; ?></p>

<?php } ?>
<!---->

<a href="#" style="color: Black;">
<h3 style="color: Black; margin-left: 30%">AUDIOMETRY REPORT</h3>
</a>

<p style="    font-size: 11px;    font-family: sans-serif;"><b>CERT. # : </b>:  1088 </p>

<p style="    font-size: 11px;    font-family: sans-serif;"><b>MR # : </b>: 010113-2040</p>

<p style="    font-size: 11px;    font-family: sans-serif;"><b>NAME : </b>: MR. MUHAMMAD NAWAZ</p>

<p style="    font-size: 11px;    font-family: sans-serif;"><b>EMPLOYER: </b>: United Energy Pakistan</p>

<!--<p style="    font-size: 11px;    font-family: sans-serif;"><b>DATE : </b>: 26/12/2017</p>-->

<!--<p style="    font-size: 11px;    font-family: sans-serif;"><b>X-RAY : </b>: Chest PA View</p>-->

<br>

<div style=" border-style: solid;border-width: thin; height:300px;">

    <div style=" border-style: solid;border-width: thin;width: 20%;text-align: center;margin-left: 40%;">WEBER at 500 Hz</div>
    <div style=" border-style: solid;border-width: thin;width: 20%;text-align: center;margin-left: 40%;"> Rt | Med. | Lt </div>

    <br>
    <div class="row">

    <div class="col-md-4">
        <div style="border-style: dotted;border-width: thin;height: 200px;width: 310px;">


         <div class="chart-container">
            <canvas id="mycanvas"></canvas>
         </div>

       <!--  A div element with a thin border. -->
    </div>


</div>
    <div class="col-md-4">
        <div class="table" style="margin-left: 44.2%;    margin-top: -48%;">
        <table>
            <tr>
                <td>SISI</td>

            </tr>
            <tr>
                <td></td>
                <td>Rt</td>
                <td>Lt</td>
            </tr>
            <tr>
                <td>f</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>dB</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>%</td>
                <td></td>
                <td></td>
            </tr>
        </table>
        </div>
    </div>
    <div class="col-md-4">
        <div style="border-style: dotted;border-width: thin;height: 200px;width: 310px;margin-left: 57%;margin-top: -48%; margin-bottom: 15%">A div element with a thin border.</div>
    </div>

<p style="font-size: 13px;margin-bottom: 5%"><b>RIGHT EAR:</b> Acceptable hearing ability</p>
<p style="font-size: 13px;"><b>LEFT EAR:</b> Acceptable hearing ability</p>
        <p style="font-size: 13px; margin-left: 90% ">01/10/2013</p>

</div>
</div>
<div class="footer" style="margin-top: 13%"> <?php foreach ($footinfo as $row): ?>
<p style="font-size: 13px; font-family: sans-serif;">This is a verified computer generated report. It does not need a stamp or a signature.</p>
		<p style="font-size: 13px; font-family: sans-serif;"><?= $row->disclaimer; ?></p>
		<p class="c" style="    margin-left: 70%;    font-size: 13px; font-family: sans-serif"><em><?= $row->report_no; ?>  <?= $row->registration_no; ?></em></p>

<!--		<p class="c" style="    margin-left: 70%;    font-size: 13px; font-family: sans-serif"><em>[Rev:00]  DAHA-I-RCD-05-202</em></p>-->
	<?php endforeach; ?>
</div>

        <script type="text/javascript" src="C:\xampp\htdocs\dah_portal\assets\js\jquery.min.js">
        </script>

        <script type="text/javascript" src="C:\xampp\htdocs\dah_portal\assets\js\Chart.min.js">
        </script>

        <script type="text/javascript" src="C:\xampp\htdocs\dah_portal\assets\js\linegraph.js">
        </script>

</body>
</html>





<!-- <p style="    font-size: 11px;    font-family: sans-serif;"><b>CERT. # : </b>: <?= $u_key->Cno; ?> </p>

<p style="    font-size: 11px;    font-family: sans-serif;"><b>MR # : </b>: <?= $u_key->MRno; ?></p>

<p style="    font-size: 11px;    font-family: sans-serif;"><b>NAME : </b>: <?= $u_key->Pname; ?></p>

<p style="    font-size: 11px;    font-family: sans-serif;"><b>EMPLOYER: </b>: <?= $u_key->Employer; ?></p>

-->











<!---->
<!---->
<!--<!DOCTYPE HTML>-->
<!--<html>-->
<!--<head>-->
<!--<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>-->
<!--<script type="text/javascript">-->
<!---->
<!--window.onload = function () {-->
<!--	var chart = new CanvasJS.Chart("chartContainer", {-->
<!--		title:{-->
<!--			text: "My First Chart in CanvasJS"              -->
<!--		},-->
<!--		data: [              -->
<!--		{-->
<!--			// Change type to "doughnut", "line", "splineArea", etc.-->
<!--			type: "line",-->
<!--			dataPoints: [-->
<!--				{ label: "apple",  y: 10  },-->
<!--				{ label: "orange", y: 15  },-->
<!--				{ label: "banana", y: 25  },-->
<!--				{ label: "mango",  y: 30  },-->
<!--				{ label: "grape",  y: 28  }-->
<!--			]-->
<!--		}-->
<!--		]-->
<!--	});-->
<!--	chart.render();-->
<!--}-->
<!--</script>-->
<!--</head>-->
<!--<body>-->
<!--<div id="chartContainer" style="height: 300px; width: 100%;"></div>-->
<!--</body>-->
<!--</html>-->
