<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Edit Enrolled Student
        <small>Add / Edit User</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit User Detail</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body table-responsive">
                    <?php $this->load->helper("form"); ?>
                              
                                <form action="<?php echo base_url('update_enroll_student'); ?>" method="post">
                                   

                                    <div class="form-group">
                                        <label style = "margin-left: 160px; font-size : 20px;"><span style = " font-size : 20px;">Student Name</label> :<span style = " font-size : 20px;"><?php echo @$student_name[0]['fname'];?></span>
  
                                        <label style = "margin-left: 20px;  font-size : 20px;"><span style = " font-size : 20px;">Program Name</label> : <span style = " font-size : 20px;"><?php echo @$program_name[0]['program_name'];?></span> 

                                        <label style = "margin-left: 20px;  font-size : 20px;"><span style = " font-size : 20px;">Batch  </label> : <span style = " font-size : 20px;"><?php echo @$batch_name[0]['batch_name'];?></span> 
                                          
                                    </div>
                                    
                                    
                                     
                                        
                                          
                                   
                                    <div class="col-md-4">
                                        <div class="form-group">
                                        <label>Course Name</label>
                                            <select class="form-control" id="sel1" name="course_id">
                                                <option>Select</option>
                                          <?php
                                    foreach($all_stds as $sel):
                                    ?>
                                   <option value="<?php echo $sel->course_id; ?>"><?php echo $sel->coursename; ?></option>
                                    <?php
                                        endforeach;
                                    ?>
                                            </select></div></div>
                                    

                                   <div class="col-md-4">
                                    <div class="form-group">
                                     <label>Select Status<span class="required">*</span></label>
                                            <select class="form-control" id="sel1" name="status">
                                                <option>Select</option>
                                          <option class="form-control" value="1">Enable</option>
                                          <option class="form-control" value="0">Disable</option>
                                            </select>
                                        </div></div>
                                    

                                     <div class="col-md-4">
                                    <div class="form-group">
                                 <label>Select Paid Status<span class="required">*</span></label>
                                            <select class="form-control" id="sel1" name="Paid">
                                          <option>Select</option>
                                          <option class="form-control" value="1">Paid</option>
                                          <option class="form-control" value="0">UnPaid</option>
                                            </select>
                                        </div>
                                    </div>



                                   
                                    <div class="form-group">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light" style="margin-left: 1%; width: 15%;">
                                                Submit
                                            </button>
                                        </div>
                                    </div>


                                </form>
                            </div>
                </div></div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>