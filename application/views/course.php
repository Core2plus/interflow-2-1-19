

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> All Courses
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
     <div class="row" style="margin: 0px;">
        <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url() ?>addcourse"><i class="fa fa-plus"></i> Add New</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
             


                          <div class="box">
            <div class="box-header">
              <h3 class="box-title">ALL Courses</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Course code</th>
                        <th>Course name</th>
                        <th>Course fees</th>
                        <th>Module Name</th>
                        <th>Course Duration</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                 <?php
                  $no = 1;
                   foreach ($show_courses as $show) {
                     
                   ?>
               
                <tr>
                   <td><?php echo $no++; ?></td>
                   <td><?php echo $show->coursecode; ?></td>
                   <td> 
                        
                            <?php echo $show->coursename;  ?>
                        
                    </td>
                     <td><?php echo $show->coursefee; ?></td>

                   <td><?php echo $show->module_name; ?></td>
                  
                   
                   <td><?php echo $show->co_duration; ?></td>

                   
                    <td>
                        <a class="btn btn-sm btn-info" href="<?php echo base_url('editCourse'); ?>?id=<?php echo $show->course_id; ?> " title="Edit"><i class="fa fa-pencil"></i>
                        </a>
                        <a class="btn btn-sm btn-danger deleteUser" href="<?php echo base_url('deletecourse'); ?>?id=<?php echo $show->course_id; ?>" data-userid="" title="Delete"><i class="fa fa-trash"></i>
                        </a>
                    </td>
                  
                </tr>
              
 
               <?php } ?>
                </tbody>
                
              </table>

            </div>
         
          </div>
                 
</section>
            </div>
      
    
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>















