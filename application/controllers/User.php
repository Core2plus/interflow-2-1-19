<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */

class User extends BaseController
{
    /**
     * This is default constructor of the class

     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
         $this->load->model('Main_Model');
        $this->isLoggedIn();   
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {

        $this->global['pageTitle'] = 'CodeInsect : Dashboard';
        
        // $result=$this->user_model->get_user();
        // $data['users']=$result->num_rows();
        // $st=$this->user_model->get_students();
        // $data['students']=$st->num_rows();
        // $e=$this->user_model->sum_earning('54');
        // $data['earning']=$e[0]['CourseFee'];
        //  $data['Rcv_Amt']=$e[0]['Rcv_Amt'];
        $data['rec']='ok';

        $this->loadViews("dashboard", $this->global, $data, NULL);

    }


    //PROGRAM

        public function program(){
            $data['all_pro'] = $this->Main_Model->show_pro();
            $this->loadViews("program", $this->global, $data, NULL);
        }

        public function addprogram(){

            
            $this->loadViews("addprogramform", $this->global);
        } 

        public function insertprogram(){
            $this->form_validation->set_rules('name','Name','trim|required|is_unique[program.program_name]');
             $this->form_validation->set_rules('doj','Date','trim|required');
             $this->form_validation->set_rules('doe','Date','trim|required');
             if($this->form_validation->run())
             {

        $data= $this->input->post();
        $this->load->model('Main_Model');
        $query = $this->Main_Model->insert_program($data);
        if($query==TRUE)
        {
            $this->session->set_flashdata('success','Record is inserted..');
            redirect(base_url('program'));
        }
        else
        {
            $this->session->set_flashdata('error','Record is not inserted..yet');
            redirect(base_url('program'));    
        }
            
        }
        else{
           $this->loadViews("addprogramform", $this->global);



        }
    }



        public function editprogram(){
            $id = $_GET['id'];
            $data['all_pro'] = $this->Main_Model->upd_pro($id);
            $this->loadViews("program/editprogramform", $this->global, $data, NULL);
        }

        public function updateprogram(){
            $data= $this->input->post();
        $this->load->model('Main_Model');
        $query = $this->Main_Model->update_program($data);
        if($query==TRUE)
        {
            $this->session->set_flashdata('success','Record is inserted..');
            redirect(base_url('program'));
        }
        else
        {
            $this->session->set_flashdata('error','Record is not inserted..yet');
            redirect(base_url('program'));    
        }
        }

        public function deleteprogram(){
            $id = $_GET['id'];
           $query = $this->Main_Model->delete_pro($id);
        if($query == TRUE)
        {
            $this->session->set_flashdata('success','Record is deleted..');
            redirect(base_url('program'));    
        }
        else
        {
            $this->session->set_flashdata('error','Record is not deleted..yet');
            redirect(base_url('program'));    
        
        }


        }
       






    //MODULE

     public function Module()
    {

        $query = $this->db->query('SELECT module.* , program.* FROM module INNER JOIN program on module.program_id = program.program_id');
            $data['data']  = $query->result_array();


        $this->loadViews("module", $this->global, $data, NULL);
    }

    public function moduleAdd(){
       // $data['all_pro'] = $this->Main_Model->show_pro();
        $query = $this->db->get('program');
            $data['all_pro'] = $query->result();
        //$data['all_bat'] = $this->Main_Model->show_bat();
            $query = $this->db->get('batch');
            $data['all_bat'] =  $query->result();

       $this->loadViews("addModule", $this->global,$data, NULL );
    }

    public function insertModel(){
        /*echo $this->input->post('program_name');
        echo $this->input->post('moduleName');
        echo $this->input->post('modstatus');*/
       $this->form_validation->set_rules('program_name','Program','trim|required');
             $this->form_validation->set_rules('moduleName','Module','trim|required|is_unique[module.module_name]');
             $this->form_validation->set_rules('modstatus','Module Status','trim|required');
             if($this->form_validation->run())
             {

        $data= $this->input->post();
        $this->load->model('Main_Model');
        $query = $this->Main_Model->insert_module($data);

       if($query==TRUE)
        {
            $this->session->set_flashdata('success','Record is inserted..');
            redirect(base_url('module'));
        }
        else
        {
            $this->session->set_flashdata('error','Record is not inserted..yet');
            redirect(base_url('module'));    
        }
    }
    else{
         $query = $this->db->get('program');
            $data['all_pro'] = $query->result();
        //$data['all_bat'] = $this->Main_Model->show_bat();
            $query = $this->db->get('batch');
            $data['all_bat'] =  $query->result();


       $this->loadViews("addModule", $this->global,$data, NULL );


    }
     
    }

    public function editmodule(){

        $id = $_GET['id'];
         $query = $this->db->get('program');
            $data['all_pro'] = $query->result();

            $this->db->where('module_id',$id);
            $query = $this->db->get('module');
            $data['all_module'] =  $query->result();
            
           /* $query = $this->db->get('batch');
            $data['all_bat'] =  $query->result();*/

            /*$this->db->where('module_id',$id);
            $query = $this->db->get('module');
            $data['modulen'] = $query->result();*/

        $this->loadViews("moduleEdit", $this->global,$data, NULL );
    }


    public function updatemodule(){
        //$data= $this->input->post();
         //$data= $this->input->post();
            $this->input->post('program_id');
            $this->input->post('name');
            $this->input->post('sel1');
         //echo $this->input->post();
       $id = $this->input->post('text_hid');

            $datas = array(
                'program_id' => $this->input->post('program_id'),
                'module_name' => $this->input->post('name'),
                'mod_status' => $this->input->post('sel1')
                );

            $this->db->where("module_id",$id);
             $query = $this->db->update('module',$datas);
              if($query==TRUE)
        {
            $this->session->set_flashdata('success','Record is inserted..');
            redirect(base_url('module'));
        }
        else
        {
            $this->session->set_flashdata('error','Record is not inserted..yet');
            redirect(base_url('module'));    
        }
    }


    public function deleteModule(){
        $id = $_GET['id'];
    
       $this->db->where('module_id',$id);
            $query = $this->db->delete('module');
            
           if($query == TRUE)
        {
            $this->session->set_flashdata('success','Record is deleted..');
            redirect(base_url('module'));    
        }
        else
        {
            $this->session->set_flashdata('error','Record is not deleted..yet');
            redirect(base_url('module'));    
        
        }
    }

    
    //COURSESTART


    public function course()
    {
        $data['show_courses'] = $this->Main_Model->show_courses();

        $this->loadViews("course", $this->global,$data,NULL);

    }

    public function editCourse(){

       $id = $_GET['id'];
       $data['all_course'] = $this->Main_Model->update_course($id);
        $data['all_pro'] = $this->Main_Model->show_pro();
        $data['all_mod'] = $this->Main_Model->show_mod();
        $this->loadViews("courseEdit", $this->global,$data,NULL);
       
    }

    public function update_cor(){

        $data= $this->input->post();
        $this->load->model('Main_Model');
        $query = $this->Main_Model->update_cor($data);

          if($query==TRUE)
        {
            $this->session->set_flashdata('success','Record is inserted..');
            redirect(base_url('course') );
        }
        else
        {
            $this->session->set_flashdata('error','Record is not inserted..yet');
            redirect(base_url('course') ); 
        }

         //$this->course();
    }


    public function addcourse(){
        $data['all_pro'] = $this->Main_Model->show_pro();
        $data['all_mod'] = $this->Main_Model->show_mod();
        $this->loadViews("addcourse", $this->global,$data,NULL);
    }

    public function insert_course()
    {
        $this->form_validation->set_rules('program_id','Program','trim|required');
             $this->form_validation->set_rules('module_id','Module','trim|required');
             $this->form_validation->set_rules('code','Module Status','trim|required|is_unique[courses.coursecode]');
             $this->form_validation->set_rules('name','Name','trim|required|is_unique[courses.coursename]');
               $this->form_validation->set_rules('fees','Fees','trim|required');
                $this->form_validation->set_rules('duration','Duration','trim|required');
                $this->form_validation->set_rules('sel1','sel1','trim|required');
             if($this->form_validation->run())
             {

        $data= $this->input->post();
        $this->load->model('Main_Model');
        $query = $this->Main_Model->insert_course($data);

       if($query==TRUE)
        {
            $this->session->set_flashdata('success','Record is inserted..');
            redirect(base_url('course'));
        }
        else
        {
            $this->session->set_flashdata('error','Record is not inserted..yet');
            redirect(base_url('course'));    
        }
    }else{
         $data['all_pro'] = $this->Main_Model->show_pro();
        $data['all_mod'] = $this->Main_Model->show_mod();
        $this->loadViews("addcourse", $this->global,$data,NULL);



    }
    }

    public function delete_course()
    {
        $id = $_GET['id'];
        $query = $this->Main_Model->delete_course($id);
          if($query==TRUE)
        {
            $this->session->set_flashdata('success','Record is Deleted..');
            redirect(base_url('course'));
        }
        else
        {
            $this->session->set_flashdata('error','Record is not Deleted..yet');
            redirect(base_url('course'));    
        }
     }   


     //BATCHES 

     public function batch()
    {
        $data['show_batch'] = $this->Main_Model->show_batch(); 
         $this->loadViews("batch", $this->global,$data,NULL);
     }   


     public function editbatch(){
         $id = $_GET['id'];

        $data['all_pro'] = $this->Main_Model->show_pro();
        $data['show_mod'] = $this->Main_Model->show_mod();
        $data['show_program_for_update_batch'] = $this->Main_Model->show_program_for_update_batch($id);
        $data['show_mod_for_update_batch'] = $this->Main_Model->show_mod_for_update_batch($id);
        $data['all_batch'] = $this->Main_Model->update_batch($id);
        $this->loadViews("editbatch", $this->global,$data,NULL);
     }

     public function updatebatch(){

        echo $id = $_POST['text_hid'];
        $data= $this->input->post();
        $this->load->model('Main_Model');
        $query = $this->Main_Model->update_bat($data,$id);
 if($query==TRUE)
        {
            $this->session->set_flashdata('success','Record is inserted..');
            redirect(base_url('batch'));
        }
        else
        {
            $this->session->set_flashdata('error','Record is not inserted..yet');
            redirect(base_url('batch'));    
        }
     }

     public function addbatch(){
        $data['all_pro'] = $this->Main_Model->show_pro();
        $data['show_mod'] = $this->Main_Model->show_mod();
        $this->loadViews("addBatch", $this->global,$data,NULL);
     }


     public function insertbatch(){

        $data= $this->input->post();
        $this->load->model('Main_Model');
        $query = $this->Main_Model->insert_batch($data);
       if($query==TRUE)
        {
            $this->session->set_flashdata('success','Record is inserted..');
            redirect(base_url('batch'));
        }
        else
        {
            $this->session->set_flashdata('error','Record is not inserted..yet');
            redirect(base_url('batch'));    
        }
     }

     public function deletebatch(){

       $id = $_GET['id'];
       $query = $this->Main_Model->delete_batch($id);
      if($query==TRUE)
        {
            $this->session->set_flashdata('success','Record is Deleted..');
            redirect(base_url('batch'));
        }
        else
        {
            $this->session->set_flashdata('error','Record is not Deleted..yet');
            redirect(base_url('batch'));    
        }
     }



     //STUDENT

     public function student(){

        $query['students'] = $this->Main_Model->retrieve_student_data();

        $this->loadViews("student", $this->global,$query,NULL);
     }

     public function editstd(){
         $id = $_GET['id'];
         $query['std_data'] = $this->Main_Model->retrieve_student_data_for_show_stds($id);
       $this->loadViews("editstd", $this->global,$query,NULL);
     }


     public function updatestd()
     {

 $data = $this->input->post();
                  
     $id = @$_POST['id'];
if(@$_FILES['image_student']['name'] && @$_FILES['image_student']['size']>0 )
{

$config = array(
             'upload_path' => "./assets/artt_images/student/",
             'allowed_types' => "jpg|png|jpeg",
             'max_size' => "5048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'encrypt_name' => TRUE,
             'max_height' => "3000",
             'max_width' => "3000"
         );



         $this->load->library('upload', $config);


         if($this->upload->do_upload('image_student'))
         {

             
             $data = $this->input->post();
             $info = $this->upload->data();
             $image_path = base_url("assets/artt_images/student/".$info['raw_name'].$info['file_ext']);
             $data['image'] =$image_path;
             
              if ($this->Main_Model->edit_student($data,$id))
              {

                 $this->session->set_flashdata('success','Record is inserted..');
                redirect(base_url('student'));
               

             }
          else
         {

             $this->session->set_flashdata('error','Record is not inserted..yet');
             
            redirect(base_url('student'));    
            
         }


     }

else
{

$error = array('error' => $this->upload->display_errors());
echo "Uploadfile";
           $this->loadViews('student',$this->global,$error,NULL);

}
}
else{
    $result=$this->Main_Model->edit_student1($data,$id);
    
    if($result)
    {
        //echo $result;
         $this->session->set_flashdata('success','Record is inserted..');
               redirect(base_url('student'));
         
         
              

             }
          else
         {
           // print_r($data);

             $this->session->set_flashdata('error','Record is not inserted..yet');
             
             redirect(base_url('student'));    
            
         }




    }






 }

     public function profilestd(){

        $id = $_GET['id'];
        $error = @$_GET['error'];
        if(@$error=='1')
        {
            $data['error']='1';

        }
        $data['show_student_profile'] = $this->Main_Model->show_student_profile($id);
        $data['show_fee_details_in_student_profile2'] = $this->Main_Model->show_fee_details_in_student_profile2($id);
        
        $this->loadViews("stdprofile", $this->global,$data,NULL);  
     }


     public function addstd(){
        $data['show_batch_for_new_student'] = $this->Main_Model->show_batch_for_new_student();

       $this->loadViews("addstd", $this->global,$data,NULL);           
     }


     public function insertstudent(){

      
        $data = $this->input->post();
        $this->load->model('Main_Model');
        $query = $this->Main_Model->insert_student($data);
        $this->Main_Model->total_fees($data);
       
        redirect('coursesregisterstudent');


   }

   public function coursesregisterstudent(){
    $data['retrieve_student_id'] =  $this->Main_Model->retrieve_student_id_to_assign_courses();
    $data['show_batch_for_new_student'] = $this->Main_Model->show_batch_for_new_student();

    $this->loadViews("courseregstd", $this->global,$data,NULL); 
   }



   /* public function coursesregisterstudentnew(){
    echo $id = $_GET['id'];
    $data["id"] = ("$id");
    //$data['retrieve_student_id'] = array('id' => '$id' );
    //exit();
    //$data['retrieve_student_id'] =  $this->Main_Model->retrieve_student_id_to_assign_courses();
    $data['show_batch_for_new_student'] = $this->Main_Model->show_batch_for_new_student();

    $this->loadViews("add_new_enrollment _to_fresh_std", $this->global,$data,NULL); 
   }
*/






   
   public function ajax(){



    $conn = mysqli_connect('localhost','root','','artt_db');

    $batch_id = $_GET['batch_id'];
    $std_id = $_GET['student_id'];
     $std_id;

    if($batch_id != "")
    {
    
        $query= "SELECT * FROM courses WHERE module_id = 
         (SELECT module_id FROM batch WHERE batch_id ='".$batch_id."')";

        


$query4 ="SELECT program_id, module_id,batch_id,batch_name FROM batch WHERE batch_id='".$batch_id."'";
         
        
         $res4 = mysqli_query($conn,$query4);
        
        
        



         
        $res = mysqli_query($conn,$query);
        ?>
        <input type="hidden" name="std_id" value="<?php echo $std_id; ?>">
        <label>Courses Name</label>
        <hr>
        <center>
        
                <?php
             while($row4 = mysqli_fetch_assoc($res4))
                { ?>
                <input type="hidden" name="program_id" value="<?php echo $row4['program_id']; ?>">
                <input type="hidden" name="batch_id" value="<?php echo $row4['batch_id']; ?>">
                <input type="hidden" name="module_id" value="<?php echo $row4['module_id']; ?>">
        <?php } ?> 
            
            
                
    
            
            <?php
                while($row = mysqli_fetch_assoc($res))
                {
                    ?>
                    
                    <input style="width:15px ; height:15px; float:left;" id="batch_id" class="minimal" class="form-control"  name="course_id[]" type="checkbox" value="<?php echo $row['course_id']; ?>" >
                    <label style=" margin:right:20px;"><?php echo $row['coursename']; ?></label>
                    
                    
                    <br>
                    <?php
                }
            ?>
            </center>
        
        <?php
    }




   }
































    public function insert_student_courses(){



          $course_id = $this->input->post('course_id[]');
          
          
          $count = count($course_id);
          for($i=0; $i<$count;$i++)
          {
            $datainsert = array(
            'student_id' =>$this->input->post('std_id'),
            'program_id' =>$this->input->post('program_id'),
            'batch_id' =>$this->input->post('batch_id'),
            'enrolled_status' => 1,
            'module_id' =>$this->input->post('module_id'),
            'course_id' =>$course_id[$i]
            );
            $query = $this->db->insert('enroll_student',$datainsert);

          }

           $this->db->query(" UPDATE enroll_student SET Freez = ( SELECT fstatus FROM student  HAVING MAX(studentid))
                            WHERE enroll_student.student_id=(SELECT MAX(studentid) FROM student)");


            $std_id = $this->input->post('std_id');

                $batch_id = $this->input->post('batch_id');

                $program_id = $this->input->post('program_id');
                
                $query = $this->db->query("update student set batch_id = '".$batch_id." ' WHERE studentid = '".$std_id."' ");



   redirect('student');
   }

 public function insert_student_courses_new(){



          $course_id = $this->input->post('course_id[]');
          //echo $this->input->post('std_id');
          //echo $this->input->post('program_id');


                          
          $count = count($course_id);
          for($i=0; $i<$count;$i++)
          {
            $datainsert = array(
            'student_id' =>$this->input->post('std_id'),
            'program_id' =>$this->input->post('program_id'),
            'batch_id' =>$this->input->post('batch_id'),
            'enrolled_status' => 1,
            'module_id' =>$this->input->post('module_id'),
            'course_id' =>$course_id[$i]
            );
            $query = $this->db->insert('enroll_student',$datainsert);

          }

           $this->db->query(" UPDATE enroll_student SET Freez = ( SELECT fstatus FROM student  HAVING MAX(studentid))
                            WHERE enroll_student.student_id=(SELECT MAX(studentid) FROM student)");


            $std_id = $this->input->post('std_id');

                $batch_id = $this->input->post('batch_id');

                $program_id = $this->input->post('program_id');
                
                $query = $this->db->query("update student set batch_id = '".$batch_id." ' WHERE studentid = '".$std_id."' ");



   redirect('student');
   }















































   public function enrollestuden(){
    
     $id = $_GET['id'];
    

    $query['enroll_student'] = $this->Main_Model->show_enrolled_student($id);

    $this->loadViews("enrollStudent", $this->global,$query,NULL);  

    

   }

   public function editEnrollstd(){


             $id=$this->input->get('id');
             $id2=$this->input->get('id2');

            $data['show_data_for_insert_new_enroll_student'] = $this->Main_Model->insert_new_enroll_student($id);

            $data['courses_for_insert_new_enroll_student'] = $this->Main_Model->courses_for_insert_new_enroll_student($id);  
            //$this->load->view('edit_new_enroll_student',$data);
             $this->loadViews("edit_new_enroll_student", $this->global,$data,NULL);  



   }


   public function insert_new_enroll_student(){
            $id = $_GET['id'];

            $data['show_data_for_insert_new_enroll_student'] = $this->Main_Model->insert_new_enroll_student($id);
            $data['courses_for_insert_new_enroll_student'] = $this->Main_Model->courses_for_insert_new_enroll_student($id);  
            $this->loadViews("insert_new_enroll_student", $this->global,$data,NULL);  
    
   }

   public function update_new_enroll_student(){

    $id = $_GET['id'];
    
        $course_name=$this->input->post('checkbox_courses[]');
        $count=count($course_name);
            for($i=0; $i<$count; $i++){
            $data = array(

                'student_id' => $this->input->post('student_id'),
                'program_id'  => $this->input->post('program_id'),
                'module_id'  => $this->input->post('module_id'),
                'course_replace_remarks' => $this->input->post('course_replace_remarks'),
                'course_id' => $course_name[$i],
                'batch_id' => $this->input->post('batch_id'),
                'Freez' => $this->input->post('Freez'),
                'enrolled_status' => 1
            );
                
        $query = $this->db->insert('enroll_student',$data);
    }

        redirect(base_url()."enrollestuden?id=".$id);
   }


   public function update_update_new_enroll_student(){

echo $id=$this->input->get('id');
            echo $id2=$this->input->get('id2');


$check = $this->db->query('SELECT enrollment_id FROM enroll_student
                           WHERE student_id =   "'.$id.'" AND course_id = "'.$id2.'"  ');

$a = 0;

 foreach($check->result() as $key){

 $a = $key->enrollment_id;

}

$data = array(

                'student_id' => $this->input->post('student_id'),
                'program_id'  => $this->input->post('program_id'),
                'module_id'  => $this->input->post('module_id'),
                'course_id' => $this->input->post('course_id'),
                'batch_id' => $this->input->post('batch_id'),
                'status' => $this->input->post('status'),
                'course_replace_remarks' => $this->input->post('course_replace_remarks'),
                'Paid' => $this->input->post('Paid'),
                'enrolled_status' => 1
            );


    $this->db->where("enrollment_id",$a);
            $this->db->update('enroll_student',$data);

                redirect(base_url()."enrollestuden?id=".$id);




   }

   


   public function delete_enroll_student(){

            $id=$this->input->get('id');
            $id2=$this->input->get('id2');

       
            $this->Main_Model->delete_enroll_student_courses($id);
            redirect(base_url()."enrollestuden?id=".$id2);

            }

    public function stdvoucher(){
        $id = $_GET['id'];

        $data['show_student_details'] = $this->Main_Model->show_student_details($id);
        
//var_dump($data);

//$data['show_student_profile'] = $this->Main_Model->show_student_profile($id);
$data['show_fee_details_in_student_profile'] = $this->Main_Model->show_fee_details_in_student_profile($id);
$data['show_discount_details_in_voucher'] = $this->Main_Model->show_discount_details_in_voucher($id);

         $this->loadViews("stdvoucher", $this->global,$data,NULL);

    } 



    public function voucher_insert()
    {
        $id = $_GET['id'];

        $total_fees = $this->input->post('total_fees');
        $student_id = $this->input->post('studentid');
        $pay_amount = $this->input->post('pay_amount');
        $remain_fees = $this->input->post('remain_fees');
        $batch_name = $this->input->post('batch_name');
        $current_date = $this->input->post('current_date');
        $due_date = $this->input->post('due_date');
        $pay_with = $this->input->post('pay_with');
        $cheque_number = $this->input->post('cheque_number');
    

            $data1 = array(
                'total_fees' => $total_fees,
                'student_id' => $student_id,
                'remain_fees' => $remain_fees,
                'batch_name' => $batch_name,
                'current_date' => $current_date,
                'due_date' => $due_date,
                'pay_with' => $pay_with,
                'cheque_number' => $cheque_number,
                'pay_amount' => $pay_amount
            );
        
            $this->db->insert('master_vocher',$data1);


          
        $course_id = $this->input->post('course_id[]');
        $student_id = $this->input->post('student_id[]');
        $received_amount = $this->input->post('received_amount[]');
        $voucher_amount = $this->input->post('voucher_amount[]');
        $count = count($received_amount);

        for($i=0; $i<$count; $i++) {

            $data = array(
                'received_amount' => $received_amount[$i]+$voucher_amount[$i],
                 'voucher_amt' => $voucher_amount[$i]
            );
            $this->db->where('student_id',$student_id[$i]);
            $this->db->where('course_id',$course_id[$i]);
            $this->db->update('enroll_student',$data);

        }

        $data['print_voucher'] = $this->Main_Model->print_voucher($id);
        $data['print_voucher1'] = $this->Main_Model->print_voucher1($id,$pay_amount,$remain_fees);

         $this->loadViews("print_voucher", $this->global,$data,NULL);

    }




        public function delete_student(){

          $id = $_GET['id'];
            
          $this->Main_Model->delete_student($id);

           redirect ('student');

        }


        public function disscountInsert(){
             $id = $_GET['id'];

            $query['discount_student'] = $this->Main_Model->discount_student($id);
        $query['discount_batch'] = $this->Main_Model->discount_batch($id);

          $this->loadViews("insertDiscount", $this->global,$query,NULL);

        }


        public function insert_discount()
    {
         $id = $_POST['id'];
       

        $data= $this->input->post();
        $this->load->model('Main_Model');
        $query = $this->Main_Model->insert_discount($data,$id);
        if($query==TRUE)
        {
            
            redirect(base_url('student'));
        }
        else
        {
            
            redirect(base_url('student'));   
        }   
    }



        public function show_all_enrolled_students(){



            $query['enroll_student'] = $this->Main_Model->show_all_enrolled_students();

            $this->loadViews("show_enrolled_studens", $this->global,$query,NULL);

        }


        public function enroll_std(){

        $data['all_stds'] = $this->Main_Model->show_stds();
        $data['all_pro'] = $this->Main_Model->show_pro();
        $data['all_bat'] = $this->Main_Model->show_bat();
        $data['all_mod'] = $this->Main_Model->show_mod();
        $data['show_courses'] = $this->Main_Model->show_courses();

        $this->loadViews("enrol_std", $this->global,$data,NULL);
        //$this->load->view('BKO/enroll_std',$data);

        }


         public function insert_enroll_student(){

            $course_name=$this->input->post('course_id[]');
            $count=count($course_name);
            for($i=0; $i<$count; $i++){
            $data = array(

                'student_id' => $this->input->post('student_id'),
                'program_id'  => $this->input->post('program_id'),
                'module_id'  => $this->input->post('module_id'),
                'course_id' => $course_name[$i],
                'batch_id' => $this->input->post('batch_id'),
                'status' => $this->input->post('status'),
                'enrolled_status' => 1 ,
                'Paid' => $this->input->post('Paid'),   
                
            );
        $query = $this->db->insert('enroll_student',$data);
    }

        redirect('show_all_enrolled_students');



    }



    public function show_data_to_update_enroll_student(){

        /* $id = $_GET['id'];
         

        $data['all_stds'] = $this->Main_Model->show_stds();
        $data['all_pro'] = $this->Main_Model->show_pro();
        $data['show_courses'] = $this->Main_Model->show_courses();

        $this->loadViews("update_enrolled_student", $this->global,$data,NULL);
        //$this->load->view('BKO/update_enrolled_student',$data);
*/
$id = $_GET['id'];
          $pid = $_GET['program'];
          $bid = $_GET['batch'];
           $mid = $_GET['module'];
           $sid = $_GET['sid'];
         //echo $id;
           $data['program_name']=$this->Main_Model->show_program($pid);
           $data['batch_name']=$this->Main_Model->show_batch1($bid);
           $data['student_name']=$this->Main_Model->show_student($sid);

        $data['all_stds'] = $this->Main_Model->show_stds1($id,$pid,$bid,$mid);
       //$data['stu'] = $this->Main_Model->show_data($id);
        //print_r($data['all_stds']);
        //$data['all_pro'] = $this->Main_Model->show_pro();
        //$data['show_courses'] = $this->Main_Model->show_courses();
//print_r($data['all_stds']);
        $this->loadViews("update_enrolled_student", $this->global,$data,NULL);
        //$this->load->view('BKO/update_enrolled_student',$data);
    }



    public function update_enroll_student(){



            $id=$this->input->get('id');
            $id2=$this->input->get('id2');

            $data['show_data_for_insert_new_enroll_student'] = $this->Main_Model->insert_new_enroll_student($id);

            $data['courses_for_insert_new_enroll_student'] = $this->Main_Model->courses_for_insert_new_enroll_student($id);  
        $this->load->view('BKO/edit_new_enroll_student',$data);

}


    public function stddiscount(){
        $query['students'] = $this->Main_Model->retrieve_student_data();
        
        //$data['all_assign'] = $this->Main_Model->show_assign();
        $this->loadViews("std_discount", $this->global,$query,NULL);
        //$this->load->view('BKO/discount_std',$query);
    }




    public function show_paid_reports_bko(){

        $data['show_paid_reports'] = $this->Main_Model->show_paid_reports();

        $this->loadViews("show_paid_reports_bko", $this->global,$data,NULL);


    }

    public function show_paid_reports_bko_for_print(){


            
        $data['show_paid_reports'] = $this->Main_Model->show_paid_details_for_print(); 
        $this->load->view('show_paid_reports_bko_for_print',$data);

                                    

        }

                public function fresh_studentd(){


                    $query['enroll_student'] = $this->db->query("SELECT
    `artt_id`
    , `cr`
    , `studentid`
    , `fname`
    , `batch_id`
FROM
    `artt_db`.`student`
    WHERE batch_id=''");
            

                //return $query;





            //$query['enroll_student'] = $this->Main_Model->show_all_enrolled_students();

            $this->loadViews("fresh_student", $this->global,$query,NULL);


                    //$this->loadViews("fresh_studentd");

                }  





                public function freezunfreez(){


           


                $query['data'] = $this->Main_Model->freez();

                 $this->loadViews("freez_unfreez", $this->global,$query,NULL);  

        
               

         //print_r($query);
         

            //$this->loadViews("freez_unfreez", $this->global,$query,NULL);


                }


                     public function freezcourse(){
    
                        $id = $_GET['id'];
                        $query['enroll_student'] = $this->Main_Model->show_enrolled_student($id);
                        $this->loadViews("freezcou", $this->global,$query,NULL);  

                 }


                 public function freez_unfreez_course(){
                 
                    $id = $_GET['id'];
                    $id2 = $_GET['id2'];



                    $data = array( 'Freez' =>  'Freez' );

                    $this->db->where("course_id",$id);
                    $this->db->update('enroll_student',$data);
                     redirect(base_url()."freezcourse?id=".$id2);
                    
                    
                 }

                 public function unfreez_course(){

                    echo $id = $_GET['id'];
                    echo $id2 = $_GET['id2'];

                     $data = array( 'Freez' =>  'UnFreez' );


                    $this->db->where("course_id",$id);
                    $this->db->update('enroll_student',$data);
                     redirect(base_url()."freezcourse?id=".$id2);

                 }



             public function coursesregisterstudentnew(){
             $id = $_GET['id'];

    $data['show_student'] = $this->Main_Model->show_student($id);
$data['show_batch_for_new_student'] = $this->Main_Model->show_batch_for_new_student(); 
    $data['courses_for_insert_new_enroll_student'] = $this->Main_Model->courses_for_insert_new_enroll_student($id);  
    $this->loadViews("add_new_enrollment _to_fresh_std", $this->global,$data,NULL);  
    
   }



   public function show_unpaid_reports_bko(){

        $data['show_unpaid_reports'] = $this->Main_Model->show_unpaid_reports();
        $this->loadViews("show_unpaid_reports_bko", $this->global,$data,NULL); 
        
    }


public function show_freeze_reports_bko(){

        $data['show_freeze_reports'] = $this->Main_Model->show_freeze_reports(); 
        $this->loadViews("show_freeze_reports_bko", $this->global,$data,NULL); 
        //$this->load->view('BKO/show_freeze_reports_bko',$data);
    }


    public function show_discount_reports_bko(){


        $data['show_discount_reports'] = $this->Main_Model->show_discount_reports(); 
         $this->loadViews("show_discount_reports_bko", $this->global,$data,NULL); 
         //$this->load->view('BKO/show_discount_reports_bko',$data);
    }

    public function view_note()
    {
        $id = $_GET['id'];
        $data['view_note'] = $this->Main_Model->view_note($id);
        $this->loadViews("view_note", $this->global,$data,NULL); 
        //$this->load->view('BKO/view_note',$data);
    }


        public function show_expense_reports_bko(){


     $data['all_expense'] = $this->Main_Model->all_expense(); 
     $this->loadViews("show_expense_reports_bko", $this->global,$data,NULL); 
        

    }


    public function show_voucher_reports_bko(){


        $data['show_voucher_reports_bko'] = $this->Main_Model->show_voucher_reports(); 
        $this->loadViews("show_voucher_reports_bko", $this->global,$data,NULL);
        


    }



           public function show_freeze_from_student_profile(){

            echo $id=$this->input->get('enrollment_id');
            echo $id2=$this->input->get('student_id');
            echo $id3=$this->input->get('module_id');
            echo $query['course_id'] = $this->input->get('course_id');
            

             

            $this->load->model('Main_Model');
            $query['show_student'] = $this->Main_Model->show_freez_form($id,$id2);
            $query['batch'] = $this->Main_Model->show_batch_for_new_batch_transfer($id3);

             $this->loadViews("show_freez_form", $this->global,$query,NULL);
             //$this->load->view('show_freez_form',$query);
   
        }


        public function unfreez_student(){
            $status=$this->input->post('status');
             $batch_id = $this->input->post('batch_id');
            " </br>";

             $enrollment_id = $this->input->post('enrollment_id');
             " </br>";

            $student_id = $this->input->post('student_id');
             " </br>";

             $module_id = $this->input->post('module_id');

            if($status=='same')
            {

            

                $data = array(


                
                'student_id'  => $student_id ,
                'batch_id'  => $batch_id ,
                'Freez' => 'UnFreez'
            
            );

             $this->db->where('enrollment_id',$enrollment_id);
            $query =  $this->db->update('enroll_student',$data);

            if ($query) {

                redirect(base_url()."profilestd?id=".$student_id);
               
            }else{
                echo "no";
            }
        }elseif($status=='new')
        {
            $course=$this->input->post('course_id');
            if($course!='')
            {

            $data = array(


                'course_id'=>$course,
                'student_id'  => $student_id ,
                'batch_id'  => $batch_id ,
                'Freez' => 'UnFreez'
            
            );

             $this->db->where('enrollment_id',$enrollment_id);
            $query =  $this->db->update('enroll_student',$data);

            if ($query) {

                redirect(base_url()."profilestd?id=".$student_id);
               
            }else{
                echo "no";
            }
        }
        else{
            redirect(base_url()."profilestd?id=".$student_id."&error=1");

        }





        }




            
            
            //echo "string";


        }












































public function ajax1(){



    $conn = mysqli_connect('localhost','root','','artt_db');

    $batch_id = $_GET['batch_id'];
    $std_id = $_GET['student_id'];
     $std_id;

    if($batch_id != "")
    {
    
        $query= "SELECT * FROM courses WHERE module_id = 
         (SELECT module_id FROM batch WHERE batch_id ='".$batch_id."')";

        


$query4 ="SELECT program_id, module_id,batch_id,batch_name FROM batch WHERE batch_id='".$batch_id."'";
         
        
         $res4 = mysqli_query($conn,$query4);
        
        
        



         
        $res = mysqli_query($conn,$query);
        ?>
        <input type="hidden" name="std_id" value="<?php echo $std_id; ?>">
        <label>Courses Name</label>
        <hr>
        <center>
        
                <?php
             while($row4 = mysqli_fetch_assoc($res4))
                { ?>
                <input type="hidden" name="program_id" value="<?php echo $row4['program_id']; ?>">
                <input type="hidden" name="batch_id" value="<?php echo $row4['batch_id']; ?>">
                <input type="hidden" name="module_id" value="<?php echo $row4['module_id']; ?>">
        <?php } ?> 
            
            
                
    
            
            <?php
                while($row = mysqli_fetch_assoc($res))
                {
                    ?>
                    
                    <input style="width:15px ; height:15px; float:left;" id="batch_id" class="minimal" class="form-control"  name="course_id" type="radio" value="<?php echo $row['course_id']; ?>" >
                    <label style=" margin:right:20px;"><?php echo $row['coursename']; ?></label>
                    
                    
                    <br>
                    <?php
                }
            ?>
            </center>
        
        <?php
    }




   }
















//OSMAN


public function settings(){

        $this->global['pageTitle'] = 'ARTT : Settings';
        $data['Head'] = 'ADMIN SETTING AREA';
        $this->loadViews('settingPage',$this->global,$data,NULL);
    }



     public function setting_update()
     {
         $this->global['pageTitle'] = 'ARTT : Settings';

    $config = array(
             'upload_path' => "./assets/artt_images/cprofile/",
             'allowed_types' => "jpg|png|jpeg",
             'max_size' => "3048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
             'max_height' => "2000",
             'encrypt_name' => TRUE,
             'max_width' => "2000"
         );

         $this->load->library('upload', $config);


         if($this->upload->do_upload('clogo'))
         {

             $data = array(
                 'companyName' => $this->input->post('cname'),
                 'aboutCompany' => $this->input->post('cinfo'),
                 'foundingDate' => $this->input->post('cdate'),
                 'website' => $this->input->post('cwebsite'),
                 'address' => $this->input->post('caddress'),
                 'email' => $this->input->post('cemail'),
                 'contactNo' => $this->input->post('ccell')
             );

             $info = $this->upload->data();
             $image_path = base_url("assets/artt_images/cprofile/".$info['raw_name'].$info['file_ext']);
             $data['logo'] =$image_path;

             if ($this->user_model->companyprofile($data)){

                 $data['cdata'] = $this->user_model->getcprofile();
                 $data['Head'] = 'ADMIN SETTING AREA';
                 $this->loadViews('settingPage',$this->global,$data,NULL);
             }
         else
         {

             $error = array('error' => $this->upload->display_errors());
             $this->loadViews('settingPage',$this->global,$error,NULL);
         }
}
     }



     
     
     public function refund(){

       $query['students'] = $this->Main_Model->retrieve_student_data();
       //print_r($query['students']);
       $this->loadViews("refundpg", $this->global,$query,NULL);

   }


public function refundcase(){


//        $id = $_GET['id'];

        $sid = $this->uri->segment('3');


       $sid =  explode('-',$sid);
       $sid = $sid[1];
//        echo $sid; die();



//        $data['show_student_profile'] = $this->Main_Model->show_student_profile($sid);
        $data['show_fee_details_in_student_profile2'] = $this->Main_Model->show_fee_details_in_student_profile2($sid);

        $this->loadViews("refundcase", $this->global,$data,NULL);

        }

//        public function refund_percentage(){
//
//        $postData = $this->input->post();
//        $data = $this->Main_model->getrefundDetails($postData);
//        echo json_encode($data);
//            }







        public function refund_applications(){

            $data = $this->input->post();
            $query['students'] = $this->Main_Model->retrieve_student_data($data);
            $this->loadViews("refund_approvals", $this->global,$query,NULL);
            }



//OSMAN

public function credit_voucher_insert()
    {
        $id = $this->uri->segment(3);

//            $_GET['id'];


        $per = $this->input->post('per');
        $refund_amount = $this->input->post('refund_amount');
        $msg = $this->input->post('msg');
        $courseid = $this->input->post('course_id_refund');
        $batchid = $this->input->post('batch_id');


        $total_fees = $this->input->post('total_fees');
        $student_id = $this->input->post('studentid');
        $pay_amount = $this->input->post('pay_amount');
        $remain_fees = $this->input->post('remain_fees');
        $batch_name = $this->input->post('batch_name');
        $current_date = $this->input->post('current_date');
        $due_date = $this->input->post('due_date');
        $pay_with = $this->input->post('pay_with');
        $cheque_number = $this->input->post('cheque_number');


        $data1 = array(
            'total_fees' => $total_fees,
            'student_id' => $student_id,
            'remain_fees' => $remain_fees,
            'batch_name' => $batch_name,
            'current_date' => $current_date,
            'due_date' => $due_date,
            'pay_with' => $pay_with,
            'cheque_number' => $cheque_number,
            'pay_amount' => $pay_amount
        );

        $this->db->insert('master_vocher',$data1);

        $refund_data = array(
                'per' => $per,
                'refund_amount' => $refund_amount,
                'msg' => $msg,
                'courseid' => $courseid,
                'batch_id' => $batchid

        );

        $course_id = $this->input->post('course_id[]');
        $student_id = $this->input->post('student_id[]');
        $received_amount = $this->input->post('received_amount[]');
        $voucher_amount = $this->input->post('voucher_amount[]');
        $count = count($received_amount);

        for($i=0; $i<$count; $i++) {

            $data = array(
//                'received_amount' => $received_amount[$i]+$voucher_amount[$i],
                'voucher_amt' => $voucher_amount[$i]

            );
            $this->db->where('student_id',$student_id[$i]);
            $this->db->where('course_id',$course_id[$i]);
            $this->db->update('enroll_student',$data);

        }

            $this->Main_Model->update_refund_status($refund_data,$id);
            $data['print_voucher'] = $this->Main_Model->print_voucher_refund($id,$courseid);
            $data['print_voucher1'] = $this->Main_Model->print_voucher1_refund($id, $pay_amount, $remain_fees);

            $this->loadViews("print_credit_voucher", $this->global, $data, NULL);

    }




    public function insert_freeze_from_student_profile(){

        //echo "Yas";

        //exit();

            $id=$this->input->get('id');
            $id2=$this->input->get('id2');
            $datas = array(
                'Freez' => 'Freez',
                'freezdate' => date("d-m-y")
                );
            $this->db->where('enrollment_id',$id);
            $this->db->update('enroll_student',$datas);
            
            
            redirect(base_url()."profilestd?id=".$id2);
            //('profilestd'.$id2);
    

    }









public function std_refund_voucher(){

//  $id = $_GET['id'];


        $id = $this->uri->segment(3);
        $crs_id = $this->input->post('course_id');
        $refund_data = $this->input->post();


        $data['show_refund_details'] = $refund_data;
        $data['show_student_details'] = $this->Main_Model->show_student_details($id);
        $data['show_fee_details_in_student_profile'] = $this->Main_Model->show_fee_details_in_student_profile1($id,$crs_id);

        $data['show_discount_details_in_voucher'] = $this->Main_Model->show_discount_details_in_voucher($id);
        $this->loadViews("stdRefundvoucher", $this->global,$data,NULL);

    }

 //OSMAN           












    



     //OSMANCLOUSE

    //expence


    public function all_expense(){


            $data['all_expense'] = $this->Main_Model->all_expense();

            $this->loadViews("expense", $this->global,$data,NULL);
            
        }



         public function show_insert_expense(){

        
            

            $this->loadViews("add_expense", $this->global);
            //m$this->load->view('add_expense');
        }

         public function insert_expense(){




        $data= $this->input->post();
        $this->load->model('Main_Model');
        $query = $this->Main_Model->insert_expense($data);
        
        ('all_expense');


        }

        public function show_expense_details($id){
            $query['data'] =  $this->Main_Model->show_expense_details($id); 
                                                
            $this->loadViews("show_expense_details", $this->global,$query,NULL);
        }

        public function show_deny_message($id){

        $this->load->model('Main_Model');
        $this->Main_Model->show_deny_message($id);

        $dataa['data'] = $this->Main_Model->show_deny_message($id);

        $this->loadViews("show_deny_message", $this->global,$dataa,NULL);

        //$this->load->view('BKO/show_deny_message',$dataa);


}




        //expence_Clouse












































    /**
     * This function is used to load the user list
     */
    function userListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->user_model->userListingCount($searchText);

			$returns = $this->paginationCompress ( "userListing/", $count, 10 );
            
            $data['userRecords'] = $this->user_model->userListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'CodeInsect : User Listing';
            
            $this->loadViews("users", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
            $data['roles'] = $this->user_model->getUserRoles();
            
            $this->global['pageTitle'] = 'CodeInsect : Add New User';

            $this->loadViews("addNew", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to check whether email already exist or not
     */
    function checkEmailExists()
    {
        $userId = $this->input->post("userId");
        $email = $this->input->post("email");

        if(empty($userId)){
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
    
    /**
     * This function is used to add new user to the system
     */
    function addNewUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('password','Password','required|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');
            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                
                $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>$roleId, 'name'=> $name,
                                    'mobile'=>$mobile, 'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));
                
                $this->load->model('user_model');
                $result = $this->user_model->addNewUser($userInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New User created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User creation failed');
                }
                
                redirect('addNew');
            }
        }
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($userId = NULL)
    {
        if($this->isAdmin() == TRUE || $userId == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($userId == null)
            {
                redirect('userListing');
            }
            
            $data['roles'] = $this->user_model->getUserRoles();
            $data['userInfo'] = $this->user_model->getUserInfo($userId);
            
            $this->global['pageTitle'] = 'CodeInsect : Edit User';
            
            $this->loadViews("editOld", $this->global, $data, NULL);
        }
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $userId = $this->input->post('userId');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('password','Password','matches[cpassword]|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','matches[password]|max_length[20]');
            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($userId);
            }
            else
            {
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                
                $userInfo = array();
                
                if(empty($password))
                {
                    $userInfo = array('email'=>$email, 'roleId'=>$roleId, 'name'=>$name,
                                    'mobile'=>$mobile, 'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
                }
                else
                {
                    $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>$roleId,
                        'name'=>ucwords($name), 'mobile'=>$mobile, 'updatedBy'=>$this->vendorId, 
                        'updatedDtm'=>date('Y-m-d H:i:s'));
                }
                
                $result = $this->user_model->editUser($userInfo, $userId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'User updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User updation failed');
                }
                
                redirect('userListing');
            }
        }
    }


    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
            $userInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->deleteUser($userId, $userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
    
    /**
     * Page not found : error 404
     */
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'CodeInsect : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    /**
     * This function used to show login history
     * @param number $userId : This is user id
     */
    function loginHistoy($userId = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $userId = ($userId == NULL ? 0 : $userId);

            $searchText = $this->input->post('searchText');
            $fromDate = $this->input->post('fromDate');
            $toDate = $this->input->post('toDate');

            $data["userInfo"] = $this->user_model->getUserInfoById($userId);

            $data['searchText'] = $searchText;
            $data['fromDate'] = $fromDate;
            $data['toDate'] = $toDate;
            
            $this->load->library('pagination');
            
            $count = $this->user_model->loginHistoryCount($userId, $searchText, $fromDate, $toDate);

            $returns = $this->paginationCompress ( "login-history/".$userId."/", $count, 10, 3);

            $data['userRecords'] = $this->user_model->loginHistory($userId, $searchText, $fromDate, $toDate, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'CodeInsect : User Login History';
            
            $this->loadViews("loginHistory", $this->global, $data, NULL);
        }        
    }

    /**
     * This function is used to show users profile
     */
    function profile($active = "details")
    {
        $data["userInfo"] = $this->user_model->getUserInfoWithRole($this->vendorId);
        $data["active"] = $active;
        
        $this->global['pageTitle'] = $active == "details" ? 'CodeInsect : My Profile' : 'CodeInsect : Change Password';
        $this->loadViews("profile", $this->global, $data, NULL);
    }

    /**
     * This function is used to update the user details
     * @param text $active : This is flag to set the active tab
     */
    function profileUpdate($active = "details")
    {
        $this->load->library('form_validation');
            
        $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
        $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->profile($active);
        }
        else
        {
            $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
            $mobile = $this->security->xss_clean($this->input->post('mobile'));
            
            $userInfo = array('name'=>$name, 'mobile'=>$mobile, 'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->editUser($userInfo, $this->vendorId);
            
            if($result == true)
            {
                $this->session->set_userdata('name', $name);
                $this->session->set_flashdata('success', 'Profile updated successfully');
            }
            else
            {
                $this->session->set_flashdata('error', 'Profile updation failed');
            }

            redirect('profile/'.$active);
        }
    }

    /**
     * This function is used to change the password of the user
     * @param text $active : This is flag to set the active tab
     */
    function changePassword($active = "changepass")
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('oldPassword','Old password','required|max_length[20]');
        $this->form_validation->set_rules('newPassword','New password','required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword','Confirm new password','required|matches[newPassword]|max_length[20]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->profile($active);
        }
        else
        {
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');
            
            $resultPas = $this->user_model->matchOldPassword($this->vendorId, $oldPassword);
            
            if(empty($resultPas))
            {
                $this->session->set_flashdata('nomatch', 'Your old password is not correct');
                redirect('profile/'.$active);
            }
            else
            {
                $usersData = array('password'=>getHashedPassword($newPassword), 'updatedBy'=>$this->vendorId,
                                'updatedDtm'=>date('Y-m-d H:i:s'));
                
                $result = $this->user_model->changePassword($this->vendorId, $usersData);
                
                if($result > 0) { $this->session->set_flashdata('success', 'Password updation successful'); }
                else { $this->session->set_flashdata('error', 'Password updation failed'); }
                
                redirect('profile/'.$active);
            }
        }
    }
    public function get_module()
    {
         $id=$this->input->post('name');
        // echo $id;
        $result=$this->user_model->getmodeule($id);
        if(!empty($result))
        {
            $option='<option value="">Select Program</option>';
            foreach($result as $k=>$v)
            {
                $option.="<option value='".$v['module_id']."'>".$v['module_name']."</option>";
            }
            echo $option;
        }
        else
        {
            $option='<option value="">Select Program</option>';
            $option.='<option value="">No Module</option>';
            echo $option;

        }
    }
}

?>