h
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="offset-sm-1 col-sm-10">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                   
                                    <li class="breadcrumb-item active">Transfer Batch</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Batch Transfer</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Student</h4>                
                                <form action="<?php echo base_url('Main/insert_enroll_student'); ?>" method="post">
                                   

                                    <div class="form-group">
                                        <label>Program Name</label>
                                        
                                            <select class="form-control" id="program_id" name="program_id" onChange="changebatch()">
                                                <option>Select</option>
                                          <?php

                                            //$conn = mysqli_connect('localhost','root','','artt');

                                            //$query = mysqli_query($conn,'select * from program');

                                            //while($row = mysqli_fetch_array($query))
                                            //{
                                                ?>
                                                    
                                   <!--<option value="<?php //echo $row['program_id']; ?>"><?php //echo $row['program_name']; ?></option>-->
                                                
                                                <?php
                                            //}

                                          ?>



                                          <?php
                                   foreach($all_pro as $sel):
                                    ?>
                                   <option value="<?php echo $sel->program_id; ?>"><?php echo $sel->program_name; ?></option>
                                    <?php
                                        endforeach;
                                    ?>
                                            </select>
                                    </div>

                                    <div class="form-group" id="batch">
                                        <label>Batch Name</label>

                                            <select class="form-control" name="batch_id" id="sel1" >
                                          <option class="form-control" value="">Choose Batch</option>
                                          <?php

                                    //foreach($all_bat as $sel):
                                    ?>
                                   <option value="<?php //echo $sel->batch_id; ?>"><?php //echo $sel->batch_name; ?></option>
                                    <?php
                                        //endforeach;
                                    ?>
                                            </select>
                                    </div>
                                    

                                    <div class="form-group" id="student">
                                       
                                          <?php

                                    //foreach($all_stds as $sel):
                                    ?>
                                   <!--<option value="<?php //echo $sel->studentid; ?>"><?php //echo $sel->fname; ?></option>-->
                                    <?php
                                       // endforeach;
                                    ?>
                                            </select>
                                    </div>
                                                                        
                               <!--     <div class="form-group"  style="box-shadow: 3px 4px 7px -3px #bfbfbf;
    border-bottom: 1px dotted #4d54674a;
    padding: 10px 0px;">
                                        <label>Module Name</label><br>
                                    <center>
                                    <?php
                                        foreach($all_mod as $sel):
                                    ?>
                                        <label><input class="form-control" name="" type="checkbox" value="<?php echo $sel->module_id; ?>">&nbsp;<?php echo $sel->module_name; ?></label>&nbsp;&nbsp;&nbsp;
                                    <?php
                                        endforeach;
                                    ?>
                                </center>
                                    </div>-->

                                    <div id = "course_id" class="form-group"  style="box-shadow: 3px 4px 7px -3px #bfbfbf;
    border-bottom: 1px dotted #4d54674a;
    padding: 10px 0px;">
                                        <label>Course Name</label><br>
                              <!--      <center>
                                    <?php
                                        foreach($show_courses as $sel):
                                    ?>
                                        <label><input class="form-control" name="" type="checkbox" value="<?php echo $sel->course_id; ?>">&nbsp;<?php echo $sel->coursename; ?></label>&nbsp;&nbsp;&nbsp;
                                    <?php
                                        endforeach;
                                    ?>
                                </center> -->
                                        
                                     
                                    </div>

                                    <div class="form-group">
                                        <label>Select Status<span class="required">*</span></label>
                                        <div>
                                            
                                            <select class="form-control" id="sel1" name="status">
                                                <option>Select</option>
                                          <option class="form-control" value="1">Enable</option>
                                          <option class="form-control" value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>




                                     <div class="form-group">
                                        <label>Select Paid Status<span class="required">*</span></label>
                                        <div>
                                            
                                            <select class="form-control" id="sel1" name="Paid">
                                                <option>Select</option>
                                          <option class="form-control" value="1">Paid</option>
                                          <option class="form-control" value="0">UnPaid</option>
                                            </select>
                                        </div>
                                    </div>



                                   
                                    <div class="form-group">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button>
                                        </div>
                                    </div>


                                </form>
<a href="<?php echo base_url('Main/show_stds'); ?>">
                                            <button style="float: right; margin: -52px 0px;" type="reset" class="btn btn-secondary waves-effect m-l-5">
                                                Cancel
                                            </button></a>
                            </div>
                        </div>
                    </div> <!-- end col -->

                    
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
       
<?php $this->load->view('BKO/includes/footer'); ?>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>    
<script type="text/javascript">
    function changebatch()
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('GET','batchajax?program='+document.getElementById('program_id').value,false);
        xmlhttp.send(null);
        
        document.getElementById('batch').innerHTML=xmlhttp.responseText;
    }
</script>
<script type="text/javascript">
    function changestudent()
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('GET','stdajax?batch='+document.getElementById('batch_id').value,false);
        xmlhttp.send(null);
        
        document.getElementById('student').innerHTML=xmlhttp.responseText;
    }
</script>

<script type="text/javascript">
    function course_change()
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('GET','ajax_courses?course='+document.getElementById('module_id').value,false);
        xmlhttp.send(null);
        
        document.getElementById('course_id').innerHTML=xmlhttp.responseText;
    }
</script>