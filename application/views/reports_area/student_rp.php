<!DOCTYPE html>
<html>

<head>
    <title> <?= $title ?> </title>
</head>
<body>

<div class="col-md-12">

    <?php foreach ($cinfo as $u_key){ ?>
    <img src="<?php base_url(); ?>assets/images/faviicon.png" style="height: 60px; width: 60px; margin-top: 1%;">
    <h1 style="color: green; text-align: center;    margin-top: -6%;"> <?php echo $u_key->companyName; ?>  </h1>

    <?php date_default_timezone_set("Asia/Karachi"); ?>
    <p style="text-align: right; margin-left: 8%; font-size: 12px;"><?= date("h:i:sa"); ?> <?= date('d-m-Y'); ?></p>


    <p style="text-align: center; margin-top: -120px; font-weight: bolder"><?= $title ?>    </p>
    <hr>

</div>

<!--(Yearly/Quarterly/Monthly Report)-->

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div style="overflow-x:auto;">
                        <table id="example2" class="table table-bordered table-hover" style="width: 100%; ">
                            <thead>
                            <tr style="background-color: #e0e7f7">
                                <th style="text-align: center;padding-bottom: 15px;">SNo.</th>
                                <th style="text-align: center;padding-bottom: 15px;">Student ID</th>
                                <th style="text-align: center;padding-bottom: 15px;">Student Name</th>
                                <th style="text-align: center;padding-bottom: 15px;">Course Name</th>
                                <th style="text-align: center;padding-bottom: 15px;">Course Code</th>


                            </tr>
                            </thead>
                            <tbody>
                            <?php $counter = 1;  foreach ($result as $key) { ?>
                            <tr>


                                <td style="text-align: center;"><?= $counter++; ?></td>
                                <td style="text-align: center;"><b><?= $key->artt_id; ?></b></td>
                                <td style="text-align: center;"><?= $key->fname; ?></td>
                                <td style="text-align: center;"><?= $key->coursename; ?></td>
                                <td style="text-align: center;"><?= $key->coursecode; ?></td>


                            </tr>


                            <?php
                            }?>


                            </tbody>

                        </table>

                        <br>
                    </div>

                </div>
                <!-- /.box-body -->

            </div>
            <!-- /.box -->


        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

</section>















<!--<hr style="border: 3px solid black; margin-top: -4px;">-->

<div style="z-index: 100; position: fixed; bottom: 38px;">
    <hr>
    <p style="font-size: 13px;    margin-bottom: -1%; text-align: center"><?php echo $u_key->aboutCompany; ?></p>
    <p style="text-align: center; font-size: 13px; padding-top: -5px;"><?php echo $u_key->website;?>&nbsp;&nbsp;<?php echo $u_key->contactNo; ?>&nbsp;&nbsp;<?php echo $u_key->address; ?>&nbsp;&nbsp;<?php echo $u_key->email; ?></p>
</div>

<?php } ?>

</body>
</html>