
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                   
                                    <li class="breadcrumb-item active">Update Batch</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Update</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            
                <div class="row">
                    <div class="col-lg-8">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Update Data</h4>
                                <?php 
                                foreach($all_batch as $show):
                                ?>                
                                <form action="<?php echo base_url('Main/update_bat/').$show->batch_id; ?>" method="post">

                                     <input type="hidden" name="text_hid" value="<?php echo $show->batch_id; ?>">

                                    <div class="form-group">
                                        <label>Program<span class="required">*</span></label>
                                        <div>
                                            <select class="form-control" id="program_id" name="program_id">
                                               
                                              <?php
                                            foreach($show_program_for_update_batch as $pro):
                                            ?>
                                                <option value="<?php echo $pro->batch_id; ?>"><?php echo $pro->program_name ?></option>


                                                   <?php
                                                    endforeach;
                                                ?>
                                            <?php
                                            foreach($all_pro as $pro):
                                            ?>
                    
                                          <option class="form-control" value="<?php echo $pro->program_id; ?>"><?php echo $pro->program_name ?></option>
                                                
                                                <?php
                                                    endforeach;
                                                ?>

                                          </select>
                                        </div>
                                    </div>



                                      <div class="form-group">
                                        <label>Module<span class="required">*</span></label>
                                        <div>
                                            <select class="form-control" id="module_id" name="module_id">
                                               
                                                

    
                                              <?php
                                            foreach($show_mod_for_update_batch as $pro):
                                            ?>
                                                <option value="<?php echo $pro->module_id; ?>"><?php echo $pro->module_name ?></option>


                                                   <?php
                                                    endforeach;
                                                ?>

                                            <?php
                                            foreach($show_mod as $pro):
                                            ?>
                    
                                          <option class="form-control" value="<?php echo $pro->module_id; ?>"><?php echo $pro->module_name ?></option>
                                                
                                                <?php
                                                    endforeach;
                                                ?>

                                          </select>
                                        </div>
                                    </div>

                                    
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control" value="<?php echo $show->batch_name; ?>" />
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Start</label>
                                        <div>
                                            <input type="date" class="form-control" name="doj" 
                                                   value="<?php echo $show->startdate; ?>"/>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label>End</label>
                                        <div>
                                            <input type="date" class="form-control" name="doe" 
                                                   value="<?php echo $show->enddate; ?>"/>
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label>Select Status<span class="required">*</span></label>
                                        <div>
                                            
                                            <select class="form-control" id="sel1" name="sel1">
                                                <option>Select</option>
                                          <option class="form-control" value="1">Enable</option>
                                          <option class="form-control" value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <!--
                                    <div class="form-group">
                                        <label>Number</label>
                                        <div>
                                            <input data-parsley-type="number" type="text"
                                                   class="form-control" required
                                                   placeholder="Enter only numbers"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Alphanumeric</label>
                                        <div>
                                            <input data-parsley-type="alphanum" type="text"
                                                   class="form-control" required
                                                   placeholder="Enter alphanumeric value"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Textarea</label>
                                        <div>
                                            <textarea required class="form-control" rows="5"></textarea>
                                        </div>
                                    </div>-->
                                    <div class="form-group">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                Update
                                            </button>
                                           
                                        </div>
                                    </div>
                                </form>
                                <a href="<?php echo base_url('Main/batchs'); ?>">
                                            <button style="float: right; margin: -52px 0px;" type="reset" class="btn btn-secondary waves-effect m-l-5">
                                                Cancel
                                            </button></a>
<?php endforeach; ?>
                            </div>
                        </div>
                    </div> <!-- end col -->

                    
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
       
<?php $this->load->view('BKO/includes/footer'); ?>
     
