 <!DOCTYPE html>
<!--
 * A Design by GraphBerry
 * Author: GraphBerry
 * Author URL: http://graphberry.com
 * License: http://graphberry.com/pages/license
-->
<html lang="en">
    
    <head>
        <meta charset=utf-8>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Interflow</title>
        <!-- Load Roboto font -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


        <!-- Load css styles -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/new/css/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/new/css/bootstrap-responsive.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/new/css/style.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/new/css/pluton.css"/>
        <!--[if IE 7]>
            <link rel="stylesheet" type="text/css" href="css/pluton-ie7.css" />
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="  <?php echo base_url(); ?>/assets/new/css/jquery.cslider.css" />
        <link rel="stylesheet" type="text/css" href="  <?php echo base_url(); ?>/assets/new/css/jquery.bxslider.css" />
        <link rel="stylesheet" type="text/css" href="  <?php echo base_url(); ?>/assets/new/css/animate.css" />
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>/assets/new/images/faviicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>/assets/new/images/faviicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>/assets/new/faviicon.png">
        <link rel="apple-touch-icon-precomposed" href="  <?php echo base_url(); ?>/assets/new/images/faviicon.png">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/new/images/faviicon.png">
    </head>
    <style type="text/css">
    
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
   /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
margin-left: 0px;

  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

.modal-3{
height:400px;
 width:400px;

}
.modal-content {
  position: absulot;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  border: 1px solid #888;
  width: 80%;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
  -webkit-animation-name: animatetop;
  -webkit-animation-duration: 0.4s;
  animation-name: animatetop;
  animation-duration: 0.4s;
  width: 700px;
    height: 500px;
}
#close-icon
{
  position: absolute;
    font-size: 27px;
    margin: 0px;
}
 img.pro {
    height: 300px !important;
    width: 100% !important;
}

    </style>
    <body>


        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <a href="#" class="brand">
                        <img src="  <?php echo base_url(); ?>assets/new/images/logo2.png" width="120" height="40" alt="Logo" />
                        <!-- This is website logo -->
                    </a>
                    <!-- Navigation button, visible on small resolution -->
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <i class="icon-menu"></i>
                    </button>
                    <!-- Main navigation -->
                    <div class="nav-collapse collapse pull-right">
                        <ul class="nav" id="top-navigation">
                            <li class="active"><a href="<?php echo base_url() ?>login/index2">Home</a></li>
                            <li><a href="#service">Services</a></li>
                            <li><a href="<?php echo base_url() ?>Menu/product">Products</a></li>
                            <li><a href="#brands">Brands</a></li>
                            <li><a href="#price">Price</a></li>
                            <li><a href="#contact">Contact</a></li>
 <li><a href="<?php echo base_url() ?>login/main">Login</a></li>
                        </ul>
                    </div>
                    <!-- End main navigation -->
                </div>
            </div>
        </div>
        <!-- Start home section -->
  <!-- End home section -->
        <!-- Service section start -->
     
        <!-- Service section end -->
        <!-- Portfolio section start -->
     <!--    <div class="section secondary-section " id="portfolio">
            <div class="triangle"></div>
            <div class="container"> -->
            
              
 
<<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 style="color: #ffffff; text-align:center; font-family: cursive;"> All Videos
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
              <div class="box" style="opacity:1; background-color: #e8e7e7; margin-top: 0%;">
                <!-- <div id="preloader"></div> -->
                <div class="box-body table-responsive">




  <?php
                 if(@$videos)
                 {

                  //$no = 1;
                   foreach ($videos as $k=>$v) {
                     $url =ltrim($v['FilePath'] , ".'\'");
                      //$url =ltrim($v['FilePath'] , "..'\'");

                   ?>

                   <div class="card">

  <video id="my_video" style="height:200px; width: 100%;"  controls>

  <source
    src="<?php echo base_url().$url;?>"
      type="video/mp4" preload="metadata" >
  <source
    src="<?php echo $v['FilePath'];?>"
    type="video/mp4" preload="metadata">
  <source
    src=""
    type="video/ogg">
  <source
    src="<?php echo '../'.$v['FilePath'];?>"
    type="video/avi" preload="metadata">
  <source
    src="<?php echo $v['FilePath'];?>"
    type="video/avi" preload="metadata">
  <source
    src="<?php echo base_url().$url;?>"
    type="video/mov" preload="metadata">
  <source
    src="<?php echo base_url().$url;?>"
    type="video/mov" preload="metadata">
  Your browser doesn't support HTML5 video tag.
</video>

<h5><?php echo $v['FileName'];?></h5>
</div>

<?php }} ?>








                </div><!-- /.box-body -->

              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>

 
                    
 
        <!--         </div>
            </div>
        </div> -->
        <!-- Portfolio section end -->
        <!-- About us section start -->
 
        <!-- Client section start -->
       
        <div class="section third-section" id="brands">
            <div class="container centered">
                <div class="sub-section">
                    <div class="title clearfix">
                        <div class="pull-left" >
                            <h3>Our Brands</h3>
                        </div>
                        <ul class="client-nav pull-right">
                            <li id="client-prev"></li>
                            <li id="client-next"></li>
                        </ul>
                    </div>
                    <ul class="row client-slider" id="clint-slider">
                         <?php foreach($brand as $images) {?>
                        <li>
                            <a href="">
                                <img src="<?php echo base_url()."assets/images/brand/".$images->image;?>"alt="client logo 1" style="width:160px;height:80px;">
                            </a>
                        </li>
                        <?php } ?> 
                    </ul>
                </div>
            </div>
        </div>
 
        <div class="footer">
            <p>&copy; 2013 Theme by <a href="http://www.graphberry.com">GraphBerry</a>, <a href="http://goo.gl/NM84K2">Documentation</a></p>
        </div>
        <!-- Footer section end -->
        <!-- ScrollUp button start -->
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

         <script> 
var modal = document.getElementById('myModal');


function clickf(){
    modal.style.display = "block";
}

function close2() {
  modal.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
  
</script> 
        <!-- ScrollUp button end -->
        <!-- Include javascript -->
        <script src="<?php echo base_url() ?>/assets/new/js/jquery.js"></script>
        <script type="text/javascript" src="  <?php echo base_url(); ?>/assets/new/js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="  <?php echo base_url(); ?>/assets/new/js/bootstrap.js"></script>
        <script type="text/javascript" src="  <?php echo base_url(); ?>/assets/new/js/modernizr.custom.js"></script>
        <script type="text/javascript" src="  <?php echo base_url(); ?>/assets/new/js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="  <?php echo base_url(); ?>/assets/new/js/jquery.cslider.js"></script>
        <script type="text/javascript" src="  <?php echo base_url(); ?>/assets/new/js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="  <?php echo base_url(); ?>/assets/new/js/jquery.inview.js"></script>
        <!-- Load google maps api and call initializeMap function defined in app.js -->
        <script async="" defer="" type="text/javascript" src=" https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <!-- css3-mediaqueries.js for IE8 or older -->
        <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="  <?php echo base_url(); ?>/assets/new/js/app.js"></script>
            <style type="text/css">
                .image-one img{
                        object-fit: fill !important;
    height: auto !important;
    width: -webkit-fill-available;
    display: block!important;
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: 100%;
    height: 500px !important;
                }
            </style>
        </body>
</html>
