
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="offset-sm-1 col-sm-10">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                   
                                    <li class="breadcrumb-item active">Enroll Teacher</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Enrollment</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Teacher</h4>                
                                <form action="<?php echo base_url('Main/insert_enrolled_teachers'); ?>" method="post">
                                   

                                    <div class="form-group">
                                        <label>Teacher Name</label>

                                            <select class="form-control" id="sel1" name="teacher_id">
                                          <option class="form-control" value="">Choose Teacher</option>
                                                <?php
                                    foreach($all_teacher as $sel):
                                    ?>
                                   <option value="<?php echo $sel->teacherid; ?>"><?php echo $sel->fname; ?></option>
                                    <?php
                                       endforeach;
                                    ?>
                                    
                                            </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Program Name</label>
                                        
                                            <select class="form-control" id="sel1" name="program_id">
                                                <option>Select Program</option>
                                          <?php
                                    foreach($all_pro as $sel):
                                    ?>
                                   <option value="<?php echo $sel->program_id; ?>"><?php echo $sel->program_name; ?></option>
                                    <?php
                                        endforeach;
                                    ?>
                                            </select>
                                    </div>
                                    

                                    <div class="form-group">
                                        <label>Course Name</label>
                                        
                                            <select class="form-control" id="sel1" name="course_id">
                                                <option>Select</option>
                                          <?php
                                    foreach($show_courses as $sel):
                                    ?>
                                   <option value="<?php echo $sel->course_id; ?>"><?php echo $sel->coursename; ?></option>
                                    <?php
                                        endforeach;
                                    ?>
                                            </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Select Status<span class="required">*</span></label>
                                        <div>
                                            
                                            <select class="form-control" id="sel1" name="status">
                                                <option>Select</option>
                                          <option class="form-control" value="1">Enable</option>
                                          <option class="form-control" value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button>
                                            <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>


                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->

                    
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
       
<?php $this->load->view('BKO/includes/footer'); ?>
     
