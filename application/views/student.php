
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> All Student
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url() ?>addstd"><i class="fa fa-plus"></i> Add New</a>
                      <a class="btn btn-primary" href="<?php echo base_url().'charts';?>"><i class="fa fa-pie-chart"></i>Chart/ Records</a>
                </div>


            
                  
            
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">All Students</h3>
                   
                </div><!-- /.box-header -->
                <div class="box-body table-responsive ">




                     <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                       
                                        <th>ARTT ID</th>
                                        <th>CR#</th>
                                        <th>Name</th>
                                       
                                        <th>Status</th>
                                        <th>Batch Name</th>
                                         <th>Phone</th>
                                        
                                        
                                       
                        
                         <th>Actions</th> 
                    </tr>
                </thead>
                <tbody>
                 <?php
                  $no = 1;
                   foreach ($students->result() as $student) {
                     
                   ?>
               
                <tr>
                                       
                                       <td><?php echo $student->artt_id;   ?> </td>
                                        <td><?php echo $student->cr;   ?> </td>
                                         <td> <?php echo $student->fname;  ?> </td>
                                           
                                            <td style = "width : 20px" > <?php
                                                if($student->fstatus=="UnFreeze")
                                                {

                                                  echo "Regular";
                                                }
                                                if($student->fstatus=="Freeze")
                                                {

                                                  echo "Freezed";
                                                }elseif($student->fstatus=="Ragular"){
                                                  echo "Regular";
                                                }
                                                elseif($student->fstatus==""){
                                                  echo "Regular";
                                                }


                                                  ?>

                                              </td>
                                       <td> 
                                            <span style = "font-size : 14px; ">

      <a href="<?php echo base_url('enrollestuden') ?>?id=<?php echo $student->studentid ?>"> <?php echo $student->batch_name;  ?>
                  
      </a>
                                            </span>
                                            </td>

                                            <td>   
                                                 <?php echo $student->phone;  ?>
                                               </td>
                                             

                                         
                                       
                                        
                        <td >
                                                                  
        <a class="btn btn-sm btn-info" href="<?php echo base_url('editstd') ?>?id=<?php echo $student->studentid ?>" title="Edit"><i class="fa fa-pencil"></i> </a>
        
         <a class="btn btn-sm btn-info" href="<?php echo base_url('profilestd') ?>?id=<?php echo $student->studentid ?>" title="Profile"><i class="fa fa-fw fa-user"></i> </a>

          <a class="btn btn-sm btn-info" href="<?php echo base_url('enrollestuden') ?>?id=<?php echo $student->studentid ?>" title="Enrolle Student"><i class="fa fa-fw fa-user-plus"></i> </a>

           <a class="btn btn-sm btn-info" href="<?php echo base_url('stdvoucher') ?>?id=<?php echo $student->studentid ?>" title="Voucher"><i class="fa fa-fw fa-vimeo-square"></i> </a>

          <a class="btn btn-sm btn-info" href="<?php echo base_url('disscountInsert') ?>?id=<?php echo $student->studentid ?>" title="Disscount"><i class="fa fa-fw fa-money"></i> </a>
                      
            <a class="btn btn-sm btn-danger deleteUser" href="<?php echo base_url('delete_student') ?>?id=<?php echo $student->studentid ?>" data-userid="" title="Delete">
            <i class="fa fa-trash"></i>
        </a>
                        </td>
                  
                </tr>
              
 
               <?php } ?>
                </tbody>
                
              </table>











                  
                    
                
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>















