
<?php $this->load->view('Admin/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('Admin/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                   
                                    <li class="breadcrumb-item active">Insert BKO</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Insert Back Officer</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
    <div class="row" style="margin: 0px;">
        <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div>

            
                <div class="row">
                    <div class="col-lg-8">
                        <div class="card m-b-30">
                            <div class="card-body">
                                    <h4 class="mt-0 header-title">Insert Roll</h4>

                                <form action="<?php echo base_url('Main/insert_roll'); ?>" method="post">

                                <div class="form-group">
                                        <label>Roll</label>
                                        <input type="text" name="name" class="form-control" required placeholder="Insert Roll Name"/>
                                    </div>
                                                    
                                                                        <div class="form-group">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button>
                                            <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </form>


                                <h4 class="mt-0 header-title">Insert Data</h4>

                                                    
                                <form action="<?php echo base_url('Main/insert_bkofficer'); ?>" method="post">
                                   

                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control" required placeholder="Type something"/>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Program Name</label>
                                        <select class="form-control form-group" name="program" id="program">
                                            <option>--Select--</option>
                                    <?php
                                    foreach($all_pro as $sel):
                                    ?>
                                    <option value="<?php echo $sel->program_id; ?>"><?php echo $sel->program_name; ?></option>
                                    <?php
                                        endforeach;
                                    ?>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>E-Mail</label>
                                        <div>
                                            <input type="email" class="form-control" required
                                                   parsley-type="email" name="email" placeholder="Enter a valid e-mail"/>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Equal To</label>
                                        <div>
                                            <input type="password" id="pass2" name="password" class=" form-control" name="password" required
                                                   placeholder="Password"/>
                                        </div>
                                        <div class="m-t-10">
                                            <input type="password" class="form-control" name="re-password" required
                                                   data-parsley-equalto="#pass2"
                                                   placeholder="Re-Type Password"/>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label>Roll Name</label>
                                        <select class="form-control form-group" name="roll" id="roll">
                                            <option>--Select--</option>
                                    <?php
                                    foreach($all_roll as $sel):
                                    ?>
                                    <option value="<?php echo $sel->roll_id; ?>"><?php echo $sel->roll_name; ?></option>
                                    <?php
                                        endforeach;
                                    ?>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Select Status<span class="required">*</span></label>
                                        
                                        <div>
                                            
                                            <select class="form-control" id="sel1" name="status">
                                                <option>Select</option>
                                          <option class="form-control" value="1">Enable</option>
                                          <option class="form-control" value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button>
                                            <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->

                    
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
       
<?php $this->load->view('Admin/includes/footer'); ?>
     
