
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Sector_model extends CI_Model{
	public function get_sector()
	{
		$this->db->select('*');
		$this->db->from('sector');
		$result=$this->db->get();
		return $result->result_array();



	}
	public function get_sector_id($id)
 	{
 		$this->db->select('*');
 		$this->db->from('sector');
 		$this->db->where('SectorID',$id);
 		$result=$this->db->get();
 		return $result->result_array();

}

public function get_setor_img($img)
{
	$this->db->select('*');
	$this->db->from('sector');
	$this->db->where('sec_image',$img);
	$result=$this->db->get();
	return $result->result_array();
}

public function dashboardsector($name,$img){
$data=array('SectorName'=>$name,'sec_image'=>$img);
$result=$this->db->update('sector',$data);
return $result;
}

public function publicdata(){
$query =  $this->db->get('sector');
return $query->result();
}

	public function update_sector($id,$name,$img)
 	{
 		$data=array('SectorName'=>$name,'sec_image'=>$img);
 		$this->db->where('SectorID',$id);
 		$result=$this->db->update('sector',$data);
 		return $result;
 	}


	public function insertsector($name,$img){
		$data=array('SectorName'=>$name,'sec_image'=>$img);
		$result=$this->db->insert('sector',$data);
		return $result;

	}
	public function delete_sector($cat)
	{
		$this->db->where('SectorID', $cat);
        $this->db->delete('sector');


	}
	public function get_sector_data($id)
{
	$result=$this->db->select('*')->from('sector')->where('SectorID',$id)->get();
	return $result->result_array();
}



}
?>
