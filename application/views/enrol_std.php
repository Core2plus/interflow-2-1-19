<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Add New
        <small>Add / Edit User</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"> Add New </h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body table-responsive">
                    <?php $this->load->helper("form"); ?>
                     <h4 class="mt-0 header-title">--</h4>                
                                <form action="<?php echo base_url('insert_enroll_student'); ?>" method="post">
                                   

                                    <div class="form-group">
                                        <label>Program Name</label>
                                        
                                            <select class="form-control" id="program_id" name="program_id" onChange="changebatch()">
                                                <option>Select</option>
                                          <?php

                                            //$conn = mysqli_connect('localhost','root','','artt');

                                            //$query = mysqli_query($conn,'select * from program');

                                            //while($row = mysqli_fetch_array($query))
                                            //{
                                                ?>
                                                    
                                   <!--<option value="<?php //echo $row['program_id']; ?>"><?php //echo $row['program_name']; ?></option>-->
                                                
                                                <?php
                                            //}

                                          ?>



                                          <?php
                                   foreach($all_pro as $sel):
                                    ?>
                                   <option value="<?php echo $sel->program_id; ?>"><?php echo $sel->program_name; ?></option>
                                    <?php
                                        endforeach;
                                    ?>
                                            </select>
                                    </div>

                                    <div class="form-group" id="batch">
                                        <label>Batch Name</label>

                                            <select class="form-control" name="batch_id" id="sel1" >
                                          <option class="form-control" value="">Choose Batch</option>
                                          <?php

                                    //foreach($all_bat as $sel):
                                    ?>
                                   <option value="<?php //echo $sel->batch_id; ?>"><?php //echo $sel->batch_name; ?></option>
                                    <?php
                                        //endforeach;
                                    ?>
                                            </select>
                                    </div>
                                    

                                    <div class="form-group" id="student">
                                       
                                          <?php

                                    //foreach($all_stds as $sel):
                                    ?>
                                   <!--<option value="<?php //echo $sel->studentid; ?>"><?php //echo $sel->fname; ?></option>-->
                                    <?php
                                       // endforeach;
                                    ?>
                                            </select>
                                    </div>
                                                                        
                               <!--     <div class="form-group"  style="box-shadow: 3px 4px 7px -3px #bfbfbf;
    border-bottom: 1px dotted #4d54674a;
    padding: 10px 0px;">
                                        <label>Module Name</label><br>
                                    <center>
                                    <?php
                                        foreach($all_mod as $sel):
                                    ?>
                                        <label><input class="form-control" name="" type="checkbox" value="<?php echo $sel->module_id; ?>">&nbsp;<?php echo $sel->module_name; ?></label>&nbsp;&nbsp;&nbsp;
                                    <?php
                                        endforeach;
                                    ?>
                                </center>
                                    </div>-->

                                    <div id = "course_id" class="form-group"  style="box-shadow: 3px 4px 7px -3px #bfbfbf;
    border-bottom: 1px dotted #4d54674a;
    padding: 10px 0px;">
                                        <label>Course Name</label><br>
                              <!--      <center>
                                    <?php
                                        foreach($show_courses as $sel):
                                    ?>
                                        <label><input class="form-control" name="" type="checkbox" value="<?php echo $sel->course_id; ?>">&nbsp;<?php echo $sel->coursename; ?></label>&nbsp;&nbsp;&nbsp;
                                    <?php
                                        endforeach;
                                    ?>
                                </center> -->
                                        
                                     
                                    </div>

                                    <div class="form-group">
                                        <label>Select Status<span class="required">*</span></label>
                                        <div>
                                            
                                            <select class="form-control" id="sel1" name="status">
                                                <option>Select</option>
                                          <option class="form-control" value="1">Enable</option>
                                          <option class="form-control" value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>




                                     <div class="form-group">
                                        <label>Select Paid Status<span class="required">*</span></label>
                                        <div>
                                            
                                            <select class="form-control" id="sel1" name="Paid">
                                                <option>Select</option>
                                          <option class="form-control" value="1">Paid</option>
                                          <option class="form-control" value="0">UnPaid</option>
                                            </select>
                                        </div>
                                    </div>



                                   
                                    <div class="form-group">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button>
                                        </div>
                                    </div>


                                </form>
                            </div>
                </div></div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>