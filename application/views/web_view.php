 <!DOCTYPE html>
<!--
 * A Design by GraphBerry
 * Author: GraphBerry
 * Author URL: http://graphberry.com
 * License: http://graphberry.com/pages/license
-->
<html lang="en">
    
    <head>
        <meta charset=utf-8>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Interflow</title>
        <!-- Load Roboto font -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


        <!-- Load css styles -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/new/css/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/new/css/bootstrap-responsive.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/new/css/style.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/new/css/pluton.css"/>
        <!--[if IE 7]>
            <link rel="stylesheet" type="text/css" href="css/pluton-ie7.css" />
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="  <?php echo base_url(); ?>/assets/new/css/jquery.cslider.css" />
        <link rel="stylesheet" type="text/css" href="  <?php echo base_url(); ?>/assets/new/css/jquery.bxslider.css" />
        <link rel="stylesheet" type="text/css" href="  <?php echo base_url(); ?>/assets/new/css/animate.css" />
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>/assets/new/images/faviicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>/assets/new/images/faviicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>/assets/new/faviicon.png">
        <link rel="apple-touch-icon-precomposed" href="  <?php echo base_url(); ?>/assets/new/images/faviicon.png">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/new/images/faviicon.png">
    </head>
    <style type="text/css">
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
   /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
margin-left: 0px;

  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

.modal-3{
height:400px;
 width:400px;

}
.modal-content {
  position: absulot;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  border: 1px solid #888;
  width: 80%;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
  -webkit-animation-name: animatetop;
  -webkit-animation-duration: 0.4s;
  animation-name: animatetop;
  animation-duration: 0.4s;
  width: 700px;
    height: 500px;
}
#close-icon
{
  position: absolute;
    font-size: 27px;
    margin: 0px;
}
 

    </style>
    <body>
       <div class="modal" id="myModal">
  <div class="modal-content">
  
   
   <i class="fa fa-times-circle-o" style="color: teal; width:200px;
    height: 300; " onclick="close2()"></i>
     <video  width="100%" height="100%" controls autoplay>
     
      <source src="<?php echo base_url(); ?>/assets/new/images/7up (1).mp4" alt="project 1" type="video/mp4">
           
      
     </video>
  
</div>
</div>


        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <a href="#" class="brand">
                        <img src="  <?php echo base_url(); ?>assets/new/images/logo2.png" width="120" height="40" alt="Logo" />
                        <!-- This is website logo -->
                    </a>
                    <!-- Navigation button, visible on small resolution -->
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <i class="icon-menu"></i>
                    </button>
                    <!-- Main navigation -->
                    <div class="nav-collapse collapse pull-right">
                        <ul class="nav" id="top-navigation">
                            <li class="active"><a href="#home">Home</a></li>
                            <li><a href="#service">Services</a></li>
                            <li><a href="<?php echo base_url() ?>Menu/product">Products</a></li>
                            <li><a href="#brands">Brands</a></li>
                            <li><a href="#price">Price</a></li>
                            <li><a href="#contact">Contact</a> </li>
 <li><a href="<?php echo base_url() ?>login/main">Login</a></li>
                        </ul>
                    </div>
                    <!-- End main navigation -->
                </div>
            </div>
        </div>
        <!-- Start home section -->
<div id="home">

            <!-- Start cSlider -->
<div id="da-slider" class="da-slider">
<div class="triangle"></div>
<!-- mask elemet use for masking background image -->
<div class="mask"></div>
<!-- All slides centred in container element -->
<div class="container-fliud">
<!-- Start first slide -->
<div class="da-slide first-slide" style="background-color: red;">
<div class="image-one">
    <img src="<?php echo base_url(); ?>/assets/new/images/7up.png">
</div>
</div>
<!-- End first slide -->
<!-- Start second slide -->
<div class="da-slide second-slide" style="background-color: green;">
<div class="image-one">
    <img src="<?php echo base_url(); ?>/assets/new/images/uniliver.png">
</div>
</div>
<!-- End second slide -->
<!-- Start third slide -->
<div class="da-slide third-slide" style="background-color: yellow;">
<div class="image-one">
    <img src="<?php echo base_url(); ?>/assets/new/images/7up.png">
</div>
</div>
<!-- Start third slide -->
<!-- Start cSlide navigation arrows -->
<div class="da-arrows">
<span class="da-arrows-prev"></span>
<span class="da-arrows-next"></span>
</div>
<!-- End cSlide navigation arrows -->
</div>
</div>


</div>
        <!-- End home section -->
        <!-- Service section start -->
     
        <!-- Service section end -->
        <!-- Portfolio section start -->
        <div class="section secondary-section " id="portfolio">
            <div class="triangle"></div>
            <div class="container">
                <div class=" title">
                    <h1>Have You Seen our Works?</h1>
                    <p>Duis mollis placerat quam, eget laoreet tellus tempor eu. Quisque dapibus in purus in dignissim.</p>
                </div>
                <!-- <ul class="nav nav-pills">
                    
                    <li class="filter" data-filter="all">
                        <a href="#noAction">All</a>
                    </li>
                    <li class="filter" data-filter="web">
                        <a href="#noAction">Web</a>
                    </li>
                    <li class="filter" data-filter="photo">
                        <a href="#noAction">Photo</a>
                    </li>
                    <li class="filter" data-filter="identity">
                        <a href="#noAction">Identity</a>
                    </li>

                </ul> -->

<ul class="nav nav-pills">
                    
                    <li class="filter" data-filter="all">
                        <a href="#noAction">All</a>
                    </li>
                    <li class="filter" data-filter="7up">
                        <a href="#noAction">7up</a>
                    </li>
                    <li class="filter" data-filter="uniliver">
                        <a href="#noAction">Uniliver</a>
                    </li>
                    <li class="filter" data-filter="ufone">
                        <a href="#noAction">Ufone</a>
                    </li>
                    <li class="filter" data-filter="bata">
                        <a href="#noAction">BATA</a>
                    </li>
                    <li class="filter" data-filter="fantas">
                        <a href="#noAction">Fantas</a>
                    </li>
                    <li class="filter" data-filter="">
                        <a href="#noAction">Pepsi</a>
                    </li>
                    <li class="filter" data-filter="">
                        <a href="#noAction">Hilal</a>
                    </li>

                </ul>


                <!-- Start details for portfolio project 1 -->
                
                    <!-- End details for portfolio project 1 -->
                    <!-- Start details for portfolio project 2 -->
                    <div id="slidingDiv1" class="toggleDiv row-fluid single-project">
                        <div class="span6">
                            <img src="  <?php echo base_url(); ?>/assets/new/images/Portfolio02.png" alt="project 2">
                        </div>
                        <div class="span6">
                            <div class="project-description">
                                <div class="project-title clearfix">
                                    <h3>Webste for Some Client</h3>
                                    <span class="show_hide close">
                                        <i class="icon-cancel"></i>
                                    </span>
                                </div>
                                <div class="project-info">
                                    <div>
                                        <span>Client 2</span>Some Client Name</div>
                                    <div>
                                        <span>Date</span>July 2013</div>
                                    <div>
                                        <span>Skills</span>HTML5, CSS3, JavaScript</div>
                                    <div>
                                        <span>Link</span>http://examplecomp.com</div>
                                </div>
                                <p>Life is a song - sing it. Life is a game - play it. Life is a challenge - meet it. Life is a dream - realize it. Life is a sacrifice - offer it. Life is love - enjoy it.</p>
                            </div>
                        </div>
                    </div>
                    <!-- End details for portfolio project 2 -->
                    <!-- Start details for portfolio project 3 -->
                    <div id="slidingDiv2" class="toggleDiv row-fluid single-project">
                        <div class="span6">
                            <img src="  <?php echo base_url(); ?>/assets/new/images/Portfolio03.png" alt="project 3">
                        </div>
                        <div class="span6">
                            <div class="project-description">
                                <div class="project-title clearfix">
                                    <h3>Webste for Some Client</h3>
                                    <span class="show_hide close">
                                        <i class="icon-cancel"></i>
                                    </span>
                                </div>
                                <div class="project-info">
                                    <div>
                                        <span>Client</span>Some Client Name</div>
                                    <div>
                                        <span>Date</span>July 2013</div>
                                    <div>
                                        <span>Skills</span>HTML5, CSS3, JavaScript</div>
                                    <div>
                                        <span>Link</span>http://examplecomp.com</div>
                                </div>
                                <p>How far you go in life depends on your being tender with the young, compassionate with the aged, sympathetic with the striving and tolerant of the weak and strong. Because someday in your life you will have been all of these.</p>
                            </div>
                        </div>
                    </div>
                    <!-- End details for portfolio project 3 -->
                    <!-- Start details for portfolio project 4 -->
                    <div id="slidingDiv3" class="toggleDiv row-fluid single-project">
                        <div class="span6">
                            <img src="  <?php echo base_url(); ?>/assets/new/images/Portfolio04.png" alt="project 4">
                        </div>
                        <div class="span6">
                            <div class="project-description">
                                <div class="project-title clearfix">
                                    <h3>Project for Some Client</h3>
                                    <span class="show_hide close">
                                        <i class="icon-cancel"></i>
                                    </span>
                                </div>
                                <div class="project-info">
                                    <div>
                                        <span>Client</span>Some Client Name</div>
                                    <div>
                                        <span>Date</span>July 2013</div>
                                    <div>
                                        <span>Skills</span>HTML5, CSS3, JavaScript</div>
                                    <div>
                                        <span>Link</span>http://examplecomp.com</div>
                                </div>
                                <p>Life's but a walking shadow, a poor player, that struts and frets his hour upon the stage, and then is heard no more; it is a tale told by an idiot, full of sound and fury, signifying nothing.</p>
                            </div>
                        </div>
                    </div>
                    <!-- End details for portfolio project 4 -->
                    <!-- Start details for portfolio project 5 -->
                    <div id="slidingDiv4" class="toggleDiv row-fluid single-project">
                        <div class="span6">
                            <img src="  <?php echo base_url(); ?>/assets/new/images/Portfolio05.png" alt="project 5">
                        </div>
                        <div class="span6">
                            <div class="project-description">
                                <div class="project-title clearfix">
                                    <h3>Webste for Some Client</h3>
                                    <span class="show_hide close">
                                        <i class="icon-cancel"></i>
                                    </span>
                                </div>
                                <div class="project-info">
                                    <div>
                                        <span>Client</span>Some Client Name</div>
                                    <div>
                                        <span>Date</span>July 2013</div>
                                    <div>
                                        <span>Skills</span>HTML5, CSS3, JavaScript</div>
                                    <div>
                                        <span>Link</span>http://examplecomp.com</div>
                                </div>
                                <p>We need to give each other the space to grow, to be ourselves, to exercise our diversity. We need to give each other space so that we may both give and receive such beautiful things as ideas, openness, dignity, joy, healing, and inclusion.</p>
                            </div>
                        </div>
                    </div>
                    <!-- End details for portfolio project 5 -->
                    <!-- Start details for portfolio project 6 -->
                    <div id="slidingDiv5" class="toggleDiv row-fluid single-project">
                        <div class="span6">
                            <img src="  <?php echo base_url(); ?>/assets/new/images/Portfolio06.png" alt="project 6">
                        </div>
                        <div class="span6">
                            <div class="project-description">
                                <div class="project-title clearfix">
                                    <h3>Webste for Some Client</h3>
                                    <span class="show_hide close">
                                        <i class="icon-cancel"></i>
                                    </span>
                                </div>
                                <div class="project-info">
                                    <div>
                                        <span>Client</span>Some Client Name</div>
                                    <div>
                                        <span>Date</span>July 2013</div>
                                    <div>
                                        <span>Skills</span>HTML5, CSS3, JavaScript</div>
                                    <div>
                                        <span>Link</span>http://examplecomp.com</div>
                                </div>
                                <p>I went to the woods because I wished to live deliberately, to front only the essential facts of life, and see if I could not learn what it had to teach, and not, when I came to die, discover that I had not lived.</p>
                            </div>
                        </div>
                    </div>
                    <!-- End details for portfolio project 6 -->
                    <!-- Start details for portfolio project 7 -->
                    <div id="slidingDiv6" class="toggleDiv row-fluid single-project">
                        <div class="span6">
                            <img src="  <?php echo base_url(); ?>/assets/new/images/Portfolio07.png" alt="project 7">
                        </div>
                        <div class="span6">
                            <div class="project-description">
                                <div class="project-title clearfix">
                                    <h3>Webste for Some Client</h3>
                                    <span class="show_hide close">
                                        <i class="icon-cancel"></i>
                                    </span>
                                </div>
                                <div class="project-info">
                                    <div>
                                        <span>Client</span>Some Client Name</div>
                                    <div>
                                        <span>Date</span>July 2013</div>
                                    <div>
                                        <span>Skills</span>HTML5, CSS3, JavaScript</div>
                                    <div>
                                        <span>Link</span>http://examplecomp.com</div>
                                </div>
                                <p>Always continue the climb. It is possible for you to do whatever you choose, if you first get to know who you are and are willing to work with a power that is greater than ourselves to do it.</p>
                            </div>
                        </div>
                    </div>
                    <!-- End details for portfolio project 7 -->
                    <!-- Start details for portfolio project 8 -->
                    <div id="slidingDiv7" class="toggleDiv row-fluid single-project">
                        <div class="span6">
                            <img src="  <?php echo base_url(); ?>/assets/new/images/Portfolio08.png" alt="project 8">
                        </div>
                        <div class="span6">
                            <div class="project-description">
                                <div class="project-title clearfix">
                                    <h3>Webste for Some Client</h3>
                                    <span class="show_hide close">
                                        <i class="icon-cancel"></i>
                                    </span>
                                </div>
                                <div class="project-info">
                                    <div>
                                        <span>Client</span>Some Client Name</div>
                                    <div>
                                        <span>Date</span>July 2013</div>
                                    <div>
                                        <span>Skills</span>HTML5, CSS3, JavaScript</div>
                                    <div>
                                        <span>Link</span>http://examplecomp.com</div>
                                </div>
                                <p>What if you gave someone a gift, and they neglected to thank you for it - would you be likely to give them another? Life is the same way. In order to attract more of the blessings that life has to offer, you must truly appreciate what you already have.</p>
                            </div>
                        </div>
                    </div>
                    <!-- End details for portfolio project 8 -->
                    <!-- Start details for portfolio project 9 -->
                    <div id="slidingDiv8" class="toggleDiv row-fluid single-project">
                        <div class="span6">
                            <img src="  <?php echo base_url(); ?>/assets/new/images/Portfolio09.png" alt="project 9">
                        </div>
                        <div class="span6">
                            <div class="project-description">
                                <div class="project-title clearfix">
                                    <h3>Webste for Some Client</h3>
                                    <span class="show_hide close">
                                        <i class="icon-cancel"></i>
                                    </span>
                                </div>
                                <div class="project-info">
                                    <div>
                                        <span>Client</span>Some Client Name</div>
                                    <div>
                                        <span>Date</span>July 2013</div>
                                    <div>
                                        <span>Skills</span>HTML5, CSS3, JavaScript</div>
                                    <div>
                                        <span>Link</span>http://examplecomp.com</div>
                                </div>
                                <p>I learned that we can do anything, but we can't do everything... at least not at the same time. So think of your priorities not in terms of what activities you do, but when you do them. Timing is everything.</p>
                            </div>
                        </div>
                    </div>
                    <!-- End details for portfolio project 9 -->
                    <ul id="portfolio-grid" class="thumbnails row">
                        <li class="span4 mix 7up">
                            <div class="thumbnail">
                  <img src="  <?php echo base_url(); ?>/assets/new/images/7up.png" alt="project 2" >    
                                <a href="#single-project" class="more show_hide" rel="#slidingDiv">
                                    <i class="icon-plus"  id="btn_open" onclick="clickf()"></i>
                                </a>
                                <h3>Thumbnail label 1</h3>
                                <p>Thumbnail caption...</p>
                                <div class="mask" ></div>
                            </div>
                        </li>
                        <li class="span4 mix uniliver">
                            <div class="thumbnail">
                                <img src="  <?php echo base_url(); ?>/assets/new/images/uniliver.png" alt="project 2">
                                <a href="#single-project" class="show_hide more" rel="#slidingDiv1">
                                    <i class="icon-plus"></i>
                                </a>
                                <h3>Thumbnail label</h3>
                                <p>Thumbnail caption...</p>
                                <div class="mask"></div>
                            </div>
                        </li>
                        <li class="span4 mix ufone">
                            <div class="thumbnail">
                                <img src="  <?php echo base_url(); ?>/assets/new/images/Ufone.png" alt="project 3">
                                <a href="#single-project" class="more show_hide" rel="#slidingDiv2">
                                    <i class="icon-plus"></i>
                                </a>
                                <h3>Thumbnail label</h3>
                                <p>Thumbnail caption...</p>
                                <div class="mask"></div>
                            </div>
                        </li>
                        <li class="span4 mix web">
                            <div class="thumbnail">
                                <img src="  <?php echo base_url(); ?>/assets/new/images/Portfolio04.png" alt="project 4">
                                <a href="#single-project" class="show_hide more" rel="#slidingDiv3">
                                    <i class="icon-plus"></i>
                                </a>
                                <h3>Thumbnail label</h3>
                                <p>Thumbnail caption...</p>
                                <div class="mask"></div>
                            </div>
                        </li>
                        <li class="span4 mix photo">
                            <div class="thumbnail">
                                <img src="  <?php echo base_url(); ?>/assets/new/images/Portfolio05.png" alt="project 5">
                                <a href="#single-project" class="show_hide more" rel="#slidingDiv4">
                                    <i class="icon-plus"></i>
                                </a>
                                <h3>Thumbnail label</h3>
                                <p>Thumbnail caption...</p>
                                <div class="mask"></div>
                            </div>
                        </li>
                        <li class="span4 mix identity">
                            <div class="thumbnail">
                                <img src="  <?php echo base_url(); ?>/assets/new/images/Portfolio06.png" alt="project 6">
                                <a href="#single-project" class="show_hide more" rel="#slidingDiv5">
                                    <i class="icon-plus"></i>
                                </a>
                                <h3>Thumbnail label</h3>
                                <p>Thumbnail caption...</p>
                                <div class="mask"></div>
                            </div>
                        </li>
                        <li class="span4 mix web">
                            <div class="thumbnail">
                                <img src="  <?php echo base_url(); ?>/assets/new/images/Portfolio07.png" alt="project 7" />
                                <a href="#single-project" class="show_hide more" rel="#slidingDiv6">
                                    <i class="icon-plus"></i>
                                </a>
                                <h3>Thumbnail label</h3>
                                <p>Thumbnail caption...</p>
                                <div class="mask"></div>
                            </div>
                        </li>
                        <li class="span4 mix photo">
                            <div class="thumbnail">
                                <img src="  <?php echo base_url(); ?>/assets/new/images/Portfolio08.png" alt="project 8">
                                <a href="#single-project" class="show_hide more" rel="#slidingDiv7">
                                    <i class="icon-plus"></i>
                                </a>
                                <h3>Thumbnail label</h3>
                                <p>Thumbnail caption...</p>
                                <div class="mask"></div>
                            </div>
                        </li>
                        <li class="span4 mix identity">
                            <div class="thumbnail">
                                <img src="  <?php echo base_url(); ?>/assets/new/images/Portfolio09.png" alt="project 9">
                                <a href="#single-project" class="show_hide more" rel="#slidingDiv8">
                                    <i class="icon-plus"></i>
                                </a>
                                <h3>Thumbnail label</h3>
                                <p>Thumbnail caption...</p>
                                <div class="mask"></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Portfolio section end -->
        <!-- About us section start -->
        
        <!-- About us section end -->
        <div class="section secondary-section">
            <div class="triangle"></div>
            <div class="container centered">
               <!--  <p class="large-text">Elegance is not the abundance of simplicity. It is the absence of complexity.</p> -->
                <a href="#" class="button">Purshase now</a>
            </div>
        </div>
        <!-- Client section start -->
        <!-- Client section start -->
       
        <div class="section third-section" id="brands">
            <div class="container centered">
                <div class="sub-section">
                    <div class="title clearfix">
                        <div class="pull-left" >
                            <h3>Our Brands</h3>
                        </div>
                        <ul class="client-nav pull-right">
                            <li id="client-prev"></li>
                            <li id="client-next"></li>
                        </ul>
                    </div>
                    <ul class="row client-slider" id="clint-slider">
                         <?php foreach($brand as $images) {?>
                        <li>
                            <a href="">
                                <img src="<?php echo base_url()."assets/images/brand/".$images->image;?>"alt="client logo 1" style="width:160px;height:80px;">
                            </a>
                        </li>
                        <?php } ?> 
                    </ul>
                </div>
            </div>
        </div>
 
        <div class="footer">
            <p>&copy; 2013 Theme by <a href="http://www.graphberry.com">GraphBerry</a>, <a href="http://goo.gl/NM84K2">Documentation</a></p>
        </div>
        <!-- Footer section end -->
        <!-- ScrollUp button start -->
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

         <script> 
var modal = document.getElementById('myModal');


function clickf(){
    modal.style.display = "block";
}

function close2() {
  modal.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
  
</script> 
        <!-- ScrollUp button end -->
        <!-- Include javascript -->
        <script src="<?php echo base_url() ?>/assets/new/js/jquery.js"></script>
        <script type="text/javascript" src="  <?php echo base_url(); ?>/assets/new/js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="  <?php echo base_url(); ?>/assets/new/js/bootstrap.js"></script>
        <script type="text/javascript" src="  <?php echo base_url(); ?>/assets/new/js/modernizr.custom.js"></script>
        <script type="text/javascript" src="  <?php echo base_url(); ?>/assets/new/js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="  <?php echo base_url(); ?>/assets/new/js/jquery.cslider.js"></script>
        <script type="text/javascript" src="  <?php echo base_url(); ?>/assets/new/js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="  <?php echo base_url(); ?>/assets/new/js/jquery.inview.js"></script>
        <!-- Load google maps api and call initializeMap function defined in app.js -->
        <script async="" defer="" type="text/javascript" src=" https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <!-- css3-mediaqueries.js for IE8 or older -->
        <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="  <?php echo base_url(); ?>/assets/new/js/app.js"></script>
            <style type="text/css">
                .image-one img{
                        object-fit: fill !important;
    height: auto !important;
    width: -webkit-fill-available;
    display: block!important;
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: 100%;
    height: 500px !important;
                }
            </style>
        </body>
</html>
