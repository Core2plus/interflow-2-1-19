<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Brand_model extends CI_Model{
	public function get_brand()
	{
		$this->db->select('*');
		$this->db->from('brand');
		$result=$this->db->get();
		return $result->result_array();
	}

	public function get_brand_id($id)
 	{
 		$this->db->select('*');
 		$this->db->from('brand');
 		$this->db->where('BrandID',$id);
 		$result=$this->db->get();
 		return $result->result_array();
 	}
 		public function get_brand_img($img)
 	{
 		$this->db->select('*');
 		$this->db->from('brand');
 		$this->db->where('image',$img);
 		$result=$this->db->get();
 		return $result->result_array();
 	}

	public function dashboardbrand($name,$img,$des){
		$data=array('BrandName'=>$name,'Description'=>$des,'image'=>$img);
		$result=$this->db->update('brand',$data);
		return $result;
	}

public function publicdata(){
	$query =  $this->db->get('brand');
return $query->result();
}

	public function updatebrand($id,$name,$des,$img)
 	{
 		$data=array('BrandName'=>$name,'Description'=>$des,'image'=>$img);
 		$this->db->where('BrandID',$id);
 		$result=$this->db->update('brand',$data);
 		return $result;
 	}

	public function insertbrand($name,$des,$img){
		$data=array('BrandName'=>$name,'Description'=>$des,'image'=>$img);
		$result=$this->db->insert('brand',$data);
		return $result;
	}

	public function delete_brand($cat)
	{
		$this->db->where('BrandID', $cat);
$this->db->delete('brand');
	}
	public function get_brand_data($id)
	{
		$result=$this->db->select('*')->from('brand')->where('BrandID',$id)->get();
		return $result->result_array();
	}



}
?>
